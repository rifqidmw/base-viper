package com.base.viper.common

interface Equatable {
    override fun equals(other: Any?): Boolean

    override fun hashCode(): Int
}