package com.base.viper.common

import com.base.viper.components.promosection.DataSubSection

object CustomPopUpParam {

    data class SuccessMessageParam(
        val title: String,
        val description: String,
        val actionButton: String,
        val cancelable: Boolean = false,
        val sectionTitle: String,
        val sectionSubTitle: String,
        val sectionData: List<DataSubSection>? = listOf(),
        val onClickedActionButton: (() -> Unit)? = null,
        val onClickedItemSection: ((itemPosition: Int) -> Unit)? = null,
        val onClickedSectionSubTitle: (() -> Unit)? = null,
        val onDismissListener: (() -> Unit)? = null
    )

}