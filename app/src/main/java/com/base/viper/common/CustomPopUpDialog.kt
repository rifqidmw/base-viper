package com.base.viper.common

import android.app.Dialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.view.Window
import androidx.constraintlayout.widget.ConstraintLayout
import com.base.viper.R
import com.base.viper.components.promosection.CustomSubSection
import com.base.viper.databinding.DialogInquiryDoneBinding
import com.base.viper.databinding.DialogPopUpWarningBinding
import com.base.viper.utils.Utils.gone
import com.base.viper.utils.Utils.setOnSingleClickListener
import com.bumptech.glide.Glide

class CustomPopUpDialog(
    private val context: Context,
) : Dialog(context) {

    private var dialog: Dialog? = null

    fun popUpSuccessMessage(
        title: String,
        description: String,
        seemore: String,
        cancelable: Boolean = false,
        onDismissListener: (() -> Unit)? = null
    ): Dialog? {
        dialog = Dialog(context)
        dialog?.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog?.setCancelable(cancelable)

        val binding: DialogInquiryDoneBinding by lazy {
            DialogInquiryDoneBinding.inflate(layoutInflater)
        }
        dialog?.setContentView(binding.root)

        val window: Window? = dialog?.window
        window?.setLayout(
            ConstraintLayout.LayoutParams.MATCH_PARENT,
            ConstraintLayout.LayoutParams.MATCH_PARENT
        )
        dialog?.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog?.setOnDismissListener {
            dialog = null
            onDismissListener?.invoke()
        }

        with(binding) {
            Glide.with(context).load(R.raw.gif_done).into(ivDone)

            tvTitle.text = title
            tvDesc.text = description
            btnSeeMore.text = seemore

            btnSeeMore.setOnSingleClickListener {
                dialog?.dismiss()
            }
        }

        dialog?.show()
        return dialog
    }

    fun popUpSuccessMessage(
        param: CustomPopUpParam.SuccessMessageParam
    ): Dialog? {
        dialog = Dialog(context)
        dialog?.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog?.setCancelable(param.cancelable)

        val binding: DialogInquiryDoneBinding by lazy {
            DialogInquiryDoneBinding.inflate(layoutInflater)
        }
        dialog?.setContentView(binding.root)

        val window: Window? = dialog?.window
        window?.setLayout(
            ConstraintLayout.LayoutParams.MATCH_PARENT,
            ConstraintLayout.LayoutParams.MATCH_PARENT
        )
        dialog?.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

        with(binding) {
            Glide.with(context).load(R.raw.gif_done).into(ivDone)

            tvTitle.text = param.title
            tvDesc.text = param.description
            btnSeeMore.text = param.actionButton

            if (param.sectionData.isNullOrEmpty()) cpsPromo.gone()
            else cpsPromo.setDataPromo(param.sectionData)

            cpsPromo.title = param.sectionTitle
            cpsPromo.subTitle = param.sectionSubTitle
            cpsPromo.setOnClickedListener(object : CustomSubSection.Listener {
                override fun onSubTitleClicked() {
                    param.onClickedSectionSubTitle?.invoke()
                }

                override fun onItemClicked(position: Int) {
                    param.onClickedItemSection?.invoke(position)
                }
            })

            btnSeeMore.setOnSingleClickListener {
                param.onClickedActionButton?.invoke()
                dialog?.dismiss()
            }
        }

        dialog?.show()
        return dialog
    }

    fun popUpWarning(
        title: String?,
        description: String?
    ): Dialog? {
        dialog = Dialog(context)
        dialog?.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog?.setCancelable(true)

        val binding: DialogPopUpWarningBinding by lazy {
            DialogPopUpWarningBinding.inflate(layoutInflater)
        }
        dialog?.setContentView(binding.root)

        val window: Window? = dialog?.window
        window?.setLayout(
            ConstraintLayout.LayoutParams.MATCH_PARENT,
            ConstraintLayout.LayoutParams.WRAP_CONTENT
        )
        dialog?.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

        with(binding) {
            tvTitle.text = title
            tvDesc.text = description
        }

        dialog?.show()
        return dialog
    }
}