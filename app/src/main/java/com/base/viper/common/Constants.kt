package com.base.viper.common

object Constants {
    enum class Sessions {
        FIRST_RUN,
        TOKEN,
        FIREBASE_TOKEN,
    }

    enum class Notification {
        TITLE, BODY, TYPE
    }

    enum class NotificationType {
        SAMPLE
    }

    enum class ToolbarEvent {
        SEARCH, NOTIFICATION, CART
    }

    enum class InquiryType {
        NEWS_AND_TIPS, REQUEST_A_QUOTE
    }

    enum class VersionType(val value: String) {
        V1("v1")
    }

    enum class StatusType(val value: String){
        PUBLISH("PUBLISH"),
        UNPUBLISH("UNPUBLISH")
    }

    enum class ShareEvent {
        WHATSAPP,
        FACEBOOK,
        TWITTER,
        TIKTOK,
        LINK
    }

    enum class ShareType(val appName: String, val packageName: String) {
        WHATSAPP("Whatsapp", "com.whatsapp"),
        WHATSAPP_BUSINESS("Whatsapp Business", "com.whatsapp.w4b"),
        FACEBOOK("Facebook", "com.facebook.katana"),
        TWITTER("Twitter", "com.twitter.android"),
        TIKTOK("Tiktok", "com.zhiliaoapp.musically"),
    }
}