package com.base.viper.module.onboarding.onboardingcontent

import android.content.Intent
import androidx.fragment.app.Fragment
import com.base.viper.module.login.LoginActivity
import com.base.viper.utils.Utils.toastShort

class OnboardingContentRouter(private val fragment: Fragment) : OnboardingContentInterface.Router {
    override fun goToLogin() {
        fragment.requireActivity().startActivity(Intent(fragment.requireContext(), LoginActivity::class.java))
        fragment.requireActivity().finish()
    }

    override fun goToRegister() {
        fragment.requireContext().toastShort("TODO: Register")
    }
}