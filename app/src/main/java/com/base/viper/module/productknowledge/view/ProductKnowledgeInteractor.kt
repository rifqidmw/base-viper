package com.base.viper.module.productknowledge.view

import android.app.Activity
import com.base.viper.R
import com.base.viper.common.Constants
import com.base.viper.module.productknowledge.entity.ProductKnowledgeCategory
import com.base.viper.module.productknowledge.entity.ProductSample
import com.base.viper.services.NetworkBuilder
import com.base.viper.services.Sessions
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.disposables.CompositeDisposable
import io.reactivex.rxjava3.schedulers.Schedulers

class ProductKnowledgeInteractor(activity: Activity) : ProductKnowledgeInterface.Interactor {

    private val disposable = CompositeDisposable()
    private val apiService = NetworkBuilder.apiService
    private val sessions = Sessions(activity)
    override fun getDataCategoryProductKnowledge(
        onSuccess: (List<ProductKnowledgeCategory>) -> Unit,
        onError: (Throwable) -> Unit
    ) {
        apiService.getProductKnowledgeCategory(Constants.VersionType.V1.value, "productknowledge")
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(onSuccess, onError)
            .let(disposable::add)
    }

    override fun getDataListProduct(
        onSuccess: (List<ProductSample>) -> Unit,
        onError: (Throwable) -> Unit
    ) {
        val dataDummy = listOf(
            ProductSample(
                R.drawable.sample_promo_1,
                "NEW_CAR",
                "Mobil Avanza Matic: Harga, Promo, dan Spesifikasi Terbaru 2023",
                "1 April 2023",
            ),
            ProductSample(
                R.drawable.sample_promo_2,
                "NEW_CAR",
                "Beda Yaris Cross Versi Indonesia dan Versi Global",
                "2 April 2023",
            ),
            ProductSample(
                R.drawable.sample_promo_3,
                "NEW_CAR",
                "Memilih Oli Yang Paling Sesuai Buat Kendaraan Anda",
                "3 April 2023",
            ),
            ProductSample(
                R.drawable.sample_promo_4,
                "SERVICE",
                "Mobil Avanza Matic: Harga, Promo, dan Spesifikasi Terbaru 2023",
                "4 April 2023",
            ),
            ProductSample(
                R.drawable.sample_promo_5,
                "SERVICE",
                "Beda Yaris Cross Versi Indonesia dan Versi Global",
                "5 April 2023",
            ),
            ProductSample(
                R.drawable.sample_promo_6,
                "SPARE_PART",
                "Memilih Oli Yang Paling Sesuai Buat Kendaraan Anda",
                "6 April 2023",
            ),
        )
        val dummyData = List(4) { dataDummy }.flatten()
        Observable.just(dummyData)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(onSuccess, onError)
            .let(disposable::add)
    }
}