package com.base.viper.module.newcars.view

import android.os.Bundle
import android.os.Handler
import android.os.Looper
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.res.ResourcesCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.base.viper.R
import com.base.viper.common.Constants
import com.base.viper.components.CustomToolbar
import com.base.viper.databinding.ActivityNewCarBinding
import com.base.viper.databinding.CustomTabBinding
import com.base.viper.databinding.TabNewsTipsBinding
import com.base.viper.module.newcars.adapter.RecyclerViewAdapter
import com.base.viper.module.newcars.entity.NewCarConstant.ContentType.ALL
import com.base.viper.module.newcars.entity.NewCarConstant.ContentType.HATCHBACK
import com.base.viper.module.newcars.entity.NewCarConstant.ContentType.HYBRID
import com.base.viper.module.newcars.entity.NewCarConstant.ContentType.MPV
import com.base.viper.module.newcars.entity.NewCarConstant.ContentType.SEDAN
import com.base.viper.module.newcars.entity.NewCarConstant.ContentType.SPORTS
import com.base.viper.module.newcars.entity.NewCarConstant.ContentType.SUV
import com.base.viper.module.newcars.entity.NewCarCount
import com.base.viper.module.newcars.entity.NewCarSample
import com.base.viper.module.newcars.view.category.NewCarCategoryFragment
import com.base.viper.utils.Utils.setOnSingleClickListener
import com.google.android.material.tabs.TabLayout
import com.google.android.material.tabs.TabLayoutMediator
import kotlinx.coroutines.launch

class NewCarActivity : AppCompatActivity(), NewCarInterface.View {

    private val presenter: NewCarPresenter = NewCarPresenter(this)
    private val binding: ActivityNewCarBinding by lazy {
        ActivityNewCarBinding.inflate(layoutInflater)
    }

    private var listCount: MutableList<NewCarCount> = arrayListOf()

    private val newCarAdapter: RecyclerViewAdapter by lazy {
        RecyclerViewAdapter(RecyclerViewAdapter.ViewType.ALL, emptyList())
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)
        presenter.onCreate(this)
        setupView()
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter.onDestroy()
    }

    override fun setupView() {
        lifecycleScope.launch {
            setupToolbar()
            setupTabLayout()
        }
        with(binding) {
            btnFilter.setOnSingleClickListener {
                presenter.goToFilter()
            }
            btnSort.setOnSingleClickListener {
                presenter.goToSort()
            }
        }
    }

    override fun setDataNewCar(data: ArrayList<NewCarSample>) {
        if (data.isNotEmpty()) {
            newCarAdapter.setListAllCar(data)
        }
    }

    private fun setupToolbar() {
        binding.toolbar.setOnClickedListener(object : CustomToolbar.Listener {
            override fun onClickedBackButton() {
                onBackPressedDispatcher.onBackPressed()
            }

            override fun onClickedCart(event: Constants.ToolbarEvent) {
                presenter.onSectionToolbar(event)
            }

            override fun onClickedNotification(event: Constants.ToolbarEvent) {
                presenter.onSectionToolbar(event)
            }

            override fun onClickedSearch(event: Constants.ToolbarEvent) {
                presenter.onSectionToolbar(event)
            }
        })
    }

    private fun setupTabLayout() {
        with(binding) {
            val tabNewCar = listOf(
                "ALL",
                "HYBRID",
                "MPV",
                "SUV",
                "HATCHBACK",
                "SEDAN",
                "SPORTS"
            )

            vpNewCar.offscreenPageLimit = 7
            vpNewCar.adapter = object : FragmentStateAdapter(this@NewCarActivity) {
                override fun getItemCount(): Int {
                    return 7
                }

                override fun createFragment(position: Int): Fragment {
                    return when (position) {
                        0 -> NewCarCategoryFragment(ALL, { countData ->
                            listCount.add(countData)
                        }, emptyList())

                        1 -> NewCarCategoryFragment(HYBRID, {countData ->
                            listCount.add(countData)
                        }, emptyList())

                        2 -> NewCarCategoryFragment(MPV, {countData ->
                            listCount.add(countData)
                        }, emptyList())

                        3 -> NewCarCategoryFragment(SUV, {countData ->
                            listCount.add(countData)
                        }, emptyList())

                        4 -> NewCarCategoryFragment(HATCHBACK, {countData ->
                            listCount.add(countData)
                        }, emptyList())

                        5 -> NewCarCategoryFragment(SEDAN, {countData ->
                            listCount.add(countData)
                        }, emptyList())

                        6 -> NewCarCategoryFragment(SPORTS, {countData ->
                            listCount.add(countData)
                        }, emptyList())

                        else -> throw IllegalArgumentException("Invalid position: $position")
                    }
                }
            }


            TabLayoutMediator(tlNewCar, vpNewCar) { tab, position ->
                val tabViewBinding = CustomTabBinding.inflate(layoutInflater)
                tabViewBinding.tvTabText.text = tabNewCar[position]
                tab.customView = tabViewBinding.root
            }.attach()

            tlNewCar.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
                override fun onTabSelected(tab: TabLayout.Tab?) {
                    tab?.let {
                        val position = it.position
                        tvTotalCar.text = when (position) {
                            0 -> getString(
                                R.string.title_car_count,
                                listCount.find { it.type == ALL }?.count.toString()
                            )
                            1 -> getString(
                                R.string.title_car_count,
                                listCount.find { it.type == HYBRID }?.count.toString()
                            )
                            2 -> getString(
                                R.string.title_car_count,
                                listCount.find { it.type == MPV }?.count.toString()
                            )
                            3 -> getString(
                                R.string.title_car_count,
                                listCount.find { it.type == SUV }?.count.toString()
                            )
                            4 -> getString(
                                R.string.title_car_count,
                                listCount.find { it.type == HATCHBACK }?.count.toString()
                            )
                            5 -> getString(
                                R.string.title_car_count,
                                listCount.find { it.type == SEDAN }?.count.toString()
                            )
                            6 -> getString(
                                R.string.title_car_count,
                                listCount.find { it.type == SPORTS }?.count.toString()
                            )

                            else -> ""
                        }
                    }

                }

                override fun onTabUnselected(tab: TabLayout.Tab?) {
                    tab?.let {
                        val tabViewBinding = CustomTabBinding.bind(tab.customView!!)
                        tabViewBinding.tvTabText.typeface = ResourcesCompat.getFont(
                            this@NewCarActivity,
                            R.font.montserrat_400_regular
                        )
                    }
                }

                override fun onTabReselected(tab: TabLayout.Tab?) {
                    //Not Yet Implemented
                }
            })

            Handler(Looper.getMainLooper()).postDelayed({
                val defaultTab = tlNewCar.getTabAt(0)
                if (defaultTab != null) {
                    tlNewCar.selectTab(defaultTab)
                    val tabViewBinding = TabNewsTipsBinding.bind(defaultTab.customView!!)
                    tabViewBinding.tvTabText.typeface =
                        ResourcesCompat.getFont(this@NewCarActivity, R.font.montserrat_700_bold)
                    binding.tvTotalCar.text = getString(
                        R.string.title_car_count,
                        listCount.find { it.type == ALL }?.count.toString()
                    )
                }
            }, 1000)
        }
    }
}