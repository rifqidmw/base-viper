package com.base.viper.module.newcars.view.filter

import androidx.fragment.app.Fragment

class NewCarFilterPresenter(
    private var view: NewCarFilterInterface.View? = null
) : NewCarFilterInterface.Presenter {

    private var interactor: NewCarFilterInterface.Interactor? = null
    private var router: NewCarFilterInterface.Router? = null

    override fun onCreate(fragment: Fragment) {
        view = fragment as NewCarFilterFragment
        interactor = NewCarFilterInteractor(fragment)
        router = NewCarFilterRouter(fragment)
    }
    
    override fun onViewCreated(fragment: Fragment) {
        view?.setupView()
    }

    override fun onDestroy() {
        view = null
        router = null
    }

    override fun goToApplyFilter() {
        router?.goToApplyFilter()
    }
}