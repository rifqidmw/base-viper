package com.base.viper.module.newstips.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.text.HtmlCompat
import androidx.recyclerview.widget.RecyclerView
import com.base.viper.R
import com.base.viper.common.BaseViewHolder
import com.base.viper.databinding.CustomPromoItemBinding
import com.base.viper.databinding.ItemNewsBinding
import com.base.viper.databinding.ItemNewsInquiryAreaBinding
import com.base.viper.module.newstips.adapter.RecyclerViewNewsTipsAdapter.ViewType.ARTICLE
import com.base.viper.module.newstips.adapter.RecyclerViewNewsTipsAdapter.ViewType.BRANCH
import com.base.viper.module.newstips.adapter.RecyclerViewNewsTipsAdapter.ViewType.CAR_MODEL
import com.base.viper.module.newstips.adapter.RecyclerViewNewsTipsAdapter.ViewType.CITY
import com.base.viper.module.newstips.adapter.RecyclerViewNewsTipsAdapter.ViewType.NEWS
import com.base.viper.module.newstips.adapter.RecyclerViewNewsTipsAdapter.ViewType.PROMO
import com.base.viper.module.newstips.adapter.RecyclerViewNewsTipsAdapter.ViewType.PROVINCE
import com.base.viper.module.newstips.entity.inquiry.Branch
import com.base.viper.module.newstips.entity.inquiry.City
import com.base.viper.module.newstips.entity.inquiry.Product
import com.base.viper.module.newstips.entity.inquiry.Province
import com.base.viper.module.newstips.entity.newstips.NewsTips
import com.base.viper.module.promo.entity.promo.Content
import com.base.viper.utils.Utils.dateTimeFormat
import com.base.viper.utils.Utils.gone
import com.base.viper.utils.Utils.setImageViewUrl
import com.base.viper.utils.Utils.setOnSingleClickListener
import com.base.viper.utils.Utils.visible

class RecyclerViewNewsTipsAdapter(private val type: ViewType, private val categoryPosition: Int = -1) :
    RecyclerView.Adapter<BaseViewHolder>() {

    enum class ViewType {
        NEWS, ARTICLE, PROMO, CAR_MODEL, PROVINCE, CITY, BRANCH
    }

    private val listNewsTips: MutableList<NewsTips.Content> = mutableListOf()
    private val listProvince: MutableList<Province> = mutableListOf()
    private val listCity: MutableList<City> = mutableListOf()
    private val listCarModel: MutableList<Product> = mutableListOf()
    private val listBranch: MutableList<Branch.Content> = mutableListOf()
    private val listSubNewsTips: MutableList<NewsTips.Content> = mutableListOf()
    private val listSubPromo: MutableList<Content> = mutableListOf()

    @SuppressLint("NotifyDataSetChanged")
    fun setListNews(list: List<NewsTips.Content>, clearAll: Boolean = false) {
        if (clearAll)
            listNewsTips.clear()
        listNewsTips.addAll(list)
        notifyDataSetChanged()
    }

    @SuppressLint("NotifyDataSetChanged")
    fun setListProvince(list: List<Province>) {
        listProvince.clear()
        listProvince.addAll(list)
        notifyDataSetChanged()
    }

    @SuppressLint("NotifyDataSetChanged")
    fun setListCity(list: List<City>){
        listCity.clear()
        listCity.addAll(list)
        notifyDataSetChanged()
    }

    @SuppressLint("NotifyDataSetChanged")
    fun setListCarModel(list: List<Product>) {
        listCarModel.clear()
        listCarModel.addAll(list)
        notifyDataSetChanged()
    }

    @SuppressLint("NotifyDataSetChanged")
    fun setListBranch(list: List<Branch.Content>) {
        listBranch.clear()
        listBranch.addAll(list)
        notifyDataSetChanged()
    }

    @SuppressLint("NotifyDataSetChanged")
    fun setListArticle(list: List<NewsTips.Content>) {
        listSubNewsTips.clear()
        listSubNewsTips.addAll(list)
        notifyDataSetChanged()
    }

    @SuppressLint("NotifyDataSetChanged")
    fun setListPromo(list: List<Content>) {
        listSubPromo.clear()
        listSubPromo.addAll(list)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder {
        return when (viewType) {
            NEWS.ordinal -> NewsTipsHolder(
                ItemNewsBinding.inflate(
                    LayoutInflater.from(parent.context), parent, false
                )
            )

            PROVINCE.ordinal -> ProvinceHolder(
                ItemNewsInquiryAreaBinding.inflate(
                    LayoutInflater.from(parent.context), parent, false
                )
            )

            CITY.ordinal -> CityHolder(
                ItemNewsInquiryAreaBinding.inflate(
                    LayoutInflater.from(parent.context), parent, false
                )
            )

            CAR_MODEL.ordinal -> CarModelHolder(
                ItemNewsInquiryAreaBinding.inflate(
                    LayoutInflater.from(parent.context), parent, false
                )
            )

            BRANCH.ordinal -> BranchHolder(
                ItemNewsInquiryAreaBinding.inflate(
                    LayoutInflater.from(parent.context), parent, false
                )
            )

            ARTICLE.ordinal -> SubNewsTipsHolder(
                CustomPromoItemBinding.inflate(
                    LayoutInflater.from(parent.context), parent, false
                )
            )

            PROMO.ordinal -> SubPromoHolder(
                CustomPromoItemBinding.inflate(
                    LayoutInflater.from(parent.context), parent, false
                )
            )

            else -> throw IllegalArgumentException("Invalid viewType $type")
        }
    }

    override fun onBindViewHolder(holder: BaseViewHolder, position: Int) {
        when (holder) {
            is NewsTipsHolder -> holder.bind(listNewsTips[position])
            is ProvinceHolder -> holder.bind(listProvince[position])
            is CityHolder -> holder.bind(listCity[position])
            is SubNewsTipsHolder -> holder.bind(listSubNewsTips[position])
            is SubPromoHolder -> holder.bind(listSubPromo[position])
            is CarModelHolder -> holder.bind(listCarModel[position])
            is BranchHolder -> holder.bind(listBranch[position])
        }
    }

    override fun getItemCount() = when (type) {
        NEWS -> listNewsTips.size
        PROVINCE -> listProvince.size
        CITY -> listCity.size
        ARTICLE -> listSubNewsTips.size
        PROMO -> listSubPromo.size
        CAR_MODEL -> listCarModel.size
        BRANCH -> listBranch.size
    }

    override fun getItemViewType(position: Int) = type.ordinal

    private var onItemClickListener: OnItemClickListener? = null

    interface OnItemClickListener {
        fun <T> onClickedListener(item: T)
    }

    fun setOnItemClickListener(onClickListener: OnItemClickListener) {
        this.onItemClickListener = onClickListener
    }

    inner class NewsTipsHolder(
        private val binding: ItemNewsBinding
    ) : BaseViewHolder(binding) {
        val context: Context = binding.root.context
        fun bind(data: NewsTips.Content) = with(binding) {
            ivNewsAndTips.setImageViewUrl(data.heroImageLink)
            tvTitle.text = data.titlePage?.let { HtmlCompat.fromHtml(it, HtmlCompat.FROM_HTML_MODE_LEGACY) }
            tvDate.text = context.getString(R.string.title_posted_on, data.publishedDate?.dateTimeFormat())

            if (categoryPosition == 0){
                tvType.apply {
                    visible()
                    text = data.contentCategory?.name
                }
            }

            root.setOnSingleClickListener {
                this@RecyclerViewNewsTipsAdapter.onItemClickListener?.onClickedListener(data)
            }

            if (absoluteAdapterPosition == listNewsTips.size - 1) vDivider.gone()
            else vDivider.visible()
        }
    }

    inner class AreaHolder(
        private val binding: ItemNewsInquiryAreaBinding
    ) : BaseViewHolder(binding) {
        val context: Context = binding.root.context
        fun bind(data: String) = with(binding) {
            tvTitle.text = data

            root.setOnSingleClickListener {
                this@RecyclerViewNewsTipsAdapter.onItemClickListener?.onClickedListener(data)
            }
        }
    }

    inner class ProvinceHolder(
        private val binding: ItemNewsInquiryAreaBinding
    ) : BaseViewHolder(binding) {
        val context: Context = binding.root.context
        fun bind(data: Province) = with(binding) {
            tvTitle.text = data.name

            root.setOnSingleClickListener {
                this@RecyclerViewNewsTipsAdapter.onItemClickListener?.onClickedListener(data)
            }
        }
    }

    inner class CityHolder(
        private val binding: ItemNewsInquiryAreaBinding
    ) : BaseViewHolder(binding) {
        val context: Context = binding.root.context
        fun bind(data: City) = with(binding) {
            tvTitle.text = data.name

            root.setOnSingleClickListener {
                this@RecyclerViewNewsTipsAdapter.onItemClickListener?.onClickedListener(data)
            }
        }
    }

    inner class CarModelHolder(
        private val binding: ItemNewsInquiryAreaBinding
    ) : BaseViewHolder(binding) {
        val context: Context = binding.root.context
        fun bind(data: Product) = with(binding) {
            tvTitle.text = data.name

            root.setOnSingleClickListener {
                this@RecyclerViewNewsTipsAdapter.onItemClickListener?.onClickedListener(data)
            }
        }
    }

    inner class BranchHolder(
        private val binding: ItemNewsInquiryAreaBinding
    ) : BaseViewHolder(binding) {
        val context: Context = binding.root.context
        fun bind(data: Branch.Content) = with(binding) {
            tvTitle.text = data.branchName

            root.setOnSingleClickListener {
                this@RecyclerViewNewsTipsAdapter.onItemClickListener?.onClickedListener(data)
            }
        }
    }

    inner class SubNewsTipsHolder(
        private val binding: CustomPromoItemBinding
    ) : BaseViewHolder(binding) {
        val context: Context = binding.root.context
        fun bind(data: NewsTips.Content) = with(binding) {
            ivPromoImage.setImageViewUrl(data.heroImageLink)

            tvPromoTitle.text = data.titleHeader?.let { HtmlCompat.fromHtml(it, HtmlCompat.FROM_HTML_MODE_LEGACY) }
            tvPromoPeriod.text = data.publishedDate?.dateTimeFormat()

            root.setOnSingleClickListener {
                onItemClickListener?.onClickedListener(data)
            }
        }
    }

    inner class SubPromoHolder(
        private val binding: CustomPromoItemBinding
    ) : BaseViewHolder(binding) {
        val context: Context = binding.root.context
        fun bind(data: Content) = with(binding) {
            ivPromoImage.setImageViewUrl(data.heroImageLink)

            tvPromoTitle.text = data.titleHeader
            tvPromoPeriod.text = String.format(
                context.getString(R.string.format_period),
                data.startDate?.dateTimeFormat(),
                data.endDate?.dateTimeFormat()
            )

            root.setOnSingleClickListener {
                onItemClickListener?.onClickedListener(data)
            }
        }
    }
}