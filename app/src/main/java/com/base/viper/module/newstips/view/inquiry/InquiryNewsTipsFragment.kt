package com.base.viper.module.newstips.view.inquiry

import android.os.Bundle
import android.view.View
import androidx.core.content.ContextCompat
import androidx.core.text.HtmlCompat
import androidx.recyclerview.widget.LinearLayoutManager
import com.base.viper.R
import com.base.viper.common.BaseBottomDialogFragment
import com.base.viper.common.Constants
import com.base.viper.components.InputView
import com.base.viper.databinding.BottomsheetNewsInquiryBinding
import com.base.viper.databinding.FragmentInquiryNewsTipsBinding
import com.base.viper.module.newstips.adapter.RecyclerViewNewsTipsAdapter
import com.base.viper.module.newstips.entity.inquiry.Branch
import com.base.viper.module.newstips.entity.inquiry.City
import com.base.viper.module.newstips.entity.inquiry.Product
import com.base.viper.module.newstips.entity.inquiry.Province
import com.base.viper.utils.Utils.capitalizeEachWord
import com.base.viper.utils.Utils.setOnSingleClickListener
import com.base.viper.utils.Utils.toastShort
import com.google.android.material.bottomsheet.BottomSheetDialog

class InquiryNewsTipsFragment(
    private val type: Constants.InquiryType
) : BaseBottomDialogFragment<FragmentInquiryNewsTipsBinding>(FragmentInquiryNewsTipsBinding::inflate), InquiryNewsTipsInterface.View {

    private val presenter: InquiryNewsTipsPresenter = InquiryNewsTipsPresenter(this)

    private val provinceAdapter: RecyclerViewNewsTipsAdapter by lazy {
        RecyclerViewNewsTipsAdapter(RecyclerViewNewsTipsAdapter.ViewType.PROVINCE)
    }

    private val cityAdapter: RecyclerViewNewsTipsAdapter by lazy {
        RecyclerViewNewsTipsAdapter(RecyclerViewNewsTipsAdapter.ViewType.CITY)
    }

    private val productAdapter: RecyclerViewNewsTipsAdapter by lazy {
        RecyclerViewNewsTipsAdapter(RecyclerViewNewsTipsAdapter.ViewType.CAR_MODEL)
    }

    private val branchAdapter: RecyclerViewNewsTipsAdapter by lazy {
        RecyclerViewNewsTipsAdapter(RecyclerViewNewsTipsAdapter.ViewType.BRANCH)
    }

    override fun setOnCreatePresenter() {
        presenter.onCreate(this)
    }

    override fun setOnViewCreatedPresenter(view: View, savedInstanceState: Bundle?) {
        presenter.onViewCreated(this)
    }

    override fun setOnDestroyPresenter() {
        presenter.onDestroy()
    }

    override fun setupView() {
        with(binding) {
            when (type) {
                Constants.InquiryType.NEWS_AND_TIPS -> tvFormTitle.text =
                    getString(R.string.title_get_the_best_promos)

                Constants.InquiryType.REQUEST_A_QUOTE -> tvFormTitle.text =
                    getString(R.string.title_request_a_quote)
            }

            etFullname.apply {
                disableEmoji()
                setMaxLenght(20)
                setImeOption(InputView.ACTION.NEXT)
                setListener(object : InputView.InputListener {
                    override fun afterTextChanged(value: String) {
                        presenter.fullName = value.capitalizeEachWord()
                        setText(presenter.fullName)
                        presenter.validateInput()
                    }
                })
            }

            etPhone.apply {
                setMaxLenght(19)
                setImeOption(InputView.ACTION.DONE)
                setInputType(InputView.TYPE.NUMBER)
                setStartCompoundText("+62", 8, R.color.reliable_black)
                setListener(object : InputView.InputListener {
                    override fun afterTextChanged(value: String) {
                        presenter.phone = value
                        presenter.validateInput()
                    }
                })
            }

            etProvince.apply {
                setListener(object : InputView.InputListener {
                    override fun onInputClick() {
                        presenter.showBottomsheetProvince()
                    }
                })
            }

            etCity.apply {
                setListener(object : InputView.InputListener {
                    override fun onInputClick() {
                        if (presenter.province != null)
                            presenter.showBottomsheetCity(presenter.province?.isocode)
                        else
                            requireContext().toastShort("Pilih Provinsi terlebih dahulu")
                    }
                })
            }

            etBranch.apply {
                setListener(object : InputView.InputListener {
                    override fun onInputClick() {
                        if (presenter.city != null)
                            presenter.showBottomsheetBranch()
                        else
                            requireContext().toastShort("Pilih Kota terlebih dahulu")
                    }

                })
            }

            etCarModel.apply {
                setListener(object : InputView.InputListener {
                    override fun onInputClick() {
                        presenter.showBottomsheetCarModel()
                    }

                })
            }

            cbTnc.apply {
                text = HtmlCompat.fromHtml(getString(R.string.title_label_inquiry_tnc), HtmlCompat.FROM_HTML_MODE_LEGACY)
                setOnCheckedChangeListener { _, b ->
                    presenter.tnc = b
                    presenter.validateInput()
                }
            }

            tvPrivacyPolicy.text = HtmlCompat.fromHtml(getString(R.string.title_label_inquiry_privacy_policy), HtmlCompat.FROM_HTML_MODE_LEGACY)

            ivClose.setOnSingleClickListener {
                this@InquiryNewsTipsFragment.dismiss()
            }
        }
    }

    override fun showBottomsheetProvince(data: List<Province>) {
        val bottomSheet =
            layoutInflater.inflate(R.layout.bottomsheet_news_inquiry, binding.root, false)
        val bsBinding = BottomsheetNewsInquiryBinding.bind(bottomSheet)

        val dialog = BottomSheetDialog(requireContext(), R.style.AlertDialogRoundedCorner)
        dialog.setContentView(bottomSheet)

        bsBinding.apply {
            tvTitle.text = getString(R.string.title_province)

            rvArea.apply {
                adapter = provinceAdapter.apply {
                    setListProvince(data)
                    setOnItemClickListener(object :
                        RecyclerViewNewsTipsAdapter.OnItemClickListener {
                        override fun <T> onClickedListener(item: T) {
                            if (item is Province) {
                                presenter.province = item
                                presenter.city = null
                                binding.etProvince.setText(item.name?:"")
                                binding.etCity.setText("")
                            }

                            presenter.validateInput()
                            dialog.dismiss()
                        }

                    })
                }
                layoutManager = LinearLayoutManager(requireContext())
            }

            ivClose.setOnClickListener {
                dialog.dismiss()
            }
        }

        dialog.show()
    }

    override fun showBottomsheetCity(data: List<City>) {
        val bottomSheet =
            layoutInflater.inflate(R.layout.bottomsheet_news_inquiry, binding.root, false)
        val bsBinding = BottomsheetNewsInquiryBinding.bind(bottomSheet)

        val dialog = BottomSheetDialog(requireContext(), R.style.AlertDialogRoundedCorner)
        dialog.setContentView(bottomSheet)

        bsBinding.apply {
            tvTitle.text = getString(R.string.title_city)

            rvArea.apply {
                adapter = cityAdapter.apply {
                    setListCity(data)
                    setOnItemClickListener(object :
                        RecyclerViewNewsTipsAdapter.OnItemClickListener {
                        override fun <T> onClickedListener(item: T) {
                            if (item is City) {
                                presenter.city = item
                                binding.etCity.setText(item.name?:"")

                                presenter.validateInput()
                                dialog.dismiss()
                            }
                        }
                    })
                }
                layoutManager = LinearLayoutManager(requireContext())
            }

            ivClose.setOnClickListener {
                dialog.dismiss()
            }
        }

        dialog.show()
    }

    override fun showBottomsheetCarModel(data: List<Product>) {
        val bottomSheet =
            layoutInflater.inflate(R.layout.bottomsheet_news_inquiry, binding.root, false)
        val bsBinding = BottomsheetNewsInquiryBinding.bind(bottomSheet)

        val dialog = BottomSheetDialog(requireContext(), R.style.AlertDialogRoundedCorner)
        dialog.setContentView(bottomSheet)

        bsBinding.apply {
            tvTitle.text = getString(R.string.title_car_model)

            rvArea.apply {
                adapter = productAdapter.apply {
                    setListCarModel(data)
                    setOnItemClickListener(object :
                        RecyclerViewNewsTipsAdapter.OnItemClickListener {
                        override fun <T> onClickedListener(item: T) {
                            if (item is Product) {
                                presenter.carModel = item
                                binding.etCarModel.setText(item.name)
                            }

                            presenter.validateInput()
                            dialog.dismiss()
                        }

                    })
                }
                layoutManager = LinearLayoutManager(requireContext())
            }

            ivClose.setOnClickListener {
                dialog.dismiss()
            }
        }

        dialog.show()
    }

    override fun showBottomsheetBranch(data: List<Branch.Content>) {
        val bottomSheet =
            layoutInflater.inflate(R.layout.bottomsheet_news_inquiry, binding.root, false)
        val bsBinding = BottomsheetNewsInquiryBinding.bind(bottomSheet)

        val dialog = BottomSheetDialog(requireContext(), R.style.AlertDialogRoundedCorner)
        dialog.setContentView(bottomSheet)

        bsBinding.apply {
            tvTitle.text = getString(R.string.title_branch)

            rvArea.apply {
                adapter = branchAdapter.apply {
                    setListBranch(data)
                    setOnItemClickListener(object :
                        RecyclerViewNewsTipsAdapter.OnItemClickListener {
                        override fun <T> onClickedListener(item: T) {
                            if (item is Branch.Content) {
                                presenter.branch = item
                                binding.etBranch.setText(item.branchName)
                            }

                            presenter.validateInput()
                            dialog.dismiss()
                        }

                    })
                }
                layoutManager = LinearLayoutManager(requireContext())
            }

            ivClose.setOnClickListener {
                dialog.dismiss()
            }
        }

        dialog.show()
    }

    override fun setErrorFullName(message: String?) {
        if (message.isNullOrEmpty()) binding.etFullname.hideError()
        else binding.etFullname.showError(message)
    }

    override fun setErrorPhone(message: String?) {
        if (message.isNullOrEmpty()) binding.etPhone.hideError()
        else binding.etPhone.showError(message)
    }

    override fun setErrorProvince(message: String?) {
        if (message.isNullOrEmpty()) binding.etProvince.hideError()
        else binding.etProvince.showError(message)
    }

    override fun setErrorCity(message: String?) {
        if (message.isNullOrEmpty()) binding.etCity.hideError()
        else binding.etCity.showError(message)
    }

    override fun setErrorCarModel(message: String?) {
        if (message.isNullOrEmpty()) binding.etCarModel.hideError()
        else binding.etCarModel.showError(message)
    }

    override fun setErrorTnC(message: String?) {
        //Not Implemented Here
    }

    override fun enableButtonSubmit() {
        binding.lySubmit.apply {
            backgroundTintList = ContextCompat.getColorStateList(requireContext(), R.color.red_8)
            setOnSingleClickListener {
                presenter.onSubmit(this@InquiryNewsTipsFragment, type)
            }
        }
    }

    override fun disableButtonSubmit() {
        binding.lySubmit.apply {
            backgroundTintList = ContextCompat.getColorStateList(requireContext(), R.color.white_8)
            setOnSingleClickListener {
                presenter.validateInput()
            }
        }
    }
}