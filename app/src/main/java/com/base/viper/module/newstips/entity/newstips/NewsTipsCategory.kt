package com.base.viper.module.newstips.entity.newstips


import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize


@Parcelize
data class NewsTipsCategory(
    @SerializedName("priority")
    val priority: Int? = null,
    @SerializedName("name")
    val name: String? = null,
    @SerializedName("description")
    val description: String? = null
) : Parcelable