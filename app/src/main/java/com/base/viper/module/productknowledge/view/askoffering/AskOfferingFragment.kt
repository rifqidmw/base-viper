package com.base.viper.module.productknowledge.view.askoffering

import android.os.Bundle
import android.view.View
import com.base.viper.common.BaseBottomDialogFragment
import com.base.viper.components.InputView
import com.base.viper.databinding.FragmentAskOfferingBinding
import com.base.viper.utils.Utils.capitalizeEachWord
import com.base.viper.utils.Utils.cleanPhoneNumber
import com.base.viper.utils.Utils.setOnSingleClickListener

class AskOfferingFragment :
    BaseBottomDialogFragment<FragmentAskOfferingBinding>(
        FragmentAskOfferingBinding::inflate
    ), AskOfferingInterface.View {

    private val presenter: AskOfferingPresenter = AskOfferingPresenter(this)

    override fun setOnCreatePresenter() {
        presenter.onCreate(this)
    }

    override fun setOnViewCreatedPresenter(view: View, savedInstanceState: Bundle?) {
        presenter.onViewCreated(this)
    }

    override fun setOnDestroyPresenter() {
        presenter.onDestroy()
    }

    override fun setupView() = with(binding) {
        tvFormTitle.text = "Minta Penawaran"

        etFullname.apply {
            disableEmoji()
            setMaxLenght(20)
            setImeOption(InputView.ACTION.NEXT)
            setListener(object : InputView.InputListener {
                override fun afterTextChanged(value: String) {
                    presenter.fullName = value.capitalizeEachWord()
                    setText(presenter.fullName)
                    presenter.validateInput()
                }
            })
        }

        etEmail.apply {
            disableEmoji()
            setImeOption(InputView.ACTION.NEXT)
            setListener(object : InputView.InputListener {
                override fun afterTextChanged(value: String) {
                    presenter.email = value
                    presenter.validateInput()
                }

            })
        }

        etPhone.apply {
            setMaxLenght(19)
            setImeOption(InputView.ACTION.DONE)
            setInputType(InputView.TYPE.NUMBER)
            setListener(object : InputView.InputListener {
                override fun afterTextChanged(value: String) {
                    presenter.phone = value.cleanPhoneNumber()
                    setText(presenter.phone)
                    presenter.validateInput()
                }
            })
        }

        ivClose.setOnSingleClickListener {
            this@AskOfferingFragment.dismiss()
        }

        btnSubmit.setOnSingleClickListener {
            presenter.onSubmit()
        }
    }

    override fun setErrorFullName(message: String?) {
        if (message.isNullOrEmpty()) binding.etFullname.hideError()
        else binding.etFullname.showError(message)
    }

    override fun setErrorEmail(message: String?) {
        if (message.isNullOrEmpty()) binding.etEmail.hideError()
        else binding.etEmail.showError(message)
    }

    override fun setErrorPhone(message: String?) {
        if (message.isNullOrEmpty()) binding.etPhone.hideError()
        else binding.etPhone.showError(message)
    }

    override fun enableButtonSubmit(isEnable: Boolean) {
        binding.btnSubmit.isEnabled = isEnable
    }
}