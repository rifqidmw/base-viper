package com.base.viper.module.addcar.registercar

import androidx.fragment.app.Fragment
import com.base.viper.utils.Utils.toastShort

class RegisterCarRouter(private val fragment: Fragment) : RegisterCarInterface.Router {
    override fun goToNotYourCar() {
        fragment.requireActivity().toastShort("TODO: Not Your Car")
    }

    override fun goToRegisterCar() {
        fragment.requireActivity().toastShort("TODO: Register Car")
    }
}