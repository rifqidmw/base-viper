package com.base.viper.module.mycardetails.view

import androidx.annotation.ColorRes
import androidx.annotation.DrawableRes
import androidx.fragment.app.Fragment
import com.base.viper.module.home.entity.MyCarsSample
import com.base.viper.module.mycardetails.common.MyCarDetailConstant

interface MyCarDetailInterface {
    interface View {
        fun setupView(data: MyCarsSample?)
        fun myCarTheme(
            @ColorRes color: Int,
            @DrawableRes background: Int,
            isLightColor: Boolean
        )
    }

    interface Presenter {
        fun onCreate(fragment: Fragment)
        fun onViewCreated(fragment: Fragment)
        fun onDestroy()
        fun onClickedSectionBookingServices(event: MyCarDetailConstant.Event)
    }

    interface Interactor

    interface Router {
        fun goToWorkShopService()
        fun goToThs()
        fun goToBodyPaint()
        fun goToAccessories()
        fun goToServicePackage()
        fun goToPromos()
    }
}