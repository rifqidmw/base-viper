package com.base.viper.module.onboarding.onboardingcontent

import androidx.fragment.app.Fragment

class OnboardingContentPresenter(
    private var view: OnboardingContentInterface.View? = null
) : OnboardingContentInterface.Presenter {

    private var interactor: OnboardingContentInterface.Interactor? = null
    private var router: OnboardingContentInterface.Router? = null

    override fun onCreate(fragment: Fragment) {
        view = fragment as OnboardingContentFragment
        interactor = OnboardingContentInteractor(fragment)
        router = OnboardingContentRouter(fragment)
    }
    
    override fun onViewCreated(fragment: Fragment) {
        view?.setupView()
    }

    override fun onDestroy() {
        view = null
        router = null
    }

    override fun goToLogin() {
        router?.goToLogin()
    }

    override fun goToRegister() {
        router?.goToRegister()
    }
}