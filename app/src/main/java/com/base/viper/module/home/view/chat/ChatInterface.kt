package com.base.viper.module.home.view.chat

import androidx.fragment.app.Fragment

interface ChatInterface {
    interface View

    interface Presenter {
        fun onCreate(fragment: Fragment)
        fun onViewCreated(fragment: Fragment)
        fun onDestroy()
    }

    interface Interactor

    interface Router
}