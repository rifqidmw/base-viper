package com.base.viper.module.productknowledge.view

import android.app.Activity
import com.base.viper.common.Constants
import com.base.viper.common.Constants.ToolbarEvent.CART
import com.base.viper.common.Constants.ToolbarEvent.NOTIFICATION
import com.base.viper.common.Constants.ToolbarEvent.SEARCH
import com.base.viper.module.productknowledge.entity.ProductKnowledgeCategory

class ProductKnowledgePresenter(
    private var view: ProductKnowledgeInterface.View? = null
) : ProductKnowledgeInterface.Presenter {

    private var interactor: ProductKnowledgeInterface.Interactor? = null
    private var router: ProductKnowledgeInterface.Router? = null

    override fun onCreate(activity: Activity) {
        view = activity as ProductKnowledgeActivity
        interactor = ProductKnowledgeInteractor(activity)
        router = ProductKnowledgeRouter(activity)

        view?.setupView()

        getDataListProduct()
    }

    override fun onDestroy() {
        view = null
        router = null
    }

    override fun onSectionToolbar(event: Constants.ToolbarEvent) {
        when (event) {
            SEARCH -> router?.goToSearch()
            NOTIFICATION -> router?.goToNotification()
            CART -> router?.goToCart()
        }
    }

    private fun getDataListProduct() {
        interactor?.getDataCategoryProductKnowledge(
            {
                val categories = it.toMutableList()
                categories.add(0, ProductKnowledgeCategory(0, "SEMUA", ""))
                view?.setupTabLayout(categories.sortedBy { sorted -> sorted.priority })
            }, {

            }
        )
    }

    override fun goToDetailProduct() {
        router?.goToDetailProduct()
    }
}