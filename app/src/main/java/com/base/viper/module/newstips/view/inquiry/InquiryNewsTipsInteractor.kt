package com.base.viper.module.newstips.view.inquiry

import androidx.fragment.app.Fragment
import com.base.viper.common.Constants
import com.base.viper.module.newstips.entity.inquiry.Branch
import com.base.viper.module.newstips.entity.inquiry.City
import com.base.viper.module.newstips.entity.inquiry.Product
import com.base.viper.module.newstips.entity.inquiry.Province
import com.base.viper.services.NetworkBuilder
import com.base.viper.services.Sessions
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.disposables.CompositeDisposable
import io.reactivex.rxjava3.schedulers.Schedulers

class InquiryNewsTipsInteractor(fragment: Fragment) : InquiryNewsTipsInterface.Interactor {

    private val disposable = CompositeDisposable()
    private val apiService = NetworkBuilder.apiService
    private val sessions = fragment.context?.let { Sessions(it) }
    override fun getDataProvince(
        onSuccess: (List<Province>) -> Unit,
        onError: (Throwable) -> Unit
    ) {
        apiService.getProvince(Constants.VersionType.V1.value)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(onSuccess, onError)
            .let(disposable::add)
    }

    override fun getDataCity(
        isocode: String,
        onSuccess: (List<City>) -> Unit,
        onError: (Throwable) -> Unit
    ) {
        apiService.getCity(Constants.VersionType.V1.value, isocode)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(onSuccess, onError)
            .let(disposable::add)
    }


    override fun getDataCarModel(onSuccess: (List<Product>) -> Unit, onError: (Throwable) -> Unit) {
        apiService.getProductList(Constants.VersionType.V1.value)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(onSuccess, onError)
            .let(disposable::add)
    }

    override fun getDataBranch(
        city: String,
        onSuccess: (Branch) -> Unit,
        onError: (Throwable) -> Unit
    ) {
        apiService.getBranch(Constants.VersionType.V1.value, city)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(onSuccess, onError)
            .let(disposable::add)
    }
}