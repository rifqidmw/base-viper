package com.base.viper.module.newcars.view.category

import androidx.fragment.app.Fragment
import com.base.viper.module.newcars.entity.NewCarSample

class NewCarCategoryPresenter(
    private var view: NewCarCategoryInterface.View? = null
) : NewCarCategoryInterface.Presenter {

    private var interactor: NewCarCategoryInterface.Interactor? = null
    private var router: NewCarCategoryInterface.Router? = null

    override fun onCreate(fragment: Fragment) {
        view = fragment as NewCarCategoryFragment
        interactor = NewCarCategoryInteractor(fragment)
        router = NewCarCategoryRouter(fragment)
    }

    override fun onViewCreated(fragment: Fragment) {
        view?.setupView()

        getDataAllCar()
        getDataHybridCar()
        getDataMpvCar()
        getDataSuvCar()
        getDataHatchbackCar()
        getDataSedanCar()
        getDataSportsCar()
    }

    override fun onDestroy() {
        view = null
        router = null
    }

    override fun goToDetailNewCar(item: NewCarSample) {
        router?.goToDetailNewCar(item)
    }

    private fun getDataAllCar() {
        interactor?.getDataAllCar({
            view?.setDataAllCar(it)
        }, {})
    }

    private fun getDataHybridCar() {
        interactor?.getDataHybridCar({
            view?.setDataHybridCar(it)
        }, {})
    }

    private fun getDataMpvCar() {
        interactor?.getDataMpvCar({
            view?.setDataMpvCar(it)
        }, {})
    }

    private fun getDataSuvCar() {
        interactor?.getDataSuvCar({
            view?.setDataSuvCar(it)
        }, {})
    }

    private fun getDataHatchbackCar() {
        interactor?.getDataHatchbackCar({
            view?.setDataHatchBackCar(it)
        }, {})
    }

    private fun getDataSedanCar() {
        interactor?.getDataSedanCar({
            view?.setDataSedanCar(it)
        }, {})
    }

    private fun getDataSportsCar() {
        interactor?.getDataSportsCar({
            view?.setDataSportsCar(it)
        }, {})
    }
}