package com.base.viper.module.login

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.base.viper.R
import com.base.viper.databinding.ActivityLoginBinding
import com.base.viper.utils.Utils.setOnSingleClickListener
import com.base.viper.utils.Utils.setStatusBarColor

class LoginActivity : AppCompatActivity(), LoginInterface.View {
    
    private val presenter: LoginPresenter = LoginPresenter(this)
    private val binding: ActivityLoginBinding by lazy {
        ActivityLoginBinding.inflate(layoutInflater)
    }
    
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)
        presenter.onCreate(this)
    }
    
    override fun onDestroy() {
        super.onDestroy()
        presenter.onDestroy()
    }

    override fun setupView() = with(binding) {
        setStatusBarColor(R.color.reliable_black, false)

        etPhone.setStartCompoundText(textStart = "+62", textPadding = 8, color = R.color.grey_13)

        btnLogin.setOnSingleClickListener {
            presenter.goToHome()
        }

        tvCreateAccount.setOnSingleClickListener {
            presenter.goToAddCar()
        }

        tvTermsAndCondition.setOnSingleClickListener {
            presenter.goToTermAndCondition()
        }

        tvPrivacyPolicy.setOnSingleClickListener {
            presenter.goToPrivacyPolicy()
        }
    }
}