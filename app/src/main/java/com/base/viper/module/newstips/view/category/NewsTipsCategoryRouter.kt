package com.base.viper.module.newstips.view.category

import android.content.Intent
import androidx.fragment.app.Fragment
import com.base.viper.module.newstips.common.NewsTipsConstant
import com.base.viper.module.newstips.entity.newstips.NewsTips
import com.base.viper.module.newstips.view.detailnewstips.DetailNewsTipsActivity

class NewsTipsCategoryRouter(private val fragment: Fragment) : NewsTipsCategoryInterface.Router {
    override fun goToDetail(item: NewsTips.Content) {
        fragment.requireActivity().startActivity(
            Intent(
                fragment.requireContext(),
                DetailNewsTipsActivity::class.java
            ).apply {
                putExtra(NewsTipsConstant.SLUG, item.slug)
                putExtra(NewsTipsConstant.ORIGIN, NewsTipsConstant.OriginEvent.NEWSTIPS.name)
            })
    }
}