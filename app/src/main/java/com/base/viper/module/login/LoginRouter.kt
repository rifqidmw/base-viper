package com.base.viper.module.login

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import com.base.viper.module.addcar.AddCarActivity
import com.base.viper.module.login.loginotp.LoginOtpActivity
import com.base.viper.utils.Utils.toastShort

class LoginRouter(private val activity: AppCompatActivity) : LoginInterface.Router {
    override fun goToAddCar() {
        activity.startActivity(Intent(activity.applicationContext, AddCarActivity::class.java))
    }

    override fun goToHome() {
        activity.startActivity(Intent(activity.applicationContext, LoginOtpActivity::class.java))
    }

    override fun goToForgotPassword() {
        activity.toastShort("TODO: Forgot Password")
    }

    override fun goToTermAndCondition() {
        activity.toastShort("TODO: Term And Condition")
    }

    override fun goToPrivacyPolicy() {
        activity.toastShort("TODO: Privacy Policy")
    }
}