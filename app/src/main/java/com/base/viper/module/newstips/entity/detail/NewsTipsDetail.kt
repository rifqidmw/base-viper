package com.base.viper.module.newstips.entity.detail


import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize

@Parcelize
data class NewsTipsDetail(
    @SerializedName("altImage")
    var altImage: String? = null,
//    @SerializedName("branch")
//    var branch: Any? = null,
    @SerializedName("category")
    var category: Category? = null,
    @SerializedName("cities")
    var cities: List<City?>? = null,
    @SerializedName("contentCategory")
    var contentCategory: ContentCategory? = null,
    @SerializedName("detail")
    var detail: Detail? = null,
//    @SerializedName("endDate")
//    var endDate: Any? = null,
//    @SerializedName("groupType")
//    var groupType: Any? = null,
    @SerializedName("heroImageLink")
    var heroImageLink: String? = null,
    @SerializedName("id")
    var id: Int? = null,
    @SerializedName("keyword")
    var keyword: String? = null,
    @SerializedName("metaDescription")
    var metaDescription: String? = null,
//    @SerializedName("metaRobots")
//    var metaRobots: List<Any?>? = null,
//    @SerializedName("priority")
//    var priority: Any? = null,
//    @SerializedName("provinces")
//    var provinces: List<Any?>? = null,
    @SerializedName("publishedDate")
    var publishedDate: String? = null,
    @SerializedName("slug")
    var slug: String? = null,
    @SerializedName("startDate")
    var startDate: String? = null,
//    @SerializedName("status")
//    var status: Any? = null,
//    @SerializedName("tag")
//    var tag: Any? = null,
    @SerializedName("titleHeader")
    var titleHeader: String? = null,
    @SerializedName("titlePage")
    var titlePage: String? = null
) : Parcelable {

    @Parcelize
    data class Category(
        @SerializedName("description")
        var description: String? = null,
        @SerializedName("faqs")
        var faqs: List<Faq?>? = null,
        @SerializedName("id")
        var id: Int? = null,
        @SerializedName("name")
        var name: String? = null,
        @SerializedName("type")
        var type: String? = null
    ) : Parcelable {

        @Parcelize
        data class Faq(
            @SerializedName("answer")
            var answer: String? = null,
            @SerializedName("id")
            var id: Int? = null,
            @SerializedName("objectCode")
            var objectCode: String? = null,
            @SerializedName("question")
            var question: String? = null,
            @SerializedName("section")
            var section: String? = null,
            @SerializedName("sequence")
            var sequence: Int? = null,
            @SerializedName("type")
            var type: String? = null
        ) : Parcelable
    }


    @Parcelize
    data class City(
//        @SerializedName("code")
//        var code: Any? = null,
        @SerializedName("id")
        var id: Int? = null,
        @SerializedName("isocode")
        var isocode: String? = null,
        @SerializedName("name")
        var name: String? = null
    ) : Parcelable


    @Parcelize
    data class ContentCategory(
        @SerializedName("category")
        var category: Category? = null,
//        @SerializedName("description")
//        var description: Any? = null,
        @SerializedName("id")
        var id: Int? = null,
        @SerializedName("name")
        var name: String? = null,
        @SerializedName("priority")
        var priority: Int? = null
    ) : Parcelable {

        @Parcelize
        data class Category(
            @SerializedName("description")
            var description: String? = null,
            @SerializedName("faqs")
            var faqs: List<Faq?>? = null,
            @SerializedName("id")
            var id: Int? = null,
            @SerializedName("name")
            var name: String? = null,
            @SerializedName("type")
            var type: String? = null
        ) : Parcelable {

            @Parcelize
            data class Faq(
                @SerializedName("answer")
                var answer: String? = null,
                @SerializedName("id")
                var id: Int? = null,
                @SerializedName("objectCode")
                var objectCode: String? = null,
                @SerializedName("question")
                var question: String? = null,
                @SerializedName("section")
                var section: String? = null,
                @SerializedName("sequence")
                var sequence: Int? = null,
                @SerializedName("type")
                var type: String? = null
            ) : Parcelable
        }
    }


    @Parcelize
    data class Detail(
        @SerializedName("detailContent")
        var detailContent: String? = null
    ) : Parcelable
}