package com.base.viper.module.splash

import android.app.Activity

class SplashPresenter(
    private var view: SplashInterface.View? = null
) : SplashInterface.Presenter {
    
    private var interactor: SplashInterface.Interactor? = null
    private var router: SplashInterface.Router? = null
    
    override fun onCreate(activity: Activity) {
        view = activity as SplashActivity
        interactor = SplashInteractor(activity)
        router = SplashRouter(activity)

        view?.setupView()
    }
    
    override fun onDestroy() {
        view = null
        router = null
    }

    override fun goToLogin() {
        router?.goToLogin()
    }

    override fun goToHome() {
        router?.goToHome()
    }
}