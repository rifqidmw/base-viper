package com.base.viper.module.promo.view.category

import androidx.fragment.app.Fragment
import androidx.paging.PagingData
import com.base.viper.module.promo.entity.promo.Content
import kotlinx.coroutines.flow.Flow

interface PromoCategoryInterface {
    interface View {
        fun setupView()
        fun setDataPromoPaged(data: PagingData<Content>)
    }

    interface Presenter {
        fun onCreate(fragment: Fragment)
        fun onViewCreated(fragment: Fragment)
        fun onDestroy()
        fun goToDetailPromo(item: Content)
    }

    interface Interactor {
        fun getDataPromo(size: Int, contentCategory: String): Flow<PagingData<Content>>
    }

    interface Router {
        fun goToDetailPromo(item: Content)
    }
}