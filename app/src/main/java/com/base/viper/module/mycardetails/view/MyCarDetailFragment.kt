package com.base.viper.module.mycardetails.view

import android.content.res.ColorStateList
import android.os.Bundle
import android.view.View
import androidx.core.content.ContextCompat
import com.base.viper.R
import com.base.viper.common.BaseBottomDialogFragment
import com.base.viper.databinding.FragmentMyCarDetailBinding
import com.base.viper.module.home.entity.MyCarsSample
import com.base.viper.module.mycardetails.common.MyCarDetailConstant
import com.base.viper.utils.Utils.setOnSingleClickListener
import com.bumptech.glide.Glide

class MyCarDetailFragment :
    BaseBottomDialogFragment<FragmentMyCarDetailBinding>(FragmentMyCarDetailBinding::inflate),
    MyCarDetailInterface.View {

    private val presenter: MyCarDetailPresenter = MyCarDetailPresenter(this)

    override fun setOnCreatePresenter() {
        presenter.onCreate(this)
    }

    override fun setOnViewCreatedPresenter(view: View, savedInstanceState: Bundle?) {
        presenter.onViewCreated(this)
    }

    override fun setOnDestroyPresenter() {
        presenter.onDestroy()
    }

    override fun myCarTheme(color: Int, background: Int, isLightColor: Boolean) {
        binding.llcHeader.backgroundTintList = ColorStateList.valueOf(
            ContextCompat.getColor(requireContext(), color)
        )
        binding.sectionCarImage.ivMyCarBackground.setImageDrawable(
            ContextCompat.getDrawable(requireContext(), background)
        )
    }

    override fun setupView(data: MyCarsSample?) = with(binding) {
        data?.let {
            setSectionToolbar(it)
            setSectionImageCar(it)
            setSectionServiceDue()
            setSectionBookingServices()
            setSectionServiceHistory()
            setSectionSubMenu()
            setSectionTradeIn()
            setSectionCredit()
            setSectionCarDetails(it)
        }

        return@with
    }

    private fun setSectionToolbar(data: MyCarsSample) = with(binding) {
        tvFormTitle.text = data.carName
        ivClose.setOnSingleClickListener {
            this@MyCarDetailFragment.dismiss()
        }
    }

    private fun setSectionImageCar(data: MyCarsSample) = with(binding.sectionCarImage) {
        Glide.with(ivCarImage)
            .load(
                ContextCompat.getDrawable(
                    requireContext(),
                    data.image
                )
            )
            .into(ivCarImage)

        tvLabelPoliceNumber.text = getString(R.string.title_label_police_number)
        tvLabelStnk.text = getString(R.string.title_label_stnk_valid_until)

        tvPoliceNumber.text = data.policeNumber
        tvStnk.text = data.stnkValidUntil
        tvWarning.text = "Perpanjang"
    }

    private fun setSectionServiceDue() {
        binding.sectionServiceDue.apply {
            tvLabelServiceDue.text = "Expiring Soon"
            tvServiceDue.text = "26 Aug 2023"

            tvServiceName.text = "RAIZE GR SPORT"
            tvServiceDesc.text = "LAYANAN BENGKEL"
        }
    }

    private fun setSectionBookingServices() = with(binding.sectionBookService) {
        clWorkshopService.setOnSingleClickListener {
            presenter.onClickedSectionBookingServices(MyCarDetailConstant.Event.WORKSHOP_SERVICE)
        }
        clToyotaHomeService.setOnSingleClickListener {
            presenter.onClickedSectionBookingServices(MyCarDetailConstant.Event.THS)
        }
        clBodyPaint.setOnClickListener {
            presenter.onClickedSectionBookingServices(MyCarDetailConstant.Event.BODY_PAINT)
        }
    }

    private fun setSectionServiceHistory() = with(binding.sectionServiceHistory) {
        tvServiceDate.text = "02 Feb 2023"
        tvLastService.text = "25.000 Km"
        tvPrice.text = "Rp 45.000"
        tvLocation.text = "Auto 2000 CEMPAKA PUTHIH"
    }

    private fun setSectionSubMenu() = with(binding.sectionSubMenu) {
        mcvAccessories.setOnSingleClickListener {
            presenter.onClickedSectionBookingServices(MyCarDetailConstant.Event.ACCESSORIES)
        }

        mcvPackage.setOnSingleClickListener {
            presenter.onClickedSectionBookingServices(MyCarDetailConstant.Event.SERVICE_PACKAGE)
        }

        mcvPromos.setOnSingleClickListener {
            presenter.onClickedSectionBookingServices(MyCarDetailConstant.Event.PROMOS)
        }
    }

    private fun setSectionTradeIn() = with(binding.sectionTradeIn) {
        tvEstimation.text = "Rp 80.000.000 - 95.000.000*"
        tvEstimationDesc.text =
            "Perkiraan harga berdasarkan jarak tempuh normal dalam setahun (15.000 Km), Warna dan Kondisi mobil mempengaruhi harga pada saat pemeriksaan"
    }

    private fun setSectionCredit() = with(binding.sectionCredit) {
        tvPayOn.text = "26 Jul 2023"
        tvLastEmi.text = "Rp 4.000.000"
        tvEmiPrice.text = "Rp 4.000.000"
    }

    private fun setSectionCarDetails(data: MyCarsSample) = with(binding.sectionDetails) {
        tvLabel1.text = getString(R.string.title_label_vin_number)
        tvLabel2.text = getString(R.string.title_label_car_variant)
        tvLabel3.text = getString(R.string.title_label_production_year)
        tvLabel4.text = getString(R.string.title_label_color)
        tvLabel5.text = getString(R.string.title_label_fuel_type)
        tvLabel6.text = getString(R.string.title_label_transmission_type)

        tvValue1.text = data.vinNumber
        tvValue2.text = data.carVariant
        tvValue3.text = data.productionYear
        tvValue4.text = data.color
        tvValue5.text = data.fuelType
        tvValue6.text = data.transmissionType
    }
}