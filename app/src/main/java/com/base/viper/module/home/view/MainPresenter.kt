package com.base.viper.module.home.view

import android.app.Activity
import com.base.viper.common.Constants
import com.base.viper.module.home.common.HomeConstant
import com.base.viper.module.home.common.HomeConstant.FragmentTag.ACCOUNT
import com.base.viper.module.home.common.HomeConstant.FragmentTag.CHAT
import com.base.viper.module.home.common.HomeConstant.FragmentTag.HOME
import com.base.viper.module.home.common.HomeConstant.FragmentTag.INITIALIZE
import com.base.viper.module.home.common.HomeConstant.FragmentTag.MENU
import com.base.viper.module.home.common.HomeConstant.FragmentTag.SERVICES

class MainPresenter(private var view: MainInterface.View? = null) : MainInterface.Presenter {

    private var interactor: MainInterface.Interactor? = null
    private var router: MainInterface.Router? = null
    var currentFragmentTag: HomeConstant.FragmentTag = INITIALIZE

    override fun onCreate(activity: Activity) {
        view = activity as MainActivity
        interactor = MainInteractor(activity)
        router = MainRouter(activity)

        initIntent(activity)

        view?.setupView(currentFragmentTag)
    }

    private fun initIntent(activity: Activity) = with(activity) {
        if (intent.hasExtra(HomeConstant.FRAGMENT_TAG)) {
            currentFragmentTag = when (intent.getStringExtra(HomeConstant.FRAGMENT_TAG)) {
                HOME.name -> HOME
                SERVICES.name -> SERVICES
                ACCOUNT.name -> ACCOUNT
                CHAT.name -> CHAT
                MENU.name -> MENU
                else -> INITIALIZE
            }
        }
    }

    override fun onDestroy() {
        view = null
        router = null
    }

    override fun onSectionToolbar(event: Constants.ToolbarEvent) {
        when (event) {
            Constants.ToolbarEvent.SEARCH -> router?.goToSearch()
            Constants.ToolbarEvent.NOTIFICATION -> router?.goToNotification()
            Constants.ToolbarEvent.CART -> router?.goToCart()
        }
    }
}