package com.base.viper.module.promo.entity.promo


import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize


@Parcelize
data class Branch(
    @SerializedName("addressId")
    var addressId: String? = null,
    @SerializedName("altText")
    var altText: String? = null,
    @SerializedName("branchName")
    var branchName: String? = null,
    @SerializedName("cellphone")
    var cellphone: String? = null,
    @SerializedName("city")
    var city: City? = null,
    @SerializedName("countryName")
    var countryName: String? = null,
    @SerializedName("defaultAddress")
    var defaultAddress: Boolean? = null,
    @SerializedName("displayName")
    var displayName: String? = null,
    @SerializedName("formattedAddress")
    var formattedAddress: String? = null,
    @SerializedName("id")
    var id: Int? = null,
    @SerializedName("isInterior")
    var isInterior: String? = null,
    @SerializedName("isShowGetAdvised")
    var isShowGetAdvised: Boolean? = null,
    @SerializedName("isocode")
    var isocode: String? = null,
    @SerializedName("latitude")
    var latitude: Double? = null,
    @SerializedName("line1")
    var line1: String? = null,
    @SerializedName("line2")
    var line2: String? = null,
    @SerializedName("longitude")
    var longitude: Double? = null,
    @SerializedName("openingHoursCode")
    var openingHoursCode: String? = null,
    @SerializedName("openingHoursName")
    var openingHoursName: String? = null,
    @SerializedName("phone")
    var phone: String? = null,
    @SerializedName("plpTestDrive")
    var plpTestDrive: Boolean? = null,
    @SerializedName("postalCode")
    var postalCode: String? = null,
    @SerializedName("shippingAddress")
    var shippingAddress: Boolean? = null,
    @SerializedName("town")
    var town: String? = null,
    @SerializedName("type")
    var type: String? = null,
    @SerializedName("url")
    var url: String? = null,
    @SerializedName("visibleDdAddressBook")
    var visibleDdAddressBook: String? = null,
    @SerializedName("workingSchedule")
    var workingSchedule: String? = null
) : Parcelable {

    @Parcelize
    data class City(
        @SerializedName("id")
        var id: Int? = null,
        @SerializedName("isocode")
        var isocode: String? = null,
        @SerializedName("name")
        var name: String? = null,
        @SerializedName("province")
        var province: String? = null
    ) : Parcelable
}