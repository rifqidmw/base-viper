package com.base.viper.module.newstips.view.category

import androidx.fragment.app.Fragment
import com.base.viper.module.newstips.entity.newstips.NewsTips

interface NewsTipsCategoryInterface {
    interface View {
        fun setupView()
        fun setDataNewsTips(data: List<NewsTips.Content>)
        fun setDataHighlight(data: NewsTips.Content)
        fun setFooterLoading(isLoading: Boolean)
    }

    interface Presenter {
        fun onCreate(fragment: Fragment)
        fun onViewCreated(fragment: Fragment)
        fun onDestroy()

        fun getDataNewsTips(firstLoad: Boolean = false)
        fun goToDetail(item: NewsTips.Content)
    }

    interface Interactor {
        fun getDataNewsTips(page: Int, size: Int, contentCategory: String,
                             onSuccess: (NewsTips) -> Unit,
                             onError: (Throwable) -> Unit
        )
    }

    interface Router {
        fun goToDetail(item: NewsTips.Content)
    }
}