package com.base.viper.module.productknowledge.view.category

import android.content.Intent
import androidx.fragment.app.Fragment
import com.base.viper.module.productknowledge.common.ProductKnowledgeConstant
import com.base.viper.module.productknowledge.entity.ProductKnowledge
import com.base.viper.module.productknowledge.view.detailproductknowledge.DetailProductKnowledgeActivity

class ProductCategoryRouter(private val fragment: Fragment) : ProductCategoryInterface.Router {
    override fun goToDetailProduct(item: ProductKnowledge.Content) {
        fragment.requireContext().startActivity(Intent(fragment.requireContext(), DetailProductKnowledgeActivity::class.java).apply {
            putExtra(ProductKnowledgeConstant.SLUG, item.slug)
        })
    }
}