package com.base.viper.module.promo.entity.promo


import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize


@Parcelize
data class Sort(
    @SerializedName("empty")
    val empty: Boolean? = false,
    @SerializedName("unsorted")
    val unsorted: Boolean? = false,
    @SerializedName("sorted")
    val sorted: Boolean? = false
) : Parcelable