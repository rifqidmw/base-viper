package com.base.viper.module.productknowledge.entity

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize

@Parcelize
data class ProductKnowledgeCategory(
    @SerializedName("priority")
    val priority: Int? = null,
    @SerializedName("name")
    val name: String? = null,
    @SerializedName("description")
    val description: String? = null
) : Parcelable