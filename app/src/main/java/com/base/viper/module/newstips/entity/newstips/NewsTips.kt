package com.base.viper.module.newstips.entity.newstips


import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize


@Parcelize
data class NewsTips(
    @SerializedName("content")
    var content: List<Content> = emptyList(),
    @SerializedName("empty")
    var empty: Boolean? = null,
    @SerializedName("first")
    var first: Boolean? = null,
    @SerializedName("last")
    var last: Boolean? = null,
    @SerializedName("number")
    var number: Int? = null,
    @SerializedName("numberOfElements")
    var numberOfElements: Int? = null,
    @SerializedName("pageable")
    var pageable: Pageable? = null,
    @SerializedName("size")
    var size: Int? = null,
    @SerializedName("sort")
    var sort: Sort? = null,
    @SerializedName("totalElements")
    var totalElements: Int? = null,
    @SerializedName("totalPages")
    var totalPages: Int? = null
) : Parcelable {

    @Parcelize
    data class Content(
        @SerializedName("altImage")
        var altImage: String? = null,
        @SerializedName("branch")
        var branch: Branch? = null,
        @SerializedName("category")
        var category: Category? = null,
        @SerializedName("cities")
        var cities: List<Cities?>? = null,
        @SerializedName("contentCategory")
        var contentCategory: ContentCategory? = null,
        @SerializedName("createdBy")
        var createdBy: String? = null,
        @SerializedName("createdDate")
        var createdDate: String? = null,
        @SerializedName("deletedDate")
        var deletedDate: String? = null,
        @SerializedName("detail")
        var detail: Detail? = null,
        @SerializedName("endDate")
        var endDate: String? = null,
        @SerializedName("groupType")
        var groupType: Int? = null,
        @SerializedName("heroImageLink")
        var heroImageLink: String? = null,
        @SerializedName("id")
        var id: Int? = null,
        @SerializedName("isDeleted")
        var isDeleted: Boolean? = null,
        @SerializedName("keyword")
        var keyword: String? = null,
        @SerializedName("metaDescription")
        var metaDescription: String? = null,
        @SerializedName("metaRobots")
        var metaRobots: List<MetaRobots>? = null,
        @SerializedName("modifiedBy")
        var modifiedBy: String? = null,
        @SerializedName("modifiedDate")
        var modifiedDate: String? = null,
        @SerializedName("modifiedReason")
        var modifiedReason: String? = null,
        @SerializedName("priority")
        var priority: Int? = null,
        @SerializedName("provinces")
        var provinces: List<String?>? = null,
        @SerializedName("publishedDate")
        var publishedDate: String? = null,
        @SerializedName("slug")
        var slug: String? = null,
        @SerializedName("startDate")
        var startDate: String? = null,
        @SerializedName("status")
        var status: String? = null,
        @SerializedName("tag")
        var tag: Tag? = null,
        @SerializedName("titleHeader")
        var titleHeader: String? = null,
        @SerializedName("titlePage")
        var titlePage: String? = null
    ) : Parcelable {

        @Parcelize
        data class Branch(
            @SerializedName("addressId")
            var addressId: String? = null,
            @SerializedName("altText")
            var altText: String? = null,
            @SerializedName("branchName")
            var branchName: String? = null,
            @SerializedName("cellphone")
            var cellphone: String? = null,
            @SerializedName("city")
            var city: City? = null,
            @SerializedName("countryName")
            var countryName: String? = null,
            @SerializedName("defaultAddress")
            var defaultAddress: String? = null,
            @SerializedName("displayName")
            var displayName: String? = null,
            @SerializedName("formattedAddress")
            var formattedAddress: String? = null,
            @SerializedName("id")
            var id: Int? = null,
            @SerializedName("isInterior")
            var isInterior: String? = null,
            @SerializedName("isShowGetAdvised")
            var isShowGetAdvised: String? = null,
            @SerializedName("isocode")
            var isocode: String? = null,
            @SerializedName("latitude")
            var latitude: String? = null,
            @SerializedName("line1")
            var line1: String? = null,
            @SerializedName("line2")
            var line2: String? = null,
            @SerializedName("longitude")
            var longitude: String? = null,
            @SerializedName("openingHoursCode")
            var openingHoursCode: String? = null,
            @SerializedName("openingHoursName")
            var openingHoursName: String? = null,
            @SerializedName("phone")
            var phone: String? = null,
            @SerializedName("plpTestDrive")
            var plpTestDrive: String? = null,
            @SerializedName("postalCode")
            var postalCode: String? = null,
            @SerializedName("shippingAddress")
            var shippingAddress: String? = null,
            @SerializedName("town")
            var town: String? = null,
            @SerializedName("type")
            var type: String? = null,
            @SerializedName("url")
            var url: String? = null,
            @SerializedName("visibleDdAddressBook")
            var visibleDdAddressBook: String? = null,
            @SerializedName("workingSchedules")
            var workingSchedules: List<String?>? = null
        ) : Parcelable {

            @Parcelize
            data class City(
                @SerializedName("id")
                var id: Int? = null,
                @SerializedName("isocode")
                var isocode: String? = null,
                @SerializedName("name")
                var name: String? = null,
                @SerializedName("province")
                var province: String? = null
            ) : Parcelable
        }


        @Parcelize
        data class Category(
            @SerializedName("createdBy")
            var createdBy: String? = null,
            @SerializedName("createdDate")
            var createdDate: String? = null,
            @SerializedName("deletedDate")
            var deletedDate: String? = null,
            @SerializedName("description")
            var description: String? = null,
            @SerializedName("faqs")
            var faqs: List<Faq?>? = null,
            @SerializedName("id")
            var id: Int? = null,
            @SerializedName("isDeleted")
            var isDeleted: Boolean? = null,
            @SerializedName("modifiedBy")
            var modifiedBy: String? = null,
            @SerializedName("modifiedDate")
            var modifiedDate: String? = null,
            @SerializedName("modifiedReason")
            var modifiedReason: String? = null,
            @SerializedName("name")
            var name: String? = null,
            @SerializedName("type")
            var type: String? = null
        ) : Parcelable {

            @Parcelize
            data class Faq(
                @SerializedName("answer")
                var answer: String? = null,
                @SerializedName("createdBy")
                var createdBy: String? = null,
                @SerializedName("createdDate")
                var createdDate: String? = null,
                @SerializedName("deletedDate")
                var deletedDate: String? = null,
                @SerializedName("id")
                var id: Int? = null,
                @SerializedName("isDeleted")
                var isDeleted: Boolean? = null,
                @SerializedName("modifiedBy")
                var modifiedBy: String? = null,
                @SerializedName("modifiedDate")
                var modifiedDate: String? = null,
                @SerializedName("modifiedReason")
                var modifiedReason: String? = null,
                @SerializedName("question")
                var question: String? = null,
                @SerializedName("sequence")
                var sequence: Int? = null
            ) : Parcelable
        }


        @Parcelize
        data class ContentCategory(
            @SerializedName("category")
            var category: Category? = null,
            @SerializedName("createdBy")
            var createdBy: String? = null,
            @SerializedName("createdDate")
            var createdDate: String? = null,
            @SerializedName("deletedDate")
            var deletedDate: String? = null,
            @SerializedName("description")
            var description: String? = null,
            @SerializedName("id")
            var id: Int? = null,
            @SerializedName("isDeleted")
            var isDeleted: Boolean? = null,
            @SerializedName("modifiedBy")
            var modifiedBy: String? = null,
            @SerializedName("modifiedDate")
            var modifiedDate: String? = null,
            @SerializedName("modifiedReason")
            var modifiedReason: String? = null,
            @SerializedName("name")
            var name: String? = null,
            @SerializedName("priority")
            var priority: Int? = null
        ) : Parcelable {

            @Parcelize
            data class Category(
                @SerializedName("createdBy")
                var createdBy: String? = null,
                @SerializedName("createdDate")
                var createdDate: String? = null,
                @SerializedName("deletedDate")
                var deletedDate: String? = null,
                @SerializedName("description")
                var description: String? = null,
                @SerializedName("faqs")
                var faqs: List<Faq?>? = null,
                @SerializedName("id")
                var id: Int? = null,
                @SerializedName("isDeleted")
                var isDeleted: Boolean? = null,
                @SerializedName("modifiedBy")
                var modifiedBy: String? = null,
                @SerializedName("modifiedDate")
                var modifiedDate: String? = null,
                @SerializedName("modifiedReason")
                var modifiedReason: String? = null,
                @SerializedName("name")
                var name: String? = null,
                @SerializedName("type")
                var type: String? = null
            ) : Parcelable {

                @Parcelize
                data class Faq(
                    @SerializedName("answer")
                    var answer: String? = null,
                    @SerializedName("createdBy")
                    var createdBy: String? = null,
                    @SerializedName("createdDate")
                    var createdDate: String? = null,
                    @SerializedName("deletedDate")
                    var deletedDate: String? = null,
                    @SerializedName("id")
                    var id: Int? = null,
                    @SerializedName("isDeleted")
                    var isDeleted: Boolean? = null,
                    @SerializedName("modifiedBy")
                    var modifiedBy: String? = null,
                    @SerializedName("modifiedDate")
                    var modifiedDate: String? = null,
                    @SerializedName("modifiedReason")
                    var modifiedReason: String? = null,
                    @SerializedName("question")
                    var question: String? = null,
                    @SerializedName("sequence")
                    var sequence: Int? = null
                ) : Parcelable
            }
        }


        @Parcelize
        data class Detail(
            @SerializedName("createdBy")
            var createdBy: String? = null,
            @SerializedName("createdDate")
            var createdDate: String? = null,
            @SerializedName("deletedDate")
            var deletedDate: String? = null,
            @SerializedName("detailContent")
            var detailContent: String? = null,
            @SerializedName("isDeleted")
            var isDeleted: Boolean? = null,
            @SerializedName("modifiedBy")
            var modifiedBy: String? = null,
            @SerializedName("modifiedDate")
            var modifiedDate: String? = null,
            @SerializedName("modifiedReason")
            var modifiedReason: String? = null
        ) : Parcelable

        @Parcelize
        data class MetaRobots(
            @SerializedName("createdDate")
            val createdDate: String? = null,
            @SerializedName("createdBy")
            val createdBy: String? = null,
            @SerializedName("modifiedDate")
            val modifiedDate: String? = null,
            @SerializedName("modifiedBy")
            val modifiedBy: String? = null,
            @SerializedName("modifiedReason")
            val modifiedReason: String? = null,
            @SerializedName("isDeleted")
            val isDeleted: Boolean? = null,
            @SerializedName("deletedDate")
            val deletedDate: String? = null,
            @SerializedName("id")
            val id: Int? = null,
            @SerializedName("description")
            val description: String? = null,
        ) : Parcelable


        @Parcelize
        data class Tag(
            @SerializedName("createdBy")
            var createdBy: String? = null,
            @SerializedName("createdDate")
            var createdDate: String? = null,
            @SerializedName("deletedDate")
            var deletedDate: String? = null,
            @SerializedName("description")
            var description: String? = null,
            @SerializedName("id")
            var id: Int? = null,
            @SerializedName("isDeleted")
            var isDeleted: Boolean? = null,
            @SerializedName("modifiedBy")
            var modifiedBy: String? = null,
            @SerializedName("modifiedDate")
            var modifiedDate: String? = null,
            @SerializedName("modifiedReason")
            var modifiedReason: String? = null,
            @SerializedName("tagName")
            var tagName: String? = null
        ) : Parcelable
    }


    @Parcelize
    data class Pageable(
        @SerializedName("offset")
        var offset: Int? = null,
        @SerializedName("pageNumber")
        var pageNumber: Int? = null,
        @SerializedName("pageSize")
        var pageSize: Int? = null,
        @SerializedName("paged")
        var paged: Boolean? = null,
        @SerializedName("sort")
        var sort: Sort? = null,
        @SerializedName("unpaged")
        var unpaged: Boolean? = null
    ) : Parcelable {

        @Parcelize
        data class Sort(
            @SerializedName("empty")
            var empty: Boolean? = null,
            @SerializedName("sorted")
            var sorted: Boolean? = null,
            @SerializedName("unsorted")
            var unsorted: Boolean? = null
        ) : Parcelable
    }


    @Parcelize
    data class Sort(
        @SerializedName("empty")
        var empty: Boolean? = null,
        @SerializedName("sorted")
        var sorted: Boolean? = null,
        @SerializedName("unsorted")
        var unsorted: Boolean? = null
    ) : Parcelable

    @Parcelize
    data class Cities(
        @SerializedName("branches")
        var branches: List<String?>? = null,
        @SerializedName("id")
        var id: Int? = null,
        @SerializedName("isocode")
        var isocode: String? = null,
        @SerializedName("name")
        var name: String? = null,
        @SerializedName("province")
        var province: Province? = null
    ) : Parcelable {

        @Parcelize
        data class Province(
            @SerializedName("cities")
            var cities: List<String?>? = null,
            @SerializedName("id")
            var id: Int? = null,
            @SerializedName("isocode")
            var isocode: String? = null,
            @SerializedName("name")
            var name: String? = null
        ) : Parcelable
    }
}