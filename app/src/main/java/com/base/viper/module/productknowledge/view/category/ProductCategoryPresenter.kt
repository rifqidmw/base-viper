package com.base.viper.module.productknowledge.view.category

import androidx.fragment.app.Fragment
import com.base.viper.module.productknowledge.entity.ProductKnowledge

class ProductCategoryPresenter(
    private var view: ProductCategoryInterface.View? = null,
    private var categoryName: String
) : ProductCategoryInterface.Presenter {

    private var interactor: ProductCategoryInterface.Interactor? = null
    private var router: ProductCategoryInterface.Router? = null

    private var enableLoadMoreData = true
    private val pageSize = 20
    private var page = 0

    override fun onCreate(fragment: Fragment) {
        view = fragment as ProductCategoryFragment
        interactor = ProductCategoryInteractor(fragment)
        router = ProductCategoryRouter(fragment)
    }
    
    override fun onViewCreated(fragment: Fragment) {
        view?.setupView()

        getDataProductKnowledge()
    }

    override fun onDestroy() {
        view = null
        router = null
    }

    override fun getDataProductKnowledge(firstLoad: Boolean) {
        if (firstLoad.not()) view?.setFooterLoading(true)

        if (enableLoadMoreData)
            interactor?.getDataProductKnowledge(
                page = page,
                size = pageSize,
                contentCategory = when (categoryName.lowercase()) {
                    "semua", "all" -> ""
                    else -> categoryName
                },
                {
                    view?.setDataProductKnowledge(it.content)
                    view?.setFooterLoading(false)

                    if (page == 0 && it.content.isNotEmpty())
                        it.content.firstOrNull()?.let { highlight -> view?.setDataHighlight(highlight) }

                    enableLoadMoreData = it.content.size >= pageSize
                    page++
                }, {
                    view?.setFooterLoading(false)
                }
            )
    }

    override fun goToDetail(item: ProductKnowledge.Content) {
        router?.goToDetailProduct(item)
    }
}