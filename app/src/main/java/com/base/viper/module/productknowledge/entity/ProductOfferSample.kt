package com.base.viper.module.productknowledge.entity

data class ProductOfferSample(
    val image: Int,
    val title: String,
    val price: Int,
    val desc: String
)
