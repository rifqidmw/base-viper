package com.base.viper.module.promo.view

import android.app.Activity
import com.base.viper.common.Constants
import com.base.viper.module.promo.entity.promo.Content
import com.base.viper.module.promo.entity.promo.Promo
import com.base.viper.module.promo.entity.promo.PromoCategory

interface PromoInterface {
    interface View {
        fun setupView()
        fun setDataPromo(data: List<Content>?)
        fun setupTabLayout(promoCategories: List<PromoCategory>): Unit?
    }

    interface Presenter {
        fun onCreate(activity: Activity)
        fun onDestroy()
        fun onSectionToolbar(event: Constants.ToolbarEvent)
    }

    interface Interactor {

        fun getDataPromo(onSuccess: (Promo) -> Unit, onError: (Throwable) -> Unit)
        fun getDataCategoryPromo(
            onSuccess: (List<PromoCategory>) -> Unit,
            onError: (Throwable) -> Unit
        )
    }

    interface Router {
        fun goToSearch()
        fun goToNotification()
        fun goToCart()
    }
}