package com.base.viper.module.promo.view.category

import androidx.fragment.app.Fragment
import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.PagingData
import com.base.viper.module.promo.common.PromoPagingSource
import com.base.viper.module.promo.entity.promo.Content
import com.base.viper.services.NetworkBuilder
import com.base.viper.services.Sessions
import io.reactivex.rxjava3.disposables.CompositeDisposable
import kotlinx.coroutines.flow.Flow

class PromoCategoryInteractor(fragment: Fragment) : PromoCategoryInterface.Interactor {

    private val disposable = CompositeDisposable()
    private val apiService = NetworkBuilder.apiService
    private val sessions = fragment.context?.let { Sessions(it) }

    override fun getDataPromo(
        size: Int,
        contentCategory: String
    ): Flow<PagingData<Content>> {
        return Pager(
            config = PagingConfig(
                pageSize = 1,
                prefetchDistance = 2,
                enablePlaceholders = false
            ),
            pagingSourceFactory = {
                PromoPagingSource(
                    apiService = apiService,
                    size = size,
                    contentCategory = contentCategory
                )
            }
        ).flow
    }
}