package com.base.viper.module.onboarding

import android.app.Activity

interface OnboardingInterface {
    interface View {
        fun setupView()
    }

    interface Presenter {
        fun onCreate(activity: Activity)
        fun onDestroy()

        fun goToLogin()
        fun goToRegister()
    }

    interface Interactor

    interface Router {
        fun goToLogin()
        fun goToRegister()
    }
}