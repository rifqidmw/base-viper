package com.base.viper.module.onboarding

import android.app.Activity

class OnboardingPresenter(
    private var view: OnboardingInterface.View? = null
) : OnboardingInterface.Presenter {
    
    private var interactor: OnboardingInterface.Interactor? = null
    private var router: OnboardingInterface.Router? = null
    
    override fun onCreate(activity: Activity) {
        view = activity as OnboardingActivity
        interactor = OnboardingInteractor(activity)
        router = OnboardingRouter(activity)

        view?.setupView()
    }
    
    override fun onDestroy() {
        view = null
        router = null
    }

    override fun goToLogin() {
        router?.goToLogin()
    }

    override fun goToRegister() {
        router?.goToRegister()
    }
}