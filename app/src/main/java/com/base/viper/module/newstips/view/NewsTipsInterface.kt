package com.base.viper.module.newstips.view

import android.app.Activity
import com.base.viper.common.Constants
import com.base.viper.module.newstips.entity.newstips.NewsTipsCategory

interface NewsTipsInterface {
    interface View {
        fun setupView()
        fun setupTabLayout(newsTipsCategories: List<NewsTipsCategory>): Unit?
    }

    interface Presenter {
        fun onCreate(activity: Activity)
        fun onDestroy()
        fun onSectionToolbar(event: Constants.ToolbarEvent)
        fun shareWithLink()
    }

    interface Interactor {
        fun getDataCategoryNewsTips(
            onSuccess: (List<NewsTipsCategory>) -> Unit,
            onError: (Throwable) -> Unit
        )
    }

    interface Router {
        fun goToSearch()
        fun goToNotification()
        fun goToCart()
        fun shareWithLink()
    }
}