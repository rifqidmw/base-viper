package com.base.viper.module.addcar.registercar

import androidx.fragment.app.Fragment

interface RegisterCarInterface {
    interface View {
        fun setupView()
    }

    interface Presenter {
        fun onCreate(fragment: Fragment)
        fun onViewCreated(fragment: Fragment)
        fun onDestroy()

        fun goToNotYourCar()
        fun goToRegisterCar()
    }

    interface Interactor

    interface Router {
        fun goToNotYourCar()
        fun goToRegisterCar()
    }
}