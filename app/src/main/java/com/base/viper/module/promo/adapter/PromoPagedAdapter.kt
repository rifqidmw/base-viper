package com.base.viper.module.promo.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import com.base.viper.R
import com.base.viper.common.BaseViewHolder
import com.base.viper.databinding.ItemPromoBinding
import com.base.viper.module.promo.entity.promo.Content
import com.base.viper.utils.Utils.dateTimeFormat
import com.base.viper.utils.Utils.gone
import com.base.viper.utils.Utils.setOnSingleClickListener
import com.base.viper.utils.Utils.visible
import com.bumptech.glide.Glide

class PromoPagedAdapter :
    PagingDataAdapter<Content, PromoPagedAdapter.ViewHolder>(DataDifferentTiator) {

    override fun onBindViewHolder(holder: PromoPagedAdapter.ViewHolder, position: Int) {
        getItem(position)?.let {
            holder.bind(it)
        }
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): PromoPagedAdapter.ViewHolder {
        return ViewHolder(
            ItemPromoBinding.inflate(
                LayoutInflater.from(
                    parent.context,
                ),
                parent,
                false
            )
        )
    }

    inner class ViewHolder(
        private val binding: ItemPromoBinding
    ) : BaseViewHolder(binding) {
        fun bind(content: Content?) = with(binding) {

            Glide.with(root.context).load(content?.heroImageLink).into(ivPromo)
            when (content?.tag?.tagName) {
                "TRENDING" -> {
                    tvLabelBestSeller.gone()
                    tvLabelTrending.visible()
                }

                "BEST SELLER" -> {
                    tvLabelBestSeller.visible()
                    tvLabelTrending.gone()
                }

                else -> {
                    tvLabelBestSeller.gone()
                    tvLabelTrending.gone()
                }
            }

            tvPromoTitle.text = content?.titlePage
            tvDate.text = String.format(
                root.context.getString(R.string.format_period),
                content?.startDate?.dateTimeFormat(),
                content?.endDate?.dateTimeFormat()
            )
            root.setOnSingleClickListener {
                onItemClickListener?.onClickedListener(content)
            }
        }
    }

    private var onItemClickListener: OnItemClickListener? = null

    interface OnItemClickListener {
        fun <T> onClickedListener(item: T)
    }

    fun setOnItemClickListener(onClickListener: OnItemClickListener) {
        this.onItemClickListener = onClickListener
    }

    object DataDifferentTiator : DiffUtil.ItemCallback<Content>() {
        override fun areItemsTheSame(oldItem: Content, newItem: Content): Boolean {
            return oldItem.id == newItem.id
        }

        override fun areContentsTheSame(oldItem: Content, newItem: Content): Boolean {
            return oldItem == newItem
        }
    }

}