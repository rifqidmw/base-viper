package com.base.viper.module.productknowledge.entity

data class ProductSample(
    var image: Int,
    var category: String,
    var title: String,
    var period: String,
)