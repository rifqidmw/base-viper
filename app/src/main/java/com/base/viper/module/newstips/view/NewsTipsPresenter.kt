package com.base.viper.module.newstips.view

import android.app.Activity
import com.base.viper.common.Constants
import com.base.viper.module.newstips.entity.newstips.NewsTipsCategory

class NewsTipsPresenter(
    private var view: NewsTipsInterface.View? = null
) : NewsTipsInterface.Presenter {

    private var interactor: NewsTipsInterface.Interactor? = null
    private var router: NewsTipsInterface.Router? = null

    override fun onCreate(activity: Activity) {
        view = activity as NewsTipsActivity
        interactor = NewsTipsInteractor(activity)
        router = NewsTipsRouter(activity)

        view?.setupView()

        getDataNewsTipsCategory()
    }

    override fun onDestroy() {
        view = null
        router = null
    }

    override fun onSectionToolbar(event: Constants.ToolbarEvent) {
        when (event) {
            Constants.ToolbarEvent.SEARCH -> router?.goToSearch()
            Constants.ToolbarEvent.NOTIFICATION -> router?.goToNotification()
            Constants.ToolbarEvent.CART -> router?.goToCart()
        }
    }

    override fun shareWithLink() {
        router?.shareWithLink()
    }

    private fun getDataNewsTipsCategory() {
        interactor?.getDataCategoryNewsTips(
            {
                val categories = it.toMutableList()
                categories.add(0, NewsTipsCategory(0, "SEMUA", ""))
                view?.setupTabLayout(categories.sortedBy { sorted -> sorted.priority })
            }, {

            }
        )
    }
}