package com.base.viper.module.productknowledge.view.askoffering

import com.base.viper.R
import com.base.viper.common.CustomPopUpDialog
import com.google.android.material.bottomsheet.BottomSheetDialogFragment

class AskOfferingRouter(private val fragment: BottomSheetDialogFragment) :
    AskOfferingInterface.Router {
    override fun goToSuccessMessage() {
        CustomPopUpDialog(fragment.requireContext()).popUpSuccessMessage(
            fragment.getString(R.string.title_inquiry_success),
            fragment.getString(R.string.desc_inquiry_success),
            fragment.getString(R.string.title_label_inquiry_see_more_product_knowledge),
            true
        ) {
            fragment.dismiss()
        }
    }
}