package com.base.viper.module.newstips.view

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.res.ResourcesCompat
import com.base.viper.R
import com.base.viper.components.CustomToolbar
import com.base.viper.databinding.ActivityNewsTipsBinding
import com.base.viper.databinding.CustomTabBinding
import com.base.viper.module.newstips.adapter.ViewPagerNewsTipsAdapter
import com.base.viper.module.newstips.entity.newstips.NewsTipsCategory
import com.google.android.material.tabs.TabLayout
import com.google.android.material.tabs.TabLayoutMediator

class NewsTipsActivity : AppCompatActivity(), NewsTipsInterface.View {

    private val presenter: NewsTipsPresenter = NewsTipsPresenter(this)
    private val binding: ActivityNewsTipsBinding by lazy {
        ActivityNewsTipsBinding.inflate(layoutInflater)
    }

    private lateinit var newsAdapter: ViewPagerNewsTipsAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)
        presenter.onCreate(this)
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter.onDestroy()
    }

    override fun setupView() {
        setupToolbar()
    }

    override fun setupTabLayout(newsTipsCategories: List<NewsTipsCategory>) = with(binding) {
        newsAdapter = ViewPagerNewsTipsAdapter(newsTipsCategories, supportFragmentManager, lifecycle)
        vpNewsTips.offscreenPageLimit = newsTipsCategories.size
        vpNewsTips.adapter = newsAdapter

        TabLayoutMediator(tlNewsTips, vpNewsTips) { tab, position ->
            val tabViewBinding = CustomTabBinding.inflate(layoutInflater)
            if (position == 0) {
                tabViewBinding.tvTabText.typeface = ResourcesCompat.getFont(
                    this@NewsTipsActivity,
                    R.font.montserrat_700_bold
                )
            }
            tabViewBinding.tvTabText.text = newsTipsCategories[position].name
            tab.customView = tabViewBinding.root
        }.attach()

        tlNewsTips.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabSelected(tab: TabLayout.Tab?) {
                tab?.let {
                    val tabViewBinding = CustomTabBinding.bind(it.customView!!)
                    tabViewBinding.tvTabText.typeface = ResourcesCompat.getFont(
                        this@NewsTipsActivity,
                        R.font.montserrat_700_bold
                    )
                }
            }

            override fun onTabUnselected(tab: TabLayout.Tab?) {
                tab?.let {
                    val tabViewBinding = CustomTabBinding.bind(tab.customView!!)
                    tabViewBinding.tvTabText.typeface = ResourcesCompat.getFont(
                        this@NewsTipsActivity,
                        R.font.montserrat_400_regular
                    )
                }
            }

            override fun onTabReselected(tab: TabLayout.Tab?) {
                //Not Yet Implemented
            }
        })

        tlNewsTips.getTabAt(0)?.select()
    }

    private fun setupToolbar() {
        binding.toolbar.setOnClickedListener(object : CustomToolbar.Listener {
            override fun onClickedBackButton() {
                onBackPressedDispatcher.onBackPressed()
            }
        })
    }
}