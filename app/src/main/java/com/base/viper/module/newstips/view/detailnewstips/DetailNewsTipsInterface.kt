package com.base.viper.module.newstips.view.detailnewstips

import android.app.Activity
import com.base.viper.common.Constants
import com.base.viper.module.newstips.common.NewsTipsConstant
import com.base.viper.module.newstips.entity.detail.NewsTipsDetail
import com.base.viper.module.newstips.entity.newstips.NewsTips
import com.base.viper.module.promo.entity.promo.Content
import com.base.viper.module.promo.entity.promo.Promo

interface DetailNewsTipsInterface {
    interface View {
        fun setupView()
        fun setDataNewsAndTips(data: NewsTipsDetail)

        fun setupSubPromo(data: List<Content>?)
        fun setupSubNewsTips(data: List<NewsTips.Content>)
        fun fragmentPromoResultListener(data: List<Content>?)
        fun fragmentNewsTipsResultListener(data: List<NewsTips.Content>?)
    }

    interface Presenter {
        fun onCreate(activity: Activity)
        fun onDestroy()

        fun onSectionToolbar(event: Constants.ToolbarEvent)
        fun goToInquiry()
        fun goToHome()
        fun onSharePromo(event: Constants.ShareEvent)
        fun goToAllPromo()
        fun goToAllNews()
        fun goToDetailPromo(slug: String?)
        fun goToDetailNewsTips(slug: String?)
    }

    interface Interactor {
        fun getDetailNewsTips(
            slug: String?,
            onSuccess: (NewsTipsDetail) -> Unit,
            onError: (Throwable) -> Unit
        )

        fun getSubPromo(size: Int, onSuccess: (Promo) -> Unit, onError: (Throwable) -> Unit)
        fun getSubNewsTips(size: Int, onSuccess: (NewsTips) -> Unit, onError: (Throwable) -> Unit)
    }

    interface Router {
        fun goToSearch()
        fun goToNotification()
        fun goToCart()
        fun goToInquiry()
        fun goToHome()
        fun shareViaWhatsapp()
        fun shareViaFacebook()
        fun shareViaTwitter()
        fun shareViaTiktok()
        fun shareWithLink()
        fun goToAllPromo()
        fun goToAllNews(origin: NewsTipsConstant.OriginEvent)
        fun goToDetailPromo(slug: String?)
        fun goToDetailNewsTips(slug: String?)
    }
}