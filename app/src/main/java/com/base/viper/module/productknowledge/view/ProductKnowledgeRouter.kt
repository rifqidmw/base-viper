package com.base.viper.module.productknowledge.view

import android.app.Activity
import android.content.Intent
import com.base.viper.module.productknowledge.view.detailproductknowledge.DetailProductKnowledgeActivity
import com.base.viper.utils.Utils.toastShort

class ProductKnowledgeRouter(private val activity: Activity) : ProductKnowledgeInterface.Router {
    override fun goToSearch() {
        activity.toastShort("Todo: Search")
    }

    override fun goToNotification() {
        activity.toastShort("Todo: Notification")
    }

    override fun goToCart() {
        activity.toastShort("Todo: Cart")
    }

    override fun goToDetailProduct() = with(activity) {
        startActivity(Intent(activity, DetailProductKnowledgeActivity::class.java))
    }
}