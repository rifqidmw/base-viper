package com.base.viper.module.addcar.registercar

import android.os.Bundle
import android.view.View
import com.base.viper.common.BaseBottomDialogFragment
import com.base.viper.databinding.FragmentRegisterCarBinding
import com.base.viper.utils.Utils.setOnSingleClickListener

class RegisterCarFragment : BaseBottomDialogFragment<FragmentRegisterCarBinding>(FragmentRegisterCarBinding::inflate), RegisterCarInterface.View {

    private val presenter: RegisterCarPresenter = RegisterCarPresenter(this)

    override fun setOnCreatePresenter() {
        presenter.onCreate(this)
    }

    override fun setOnViewCreatedPresenter(view: View, savedInstanceState: Bundle?) {
        presenter.onViewCreated(this)
    }

    override fun setOnDestroyPresenter() {
        presenter.onDestroy()
    }

    override fun setupView() {
        with(binding){
            ivClose.setOnSingleClickListener {
                this@RegisterCarFragment.dismiss()
            }

            flNotYourCar.setOnSingleClickListener {
                presenter.goToNotYourCar()
            }

            btnAddCar.setOnSingleClickListener {
                presenter.goToRegisterCar()
            }
        }
    }
}