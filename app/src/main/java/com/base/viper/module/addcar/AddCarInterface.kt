package com.base.viper.module.addcar

import android.app.Activity

interface AddCarInterface {
    interface View {
        fun setupView()
    }

    interface Presenter {
        fun onCreate(activity: Activity)
        fun onDestroy()

        fun goToFindCar()
        fun goToRegisterCar()
    }

    interface Interactor

    interface Router {
        fun goToFindCar()
        fun goToRegisterCar()
    }
}