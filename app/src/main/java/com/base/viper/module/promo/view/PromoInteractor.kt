package com.base.viper.module.promo.view

import android.app.Activity
import com.base.viper.common.Constants
import com.base.viper.module.promo.entity.promo.Promo
import com.base.viper.module.promo.entity.promo.PromoCategory
import com.base.viper.services.NetworkBuilder
import com.base.viper.services.Sessions
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.disposables.CompositeDisposable
import io.reactivex.rxjava3.schedulers.Schedulers

class PromoInteractor(activity: Activity) : PromoInterface.Interactor {

    private val disposable = CompositeDisposable()
    private val apiService = NetworkBuilder.apiService
    private val sessions = Sessions(activity)

    override fun getDataCategoryPromo(
        onSuccess: (List<PromoCategory>) -> Unit,
        onError: (Throwable) -> Unit
    ) {
        apiService.getCategoryByType(
            Constants.VersionType.V1.value,
            "promo"
        )
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(onSuccess, onError)
            .let(disposable::add)
    }

    override fun getDataPromo(
        onSuccess: (Promo) -> Unit,
        onError: (Throwable) -> Unit
    ) {
        apiService.getPromo(Constants.VersionType.V1.value)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(onSuccess, onError)
            .let(disposable::add)
    }
}