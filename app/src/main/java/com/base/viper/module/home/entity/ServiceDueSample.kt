package com.base.viper.module.home.entity

data class ServiceDueSample(
    val due: String,
    val serviceName: String,
    val desc: String,
    val isNearExpire: Boolean,
)