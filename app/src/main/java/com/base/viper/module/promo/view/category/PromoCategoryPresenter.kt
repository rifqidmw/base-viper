package com.base.viper.module.promo.view.category

import android.util.Log
import androidx.fragment.app.Fragment
import androidx.lifecycle.LifecycleCoroutineScope
import androidx.lifecycle.lifecycleScope
import androidx.paging.PagingData
import com.base.viper.module.newstips.view.category.NewsTipsCategoryPresenter
import com.base.viper.module.promo.entity.promo.Content
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach

class PromoCategoryPresenter(
    private var view: PromoCategoryInterface.View? = null,
    private var contentCategory: String
) : PromoCategoryInterface.Presenter {

    private var interactor: PromoCategoryInterface.Interactor? = null
    private var router: PromoCategoryInterface.Router? = null
    private var promoDataFlow: Flow<PagingData<Content>>? = null

    override fun onCreate(fragment: Fragment) {
        view = fragment as PromoCategoryFragment
        interactor = PromoCategoryInteractor(fragment)
        router = PromoCategoryRouter(fragment)
    }

    override fun onViewCreated(fragment: Fragment) {
        view?.setupView()

        getPromo(fragment.lifecycleScope)
    }

    override fun onDestroy() {
        view = null
        router = null
        promoDataFlow = null
    }

    private fun getPromo(lifecycleScope: LifecycleCoroutineScope) {
        promoDataFlow =
            promoDataFlow ?: interactor?.getDataPromo(size = 20, contentCategory = contentCategory)

        promoDataFlow?.onEach { pagingData ->
            view?.setDataPromoPaged(pagingData)
        }?.catch { exception ->
            Log.e(
                NewsTipsCategoryPresenter::class.java.simpleName,
                "News Tips Pagination Error: $exception"
            )
        }?.launchIn(lifecycleScope)
    }

    override fun goToDetailPromo(item: Content) {
        router?.goToDetailPromo(item)
    }
}