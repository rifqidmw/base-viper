package com.base.viper.module.home.view.home.servicebooking

import android.app.Activity
import com.base.viper.common.Constants

interface ServiceBookingInterface {
    interface View {
        fun setupView()
    }

    interface Presenter {
        fun onCreate(activity: Activity)
        fun onDestroy()

        fun onSectionToolbar(event: Constants.ToolbarEvent)
    }

    interface Interactor

    interface Router
}