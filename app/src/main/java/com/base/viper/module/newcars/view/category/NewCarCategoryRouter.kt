package com.base.viper.module.newcars.view.category

import androidx.fragment.app.Fragment
import com.base.viper.module.newcars.entity.NewCarSample

class NewCarCategoryRouter(private val fragment: Fragment) : NewCarCategoryInterface.Router {
    override fun goToDetailNewCar(item: NewCarSample) {
         /*
        fragment.requireActivity().startActivity(
            Intent(fragment.context, DetailNewCarActivity::class.java).apply {
                putExtra("DATA_NEW_CAR", item)
            }
        )
         */
    }
}