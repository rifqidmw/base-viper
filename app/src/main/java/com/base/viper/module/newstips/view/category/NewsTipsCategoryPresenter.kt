package com.base.viper.module.newstips.view.category

import androidx.fragment.app.Fragment
import com.base.viper.module.newstips.entity.newstips.NewsTips

class NewsTipsCategoryPresenter(
    private var view: NewsTipsCategoryInterface.View? = null,
    private var categoryName: String
) : NewsTipsCategoryInterface.Presenter {

    private var interactor: NewsTipsCategoryInterface.Interactor? = null
    private var router: NewsTipsCategoryInterface.Router? = null

    private var enableLoadMoreData = true
    private val pageSize = 20
    private var page = 0

    override fun onCreate(fragment: Fragment) {
        view = fragment as NewsTipsCategoryFragment
        interactor = NewsTipsCategoryInteractor(fragment)
        router = NewsTipsCategoryRouter(fragment)
    }

    override fun onViewCreated(fragment: Fragment) {
        view?.setupView()

        getDataNewsTips(firstLoad = true)
    }

    override fun onDestroy() {
        view = null
        router = null
    }

    override fun getDataNewsTips(firstLoad: Boolean) {
        if (firstLoad.not()) view?.setFooterLoading(true)

        if (enableLoadMoreData)
            interactor?.getDataNewsTips(
                page = page,
                size = pageSize,
                contentCategory = when (categoryName.lowercase()) {
                    "semua", "all" -> ""
                    else -> categoryName
                },
                {
                    view?.setDataNewsTips(it.content)
                    view?.setFooterLoading(false)

                    if (page == 0 && it.content.isNotEmpty())
                        it.content.firstOrNull()?.let { highlight -> view?.setDataHighlight(highlight) }

                    enableLoadMoreData = it.content.size >= pageSize
                    page++
                }, {
                    view?.setFooterLoading(false)
                }
            )
    }


    override fun goToDetail(item: NewsTips.Content) {
        router?.goToDetail(item)
    }
}