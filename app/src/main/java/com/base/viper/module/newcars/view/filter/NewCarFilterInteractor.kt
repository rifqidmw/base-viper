package com.base.viper.module.newcars.view.filter

import androidx.fragment.app.Fragment
import com.base.viper.services.NetworkBuilder
import com.base.viper.services.Sessions
import io.reactivex.rxjava3.disposables.CompositeDisposable

class NewCarFilterInteractor(fragment: Fragment) : NewCarFilterInterface.Interactor {
    
    private val disposable = CompositeDisposable()
    private val apiService = NetworkBuilder.apiService
    private val sessions = fragment.context?.let { Sessions(it) }
}