package com.base.viper.module.addcar

import android.app.Activity

class AddCarPresenter(
    private var view: AddCarInterface.View? = null
) : AddCarInterface.Presenter {
    
    private var interactor: AddCarInterface.Interactor? = null
    private var router: AddCarInterface.Router? = null
    
    override fun onCreate(activity: Activity) {
        view = activity as AddCarActivity
        interactor = AddCarInteractor(activity)
        router = AddCarRouter(activity)

        view?.setupView()
    }
    
    override fun onDestroy() {
        view = null
        router = null
    }

    override fun goToFindCar() {
        router?.goToFindCar()
    }

    override fun goToRegisterCar() {
        router?.goToRegisterCar()
    }
}