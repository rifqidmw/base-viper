package com.base.viper.module.onboarding.onboardingcontent

import androidx.fragment.app.Fragment

interface OnboardingContentInterface {
    interface View {
        fun setupView()
    }

    interface Presenter {
        fun onCreate(fragment: Fragment)
        fun onViewCreated(fragment: Fragment)
        fun onDestroy()

        fun goToLogin()
        fun goToRegister()
    }

    interface Interactor

    interface Router {
        fun goToLogin()
        fun goToRegister()
    }
}