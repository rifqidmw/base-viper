package com.base.viper.module.splash

import android.app.Activity

interface SplashInterface {
    interface View {
        fun setupView()
    }

    interface Presenter {
        fun onCreate(activity: Activity)
        fun onDestroy()

        fun goToLogin()
        fun goToHome()
    }

    interface Interactor

    interface Router {
        fun goToLogin()
        fun goToHome()
    }
}