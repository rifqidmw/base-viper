package com.base.viper.module.home.view.home

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.LinearSnapHelper
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.SnapHelper
import androidx.viewpager2.widget.ViewPager2
import com.base.viper.R
import com.base.viper.common.CustomPopUpDialog
import com.base.viper.common.GridItemSpaceDecoration
import com.base.viper.common.HorizontalMarginItemDecoration
import com.base.viper.databinding.FragmentHomeBinding
import com.base.viper.module.home.adapter.RecyclerViewAdapter
import com.base.viper.module.home.adapter.ViewPager2Adapter
import com.base.viper.module.home.adapter.ViewPager2Adapter.Type.TRENDING_CAR
import com.base.viper.module.home.common.HomeConstant
import com.base.viper.module.home.entity.BranchLocationSample
import com.base.viper.module.home.entity.MyCarsSample
import com.base.viper.module.home.entity.PreferredMenuSample
import com.base.viper.module.home.entity.ServiceDueSample
import com.base.viper.module.home.entity.TrendingCarSample
import com.base.viper.module.home.view.MainActivity
import com.base.viper.module.newstips.entity.newstips.NewsTips
import com.base.viper.module.promo.entity.promo.Content
import com.base.viper.utils.Utils.dp
import com.base.viper.utils.Utils.formatRupiah
import com.base.viper.utils.Utils.gone
import com.base.viper.utils.Utils.next
import com.base.viper.utils.Utils.previous
import com.base.viper.utils.Utils.setOnSingleClickListener
import com.base.viper.utils.Utils.setStatusBarColor
import com.base.viper.utils.Utils.visible

class HomeFragment : Fragment(), HomeInterface.View, RecyclerViewAdapter.OnItemClickListener {

    private val presenter: HomePresenter = HomePresenter(this)
    private val binding: FragmentHomeBinding by lazy {
        FragmentHomeBinding.inflate(layoutInflater)
    }
    private val serviceDueAdapter: RecyclerViewAdapter by lazy {
        RecyclerViewAdapter(RecyclerViewAdapter.ViewType.SERVICE_DUE)
    }
    private val myCarsAdapter: RecyclerViewAdapter by lazy {
        RecyclerViewAdapter(RecyclerViewAdapter.ViewType.MY_CARS)
    }
    private val preferredCategoryAdapter: RecyclerViewAdapter by lazy {
        RecyclerViewAdapter(RecyclerViewAdapter.ViewType.MENU)
    }
    private val branchLocationAdapter: RecyclerViewAdapter by lazy {
        RecyclerViewAdapter(RecyclerViewAdapter.ViewType.BRANCH_LOCATION)
    }
    private val promoAdapter: RecyclerViewAdapter by lazy {
        RecyclerViewAdapter(RecyclerViewAdapter.ViewType.PROMO)
    }
    private val trendingCarAdapter: ViewPager2Adapter by lazy {
        ViewPager2Adapter(
            TRENDING_CAR,
            requireActivity().supportFragmentManager,
            lifecycle
        )
    }
    private val newsAndTipsAdapter: RecyclerViewAdapter by lazy {
        RecyclerViewAdapter(RecyclerViewAdapter.ViewType.NEWS_AND_TIPS)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        presenter.onCreate(this)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        presenter.onViewCreated(this)
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter.onDestroy()
    }

    override fun setupView() {
        setupServiceDueAdapter()
        setupMyCarsAdapter()
        setupPreferredCategoryAdapter()
        setupPromoAdapter()
        setupNewsAndTipsAdapter()
        setupBranchLocationAdapter()
        setupTrendingCarAdapter()
    }

    override fun <T> onClickedListener(item: T) {
        if (item is ServiceDueSample) presenter.goToServiceDue()
        if (item is MyCarsSample) presenter.goToDetailMyCar(item)
        if (item is PreferredMenuSample) presenter.goToPreferredCategory(item.menuCode)
        if (item is BranchLocationSample) presenter.goToDetailBranchLocation(item)
        if (item is Content) presenter.goToDetailPromo(item)
        if (item is NewsTips.Content) presenter.goToDetailNewsAndTips(item)
    }

    //Setup View
    override fun homeTheme(color: Int, drawable: Int, isLightColor: Boolean) {
        requireActivity().setStatusBarColor(color, isLightColor)
        val currentActivity = activity
        if (currentActivity is MainActivity) currentActivity.setupToolbarTheme(color)
        binding.sectionMyCar.ivBackgroundMyCars.background = ContextCompat.getDrawable(
            requireContext(), drawable
        )
    }

    override fun setGreeting(greetings: String) {
        binding.sectionMyCar.tvWelcomeUser.text = greetings
    }

    private fun setupMyCarsAdapter() {
        myCarsAdapter.setOnItemClickListener(this)
        binding.sectionMyCar.rvMyCars.apply {
            adapter = myCarsAdapter
            layoutManager = LinearLayoutManager(requireContext(), RecyclerView.HORIZONTAL, false)
        }
    }

    private fun setupServiceDueAdapter() {
        serviceDueAdapter.setOnItemClickListener(this)
        binding.sectionServiceDue.rvServiceBooking.apply {
            adapter = serviceDueAdapter
            layoutManager = LinearLayoutManager(requireContext(), RecyclerView.HORIZONTAL, false)
        }
    }

    private fun setupPreferredCategoryAdapter() {
        preferredCategoryAdapter.setOnItemClickListener(this)
        binding.sectionPrefferedCategory.rvPreferredCategory.apply {
            adapter = preferredCategoryAdapter
            layoutManager = GridLayoutManager(requireContext(), 3)
            addItemDecoration(GridItemSpaceDecoration(3, 10, false))
        }
    }

    private fun setupPromoAdapter() {
        promoAdapter.setOnItemClickListener(this)
        binding.sectionPromos.rvPromo.apply {
            adapter = promoAdapter
            layoutManager = LinearLayoutManager(requireContext(), RecyclerView.HORIZONTAL, false)
            addItemDecoration(
                HorizontalMarginItemDecoration(
                    16.dp,
                    lastItemEndMargin = 16.dp
                )
            )
        }

        binding.sectionPromos.btnViewAllPromo.setOnClickListener {
            presenter.goToPromo()
        }
    }

    private fun setupNewsAndTipsAdapter() {
        newsAndTipsAdapter.setOnItemClickListener(this)
        val snapHelper: SnapHelper = LinearSnapHelper()
        binding.sectionNewsAndTips.rvNewsAndTips.apply {
            layoutManager = LinearLayoutManager(requireContext(), RecyclerView.HORIZONTAL, false)
            adapter = newsAndTipsAdapter
            snapHelper.attachToRecyclerView(this)
            addItemDecoration(
                HorizontalMarginItemDecoration(
                    16.dp,
                    lastItemEndMargin = 16.dp
                )
            )
        }

        binding.sectionNewsAndTips.btnViewAllNewsAndTips.setOnSingleClickListener {
            presenter.goToAllNewsAndTips()
        }
    }

    private fun setupBranchLocationAdapter() {
        branchLocationAdapter.setOnItemClickListener(this)
        binding.sectionBranchLocation.rvBranchLocation.apply {
            layoutManager = LinearLayoutManager(requireContext(), RecyclerView.HORIZONTAL, false)
            adapter = branchLocationAdapter
            addItemDecoration(
                HorizontalMarginItemDecoration(
                    horizontalMargin = 0.dp,
                    firstItemStartMargin = 8.dp,
                    lastItemEndMargin = 4.dp
                )
            )
        }
    }

    private fun setupTrendingCarAdapter() {
        binding.sectionTrendingCars.vpTrendingCars.apply {
            adapter = trendingCarAdapter
            (getChildAt(0) as RecyclerView).overScrollMode = RecyclerView.OVER_SCROLL_NEVER
        }
    }
    //Setup View

    //Set Data
    override fun setDataMyCars(data: List<MyCarsSample>) = with(binding.sectionMyCar) {
        if (data.isNotEmpty()) {
            myCarsAdapter.setListMyCars(data)
            val snapHelper: SnapHelper = LinearSnapHelper()
            rvMyCars.apply {
                snapHelper.attachToRecyclerView(this)
                cpiIndicator.attachToRecyclerView(this, snapHelper)
            }

            flAddCar.setOnSingleClickListener {
                presenter.goToAddCar()
            }
        }
    }

    override fun setShimmerMyCars(isShimmer: Boolean) = with(binding.sectionMyCar) {
        shimmerSectionMyCar.root.visible(isShimmer)
        rvMyCars.visible(!isShimmer)
        cpiIndicator.visible(!isShimmer)
    }

    override fun setDataServiceDue(data: List<ServiceDueSample>) = with(binding.sectionServiceDue) {
        if (data.isNotEmpty()) {
            serviceDueAdapter.setListServiceDue(data)
            val snapHelper: SnapHelper = LinearSnapHelper()
            rvServiceBooking.apply {
                snapHelper.attachToRecyclerView(this)
                cpiServiceBooking.attachToRecyclerView(this, snapHelper)
            }
        }
    }

    override fun setShimmerServiceDue(isShimmer: Boolean) = with(binding.sectionServiceDue) {
        shimmerSectionServiceDue.root.visible(isShimmer)
        rvServiceBooking.visible(!isShimmer)
        cpiServiceBooking.visible(!isShimmer)
    }

    override fun setDataPreferredCategory(data: List<PreferredMenuSample>) {
        if (data.isNotEmpty()) {
            preferredCategoryAdapter.setListMenu(data)
        }
    }

    override fun setShimmerPreferredCategory(isShimmer: Boolean) =
        with(binding.sectionPrefferedCategory) {
            shimmerSectionPreferredMenu.root.visible(isShimmer)
            rvPreferredCategory.visible(!isShimmer)
        }

    override fun setDataPromo(data: List<Content>?) = with(binding.sectionPromos) {
        if (data.isNullOrEmpty()) {
            // TODO Negative Case
        } else {
            promoAdapter.setListPromo(data)
            val snapHelper: SnapHelper = LinearSnapHelper()
            rvPromo.apply {
                snapHelper.attachToRecyclerView(this)
                cpiIndicatorPromo.attachToRecyclerView(this, snapHelper)
            }
        }
        return@with
    }

    override fun setShimmerPromo(isShimmer: Boolean) = with(binding.sectionPromos) {
        shimmerSectionPromo.root.visible(isShimmer)
        rvPromo.visible(!isShimmer)
        cpiIndicatorPromo.visible(!isShimmer)
    }

    override fun setDataNewsAndTips(data: List<NewsTips.Content>) {
        if (data.isNotEmpty()) {
            binding.sectionNewsAndTips.root.visible()
            newsAndTipsAdapter.setListNewsAndTips(data)
        } else {
            binding.sectionNewsAndTips.root.gone()
        }
    }

    override fun setShimmerNewsAndTips(isShimmer: Boolean) = with(binding.sectionNewsAndTips) {
        shimmerSectionNewsTips.root.visible(isShimmer)
        rvNewsAndTips.visible(!isShimmer)
    }

    override fun setDataBranchLocation(data: List<BranchLocationSample>) =
        with(binding.sectionBranchLocation) {
            if (data.isNotEmpty()) {
                branchLocationAdapter.setListBranch(data)
                val snapHelper: SnapHelper = LinearSnapHelper()
                rvBranchLocation.apply {
                    snapHelper.attachToRecyclerView(this)
                    cpiIndicatorBranch.attachToRecyclerView(this, snapHelper)
                }

                btnViewAllBranch.setOnSingleClickListener {
                    presenter.goToAllBranchLocation()
                }
            }
        }

    override fun setShimmerBranchLocation(isShimmer: Boolean) =
        with(binding.sectionBranchLocation) {
            shimmerSectionBranchLocation.root.visible(isShimmer)
            rvBranchLocation.visible(!isShimmer)
            cpiIndicatorBranch.visible(!isShimmer)
        }

    override fun setDataTrendingCars(data: List<TrendingCarSample>) =
        with(binding.sectionTrendingCars) {
            if (data.isNotEmpty()) {
                trendingCarAdapter.setTrendingCar(data)

                val firstData = data.first()
                tvTrendingCarName.text = firstData.carName
                tvPrice.text = String.format(
                    getString(R.string.format_rupiah),
                    firstData.price.formatRupiah()
                )

                vpTrendingCars.registerOnPageChangeCallback(object :
                    ViewPager2.OnPageChangeCallback() {
                    override fun onPageSelected(position: Int) {
                        val currentData = data[position]
                        tvTrendingCarName.text = currentData.carName
                        tvPrice.text = String.format(
                            getString(R.string.format_rupiah),
                            currentData.price.formatRupiah()
                        )

                        ibPrevious.isEnabled = position > 0
                        ibPrevious.background = when {
                            ibPrevious.isEnabled -> ContextCompat.getDrawable(
                                requireContext(),
                                R.drawable.ic_arrow_left_red_circle_enable
                            )

                            else -> ContextCompat.getDrawable(
                                requireContext(),
                                R.drawable.ic_arrow_left_grey_circle_disable
                            )
                        }

                        ibNext.isEnabled = position < data.size - 1
                        ibNext.background = when {
                            ibNext.isEnabled -> ContextCompat.getDrawable(
                                requireContext(),
                                R.drawable.ic_arrow_right_red_circle_enable
                            )

                            else -> ContextCompat.getDrawable(
                                requireContext(),
                                R.drawable.ic_arrow_right_grey_circle_disable
                            )
                        }
                        setTrendingCarListener(currentData)
                        vpTrendingCars.currentItem = position
                    }
                })

                ibNext.setOnSingleClickListener {
                    vpTrendingCars.next()
                }

                ibPrevious.setOnSingleClickListener {
                    vpTrendingCars.previous()
                }
            }
        }

    private fun setTrendingCarListener(trendingCar: TrendingCarSample) =
        with(binding.sectionTrendingCars) {
            btnCompare.setOnSingleClickListener {
                presenter.onSectionTrendingCar(trendingCar, HomeConstant.TrendingCarEvent.COMPARE)
            }

            btnTestDrive.setOnSingleClickListener {
                presenter.onSectionTrendingCar(
                    trendingCar,
                    HomeConstant.TrendingCarEvent.TEST_DRIVE
                )
            }

            btnInquire.setOnSingleClickListener {
                presenter.onSectionTrendingCar(trendingCar, HomeConstant.TrendingCarEvent.INQUIRE)
            }

            btnExplore.setOnSingleClickListener {
                presenter.onSectionTrendingCar(trendingCar, HomeConstant.TrendingCarEvent.EXPLORE)
            }
        }
    //Set Data

    override fun showPopUpError(throwable: Throwable) {
        CustomPopUpDialog(requireContext()).popUpWarning(
            "Something Went Wrong",
            throwable.message.toString()
        )
    }
}