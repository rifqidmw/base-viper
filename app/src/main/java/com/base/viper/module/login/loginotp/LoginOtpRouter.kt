package com.base.viper.module.login.loginotp

import android.app.Activity
import android.content.Intent
import com.base.viper.module.home.view.MainActivity

class LoginOtpRouter(private val activity: Activity) : LoginOtpInterface.Router {
    override fun goToHome() {
        activity.startActivity(Intent(activity.applicationContext, MainActivity::class.java).apply {
            flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        })
        activity.finish()
    }
}