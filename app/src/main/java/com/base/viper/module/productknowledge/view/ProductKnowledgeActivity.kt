package com.base.viper.module.productknowledge.view

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.res.ResourcesCompat
import com.base.viper.R
import com.base.viper.components.CustomToolbar
import com.base.viper.databinding.ActivityProductKnowledgeBinding
import com.base.viper.databinding.CustomTabBinding
import com.base.viper.module.productknowledge.adapter.ViewPagerProductKnowledgeAdapter
import com.base.viper.module.productknowledge.entity.ProductKnowledgeCategory
import com.google.android.material.tabs.TabLayout
import com.google.android.material.tabs.TabLayoutMediator

class ProductKnowledgeActivity : AppCompatActivity(), ProductKnowledgeInterface.View {

    private val presenter: ProductKnowledgePresenter = ProductKnowledgePresenter(this)
    private val binding: ActivityProductKnowledgeBinding by lazy {
        ActivityProductKnowledgeBinding.inflate(layoutInflater)
    }

    private lateinit var categoryProductAdapter: ViewPagerProductKnowledgeAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)
        presenter.onCreate(this)
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter.onDestroy()
    }

    override fun setupView() {
        setupToolbar()
    }

    private fun setupToolbar() {
        binding.toolbar.setOnClickedListener(object : CustomToolbar.Listener {
            override fun onClickedBackButton() {
                onBackPressedDispatcher.onBackPressed()
            }
        })
    }

    override fun setupTabLayout(productKnowledgeCategories: List<ProductKnowledgeCategory>): Unit? = with(binding) {
        categoryProductAdapter = ViewPagerProductKnowledgeAdapter(productKnowledgeCategories, supportFragmentManager, lifecycle)
        vpProduct.offscreenPageLimit = productKnowledgeCategories.size
        vpProduct.adapter = categoryProductAdapter

        TabLayoutMediator(tlProduct, vpProduct) { tab, position ->
            val tabViewBinding = CustomTabBinding.inflate(layoutInflater)
            if (position == 0) {
                tabViewBinding.tvTabText.typeface = ResourcesCompat.getFont(
                    this@ProductKnowledgeActivity,
                    R.font.montserrat_700_bold
                )
            }
            tabViewBinding.tvTabText.text = productKnowledgeCategories[position].name
            tab.customView = tabViewBinding.root
        }.attach()

        tlProduct.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabSelected(tab: TabLayout.Tab?) {
                tab?.let {
                    val tabViewBinding = CustomTabBinding.bind(it.customView!!)
                    tabViewBinding.tvTabText.typeface = ResourcesCompat.getFont(
                        this@ProductKnowledgeActivity,
                        R.font.montserrat_700_bold
                    )
                }
            }

            override fun onTabUnselected(tab: TabLayout.Tab?) {
                tab?.let {
                    val tabViewBinding = CustomTabBinding.bind(tab.customView!!)
                    tabViewBinding.tvTabText.typeface = ResourcesCompat.getFont(
                        this@ProductKnowledgeActivity,
                        R.font.montserrat_400_regular
                    )
                }
            }

            override fun onTabReselected(tab: TabLayout.Tab?) {
                //Not Yet Implemented
            }
        })

        tlProduct.getTabAt(0)?.select()
    }
}