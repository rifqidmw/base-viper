package com.base.viper.module.productknowledge.entity

data class ProductAdvantageSample(
    val title: String,
    val desc: String
)
