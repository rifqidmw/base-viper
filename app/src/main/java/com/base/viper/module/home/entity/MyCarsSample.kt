package com.base.viper.module.home.entity

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class MyCarsSample(
    var carName: String,
    var serviceDue: String,
    var policeNumber: String,
    var image: Int,
    var vinNumber: String,
    var productionYear: String,
    var color: String,
    var stnkValidUntil: String,
    var carVariant: String,
    var transmissionType: String,
    var fuelType: String,
) : Parcelable