package com.base.viper.module.home.view.home.servicebooking

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.base.viper.components.CustomToolbar
import com.base.viper.databinding.ActivityServiceBookingBinding

class ServiceBookingActivity : AppCompatActivity(), ServiceBookingInterface.View {
    
    private val presenter: ServiceBookingPresenter = ServiceBookingPresenter(this)
    private val binding: ActivityServiceBookingBinding by lazy {
        ActivityServiceBookingBinding.inflate(layoutInflater)
    }
    
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)
        presenter.onCreate(this)
    }
    
    override fun onDestroy() {
        super.onDestroy()
        presenter.
        onDestroy()
    }

    override fun setupView() {
        setupToolbar()
    }

    private fun setupToolbar() {
        binding.toolbar.setOnClickedListener(object : CustomToolbar.Listener {
            override fun onClickedBackButton() {
                onBackPressedDispatcher.onBackPressed()
            }
        })
    }
}