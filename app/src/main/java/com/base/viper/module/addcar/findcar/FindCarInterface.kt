package com.base.viper.module.addcar.findcar

import androidx.fragment.app.Fragment

interface FindCarInterface {
    interface View {
        fun setupView()
    }

    interface Presenter {
        fun onCreate(fragment: Fragment)
        fun onViewCreated(fragment: Fragment)
        fun onDestroy()

        fun goToRegisterCar()
        fun goToNotExistingCustomer()
    }

    interface Interactor

    interface Router {
        fun goToRegisterCar()
        fun goToNotExistingCustomer()
    }
}