package com.base.viper.module.home.view

import android.content.res.ColorStateList
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import com.base.viper.R
import com.base.viper.common.Constants
import com.base.viper.databinding.ActivityMainBinding
import com.base.viper.module.home.common.HomeConstant
import com.base.viper.module.home.common.HomeConstant.FragmentTag.ACCOUNT
import com.base.viper.module.home.common.HomeConstant.FragmentTag.CHAT
import com.base.viper.module.home.common.HomeConstant.FragmentTag.HOME
import com.base.viper.module.home.common.HomeConstant.FragmentTag.MENU
import com.base.viper.module.home.common.HomeConstant.FragmentTag.SERVICES
import com.base.viper.module.home.view.account.AccountFragment
import com.base.viper.module.home.view.chat.ChatFragment
import com.base.viper.module.home.view.home.HomeFragment
import com.base.viper.module.home.view.menu.MenuFragment
import com.base.viper.module.home.view.services.ServicesFragment
import com.base.viper.utils.Utils.setOnSingleClickListener

class MainActivity : AppCompatActivity(), MainInterface.View {

    private val presenter: MainPresenter = MainPresenter(this)
    private val binding: ActivityMainBinding by lazy {
        ActivityMainBinding.inflate(layoutInflater)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)
        presenter.onCreate(this)
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter.onDestroy()
    }

    override fun setupView(tag: HomeConstant.FragmentTag) {
        setupToolbar()

        when (tag) {
            SERVICES -> replaceFragment(ServicesFragment(), SERVICES)
            ACCOUNT -> replaceFragment(AccountFragment(), ACCOUNT)
            CHAT -> replaceFragment(ChatFragment(), CHAT)
            MENU -> replaceFragment(MenuFragment(), MENU)
            else -> replaceFragment(HomeFragment(), HOME)
        }

        binding.bottomNavBar.itemIconTintList = null
        binding.bottomNavBar.setOnItemSelectedListener {
            when (it.itemId) {
                R.id.home -> replaceFragment(HomeFragment(), HOME)
                R.id.services -> replaceFragment(ServicesFragment(), SERVICES)
                R.id.account -> replaceFragment(AccountFragment(), ACCOUNT)
                R.id.chat -> replaceFragment(ChatFragment(), CHAT)
                R.id.menus -> replaceFragment(MenuFragment(), MENU)
            }
            return@setOnItemSelectedListener true
        }
    }

    private fun setupToolbar() = with(binding) {
        flSearch.setOnSingleClickListener {
            presenter.onSectionToolbar(Constants.ToolbarEvent.SEARCH)
        }

        flNotification.setOnSingleClickListener {
            presenter.onSectionToolbar(Constants.ToolbarEvent.NOTIFICATION)
        }

        flCart.setOnSingleClickListener {
            presenter.onSectionToolbar(Constants.ToolbarEvent.CART)
        }
    }

    override fun replaceFragment(fragment: Fragment, tag: HomeConstant.FragmentTag) {
        supportFragmentManager.beginTransaction()
            .replace(binding.fragmentContainer.id, fragment, tag.name)
            .commit()
        presenter.currentFragmentTag = tag
    }

    override fun setupToolbarTheme(color: Int) {
        binding.toolbar.backgroundTintList = ColorStateList.valueOf(
            ContextCompat.getColor(this, color)
        )
    }
}