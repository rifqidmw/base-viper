package com.base.viper.module.promo.view.detailpromo

import android.app.Activity
import com.base.viper.common.Constants
import java.math.BigInteger

class DetailPromoPresenter(
    private var view: DetailPromoInterface.View? = null
) : DetailPromoInterface.Presenter {

    private var interactor: DetailPromoInterface.Interactor? = null
    private var router: DetailPromoInterface.Router? = null
    private var slug: String? = null
    var bundlePrice1 = ""
    var bundlePrice2 = ""

    override fun onCreate(activity: Activity) {
        view = activity as DetailPromoActivity
        interactor = DetailPromoInteractor(activity)
        router = DetailPromoRouter(activity)

        initIntentData(activity)
        view?.setupView()

        getDetailPromo(slug)
        getSubPromo()
    }

    override fun onDestroy() {
        view = null
        router = null
    }

    private fun initIntentData(activity: Activity) = with(activity.intent) {
        slug = this.getStringExtra("PROMO_SLUG")
    }

    override fun onSectionToolbar(event: Constants.ToolbarEvent) {
        when (event) {
            Constants.ToolbarEvent.SEARCH -> router?.goToSearch()
            Constants.ToolbarEvent.NOTIFICATION -> router?.goToNotification()
            Constants.ToolbarEvent.CART -> router?.goToCart()
        }
    }

    override fun goToFormRequestQuote() {
        router?.goToFormRequestQuote()
    }

    private fun getDetailPromo(slug: String?) {
        interactor?.getDetailPromo(slug,
            {
                view?.setupDataNonRegular(it)
            }, {

            }
        )
    }

    private fun getSubPromo() {
        interactor?.getSubPromo(10,
            {
                val filteredPromo = it.content.filter { content ->
                    content.status == Constants.StatusType.PUBLISH.value
                }
                view?.setupSubPromo(filteredPromo)
                view?.fragmentResultListener(filteredPromo)
            }, {

            }
        )
    }

    override fun simulatePrice() {
        //This is Only Sample
        val bundlePrice1BigInt = BigInteger(bundlePrice1)
        val bundlePrice2BigInt = BigInteger(bundlePrice2)

        val beforeDiscountPrice = bundlePrice1BigInt + bundlePrice2BigInt
        val afterDiscount =
            beforeDiscountPrice - ((beforeDiscountPrice * BigInteger("10")) / BigInteger("100"))
        val priceDifference = beforeDiscountPrice - afterDiscount

        view?.setCalculationResult(afterDiscount, beforeDiscountPrice, priceDifference)
    }

    override fun onSharePromo(event: Constants.ShareEvent) {
        when (event) {
            Constants.ShareEvent.WHATSAPP -> router?.shareViaWhatsapp()
            Constants.ShareEvent.FACEBOOK -> router?.shareViaFacebook()
            Constants.ShareEvent.TWITTER -> router?.shareViaTwitter()
            Constants.ShareEvent.TIKTOK -> router?.shareViaTiktok()
            Constants.ShareEvent.LINK -> router?.shareWithLink()
        }
    }

    override fun goToHome() {
        router?.goToHome()
    }

    override fun goToAllPromo() {
        router?.goToAllPromo()
    }

    override fun goToDetailPromo(slug: String?) {
        router?.goToDetailPromo(slug)
    }

    override fun goToCart() {
        router?.goToCart()
    }
}