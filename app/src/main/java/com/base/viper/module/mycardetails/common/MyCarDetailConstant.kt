package com.base.viper.module.mycardetails.common

object MyCarDetailConstant {

    enum class Event {
        WORKSHOP_SERVICE, THS, BODY_PAINT,

        ACCESSORIES, SERVICE_PACKAGE, PROMOS
    }

}