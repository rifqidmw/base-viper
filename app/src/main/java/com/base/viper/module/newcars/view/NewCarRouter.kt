package com.base.viper.module.newcars.view

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import com.base.viper.module.newcars.entity.NewCarSample
import com.base.viper.module.newcars.view.filter.NewCarFilterFragment
import com.base.viper.module.newcars.view.sort.NewCarSortFragment
import com.base.viper.utils.Utils.toastShort

class NewCarRouter(
    private val activity: AppCompatActivity
) : NewCarInterface.Router {
    override fun goToSearch() {
        activity.toastShort("Todo: Search")
    }

    override fun goToNotification() {
        activity.toastShort("Todo: Notification")
    }

    override fun goToCart() {
        activity.toastShort("Todo: Cart")
    }

    override fun goToAllNewCar(data: ArrayList<NewCarSample>) {
        val intent = Intent(activity, NewCarActivity::class.java)
        intent.putParcelableArrayListExtra("LIST_ALL_NEW_CAR", data)
        activity.startActivity(intent)
    }

    override fun goToFilter() {
        val bottomSheet = NewCarFilterFragment()
        bottomSheet.show(activity.supportFragmentManager, "FILTER_CAR")
    }

    override fun goToSort() {
        val bottomSheet = NewCarSortFragment()
        bottomSheet.show(activity.supportFragmentManager, "SORT_CAR")
    }
}