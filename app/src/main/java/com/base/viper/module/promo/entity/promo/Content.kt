package com.base.viper.module.promo.entity.promo

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize

@Parcelize
data class Content(
    @SerializedName("createdDate")
    val createdDate: String? = null,
    @SerializedName("createdBy")
    val createdBy: String? = null,
    @SerializedName("modifiedDate")
    val modifiedDate: String? = null,
    @SerializedName("modifiedBy")
    val modifiedBy: String? = null,
    @SerializedName("modifiedReason")
    val modifiedReason: String? = null,
    @SerializedName("isDeleted")
    val isDeleted: Boolean? = false,
    @SerializedName("deletedDate")
    val deletedDate: String? = null,
    @SerializedName("id")
    val id: Int? = null,
    @SerializedName("publishedDate")
    val publishedDate: String? = null,
    @SerializedName("startDate")
    val startDate: String? = null,
    @SerializedName("endDate")
    val endDate: String? = null,
    @SerializedName("titlePage")
    val titlePage: String? = null,
    @SerializedName("titleHeader")
    val titleHeader: String? = null,
    @SerializedName("keyword")
    val keyword: String? = null,
    @SerializedName("metaDescription")
    val metaDescription: String? = null,
    @SerializedName("altImage")
    val altImage: String? = null,
    @SerializedName("heroImageLink")
    val heroImageLink: String? = null,
    @SerializedName("status")
    val status: String? = null,
    @SerializedName("branch")
    val branch: Branch? = null,
    @SerializedName("category")
    val category: Category? = null,
    @SerializedName("slug")
    val slug: String? = null,
    @SerializedName("groupType")
    val groupType: String? = null,
    @SerializedName("contentCategory")
    val contentCategory: ContentCategory? = null,
    @SerializedName("priority")
    val priority: String? = null,
    @SerializedName("detail")
    val detail: Detail? = null,
    @SerializedName("tag")
    val tag: Tag? = null,
    @SerializedName("cities")
    val cities: List<Cities>? = null,
    @SerializedName("provinces")
    val provinces: List<Province>? = null,
    @SerializedName("metaRobots")
    val metaRobots: List<MetaRobots>? = null
) : Parcelable