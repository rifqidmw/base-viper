package com.base.viper.module.productknowledge.view

import android.app.Activity
import com.base.viper.common.Constants
import com.base.viper.module.productknowledge.entity.ProductKnowledgeCategory
import com.base.viper.module.productknowledge.entity.ProductSample

interface ProductKnowledgeInterface {
    interface View {
        fun setupView()
        fun setupTabLayout(productKnowledgeCategories: List<ProductKnowledgeCategory>): Unit?
    }
    
    interface Presenter {
        fun onCreate(activity: Activity)
        fun onDestroy()
        fun onSectionToolbar(event: Constants.ToolbarEvent)
        fun goToDetailProduct()
    }
    
    interface Interactor {

        fun getDataCategoryProductKnowledge(
            onSuccess: (List<ProductKnowledgeCategory>) -> Unit,
            onError: (Throwable) -> Unit
        )

        fun getDataListProduct(
            onSuccess: (List<ProductSample>) -> Unit,
            onError: (Throwable) -> Unit
        )
    }
    
    interface Router {

        fun goToSearch()
        fun goToNotification()
        fun goToCart()
        fun goToDetailProduct()
    }
}