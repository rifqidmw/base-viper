package com.base.viper.module.newstips.view.category

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.text.HtmlCompat
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.base.viper.databinding.FragmentNewsTipsCategoryBinding
import com.base.viper.module.newstips.adapter.RecyclerViewNewsTipsAdapter
import com.base.viper.module.newstips.entity.newstips.NewsTips
import com.base.viper.module.newstips.entity.newstips.NewsTipsCategory
import com.base.viper.utils.Utils.dateTimeFormat
import com.base.viper.utils.Utils.gone
import com.base.viper.utils.Utils.setImageViewUrl
import com.base.viper.utils.Utils.setOnSingleClickListener
import com.base.viper.utils.Utils.visible

class NewsTipsCategoryFragment(
    category: NewsTipsCategory,
) : Fragment(), NewsTipsCategoryInterface.View, RecyclerViewNewsTipsAdapter.OnItemClickListener {

    private val presenter: NewsTipsCategoryPresenter = NewsTipsCategoryPresenter(this, category.name?:"")
    private val binding: FragmentNewsTipsCategoryBinding by lazy {
        FragmentNewsTipsCategoryBinding.inflate(layoutInflater)
    }

    private val newsAdapter: RecyclerViewNewsTipsAdapter by lazy {
        RecyclerViewNewsTipsAdapter(RecyclerViewNewsTipsAdapter.ViewType.NEWS)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        presenter.onCreate(this)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        presenter.onViewCreated(this)
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter.onDestroy()
    }

    override fun setupView() {
        setupNewsAdapter()
    }

    override fun setDataNewsTips(data: List<NewsTips.Content>) {
        newsAdapter.setListNews(data)
    }

    override fun setDataHighlight(data: NewsTips.Content) = with(binding) {
        lyHighlight.visible()

        ivHighlight.setImageViewUrl(data.heroImageLink)
        tvHighlighTitle.text = data.titlePage?.let { HtmlCompat.fromHtml(it, HtmlCompat.FROM_HTML_MODE_LEGACY) }
        tvHighlighDate.text = data.publishedDate?.dateTimeFormat()

        lyHighlight.setOnSingleClickListener {
            presenter.goToDetail(data)
        }
    }

    override fun setFooterLoading(isLoading: Boolean) = with(binding) {
        if (isLoading) pbFooterLoading.visible() else pbFooterLoading.gone()
    }

    private fun setupNewsAdapter() {
        with(binding) {
            rvNews.apply {
                setHasFixedSize(true)
                layoutManager = LinearLayoutManager(requireContext())
                adapter = newsAdapter.apply {
                    setOnItemClickListener(this@NewsTipsCategoryFragment)
                }

                addOnScrollListener(object : RecyclerView.OnScrollListener(){
                    override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                        super.onScrolled(recyclerView, dx, dy)
                        val layoutManager = recyclerView.layoutManager as LinearLayoutManager
                        val lastVisibleItemPosition = layoutManager.findLastVisibleItemPosition()
                        val totalItemCount = layoutManager.itemCount

                        if (lastVisibleItemPosition == totalItemCount - 1) {
                            if (binding.pbFooterLoading.isVisible.not())
                                presenter.getDataNewsTips()
                        }
                    }
                })
            }
        }
    }

    override fun <T> onClickedListener(item: T) {
        if (item is NewsTips.Content) presenter.goToDetail(item = item)
    }
}