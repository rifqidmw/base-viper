package com.base.viper.module.promo.entity.promo

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize

@Parcelize
data class Cities(
    @SerializedName("branches")
    var branches: List<String?>? = null,
    @SerializedName("id")
    var id: Int? = null,
    @SerializedName("isocode")
    var isocode: String? = null,
    @SerializedName("name")
    var name: String? = null,
    @SerializedName("province")
    var province: Province? = null
) : Parcelable {

    @Parcelize
    data class Province(
        @SerializedName("cities")
        var cities: List<String?>? = null,
        @SerializedName("id")
        var id: Int? = null,
        @SerializedName("isocode")
        var isocode: String? = null,
        @SerializedName("name")
        var name: String? = null
    ) : Parcelable
}
