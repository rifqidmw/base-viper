package com.base.viper.module.splash

import android.app.Activity
import android.content.Intent
import android.os.Build.VERSION.SDK_INT
import com.base.viper.R
import com.base.viper.module.home.view.MainActivity
import com.base.viper.module.onboarding.OnboardingActivity

class SplashRouter(private val activity: Activity) : SplashInterface.Router {
    override fun goToLogin() {

        activity.startActivity(Intent(activity.applicationContext, OnboardingActivity::class.java))
        activity.finish()
        when {
            SDK_INT >= 34 -> activity.overrideActivityTransition(
                Activity.OVERRIDE_TRANSITION_CLOSE,
                R.anim.fade_out,
                R.anim.fade_out
            )

            else -> activity.overridePendingTransition(
                R.anim.fade_out,
                R.anim.fade_out
            )
        }

    }

    override fun goToHome() {
        activity.startActivity(Intent(activity.applicationContext, MainActivity::class.java))
        activity.finish()
    }
}