package com.base.viper.module.promo.adapter

import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.Lifecycle
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.base.viper.module.promo.entity.bundle.BundleSample
import com.base.viper.module.promo.view.detailpromo.bundling.BundlingFragment

class BundlingPagerAdapter(
    private val listBundle: List<BundleSample>,
    fragmentManager: FragmentManager,
    lifecycle: Lifecycle,
) : FragmentStateAdapter(fragmentManager, lifecycle) {

    override fun getItemCount() = listBundle.size

    override fun createFragment(position: Int): Fragment {
        return BundlingFragment().apply {
            arguments = Bundle().apply {
                this.putParcelable("DATA_BUNDLE", listBundle[position])
            }
        }
    }
}