package com.base.viper.module.home.view.home

import android.content.Context
import androidx.fragment.app.Fragment
import com.base.viper.R
import com.base.viper.common.Constants
import com.base.viper.module.home.common.HomeConstant
import com.base.viper.module.home.common.HomeConstant.TrendingCarEvent.COMPARE
import com.base.viper.module.home.common.HomeConstant.TrendingCarEvent.EXPLORE
import com.base.viper.module.home.common.HomeConstant.TrendingCarEvent.INQUIRE
import com.base.viper.module.home.common.HomeConstant.TrendingCarEvent.TEST_DRIVE
import com.base.viper.module.home.entity.BranchLocationSample
import com.base.viper.module.home.entity.Menu
import com.base.viper.module.home.entity.Menu.ACCESSORIES
import com.base.viper.module.home.entity.Menu.ROAD_SIDE_ASSISTANCE
import com.base.viper.module.home.entity.Menu.SERVICE_COUPON
import com.base.viper.module.home.entity.Menu.SERVICE_PACKAGE
import com.base.viper.module.home.entity.Menu.TRACKING_ORDERS
import com.base.viper.module.home.entity.Menu.TRADE_IN
import com.base.viper.module.home.entity.MyCarsSample
import com.base.viper.module.home.entity.TrendingCarSample
import com.base.viper.module.newstips.entity.newstips.NewsTips
import com.base.viper.module.promo.entity.promo.Content
import java.util.Calendar

class HomePresenter(private var view: HomeInterface.View? = null) : HomeInterface.Presenter {

    private var interactor: HomeInterface.Interactor? = null
    private var router: HomeInterface.Router? = null

    override fun onCreate(fragment: Fragment) {
        view = fragment as HomeFragment
        interactor = HomeInteractor(fragment)
        router = HomeRouter(fragment)
    }

    override fun onDestroy() {
        view = null
        router = null
    }

    override fun onViewCreated(fragment: Fragment) {
        view?.setupView()

        setTheme()
        setGreetings(fragment.requireContext(), "John Cena")
        getDataServiceDue()
        getDataMyCars()
        getDataPreferredCategory()
        getDataBranchLocation()
        getDataPromo()
        getDataTrendingCars()
        getDataNewsAndTips()
    }

    private fun setTheme() {
        when (Calendar.getInstance()[Calendar.HOUR_OF_DAY]) {
            in 19..23 -> view?.homeTheme(
                R.color.reliable_black,
                R.drawable.bg_linear_black,
                isLightColor = false,
            )

            else -> view?.homeTheme(
                R.color.supportive_red,
                R.drawable.bg_linear_red,
                isLightColor = false
            )
        }
    }

    private fun setGreetings(context: Context, username: String) {
        val greetings = String.format(
            context.getString(
                when (Calendar.getInstance()[Calendar.HOUR_OF_DAY]) {
                    in 0..11 -> R.string.title_greetings_morning
                    in 12..15 -> R.string.title_greetings_afternoon
                    in 16..18 -> R.string.title_greetings_evening
                    in 19..23 -> R.string.title_greetings_night
                    else -> R.string.title_greetings_default
                }
            ), username
        )
        view?.setGreeting(greetings)
    }

    private fun getDataServiceDue() {
        view?.setShimmerServiceDue(true)
        interactor?.getDataServiceDue({
            view?.setShimmerServiceDue(false)
            view?.setDataServiceDue(it)
        }, {
            view?.setShimmerServiceDue(false)
        })
    }

    private fun getDataMyCars() {
        view?.setShimmerMyCars(true)
        interactor?.getDataMyCars({
            view?.setShimmerMyCars(false)
            view?.setDataMyCars(it)
        }, {
            view?.setShimmerMyCars(false)
        })
    }

    private fun getDataPreferredCategory() {
        view?.setShimmerPreferredCategory(true)
        interactor?.getDataPreferredCategory({
            view?.setShimmerPreferredCategory(false)
            view?.setDataPreferredCategory(it)
        }, {
            view?.setShimmerPreferredCategory(false)
        })
    }

    private fun getDataBranchLocation() {
        view?.setShimmerBranchLocation(true)
        interactor?.getDataBranchLocation({
            view?.setShimmerBranchLocation(false)
            view?.setDataBranchLocation(it)
        }, {
            view?.setShimmerBranchLocation(false)
        })
    }

    private fun getDataPromo() {
        view?.setShimmerPromo(true)
        interactor?.getDataPromo(10, {
            view?.setShimmerPromo(false)
            val filteredPromo = it.content.filter { content ->
                content.status == Constants.StatusType.PUBLISH.value
            }
            view?.setDataPromo(it.content)
        }, {
            view?.setShimmerPromo(false)
        })
    }

    private fun getDataTrendingCars() {
        interactor?.getDataTrendingCars({
            view?.setDataTrendingCars(it)
        }, {

        })
    }

    private fun getDataNewsAndTips() {
        view?.setShimmerNewsAndTips(true)
        interactor?.getDataNewsAndTips(10, {
            view?.setShimmerNewsAndTips(false)
            val filteredNewsTips = it.content.filter { content ->
                content.status == Constants.StatusType.PUBLISH.value
            }
            view?.setDataNewsAndTips(it.content)
        }, {
            view?.setShimmerNewsAndTips(false)
            view?.setDataNewsAndTips(emptyList())
        })
    }

    override fun goToServiceDue() {
        router?.goToServiceDue()
    }

    override fun goToDetailMyCar(item: MyCarsSample) {
        router?.goToDetailMyCar(item)
    }

    override fun goToAddCar() {
        router?.goToAddCar()
    }

    override fun goToPreferredCategory(menuCode: Menu) {
        when (menuCode) {
            SERVICE_COUPON -> router?.goToServiceCoupon()
            SERVICE_PACKAGE -> router?.goToServicePackage()
            TRADE_IN -> router?.goToTradeIn()
            TRACKING_ORDERS -> router?.goToTrackingOrders()
            ACCESSORIES -> router?.goToAccessories()
            ROAD_SIDE_ASSISTANCE -> router?.goToRoadSideAssistance()
        }
    }

    override fun goToAllBranchLocation() {
        router?.goToAllBranchLocation()
    }

    override fun goToDetailBranchLocation(item: BranchLocationSample) {
        router?.goToDetailBranchLocation()
    }

    override fun goToPromo() {
        router?.goToAllPromo()
    }

    override fun goToDetailPromo(item: Content) {
        router?.goToDetailPromo(item)
    }

    override fun onSectionTrendingCar(
        trendingCar: TrendingCarSample,
        event: HomeConstant.TrendingCarEvent
    ) {
        when (event) {
            COMPARE -> router?.goToCompareCar()
            TEST_DRIVE -> router?.goToTestDrive()
            INQUIRE -> router?.goToInquireCar()
            EXPLORE -> router?.goToExploreCar()
        }
    }

    override fun goToDetailNewsAndTips(item: NewsTips.Content) {
        router?.goToDetailNewsAndTips(item)
    }

    override fun goToAllNewsAndTips() {
        router?.goToAllNewsAndTips()
    }
}