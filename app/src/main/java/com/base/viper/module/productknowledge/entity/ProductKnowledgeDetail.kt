package com.base.viper.module.productknowledge.entity


import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize


@Parcelize
data class ProductKnowledgeDetail(
    @SerializedName("altImage")
    var altImage: String? = null,
    @SerializedName("branch")
    var branch: String? = null,
    @SerializedName("category")
    var category: Category? = null,
    @SerializedName("cities")
    var cities: List<String?>? = null,
    @SerializedName("contentCategory")
    var contentCategory: ContentCategory? = null,
    @SerializedName("detail")
    var detail: Detail? = null,
    @SerializedName("endDate")
    var endDate: String? = null,
    @SerializedName("groupType")
    var groupType: String? = null,
    @SerializedName("heroImageLink")
    var heroImageLink: String? = null,
    @SerializedName("id")
    var id: Int? = null,
    @SerializedName("keyword")
    var keyword: String? = null,
    @SerializedName("metaDescription")
    var metaDescription: String? = null,
    @SerializedName("metaRobots")
    var metaRobots: List<MetaRobot?>? = null,
    @SerializedName("priority")
    var priority: String? = null,
    @SerializedName("provinces")
    var provinces: List<String?>? = null,
    @SerializedName("publishedDate")
    var publishedDate: String? = null,
    @SerializedName("slug")
    var slug: String? = null,
    @SerializedName("startDate")
    var startDate: String? = null,
    @SerializedName("status")
    var status: String? = null,
    @SerializedName("tag")
    var tag: String? = null,
    @SerializedName("titleHeader")
    var titleHeader: String? = null,
    @SerializedName("titlePage")
    var titlePage: String? = null
) : Parcelable {

    @Parcelize
    data class Category(
        @SerializedName("description")
        var description: String? = null,
        @SerializedName("faqs")
        var faqs: List<Faq?>? = null,
        @SerializedName("id")
        var id: Int? = null,
        @SerializedName("name")
        var name: String? = null,
        @SerializedName("type")
        var type: String? = null
    ) : Parcelable {

        @Parcelize
        data class Faq(
            @SerializedName("answer")
            var answer: String? = null,
            @SerializedName("id")
            var id: Int? = null,
            @SerializedName("objectCode")
            var objectCode: String? = null,
            @SerializedName("question")
            var question: String? = null,
            @SerializedName("section")
            var section: String? = null,
            @SerializedName("sequence")
            var sequence: Int? = null,
            @SerializedName("type")
            var type: String? = null
        ) : Parcelable
    }


    @Parcelize
    data class ContentCategory(
        @SerializedName("category")
        var category: Category? = null,
        @SerializedName("description")
        var description: String? = null,
        @SerializedName("id")
        var id: Int? = null,
        @SerializedName("name")
        var name: String? = null,
        @SerializedName("priority")
        var priority: Int? = null
    ) : Parcelable {

        @Parcelize
        data class Category(
            @SerializedName("description")
            var description: String? = null,
            @SerializedName("faqs")
            var faqs: List<Faq>? = null,
            @SerializedName("id")
            var id: Int? = null,
            @SerializedName("name")
            var name: String? = null,
            @SerializedName("type")
            var type: String? = null
        ) : Parcelable {

            @Parcelize
            data class Faq(
                @SerializedName("answer")
                var answer: String? = null,
                @SerializedName("id")
                var id: Int? = null,
                @SerializedName("objectCode")
                var objectCode: String? = null,
                @SerializedName("question")
                var question: String? = null,
                @SerializedName("section")
                var section: String? = null,
                @SerializedName("sequence")
                var sequence: Int? = null,
                @SerializedName("type")
                var type: String? = null
            ) : Parcelable
        }
    }


    @Parcelize
    data class Detail(
        @SerializedName("detailContent")
        var detailContent: String? = null
    ) : Parcelable


    @Parcelize
    data class MetaRobot(
        @SerializedName("code")
        var code: String? = null,
        @SerializedName("description")
        var description: String? = null
    ) : Parcelable
}