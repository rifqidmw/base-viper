package com.base.viper.module.newcars.view.filter

import androidx.fragment.app.Fragment

interface NewCarFilterInterface {
    interface View {
        fun setupView()
    }

    interface Presenter {
        fun onCreate(fragment: Fragment)
        fun onViewCreated(fragment: Fragment)
        fun onDestroy()
        fun goToApplyFilter()
    }

    interface Interactor

    interface Router {
        fun goToApplyFilter()
    }
}