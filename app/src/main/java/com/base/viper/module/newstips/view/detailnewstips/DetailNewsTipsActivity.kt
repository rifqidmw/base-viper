package com.base.viper.module.newstips.view.detailnewstips

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.core.text.HtmlCompat
import androidx.recyclerview.widget.LinearLayoutManager
import com.base.viper.R
import com.base.viper.common.Constants
import com.base.viper.common.CustomPopUpDialog
import com.base.viper.common.CustomPopUpParam
import com.base.viper.common.HorizontalMarginItemDecoration
import com.base.viper.components.CustomToolbar
import com.base.viper.components.promosection.MapperToDataSubSection.contentToListNewsTips
import com.base.viper.components.promosection.MapperToDataSubSection.contentToListPromo
import com.base.viper.databinding.ActivityDetailNewsTipsBinding
import com.base.viper.module.newstips.adapter.RecyclerViewNewsTipsAdapter
import com.base.viper.module.newstips.entity.detail.NewsTipsDetail
import com.base.viper.module.newstips.entity.newstips.NewsTips
import com.base.viper.module.promo.entity.promo.Content
import com.base.viper.utils.Utils.dateTimeFormat
import com.base.viper.utils.Utils.dp
import com.base.viper.utils.Utils.gone
import com.base.viper.utils.Utils.setHtmlView
import com.base.viper.utils.Utils.setImageViewUrl
import com.base.viper.utils.Utils.setOnSingleClickListener
import com.base.viper.utils.Utils.visible

class DetailNewsTipsActivity : AppCompatActivity(), DetailNewsTipsInterface.View {
    private val presenter: DetailNewsTipsPresenter = DetailNewsTipsPresenter(this)
    private val binding: ActivityDetailNewsTipsBinding by lazy {
        ActivityDetailNewsTipsBinding.inflate(layoutInflater)
    }

    private val articleAdapter: RecyclerViewNewsTipsAdapter by lazy {
        RecyclerViewNewsTipsAdapter(RecyclerViewNewsTipsAdapter.ViewType.ARTICLE)
    }

    private val promoAdapter: RecyclerViewNewsTipsAdapter by lazy {
        RecyclerViewNewsTipsAdapter(RecyclerViewNewsTipsAdapter.ViewType.PROMO)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)
        presenter.onCreate(this)
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter.onDestroy()
    }

    override fun setupView() {
        setupToolbar()
        setupListener()
    }

    private fun setupToolbar() {
        binding.toolbar.setOnClickedListener(object : CustomToolbar.Listener {
            override fun onClickedBackButton() {
                onBackPressedDispatcher.onBackPressed()
            }
        })
    }

    private fun setupListener() = with(binding) {
        ibWhatsapp.setOnSingleClickListener {
            presenter.onSharePromo(Constants.ShareEvent.WHATSAPP)
        }

        ibFacebook.setOnSingleClickListener {
            presenter.onSharePromo(Constants.ShareEvent.FACEBOOK)
        }

        ibTwitter.setOnSingleClickListener {
            presenter.onSharePromo(Constants.ShareEvent.TWITTER)
        }

        ibTiktok.setOnSingleClickListener {
            presenter.onSharePromo(Constants.ShareEvent.TIKTOK)
        }

        ibClipboard.setOnSingleClickListener {
            presenter.onSharePromo(Constants.ShareEvent.LINK)
        }

        tvSubTitlePromo.setOnSingleClickListener {
            presenter.goToAllPromo()
        }

        tvSubTitleOtherArticle.setOnSingleClickListener {
            presenter.goToAllNews()
        }

        ivShare.setOnSingleClickListener {
            presenter.onSharePromo(Constants.ShareEvent.LINK)
        }

        btnInquiry.setOnSingleClickListener {
            presenter.goToInquiry()
        }
    }

    override fun setDataNewsAndTips(data: NewsTipsDetail) = with(binding) {
        ivNewsAndTips.setImageViewUrl(data.heroImageLink)
        tvTitleArticle.text = data.titleHeader?.let { HtmlCompat.fromHtml(it, HtmlCompat.FROM_HTML_MODE_LEGACY) }
        tvDesc.setHtmlView(data.detail?.detailContent)
        tvDate.text = data.publishedDate?.dateTimeFormat()
    }

    override fun setupSubPromo(data: List<Content>?) {
        if (data.isNullOrEmpty()) binding.clPromo.gone()
        else {
            binding.rvPromo.apply {
                visible()
                adapter = promoAdapter.apply {
                    setListPromo(data)
                    setOnItemClickListener(object : RecyclerViewNewsTipsAdapter.OnItemClickListener{
                        override fun <T> onClickedListener(item: T) {
                            if (item is Content) presenter.goToDetailPromo(item.slug)
                        }

                    })
                }
                layoutManager = LinearLayoutManager(this@DetailNewsTipsActivity, LinearLayoutManager.HORIZONTAL, false)
                addItemDecoration(
                    HorizontalMarginItemDecoration(
                        horizontalMargin = 16.dp,
                        lastItemEndMargin = 16.dp
                    )
                )
            }
        }
    }

    override fun setupSubNewsTips(data: List<NewsTips.Content>) {
        if (data.isEmpty()) binding.clOtherArticle.gone()
        else {
            binding.rvOtherArticle.apply {
                visible()
                adapter = articleAdapter.apply {
                    setListArticle(data)
                    setOnItemClickListener(object : RecyclerViewNewsTipsAdapter.OnItemClickListener{
                        override fun <T> onClickedListener(item: T) {
                            if (item is NewsTips.Content) presenter.goToDetailNewsTips(item.slug)
                        }

                    })
                }
                layoutManager = LinearLayoutManager(this@DetailNewsTipsActivity, LinearLayoutManager.HORIZONTAL, false)
                addItemDecoration(
                    HorizontalMarginItemDecoration(
                        horizontalMargin = 16.dp,
                        lastItemEndMargin = 16.dp
                    )
                )
            }
        }
    }

    override fun fragmentPromoResultListener(data: List<Content>?) {
        val listData = data?.contentToListPromo(this@DetailNewsTipsActivity)
        supportFragmentManager.setFragmentResultListener(
            "SUCCESS_INQUIRY",
            this@DetailNewsTipsActivity
        ) { _, bundle ->
            val status = bundle.getString("status")
            if (status == "success" && !listData.isNullOrEmpty()) {
                val param = CustomPopUpParam.SuccessMessageParam(
                    title = getString(R.string.title_inquiry_success),
                    description = getString(R.string.desc_inquiry_success),
                    actionButton = getString(R.string.title_label_inquiry_go_to_home).uppercase(),
                    cancelable = false,
                    sectionTitle = getString(R.string.title_label_inquiry_other_promo).uppercase(),
                    sectionSubTitle = getString(R.string.title_label_inquiry_see_all).uppercase(),
                    sectionData = listData,
                    onClickedActionButton = {
                        presenter.goToHome()
                    },
                    onClickedItemSection = {
                        presenter.goToDetailPromo(data[it].slug)
                    },
                    onClickedSectionSubTitle = {

                    },
                    onDismissListener = null
                )
                CustomPopUpDialog(this@DetailNewsTipsActivity).popUpSuccessMessage(param)
            }
        }
    }

    override fun fragmentNewsTipsResultListener(data: List<NewsTips.Content>?) {
        val listData = data?.contentToListNewsTips(this@DetailNewsTipsActivity)
        supportFragmentManager.setFragmentResultListener(
            "SUCCESS_INQUIRY",
            this@DetailNewsTipsActivity
        ) { _, bundle ->
            val status = bundle.getString("status")
            if (status == "success" && !listData.isNullOrEmpty()) {
                val param = CustomPopUpParam.SuccessMessageParam(
                    title = getString(R.string.title_inquiry_success),
                    description = getString(R.string.desc_inquiry_success),
                    actionButton = getString(R.string.title_label_inquiry_go_to_home).uppercase(),
                    cancelable = false,
                    sectionTitle = getString(R.string.title_label_inquiry_other_article).uppercase(),
                    sectionSubTitle = getString(R.string.title_label_inquiry_see_all).uppercase(),
                    sectionData = listData,
                    onClickedActionButton = {
                        presenter.goToHome()
                    },
                    onClickedItemSection = {
                        presenter.goToDetailNewsTips(data[it].slug)
                    },
                    onClickedSectionSubTitle = {

                    },
                    onDismissListener = null
                )
                CustomPopUpDialog(this@DetailNewsTipsActivity).popUpSuccessMessage(param)
            }
        }
    }
}