package com.base.viper.module.promo.adapter

import android.annotation.SuppressLint
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.Lifecycle
import androidx.viewpager2.adapter.FragmentStateAdapter
import androidx.viewpager2.adapter.FragmentViewHolder
import com.base.viper.module.promo.entity.promo.Content
import com.base.viper.module.promo.entity.promo.PromoCategory
import com.base.viper.module.promo.view.category.PromoCategoryFragment

class ViewPagerAdapter(
    private val listCategory: List<PromoCategory>,
    fragmentManager: FragmentManager,
    lifecycle: Lifecycle,
) : FragmentStateAdapter(fragmentManager, lifecycle) {

    private val listAllPromo: MutableList<Content?> = mutableListOf()

    @SuppressLint("NotifyDataSetChanged")
    fun setDataListPromo(data: List<Content?>) {
        listAllPromo.clear()
        listAllPromo.addAll(data)
        notifyItemRangeChanged(0, listCategory.size, "PAYLOAD")
    }

    override fun onBindViewHolder(
        holder: FragmentViewHolder,
        position: Int,
        payloads: MutableList<Any>
    ) {
        super.onBindViewHolder(holder, position, payloads)
    }

    override fun getItemCount() = listCategory.size

    override fun createFragment(position: Int) =
        PromoCategoryFragment(listCategory[position].name ?: "")
}