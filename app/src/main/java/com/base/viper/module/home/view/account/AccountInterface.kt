package com.base.viper.module.home.view.account

import androidx.fragment.app.Fragment

interface AccountInterface {
    interface View

    interface Presenter {
        fun onCreate(fragment: Fragment)
        fun onViewCreated(fragment: Fragment)
        fun onDestroy()
    }

    interface Interactor

    interface Router
}