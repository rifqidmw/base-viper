package com.base.viper.module.productknowledge.view.detailproductknowledge

import android.app.Activity
import com.base.viper.common.Constants
import com.base.viper.module.productknowledge.entity.ProductAdvantageSample
import com.base.viper.module.productknowledge.entity.ProductFaqSample
import com.base.viper.module.productknowledge.entity.ProductKnowledgeDetail
import com.base.viper.module.productknowledge.entity.ProductOfferSample
import com.base.viper.module.productknowledge.entity.ProductPromoSample
import com.base.viper.module.productknowledge.entity.ProductSpecificationSample
import com.base.viper.module.promo.entity.promo.Content
import com.base.viper.module.promo.entity.promo.Promo

interface DetailProductKnowledgeInterface {
    interface View {
        fun setupView()

        fun setDataProductKnowledge(data: ProductKnowledgeDetail)
        fun setDataProductAdvantage(data: List<ProductAdvantageSample>)
        fun setDataProductOffer(data: List<ProductOfferSample>)
        fun setDataProductSpecification(data: List<ProductSpecificationSample>)
        fun setDataProductFaq(data: List<ProductKnowledgeDetail.ContentCategory.Category.Faq>?)

        fun setupSubPromo(data: List<Content>)
        fun fragmentPromoResultListener(data: List<Content>?)
    }
    
    interface Presenter {
        fun onCreate(activity: Activity)
        fun onDestroy()

        fun onSectionToolbar(event: Constants.ToolbarEvent)

        fun goToHome()
        fun goToAllPromo()
        fun goToDetailPromo(slug: String?)
        fun shareWithLink()
    }
    
    interface Interactor {
        fun getDetailProductKnowledge(
            slug: String?,
            onSuccess: (ProductKnowledgeDetail) -> Unit,
            onError: (Throwable) -> Unit
        )

        fun getSubPromo(size: Int, onSuccess: (Promo) -> Unit, onError: (Throwable) -> Unit)

        fun getDataProductAdvantage(
            onSuccess: (List<ProductAdvantageSample>) -> Unit,
            onError: (Throwable) -> Unit
        )
        fun getDataProductOffer(
            onSuccess: (List<ProductOfferSample>) -> Unit,
            onError: (Throwable) -> Unit
        )
        fun getDataProductSpecification(
            onSuccess: (List<ProductSpecificationSample>) -> Unit,
            onError: (Throwable) -> Unit
        )
        fun getDataProductPromo(
            onSuccess: (List<ProductPromoSample>) -> Unit,
            onError: (Throwable) -> Unit
        )
        fun getDataProductFaq(
            onSuccess: (List<ProductFaqSample>) -> Unit,
            onError: (Throwable) -> Unit
        )
    }
    
    interface Router {
        fun goToSearch()
        fun goToNotification()
        fun goToCart()
        fun shareWithLink()
        fun goToHome()
        fun goToAllPromo()
        fun goToDetailPromo(slug: String?)
    }
}