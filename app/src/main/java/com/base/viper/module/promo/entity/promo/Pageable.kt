package com.base.viper.module.promo.entity.promo


import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize


@Parcelize
data class Pageable(
    @SerializedName("sort")
    val sort: Sort? = null,
    @SerializedName("offset")
    val offset: Int? = null,
    @SerializedName("pageNumber")
    val pageNumber: Int? = null,
    @SerializedName("pageSize")
    val pageSize: Int? = null,
    @SerializedName("unpaged")
    val unpaged: Boolean? = false,
    @SerializedName("paged")
    val paged: Boolean? = false
) : Parcelable