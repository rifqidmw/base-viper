package com.base.viper.module.addcar.findcar

import androidx.fragment.app.Fragment

class FindCarPresenter(
    private var view: FindCarInterface.View? = null
) : FindCarInterface.Presenter {

    private var interactor: FindCarInterface.Interactor? = null
    private var router: FindCarInterface.Router? = null

    override fun onCreate(fragment: Fragment) {
        view = fragment as FindCarFragment
        interactor = FindCarInteractor(fragment)
        router = FindCarRouter(fragment)
    }
    
    override fun onViewCreated(fragment: Fragment) {
        view?.setupView()
    }

    override fun onDestroy() {
        view = null
        router = null
    }

    override fun goToRegisterCar() {
        router?.goToRegisterCar()
    }

    override fun goToNotExistingCustomer() {
        router?.goToNotExistingCustomer()
    }
}