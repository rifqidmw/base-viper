package com.base.viper.module.home.view.menu

import androidx.fragment.app.Fragment

interface MenuInterface {
    interface View

    interface Presenter {
        fun onCreate(fragment: Fragment)
        fun onViewCreated(fragment: Fragment)
        fun onDestroy()
    }

    interface Interactor

    interface Router
}