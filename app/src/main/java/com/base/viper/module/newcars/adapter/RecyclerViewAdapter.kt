package com.base.viper.module.newcars.adapter

import android.annotation.SuppressLint
import android.graphics.Paint
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.base.viper.R
import com.base.viper.common.BaseViewHolder
import com.base.viper.databinding.ItemHatchbackCarBinding
import com.base.viper.databinding.ItemHybridCarBinding
import com.base.viper.databinding.ItemMpvCarBinding
import com.base.viper.databinding.ItemNewCarBinding
import com.base.viper.databinding.ItemNewCarFilterBinding
import com.base.viper.databinding.ItemNewCarSortBinding
import com.base.viper.databinding.ItemSedanCarBinding
import com.base.viper.databinding.ItemSportsCarBinding
import com.base.viper.databinding.ItemSuvCarBinding
import com.base.viper.module.newcars.adapter.RecyclerViewAdapter.ViewType.ALL
import com.base.viper.module.newcars.adapter.RecyclerViewAdapter.ViewType.FILTER
import com.base.viper.module.newcars.adapter.RecyclerViewAdapter.ViewType.HATCHBACK
import com.base.viper.module.newcars.adapter.RecyclerViewAdapter.ViewType.HYBRID
import com.base.viper.module.newcars.adapter.RecyclerViewAdapter.ViewType.MPV
import com.base.viper.module.newcars.adapter.RecyclerViewAdapter.ViewType.SEDAN
import com.base.viper.module.newcars.adapter.RecyclerViewAdapter.ViewType.SORT
import com.base.viper.module.newcars.adapter.RecyclerViewAdapter.ViewType.SPORTS
import com.base.viper.module.newcars.adapter.RecyclerViewAdapter.ViewType.SUV
import com.base.viper.module.newcars.entity.NewCarSample
import com.base.viper.utils.Utils.formatRupiah
import com.base.viper.utils.Utils.setOnSingleClickListener
import com.base.viper.utils.Utils.visible

class RecyclerViewAdapter(
    private val type: ViewType,
    private val option: List<String>,
    private val onItemClick: (String) -> Unit = {}
) : RecyclerView.Adapter<BaseViewHolder>() {

    enum class ViewType {
        ALL, HYBRID, MPV, SUV, HATCHBACK, SEDAN, SPORTS, FILTER, SORT
    }

    private val listAllCar: MutableList<NewCarSample> = mutableListOf()
    private val listHybridCar: MutableList<NewCarSample> = mutableListOf()
    private val listMpvCar: MutableList<NewCarSample> = mutableListOf()
    private val listSuvCar: MutableList<NewCarSample> = mutableListOf()
    private val listHatchbackCar: MutableList<NewCarSample> = mutableListOf()
    private val listSedanCar: MutableList<NewCarSample> = mutableListOf()
    private val listSportsCar: MutableList<NewCarSample> = mutableListOf()

    private var selectedSortingOption: String? = null

    @SuppressLint("NotifyDataChanged")
    fun setListAllCar(data: List<NewCarSample>) {
        listAllCar.clear()
        listAllCar.addAll(data)
        notifyDataSetChanged()
    }

    @SuppressLint("NotifyDataChanged")
    fun setListHybridCar(data: List<NewCarSample>) {
        listHybridCar.clear()
        listHybridCar.addAll(data)
        notifyDataSetChanged()
    }

    @SuppressLint("NotifyDataChanged")
    fun setListMpvCar(data: List<NewCarSample>) {
        listMpvCar.clear()
        listMpvCar.addAll(data)
        notifyDataSetChanged()
    }

    @SuppressLint("NotifyDataChanged")
    fun setListSuvCar(data: List<NewCarSample>) {
        listSuvCar.clear()
        listSuvCar.addAll(data)
        notifyDataSetChanged()
    }

    @SuppressLint("NotifyDataChanged")
    fun setListHatchbackCar(data: List<NewCarSample>) {
        listHatchbackCar.clear()
        listHatchbackCar.addAll(data)
        notifyDataSetChanged()
    }

    @SuppressLint("NotifyDataChanged")
    fun setListSedanCar(data: List<NewCarSample>) {
        listSedanCar.clear()
        listSedanCar.addAll(data)
        notifyDataSetChanged()
    }

    @SuppressLint("NotifyDataChanged")
    fun setListSportsCar(data: List<NewCarSample>) {
        listSportsCar.clear()
        listSportsCar.addAll(data)
        notifyDataSetChanged()
    }

    private var onItemClickListener: OnItemClickListener? = null

    interface OnItemClickListener {
        fun <T> onClickedListener(item: T)
    }

    fun setOnItemClickListener(onClickListener: OnItemClickListener) {
        this.onItemClickListener = onClickListener
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder {
        return when (viewType) {
            ALL.ordinal -> NewCarViewHolder(
                ItemNewCarBinding.inflate(
                    LayoutInflater.from(parent.context), parent, false
                )
            )
            HYBRID.ordinal -> HybridCarHolder(
                ItemHybridCarBinding.inflate(
                    LayoutInflater.from(parent.context), parent, false
                )
            )
            MPV.ordinal -> MpvCarHoler(
                ItemMpvCarBinding.inflate(
                    LayoutInflater.from(parent.context), parent, false
                )
            )
            SUV.ordinal -> SuvCarHoler(
                ItemSuvCarBinding.inflate(
                    LayoutInflater.from(parent.context), parent, false
                )
            )
            HATCHBACK.ordinal -> HatchbackCarHoler(
                ItemHatchbackCarBinding.inflate(
                    LayoutInflater.from(parent.context), parent, false
                )
            )
            SEDAN.ordinal -> SedanCarHoler(
                ItemSedanCarBinding.inflate(
                    LayoutInflater.from(parent.context), parent, false
                )
            )
            SPORTS.ordinal -> SportsCarHoler(
                ItemSportsCarBinding.inflate(
                    LayoutInflater.from(parent.context), parent, false
                )
            )
            FILTER.ordinal -> FilterNewCarViewHolder(
                ItemNewCarFilterBinding.inflate(
                    LayoutInflater.from(parent.context), parent, false
                )
            )
            SORT.ordinal -> SortNewCarViewHolder(
                ItemNewCarSortBinding.inflate(
                    LayoutInflater.from(parent.context), parent, false
                )
            )
            else -> throw IllegalArgumentException("Invalid viewType $type")
        }
    }

    override fun getItemCount() = when (type) {
        ALL -> listAllCar.size
        HYBRID -> listHybridCar.size
        MPV -> listMpvCar.size
        SUV -> listSuvCar.size
        HATCHBACK -> listHatchbackCar.size
        SEDAN -> listSedanCar.size
        SPORTS -> listSportsCar.size
        FILTER -> option.size
        SORT -> option.size
    }

    override fun getItemViewType(position: Int) = type.ordinal

    override fun onBindViewHolder(holder: BaseViewHolder, position: Int) {
        when (holder) {
            is NewCarViewHolder -> holder.bind(listAllCar[position])
            is HybridCarHolder -> holder.bind(listHybridCar[position])
            is MpvCarHoler -> holder.bind(listMpvCar[position])
            is SuvCarHoler -> holder.bind(listSuvCar[position])
            is HatchbackCarHoler -> holder.bind(listHatchbackCar[position])
            is SedanCarHoler -> holder.bind(listSedanCar[position])
            is SportsCarHoler -> holder.bind(listSportsCar[position])
            is FilterNewCarViewHolder -> holder.bind(option[position], onItemClick)
            is SortNewCarViewHolder -> holder.bind(option[position], onItemClick)
        }
    }

    inner class NewCarViewHolder(
        private val binding: ItemNewCarBinding
    ) : BaseViewHolder(binding) {
        fun bind(data: NewCarSample) = with(binding) {
            if ((absoluteAdapterPosition + 1) % 2 == 0) vDividerLeft.visible()
            else vDividerRight.visible()
            ivNewCar.setImageDrawable(
                ContextCompat.getDrawable(root.context, data.image)
            )
            tvLabel.visible(data.isBestSeller)
            tvStarting.text = tvStarting.context.getString(R.string.title_starting_price)
            tvPrice.text = "Rp. ${data.price.toString().formatRupiah()}"
            tvPriceBefore.text = "Rp. ${data.priceBefore.toString().formatRupiah()}"
            tvPriceBefore.paintFlags = tvPriceBefore.paintFlags or Paint.STRIKE_THRU_TEXT_FLAG
            root.setOnSingleClickListener {
                onItemClickListener?.onClickedListener(data)
            }
        }
    }
    inner class HybridCarHolder(
        private val binding: ItemHybridCarBinding
    ) : BaseViewHolder(binding) {
        fun bind(data: NewCarSample) = with(binding) {
            if ((absoluteAdapterPosition + 1) % 2 == 0) vDividerLeft.visible()
            else vDividerRight.visible()
            ivNewCar.setImageDrawable(
                ContextCompat.getDrawable(root.context, data.image)
            )
            tvLabel.visible(data.isBestSeller)
            tvNewCarTitle.text = data.carName
            tvStarting.text = tvStarting.context.getString(R.string.title_starting_price)
            tvPrice.text = "Rp. ${data.price.toString().formatRupiah()}"
            tvPriceBefore.text = "Rp. ${data.priceBefore.toString().formatRupiah()}"
            tvPriceBefore.paintFlags = tvPriceBefore.paintFlags or Paint.STRIKE_THRU_TEXT_FLAG
            root.setOnSingleClickListener {
                onItemClickListener?.onClickedListener(data)
            }
        }
    }

    inner class MpvCarHoler(
        private val binding: ItemMpvCarBinding
    ) : BaseViewHolder(binding) {
        fun bind(data: NewCarSample) = with(binding) {
            if ((absoluteAdapterPosition + 1) % 2 == 0) vDividerLeft.visible()
            else vDividerRight.visible()
            ivNewCar.setImageDrawable(
                ContextCompat.getDrawable(root.context, data.image)
            )
            tvLabel.visible(data.isBestSeller)
            tvNewCarTitle.text = data.carName
            tvStarting.text = tvStarting.context.getString(R.string.title_starting_price)
            tvPrice.text = "Rp. ${data.price.toString().formatRupiah()}"
            tvPriceBefore.text = "Rp. ${data.priceBefore.toString().formatRupiah()}"
            tvPriceBefore.paintFlags = tvPriceBefore.paintFlags or Paint.STRIKE_THRU_TEXT_FLAG
            root.setOnSingleClickListener {
                onItemClickListener?.onClickedListener(data)
            }
        }
    }

    inner class SuvCarHoler(
        private val binding: ItemSuvCarBinding
    ) : BaseViewHolder(binding) {
        fun bind(data: NewCarSample) = with(binding) {
            if ((absoluteAdapterPosition + 1) % 2 == 0) vDividerLeft.visible()
            else vDividerRight.visible()
            ivNewCar.setImageDrawable(
                ContextCompat.getDrawable(root.context, data.image)
            )
            tvLabel.visible(data.isBestSeller)
            tvNewCarTitle.text = data.carName
            tvStarting.text = tvStarting.context.getString(R.string.title_starting_price)
            tvPrice.text = "Rp. ${data.price.toString().formatRupiah()}"
            tvPriceBefore.text = "Rp. ${data.priceBefore.toString().formatRupiah()}"
            tvPriceBefore.paintFlags = tvPriceBefore.paintFlags or Paint.STRIKE_THRU_TEXT_FLAG
            root.setOnSingleClickListener {
                onItemClickListener?.onClickedListener(data)
            }
        }
    }

    inner class HatchbackCarHoler(
        private val binding: ItemHatchbackCarBinding
    ) : BaseViewHolder(binding) {
        fun bind(data: NewCarSample) = with(binding) {
            if ((absoluteAdapterPosition + 1) % 2 == 0) vDividerLeft.visible()
            else vDividerRight.visible()
            ivNewCar.setImageDrawable(
                ContextCompat.getDrawable(root.context, data.image)
            )
            tvLabel.visible(data.isBestSeller)
            tvNewCarTitle.text = data.carName
            tvStarting.text = tvStarting.context.getString(R.string.title_starting_price)
            tvPrice.text = "Rp. ${data.price.toString().formatRupiah()}"
            tvPriceBefore.text = "Rp. ${data.priceBefore.toString().formatRupiah()}"
            tvPriceBefore.paintFlags = tvPriceBefore.paintFlags or Paint.STRIKE_THRU_TEXT_FLAG
            root.setOnSingleClickListener {
                onItemClickListener?.onClickedListener(data)
            }
        }
    }

    inner class SedanCarHoler(
        private val binding: ItemSedanCarBinding
    ) : BaseViewHolder(binding) {
        fun bind(data: NewCarSample) = with(binding) {
            if ((absoluteAdapterPosition + 1) % 2 == 0) vDividerLeft.visible()
            else vDividerRight.visible()
            ivNewCar.setImageDrawable(
                ContextCompat.getDrawable(root.context, data.image)
            )
            tvLabel.visible(data.isBestSeller)
            tvNewCarTitle.text = data.carName
            tvStarting.text = tvStarting.context.getString(R.string.title_starting_price)
            tvPrice.text = "Rp. ${data.price.toString().formatRupiah()}"
            tvPriceBefore.text = "Rp. ${data.priceBefore.toString().formatRupiah()}"
            tvPriceBefore.paintFlags = tvPriceBefore.paintFlags or Paint.STRIKE_THRU_TEXT_FLAG
            root.setOnSingleClickListener {
                onItemClickListener?.onClickedListener(data)
            }
        }
    }
    inner class SportsCarHoler(
        private val binding: ItemSportsCarBinding
    ) : BaseViewHolder(binding) {
        fun bind(data: NewCarSample) = with(binding) {
            if ((absoluteAdapterPosition + 1) % 2 == 0) vDividerLeft.visible()
            else vDividerRight.visible()
            ivNewCar.setImageDrawable(
                ContextCompat.getDrawable(root.context, data.image)
            )
            tvLabel.visible(data.isBestSeller)
            tvNewCarTitle.text = data.carName
            tvStarting.text = tvStarting.context.getString(R.string.title_starting_price)
            tvPrice.text = "Rp. ${data.price.toString().formatRupiah()}"
            tvPriceBefore.text = "Rp. ${data.priceBefore.toString().formatRupiah()}"
            tvPriceBefore.paintFlags = tvPriceBefore.paintFlags or Paint.STRIKE_THRU_TEXT_FLAG
            root.setOnSingleClickListener {
                onItemClickListener?.onClickedListener(data)
            }
        }
    }

    inner class FilterNewCarViewHolder(
        private val binding: ItemNewCarFilterBinding
    ) : BaseViewHolder(binding) {
        fun bind(item: String, onItemClickListener: (String) -> Unit) {
            with(binding) {
                tvTitle.text = item
                root.setOnClickListener {
                    onItemClickListener.invoke(item)
                }
            }
        }
    }

    inner class SortNewCarViewHolder(
        private val binding: ItemNewCarSortBinding
    ) : BaseViewHolder(binding) {
        fun bind(item: String, onItemClickListener: (String) -> Unit) {
            with(binding) {
                tvTitle.text = item
                val isSelected = item == selectedSortingOption
                rbSort.isChecked = isSelected
                root.isSelected = isSelected

                root.setOnClickListener {
                    selectedSortingOption = item
                    notifyDataSetChanged()
                    onItemClickListener.invoke(item)
                }
            }
        }
    }
}