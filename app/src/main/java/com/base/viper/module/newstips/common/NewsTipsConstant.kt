package com.base.viper.module.newstips.common

object NewsTipsConstant {

    const val DATA_NEWS_TIPS = "dataNewsTips"
    const val ORIGIN = "origin"
    const val SLUG = "slug"

    enum class ContentType {
        NEWS, TIPS_TRICKS, PRODUCT_REVIEWS, EVENTS
    }

    enum class AreaType {
        PROVINCE, CITY, BRANCH
    }

    enum class OriginEvent {
        HOME,
        NEWSTIPS
    }
}