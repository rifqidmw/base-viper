package com.base.viper.module.promo.view.detailpromo.bundling

import androidx.fragment.app.Fragment
import com.base.viper.module.promo.entity.bundle.BundleSample
import com.base.viper.utils.Utils.parcelable

class BundlingPresenter(
    private var view: BundlingInterface.View? = null
) : BundlingInterface.Presenter {

    private var interactor: BundlingInterface.Interactor? = null
    private var router: BundlingInterface.Router? = null

    private var bundleSample: BundleSample? = null

    override fun onCreate(fragment: Fragment) {
        view = fragment as BundlingFragment
        interactor = BundlingInteractor(fragment)
        router = BundlingRouter(fragment)
    }

    override fun onViewCreated(fragment: Fragment) {
        getDataBundle(fragment)

        view?.setDataBundling(bundleSample)
    }

    override fun onDestroy() {
        view = null
        router = null
    }

    private fun getDataBundle(fragment: Fragment) = with(fragment.arguments) {
        bundleSample = this?.parcelable("DATA_BUNDLE")
    }
}