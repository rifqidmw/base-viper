package com.base.viper.module.promo.entity.promo


import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize


@Parcelize
data class Faqs(
    @SerializedName("createdDate")
    val createdDate: String? = null,
    @SerializedName("createdBy")
    val createdBy: String? = null,
    @SerializedName("modifiedDate")
    val modifiedDate: String? = null,
    @SerializedName("modifiedBy")
    val modifiedBy: String? = null,
    @SerializedName("modifiedReason")
    val modifiedReason: String? = null,
    @SerializedName("isDeleted")
    val isDeleted: Boolean? = null,
    @SerializedName("deletedDate")
    val deletedDate: String? = null,
    @SerializedName("id")
    val id: Int? = null,
    @SerializedName("sequence")
    val sequence: Int? = null,
    @SerializedName("question")
    val question: String? = null,
    @SerializedName("answer")
    val answer: String? = null
) : Parcelable