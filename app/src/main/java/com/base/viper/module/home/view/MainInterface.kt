package com.base.viper.module.home.view

import android.app.Activity
import androidx.annotation.ColorRes
import androidx.fragment.app.Fragment
import com.base.viper.common.Constants
import com.base.viper.module.home.common.HomeConstant

interface MainInterface {

    interface View {
        fun replaceFragment(fragment: Fragment, tag: HomeConstant.FragmentTag)
        fun setupView(tag: HomeConstant.FragmentTag)
        fun setupToolbarTheme(@ColorRes color: Int)
    }

    interface Presenter {
        fun onDestroy()
        fun onCreate(activity: Activity)
        fun onSectionToolbar(event: Constants.ToolbarEvent)
    }

    interface Router {
        fun goToSearch()
        fun goToNotification()
        fun goToCart()
    }

    interface Interactor
}