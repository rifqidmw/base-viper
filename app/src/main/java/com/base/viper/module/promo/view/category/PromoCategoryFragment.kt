package com.base.viper.module.promo.view.category

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.paging.PagingData
import androidx.recyclerview.widget.GridLayoutManager
import com.base.viper.common.GridItemSpaceDecoration
import com.base.viper.databinding.FragmentPromoCategoryBinding
import com.base.viper.module.newstips.adapter.FooterLoadingAdapter
import com.base.viper.module.promo.adapter.PromoPagedAdapter
import com.base.viper.module.promo.entity.promo.Content
import com.base.viper.utils.Utils.dp

class PromoCategoryFragment(
    private val contentCategory: String
) : Fragment(), PromoCategoryInterface.View, PromoPagedAdapter.OnItemClickListener {

    private val presenter: PromoCategoryPresenter = PromoCategoryPresenter(this, contentCategory)
    private val binding: FragmentPromoCategoryBinding by lazy {
        FragmentPromoCategoryBinding.inflate(layoutInflater)
    }
    private val promoAdapter: PromoPagedAdapter by lazy {
        PromoPagedAdapter()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        presenter.onCreate(this)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        presenter.onViewCreated(this)
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter.onDestroy()
    }

    override fun setupView() {
        binding.rvPromoCategory.apply {
            layoutManager = GridLayoutManager(
                this@PromoCategoryFragment.requireContext(),
                2,
                GridLayoutManager.VERTICAL,
                false
            )
            adapter = promoAdapter.apply {
                setOnItemClickListener(this@PromoCategoryFragment)
                withLoadStateFooter(
                    footer = FooterLoadingAdapter { promoAdapter.retry() }
                )
            }
            addItemDecoration(GridItemSpaceDecoration(2, 16.dp, true))
        }

    }

    override fun setDataPromoPaged(data: PagingData<Content>) {
        promoAdapter.submitData(lifecycle, data)
    }

    override fun <T> onClickedListener(item: T) {
        when (item) {
            is Content -> presenter.goToDetailPromo(item)
        }
    }
}