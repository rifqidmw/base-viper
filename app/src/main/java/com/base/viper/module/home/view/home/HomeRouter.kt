package com.base.viper.module.home.view.home

import android.content.Intent
import android.os.Bundle
import com.base.viper.module.addcar.AddCarActivity
import com.base.viper.module.home.entity.MyCarsSample
import com.base.viper.module.home.view.home.servicebooking.ServiceBookingActivity
import com.base.viper.module.mycardetails.view.MyCarDetailFragment
import com.base.viper.module.newcars.view.NewCarActivity
import com.base.viper.module.newstips.common.NewsTipsConstant
import com.base.viper.module.newstips.entity.newstips.NewsTips
import com.base.viper.module.newstips.view.NewsTipsActivity
import com.base.viper.module.newstips.view.detailnewstips.DetailNewsTipsActivity
import com.base.viper.module.productknowledge.view.ProductKnowledgeActivity
import com.base.viper.module.promo.entity.promo.Content
import com.base.viper.module.promo.view.PromoActivity
import com.base.viper.module.promo.view.detailpromo.DetailPromoActivity
import com.base.viper.utils.Utils.toastShort

class HomeRouter(private val fragment: HomeFragment) : HomeInterface.Router {

    override fun goToServiceDue() {
        fragment.requireActivity().startActivity(Intent(fragment.requireContext(), ServiceBookingActivity::class.java))
    }

    override fun goToDetailMyCar(item: MyCarsSample) {
        val bottomSheet = MyCarDetailFragment()
        val bundle = Bundle().apply {
            putParcelable("DATA_MY_CAR", item)
        }
        bottomSheet.arguments = bundle
        bottomSheet.show(fragment.parentFragmentManager, "DETAIL_MY_CAR")
    }

    override fun goToServiceCoupon() {
        fragment.requireActivity().toastShort("Todo: Service Coupon")
    }

    override fun goToServicePackage() {
        fragment.requireActivity().toastShort("Todo: Service Package")
    }

    override fun goToTradeIn() {
        fragment.requireActivity().toastShort("Todo: Trade In")
    }

    override fun goToAccessories() {
        fragment.requireActivity().toastShort("Todo: Accessories")
    }

    override fun goToTrackingOrders() {
        fragment.requireActivity().toastShort("Todo: Tracking Orders")
    }

    override fun goToRoadSideAssistance() {
        fragment.requireActivity().toastShort("Todo: Road Side Assistance")
    }

    override fun goToCompareCar() {
        fragment.requireActivity().toastShort("Todo: Compare Car")
    }

    override fun goToTestDrive() {
        fragment.requireActivity().toastShort("Todo: Test Drive")
    }

    override fun goToInquireCar() {
        fragment.requireActivity().startActivity(
            Intent(fragment.context, NewCarActivity::class.java)
        )
    }

    override fun goToExploreCar() {
        fragment.requireActivity().startActivity(
            Intent(fragment.context, ProductKnowledgeActivity::class.java)
        )
    }

    override fun goToAllPromo() {
        fragment.requireActivity().startActivity(
            Intent(fragment.context, PromoActivity::class.java)
        )
    }

    override fun goToDetailPromo(item: Content) {
        fragment.requireActivity().startActivity(
            Intent(fragment.context, DetailPromoActivity::class.java).apply {
                putExtra("PROMO_SLUG", item.slug)
            }
        )
    }

    override fun goToAllBranchLocation() {
        fragment.requireActivity().toastShort("Todo: All Branch Location")
    }

    override fun goToDetailBranchLocation() {
        fragment.requireActivity().toastShort("Todo: Detail Branch Location")
    }

    override fun goToDetailNewsAndTips(item: NewsTips.Content) {
        fragment.requireActivity().startActivity(
            Intent(
                fragment.requireContext(),
                DetailNewsTipsActivity::class.java
            ).apply {
                putExtra(NewsTipsConstant.SLUG, item.slug)
                putExtra(NewsTipsConstant.ORIGIN, NewsTipsConstant.OriginEvent.HOME.name)
            })
    }

    override fun goToAllNewsAndTips() {
        fragment.requireActivity()
            .startActivity(Intent(fragment.requireContext(), NewsTipsActivity::class.java))
    }

    override fun goToAddCar() {
        fragment.requireActivity().startActivity(Intent(fragment.requireContext(), AddCarActivity::class.java))
    }
}