package com.base.viper.module.home.view.home

import com.base.viper.R
import com.base.viper.common.Constants
import com.base.viper.module.home.entity.BranchLocationSample
import com.base.viper.module.home.entity.Menu
import com.base.viper.module.home.entity.MyCarsSample
import com.base.viper.module.home.entity.PreferredMenuSample
import com.base.viper.module.home.entity.ServiceDueSample
import com.base.viper.module.home.entity.TrendingCarSample
import com.base.viper.module.newstips.entity.newstips.NewsTips
import com.base.viper.module.promo.entity.promo.Promo
import com.base.viper.services.NetworkBuilder
import com.base.viper.services.Sessions
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.disposables.CompositeDisposable
import io.reactivex.rxjava3.schedulers.Schedulers
import java.util.concurrent.TimeUnit

class HomeInteractor(private val fragment: HomeFragment) : HomeInterface.Interactor {

    private val disposable = CompositeDisposable()
    private val apiService = NetworkBuilder.apiService
    private val sessions = fragment.context?.let { Sessions(it) }

    override fun getDataServiceDue(
        onSuccess: (List<ServiceDueSample>) -> Unit,
        onError: (Throwable) -> Unit
    ) {
        val dataDummy = listOf(
            ServiceDueSample(
                "25 Aug 2023",
                "INNOVA ZENIX HYBRID",
                "Promo impian Innova",
                true,
            ),
            ServiceDueSample(
                "01 Dec 2023",
                "RAIZE",
                "Promo impian Innova",
                false,
            ),
            ServiceDueSample(
                "01 Dec 2023",
                "RAIZE",
                "Promo impian Innova",
                true,
            ),
        )

        Observable.just(dataDummy)
            .delay(3, TimeUnit.SECONDS)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(onSuccess, onError)
            .let(disposable::add)
    }

    override fun getDataMyCars(
        onSuccess: (List<MyCarsSample>) -> Unit,
        onError: (Throwable) -> Unit
    ) {
        val dataDummy = listOf(
            MyCarsSample(
                carName = "NEW FORTUNER GR SPORT",
                serviceDue = "18 Sept 2023",
                policeNumber = "B 2178 UIM",
                image = R.drawable.sample_my_car_4,
                vinNumber = "KBJAAA3GS800524077",
                productionYear = "2023",
                color = "Pearl White",
                stnkValidUntil = "12 - 09 - 23",
                carVariant = "Innova Zenix 2.0 V CVT",
                transmissionType = "A/T",
                fuelType = "Gas",
            ),
            MyCarsSample(
                carName = "KIJANG INNOVA ZENIX HYBRID EV",
                serviceDue = "20 Sept 2023",
                policeNumber = "B 4321 0M",
                image = R.drawable.sample_my_car_5,
                vinNumber = "KBJAAA3GS800524079",
                productionYear = "2023",
                color = "Pearl White",
                stnkValidUntil = "12 - 09 - 24",
                carVariant = "Veloz MT Type",
                transmissionType = "A/T",
                fuelType = "Gas"
            ),
            MyCarsSample(
                carName = "RAIZE GR SPORT",
                serviceDue = "22 Sept 2023",
                policeNumber = "B 1458 S0M",
                image = R.drawable.sample_my_car_6,
                vinNumber = "KBJAAA3GS800524060",
                productionYear = "2018",
                color = "Turquoise",
                stnkValidUntil = "12 - 09 - 21",
                carVariant = "Raize 1.0T GR Sport One Tone",
                transmissionType = "A/T",
                fuelType = "Bensin"
            ),
        )

        Observable.just(dataDummy)
            .delay(3, TimeUnit.SECONDS)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(onSuccess, onError)
            .let(disposable::add)
    }

    override fun getDataPreferredCategory(
        onSuccess: (List<PreferredMenuSample>) -> Unit,
        onError: (Throwable) -> Unit
    ) {
        val dataDummy = listOf(
            PreferredMenuSample(
                Menu.SERVICE_COUPON,
                R.drawable.ic_menu_service_coupon,
                "Kupon\nServis"
            ),
            PreferredMenuSample(
                Menu.SERVICE_PACKAGE,
                R.drawable.ic_menu_service_package,
                "Paket\nServis"
            ),
            PreferredMenuSample(
                Menu.ACCESSORIES,
                R.drawable.ic_menu_trade_in,
                "Akesoris"
            ),
            PreferredMenuSample(
                Menu.TRACKING_ORDERS,
                R.drawable.ic_menu_accessories,
                "Lacak\nPesanan"
            ),
            PreferredMenuSample(
                Menu.ROAD_SIDE_ASSISTANCE,
                R.drawable.ic_menu_tracking_orders,
                "Roadside\nAssistance"
            ),
            PreferredMenuSample(
                Menu.TRADE_IN,
                R.drawable.ic_menu_road_side_assistance,
                "Trade In"
            ),
        )

        Observable.just(dataDummy)
            .delay(3, TimeUnit.SECONDS)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(onSuccess, onError)
            .let(disposable::add)
    }

    override fun getDataBranchLocation(
        onSuccess: (List<BranchLocationSample>) -> Unit,
        onError: (Throwable) -> Unit
    ) {
        val dataDummy = listOf(
            BranchLocationSample(
                R.drawable.sample_branch_location_3,
                "Auto2000 Ambassador Mall Satrio",
                "Service : Showrooms",
                "Open",
                "- Closes 5:30 PM",
                "3.5 KM"
            ),
            BranchLocationSample(
                R.drawable.sample_branch_location_1,
                "Auto 2000 Cempaka Putih",
                "Service : Showrooms",
                "Open",
                "- Closes 5:30 PM",
                "5.5 KM"
            ),
            BranchLocationSample(
                R.drawable.sample_branch_location_2,
                "Auto2000 Scouts",
                "Service : Showrooms",
                "Open",
                "- Closes 3:30 PM",
                "7.5 KM"
            ),
        )
        Observable.just(dataDummy)
            .delay(3, TimeUnit.SECONDS)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(onSuccess, onError)
            .let(disposable::add)
    }

    override fun getDataPromo(
        size: Int,
        onSuccess: (Promo) -> Unit,
        onError: (Throwable) -> Unit
    ) {
        apiService.getPromo(Constants.VersionType.V1.value, size = size.toString())
            .delay(3, TimeUnit.SECONDS)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(onSuccess, onError)
            .let(disposable::add)
    }

    override fun getDataTrendingCars(
        onSuccess: (List<TrendingCarSample>) -> Unit,
        onError: (Throwable) -> Unit
    ) {
        val dataDummy = listOf(
            TrendingCarSample(
                "RAIZE GR SPORT 1",
                "602100000",
                R.drawable.sample_trending_car_1
            ),
            TrendingCarSample(
                "RAIZE GR SPORT 2",
                "555555000",
                R.drawable.sample_trending_car_1
            ),
            TrendingCarSample(
                "RAIZE GR SPORT 3",
                "999999000",
                R.drawable.sample_trending_car_1
            ),
        )
        Observable.just(dataDummy)
            .delay(3, TimeUnit.SECONDS)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(onSuccess, onError)
            .let(disposable::add)
    }

    override fun getDataNewsAndTips(
        size: Int,
        onSuccess: (NewsTips) -> Unit,
        onError: (Throwable) -> Unit
    ) {
        apiService.getNewsTips(Constants.VersionType.V1.value, size = size.toString())
            .delay(3, TimeUnit.SECONDS)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(onSuccess, onError)
            .let(disposable::add)
    }
}