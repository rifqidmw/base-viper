package com.base.viper.module.home.entity

data class TrendingCarSample(
    var carName: String,
    var price: String,
    var image: Int,
)