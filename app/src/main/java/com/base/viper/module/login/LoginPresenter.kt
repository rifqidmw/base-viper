package com.base.viper.module.login

import android.app.Activity

class LoginPresenter(
    private var view: LoginInterface.View? = null
) : LoginInterface.Presenter {
    
    private var interactor: LoginInterface.Interactor? = null
    private var router: LoginInterface.Router? = null
    
    override fun onCreate(activity: Activity) {
        view = activity as LoginActivity
        interactor = LoginInteractor(activity)
        router = LoginRouter(activity)

        view?.setupView()
    }
    
    override fun onDestroy() {
        view = null
        router = null
    }

    override fun goToAddCar() {
        router?.goToAddCar()
    }

    override fun goToHome() {
        router?.goToHome()
    }

    override fun goToForgotPassword() {
        router?.goToForgotPassword()
    }

    override fun goToTermAndCondition() {
        router?.goToTermAndCondition()
    }

    override fun goToPrivacyPolicy() {
        router?.goToPrivacyPolicy()
    }
}