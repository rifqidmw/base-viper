package com.base.viper.module.home.adapter

import android.annotation.SuppressLint
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.Lifecycle
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.base.viper.module.home.adapter.ViewPager2Adapter.Type.TRENDING_CAR
import com.base.viper.module.home.entity.TrendingCarSample
import com.base.viper.module.home.view.home.trendingcars.TrendingCarFragment

class ViewPager2Adapter(
    private val type: Type,
    fragmentManager: FragmentManager,
    lifecycle: Lifecycle,
) : FragmentStateAdapter(fragmentManager, lifecycle) {

    private val listTrendingCar: MutableList<TrendingCarSample> = mutableListOf()

    enum class Type {
        TRENDING_CAR
    }

    @SuppressLint("NotifyDataSetChanged")
    fun setTrendingCar(data: List<TrendingCarSample>) {
        listTrendingCar.clear()
        listTrendingCar.addAll(data)
        notifyDataSetChanged()
    }

    override fun getItemViewType(position: Int): Int {
        return type.ordinal
    }

    override fun getItemCount() = when (type) {
        TRENDING_CAR -> listTrendingCar.size
    }

    override fun createFragment(position: Int): Fragment {
        return when (type) {
            TRENDING_CAR -> TrendingCarFragment(listTrendingCar[position])
        }
    }
}