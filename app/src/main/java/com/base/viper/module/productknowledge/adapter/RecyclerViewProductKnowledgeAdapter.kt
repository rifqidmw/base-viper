package com.base.viper.module.productknowledge.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.core.view.isVisible
import androidx.recyclerview.widget.RecyclerView
import com.base.viper.R
import com.base.viper.common.BaseViewHolder
import com.base.viper.databinding.ItemDetailProductKnowledgeAdvantageBinding
import com.base.viper.databinding.ItemDetailProductKnowledgeFaqBinding
import com.base.viper.databinding.ItemDetailProductKnowledgeOfferBinding
import com.base.viper.databinding.ItemDetailProductKnowledgePromoBinding
import com.base.viper.databinding.ItemDetailProductKnowledgeSpecificationBinding
import com.base.viper.databinding.ItemProductBinding
import com.base.viper.module.productknowledge.entity.ProductAdvantageSample
import com.base.viper.module.productknowledge.entity.ProductKnowledge
import com.base.viper.module.productknowledge.entity.ProductKnowledgeDetail
import com.base.viper.module.productknowledge.entity.ProductOfferSample
import com.base.viper.module.productknowledge.entity.ProductSpecificationSample
import com.base.viper.module.promo.entity.promo.Content
import com.base.viper.utils.Utils.dateTimeFormat
import com.base.viper.utils.Utils.formatRupiah
import com.base.viper.utils.Utils.gone
import com.base.viper.utils.Utils.setImageViewUrl
import com.base.viper.utils.Utils.setOnSingleClickListener
import com.base.viper.utils.Utils.visible
import com.bumptech.glide.Glide

class RecyclerViewProductKnowledgeAdapter(private val type: ViewType) : RecyclerView.Adapter<BaseViewHolder>() {

    enum class ViewType {
        PRODUCT, PRODUCT_DETAIL_ADVANTAGE, PRODUCT_DETAIL_OFFER, PRODUCT_DETAIL_SPECIFICATION, PRODUCT_DETAIL_PROMO, PRODUCT_DETAIL_FAQ
    }

    private val listProduct: MutableList<ProductKnowledge.Content> = mutableListOf()
    private val listProductDetailAdvantage: MutableList<ProductAdvantageSample> = mutableListOf()
    private val listProductDetailOffer: MutableList<ProductOfferSample> = mutableListOf()
    private val listProductDetailSpecification: MutableList<ProductSpecificationSample> = mutableListOf()
    private val listProductDetailPromo: MutableList<Content> = mutableListOf()
    private val listProductDetailFaq: MutableList<ProductKnowledgeDetail.ContentCategory.Category.Faq> = mutableListOf()

    @SuppressLint("NotifyDataSetChanged")
    fun setDataListProduct(data: List<ProductKnowledge.Content>, clearAll: Boolean = false) {
        if (clearAll)
            listProduct.clear()
        listProduct.addAll(data)
        notifyDataSetChanged()
    }

    @SuppressLint("NotifyDataSetChanged")
    fun setDataListProductDetailAdvantage(data: List<ProductAdvantageSample>) {
        listProductDetailAdvantage.clear()
        listProductDetailAdvantage.addAll(data)
        notifyDataSetChanged()
    }

    @SuppressLint("NotifyDataSetChanged")
    fun setDataListProductDetailOffer(data: List<ProductOfferSample>) {
        listProductDetailOffer.clear()
        listProductDetailOffer.addAll(data)
        notifyDataSetChanged()
    }

    @SuppressLint("NotifyDataSetChanged")
    fun setDataListProductDetailSpecification(data: List<ProductSpecificationSample>) {
        listProductDetailSpecification.clear()
        listProductDetailSpecification.addAll(data)
        notifyDataSetChanged()
    }

    @SuppressLint("NotifyDataSetChanged")
    fun setDataListProductDetailPromo(data: List<Content>) {
        listProductDetailPromo.clear()
        listProductDetailPromo.addAll(data)
        notifyDataSetChanged()
    }

    @SuppressLint("NotifyDataSetChanged")
    fun setDataListProductDetailFaq(data: List<ProductKnowledgeDetail.ContentCategory.Category.Faq>) {
        listProductDetailFaq.clear()
        listProductDetailFaq.addAll(data)
        notifyDataSetChanged()
    }

    private var onItemClickListener: OnItemClickListener? = null

    interface OnItemClickListener {
        fun <T> onClickedListener(item: T)
    }

    fun setOnItemClickListener(onClickListener: OnItemClickListener) {
        this.onItemClickListener = onClickListener
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder {
        return when (viewType) {
            ViewType.PRODUCT.ordinal -> ProductViewHolder(
                ItemProductBinding.inflate(
                    LayoutInflater.from(parent.context), parent, false
                )
            )
            ViewType.PRODUCT_DETAIL_ADVANTAGE.ordinal -> ProductAdvantageViewHolder(
                ItemDetailProductKnowledgeAdvantageBinding.inflate(
                    LayoutInflater.from(parent.context), parent, false
                )
            )
            ViewType.PRODUCT_DETAIL_OFFER.ordinal -> ProductOfferViewHolder(
                ItemDetailProductKnowledgeOfferBinding.inflate(
                    LayoutInflater.from(parent.context), parent, false
                )
            )
            ViewType.PRODUCT_DETAIL_SPECIFICATION.ordinal -> ProductSpecificationViewHolder(
                ItemDetailProductKnowledgeSpecificationBinding.inflate(
                    LayoutInflater.from(parent.context), parent, false
                )
            )
            ViewType.PRODUCT_DETAIL_PROMO.ordinal -> ProductPromoViewHolder(
                ItemDetailProductKnowledgePromoBinding.inflate(
                    LayoutInflater.from(parent.context), parent, false
                )
            )
            ViewType.PRODUCT_DETAIL_FAQ.ordinal -> ProductFaqViewHolder(
                ItemDetailProductKnowledgeFaqBinding.inflate(
                    LayoutInflater.from(parent.context), parent, false
                )
            )

            else -> throw IllegalArgumentException("Invalid viewType $type")
        }
    }

    override fun onBindViewHolder(holder: BaseViewHolder, position: Int) {
        when (holder) {
            is ProductViewHolder -> holder.bind(listProduct[position])
            is ProductAdvantageViewHolder -> holder.bind(listProductDetailAdvantage[position], position)
            is ProductOfferViewHolder -> holder.bind(listProductDetailOffer[position])
            is ProductSpecificationViewHolder -> holder.bind(listProductDetailSpecification[position])
            is ProductPromoViewHolder -> holder.bind(listProductDetailPromo[position])
            is ProductFaqViewHolder -> holder.bind(listProductDetailFaq[position], position)
        }
    }

    override fun getItemCount() = when (type){
        ViewType.PRODUCT -> listProduct.size
        ViewType.PRODUCT_DETAIL_ADVANTAGE -> listProductDetailAdvantage.size
        ViewType.PRODUCT_DETAIL_SPECIFICATION -> listProductDetailSpecification.size
        ViewType.PRODUCT_DETAIL_PROMO -> listProductDetailPromo.size
        ViewType.PRODUCT_DETAIL_FAQ -> listProductDetailFaq.size
        ViewType.PRODUCT_DETAIL_OFFER -> listProductDetailOffer.size
    }

    override fun getItemViewType(position: Int) = type.ordinal

    inner class ProductViewHolder(
        private val binding: ItemProductBinding
    ) : BaseViewHolder(binding) {
        val context: Context = binding.root.context
        fun bind(product: ProductKnowledge.Content) = with(binding) {
            Glide.with(ivProduct)
                .load(product.heroImageLink)
                .into(ivProduct)
            tvProductTitle.text = product.titleHeader
            tvPeriod.text = context.getString(R.string.title_posted_on, product.publishedDate?.dateTimeFormat())
            root.setOnSingleClickListener {
                onItemClickListener?.onClickedListener(product)
            }
        }
    }

    inner class ProductAdvantageViewHolder(
        private val binding: ItemDetailProductKnowledgeAdvantageBinding
    ) : BaseViewHolder(binding) {
        fun bind(item: ProductAdvantageSample, position: Int) = with(binding) {
            tvTitle.text = "${position+1}. ${item.title}"
            tvDesc.text = item.desc

            root.setOnSingleClickListener {
                onItemClickListener?.onClickedListener(item)
            }
        }
    }

    inner class ProductOfferViewHolder(
        private val binding: ItemDetailProductKnowledgeOfferBinding
    ) : BaseViewHolder(binding) {
        fun bind(item: ProductOfferSample) = with(binding) {
            ivCars.setImageDrawable(
                ContextCompat.getDrawable(
                    root.context, item.image
                )
            )

            tvTitleOffer.text = item.title
            tvDescOffer.text = item.desc
            tvPriceOffer.text = "Rp. ${item.price.toString().formatRupiah()}"


            root.setOnSingleClickListener {
                onItemClickListener?.onClickedListener(item)
            }
        }
    }

    inner class ProductSpecificationViewHolder(
        private val binding: ItemDetailProductKnowledgeSpecificationBinding
    ) : BaseViewHolder(binding) {
        fun bind(item: ProductSpecificationSample) = with(binding) {
            tvTitle.text = item.title
            tvTitle.setCompoundDrawablesWithIntrinsicBounds(item.image, 0, 0, 0)
            tvDesc.text = item.desc
            root.setOnSingleClickListener {
                onItemClickListener?.onClickedListener(item)
            }
        }
    }

    inner class ProductPromoViewHolder(
        private val binding: ItemDetailProductKnowledgePromoBinding
    ) : BaseViewHolder(binding) {
        val context: Context = binding.root.context
        fun bind(item: Content) = with(binding) {
            ivPromo.setImageViewUrl(item.heroImageLink)
            tvTitle.text = item.titleHeader
            tvDate.text = context.getString(R.string.title_posted_on, item.publishedDate?.dateTimeFormat())

            root.setOnSingleClickListener {
                onItemClickListener?.onClickedListener(item)
            }
        }
    }

    inner class ProductFaqViewHolder(
        private val binding: ItemDetailProductKnowledgeFaqBinding
    ) : BaseViewHolder(binding) {
        fun bind(item: ProductKnowledgeDetail.ContentCategory.Category.Faq, position: Int) = with(binding) {
            tvTitle.text = item.question
            tvDesc.text = item.answer

            root.setOnSingleClickListener {
                if (tvDesc.isVisible) {
                    tvDesc.gone()
                    ivToggle.setImageDrawable(
                        ContextCompat.getDrawable(root.context, R.drawable.ic_plus_black)
                    )
                }
                else {
                    tvDesc.visible()
                    ivToggle.setImageDrawable(
                        ContextCompat.getDrawable(root.context, R.drawable.ic_minus_black)
                    )
                    val recyclerView = itemView.findRecyclerView()
                    recyclerView?.smoothScrollToPosition(position)
                }
            }
        }
    }

    fun View.findRecyclerView(): RecyclerView? {
        var parent = parent
        while (parent != null) {
            if (parent is RecyclerView) {
                return parent
            }
            parent = parent.parent
        }
        return null
    }
}