package com.base.viper.module.login.loginotp

import android.app.Activity

interface LoginOtpInterface {
    interface View {
        fun setupView()
    }
    
    interface Presenter {
        fun onCreate(activity: Activity)
        fun onDestroy()

        fun goToHome()
    }
    
    interface Interactor {

        fun setToken(token: String)
    }
    
    interface Router {
        fun goToHome()
    }
}