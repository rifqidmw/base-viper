package com.base.viper.module.home.view.home

import androidx.annotation.ColorRes
import androidx.annotation.DrawableRes
import androidx.fragment.app.Fragment
import com.base.viper.module.home.common.HomeConstant
import com.base.viper.module.home.entity.BranchLocationSample
import com.base.viper.module.home.entity.Menu
import com.base.viper.module.home.entity.MyCarsSample
import com.base.viper.module.home.entity.PreferredMenuSample
import com.base.viper.module.home.entity.ServiceDueSample
import com.base.viper.module.home.entity.TrendingCarSample
import com.base.viper.module.newstips.entity.newstips.NewsTips
import com.base.viper.module.promo.entity.promo.Content
import com.base.viper.module.promo.entity.promo.Promo

interface HomeInterface {

    interface View {
        fun setupView()
        fun setDataServiceDue(data: List<ServiceDueSample>)
        fun setDataMyCars(data: List<MyCarsSample>)
        fun setDataTrendingCars(data: List<TrendingCarSample>)
        fun setDataPreferredCategory(data: List<PreferredMenuSample>)
        fun setDataPromo(data: List<Content>?)
        fun setDataBranchLocation(data: List<BranchLocationSample>)
        fun setDataNewsAndTips(data: List<NewsTips.Content>)
        fun setShimmerServiceDue(isShimmer: Boolean)
        fun setShimmerMyCars(isShimmer: Boolean)
        fun setShimmerPreferredCategory(isShimmer: Boolean)
        fun setShimmerBranchLocation(isShimmer: Boolean)
        fun setShimmerPromo(isShimmer: Boolean)
        fun setShimmerNewsAndTips(isShimmer: Boolean)
        fun homeTheme(@ColorRes color: Int, @DrawableRes drawable: Int, isLightColor: Boolean)
        fun setGreeting(greetings: String)
        fun showPopUpError(throwable: Throwable)
    }

    interface Presenter {
        fun onCreate(fragment: Fragment)
        fun onDestroy()
        fun onViewCreated(fragment: Fragment)
        fun goToPreferredCategory(menuCode: Menu)
        fun onSectionTrendingCar(
            trendingCar: TrendingCarSample,
            event: HomeConstant.TrendingCarEvent
        )

        fun goToDetailBranchLocation(item: BranchLocationSample)
        fun goToAllBranchLocation()
        fun goToPromo()
        fun goToDetailPromo(item: Content)
        fun goToDetailNewsAndTips(item: NewsTips.Content)
        fun goToAllNewsAndTips()
        fun goToServiceDue()
        fun goToAddCar()
        fun goToDetailMyCar(item: MyCarsSample)
    }

    interface Interactor {
        fun getDataServiceDue(
            onSuccess: (List<ServiceDueSample>) -> Unit,
            onError: (Throwable) -> Unit
        )

        fun getDataMyCars(
            onSuccess: (List<MyCarsSample>) -> Unit,
            onError: (Throwable) -> Unit
        )

        fun getDataPreferredCategory(
            onSuccess: (List<PreferredMenuSample>) -> Unit,
            onError: (Throwable) -> Unit
        )

        fun getDataTrendingCars(
            onSuccess: (List<TrendingCarSample>) -> Unit,
            onError: (Throwable) -> Unit
        )

        fun getDataPromo(
            size: Int,
            onSuccess: (Promo) -> Unit,
            onError: (Throwable) -> Unit
        )

        fun getDataBranchLocation(
            onSuccess: (List<BranchLocationSample>) -> Unit,
            onError: (Throwable) -> Unit
        )

        fun getDataNewsAndTips(
            size: Int,
            onSuccess: (NewsTips) -> Unit,
            onError: (Throwable) -> Unit
        )
    }

    interface Router {
        fun goToServiceDue()
        fun goToDetailMyCar(item: MyCarsSample)

        fun goToServiceCoupon()
        fun goToServicePackage()
        fun goToTradeIn()
        fun goToAccessories()
        fun goToTrackingOrders()
        fun goToRoadSideAssistance()

        fun goToAllBranchLocation()
        fun goToDetailBranchLocation()

        fun goToAllPromo()
        fun goToDetailPromo(item: Content)

        fun goToTestDrive()
        fun goToInquireCar()
        fun goToExploreCar()
        fun goToCompareCar()

        fun goToDetailNewsAndTips(item: NewsTips.Content)
        fun goToAllNewsAndTips()
        fun goToAddCar()
    }
}