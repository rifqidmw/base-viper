package com.base.viper.module.newcars.entity

class NewCarConstant {
    enum class ToolbarEvent {
        SEARCH, NOTIFICATION, CART
    }

    enum class ContentType {
        ALL, HYBRID, MPV, SUV, HATCHBACK, SEDAN, SPORTS
    }
}