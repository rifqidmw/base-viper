package com.base.viper.module.productknowledge.view.detailproductknowledge

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.LinearSnapHelper
import androidx.recyclerview.widget.SnapHelper
import com.base.viper.R
import com.base.viper.common.CustomPopUpDialog
import com.base.viper.common.CustomPopUpParam
import com.base.viper.common.HorizontalMarginItemDecoration
import com.base.viper.common.VerticalMarginItemDecoration
import com.base.viper.components.CustomToolbar
import com.base.viper.components.promosection.MapperToDataSubSection.contentToListPromo
import com.base.viper.databinding.ActivityDetailProductKnowledgeBinding
import com.base.viper.module.productknowledge.adapter.RecyclerViewProductKnowledgeAdapter
import com.base.viper.module.productknowledge.entity.ProductAdvantageSample
import com.base.viper.module.productknowledge.entity.ProductKnowledgeDetail
import com.base.viper.module.productknowledge.entity.ProductOfferSample
import com.base.viper.module.productknowledge.entity.ProductSpecificationSample
import com.base.viper.module.productknowledge.view.askoffering.AskOfferingFragment
import com.base.viper.module.promo.entity.promo.Content
import com.base.viper.utils.Utils.dateTimeFormat
import com.base.viper.utils.Utils.dp
import com.base.viper.utils.Utils.setHtmlView
import com.base.viper.utils.Utils.setImageViewUrl
import com.base.viper.utils.Utils.setOnSingleClickListener

class DetailProductKnowledgeActivity : AppCompatActivity(), DetailProductKnowledgeInterface.View,
    RecyclerViewProductKnowledgeAdapter.OnItemClickListener {

    private val presenter: DetailProductKnowledgePresenter = DetailProductKnowledgePresenter(this)
    private val binding: ActivityDetailProductKnowledgeBinding by lazy {
        ActivityDetailProductKnowledgeBinding.inflate(layoutInflater)
    }

    private val advantageAdapter: RecyclerViewProductKnowledgeAdapter by lazy {
        RecyclerViewProductKnowledgeAdapter(RecyclerViewProductKnowledgeAdapter.ViewType.PRODUCT_DETAIL_ADVANTAGE)
    }

    private val offerAdapter: RecyclerViewProductKnowledgeAdapter by lazy {
        RecyclerViewProductKnowledgeAdapter(RecyclerViewProductKnowledgeAdapter.ViewType.PRODUCT_DETAIL_OFFER)
    }

    private val specificationAdapter: RecyclerViewProductKnowledgeAdapter by lazy {
        RecyclerViewProductKnowledgeAdapter(RecyclerViewProductKnowledgeAdapter.ViewType.PRODUCT_DETAIL_SPECIFICATION)
    }

    private val promoAdapter: RecyclerViewProductKnowledgeAdapter by lazy {
        RecyclerViewProductKnowledgeAdapter(RecyclerViewProductKnowledgeAdapter.ViewType.PRODUCT_DETAIL_PROMO)
    }

    private val faqAdapter: RecyclerViewProductKnowledgeAdapter by lazy {
        RecyclerViewProductKnowledgeAdapter(RecyclerViewProductKnowledgeAdapter.ViewType.PRODUCT_DETAIL_FAQ)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)
        presenter.onCreate(this)
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter.onDestroy()
    }

    override fun setupView() {
        setupToolbar()
        setupListener()
        setupProductAdvantageAdapter()
        setupProductOfferAdapter()
        setupProductSpecificationAdapter()
        setupProductPromoAdapter()
        setupProductFaqAdapter()
    }

    override fun setDataProductKnowledge(data: ProductKnowledgeDetail) {
        with(binding){
            ivProduct.setImageViewUrl(data.heroImageLink)
            tvTitleProduct.text = data.titleHeader
            tvDesc.setHtmlView(data.detail?.detailContent)
            tvDate.text = data.publishedDate?.dateTimeFormat()
        }
    }

    private fun setupToolbar() {
        binding.toolbar.setOnClickedListener(object : CustomToolbar.Listener {
            override fun onClickedBackButton() {
                onBackPressedDispatcher.onBackPressed()
            }
        })
    }

    private fun setupListener() = with(binding) {
        ivShare.setOnSingleClickListener {
            presenter.shareWithLink()
        }
    }

    private fun setupProductAdvantageAdapter() {
        advantageAdapter.setOnItemClickListener(this)
        val snapHelper: SnapHelper = LinearSnapHelper()
        binding.rvAdvantage.apply {
            adapter = advantageAdapter
            layoutManager = LinearLayoutManager(
                this@DetailProductKnowledgeActivity,
                LinearLayoutManager.HORIZONTAL,
                false
            )
            snapHelper.attachToRecyclerView(this)
        }
    }

    private fun setupProductOfferAdapter() {
        offerAdapter.setOnItemClickListener(this)
        val snapHelper: SnapHelper = LinearSnapHelper()
        binding.rvOffer.apply {
            adapter = offerAdapter
            layoutManager = LinearLayoutManager(
                this@DetailProductKnowledgeActivity,
                LinearLayoutManager.HORIZONTAL,
                false
            )
            snapHelper.attachToRecyclerView(this)
            addItemDecoration(
                HorizontalMarginItemDecoration(
                    horizontalMargin = 16.dp,
                    lastItemEndMargin = 16.dp
                )
            )
        }
    }

    private fun setupProductSpecificationAdapter() {
        specificationAdapter.setOnItemClickListener(this)
        binding.rvSpecification.apply {
            adapter = specificationAdapter
            layoutManager = LinearLayoutManager(this@DetailProductKnowledgeActivity)
            addItemDecoration(
                VerticalMarginItemDecoration(
                    verticalMargin = 8.dp
                )
            )
        }

        binding.btnAskOffering2.setOnSingleClickListener {
            val bottomSheet = AskOfferingFragment()
            bottomSheet.show(supportFragmentManager, "ASK_OFFERING")
        }
    }

    private fun setupProductPromoAdapter() {
        promoAdapter.setOnItemClickListener(this)
        binding.rvPromo.apply {
            adapter = promoAdapter
            layoutManager = LinearLayoutManager(
                this@DetailProductKnowledgeActivity,
                LinearLayoutManager.HORIZONTAL,
                false
            )
        }
    }

    private fun setupProductFaqAdapter() {
        faqAdapter.setOnItemClickListener(this)
        binding.rvFaq.apply {
            adapter = faqAdapter
            layoutManager = LinearLayoutManager(this@DetailProductKnowledgeActivity)
        }
    }

    override fun setDataProductAdvantage(data: List<ProductAdvantageSample>) {
        advantageAdapter.setDataListProductDetailAdvantage(data)
    }

    override fun setDataProductOffer(data: List<ProductOfferSample>) {
        offerAdapter.setDataListProductDetailOffer(data)
    }

    override fun setDataProductSpecification(data: List<ProductSpecificationSample>) {
        specificationAdapter.setDataListProductDetailSpecification(data)
    }

    override fun setDataProductFaq(data: List<ProductKnowledgeDetail.ContentCategory.Category.Faq>?) {
        data?.let { faqAdapter.setDataListProductDetailFaq(it) }
    }

    override fun setupSubPromo(data: List<Content>) {
        promoAdapter.setDataListProductDetailPromo(data)
    }

    override fun fragmentPromoResultListener(data: List<Content>?) {
        val listData = data?.contentToListPromo(this@DetailProductKnowledgeActivity)
        supportFragmentManager.setFragmentResultListener(
            "SUCCESS_INQUIRY",
            this@DetailProductKnowledgeActivity
        ) { _, bundle ->
            val status = bundle.getString("status")
            if (status == "success" && !listData.isNullOrEmpty()) {
                val param = CustomPopUpParam.SuccessMessageParam(
                    title = getString(R.string.title_inquiry_success),
                    description = getString(R.string.desc_inquiry_success),
                    actionButton = getString(R.string.title_label_inquiry_go_to_home).uppercase(),
                    cancelable = false,
                    sectionTitle = getString(R.string.title_label_inquiry_other_promo).uppercase(),
                    sectionSubTitle = getString(R.string.title_label_inquiry_see_all).uppercase(),
                    sectionData = listData,
                    onClickedActionButton = {
                        presenter.goToHome()
                    },
                    onClickedItemSection = {
                        presenter.goToDetailPromo(data[it].slug)
                    },
                    onClickedSectionSubTitle = {

                    },
                    onDismissListener = null
                )
                CustomPopUpDialog(this@DetailProductKnowledgeActivity).popUpSuccessMessage(param)
            }
        }
    }

    override fun <T> onClickedListener(item: T) {
        if (item is ProductOfferSample)  {
            val bottomSheet = AskOfferingFragment()
            bottomSheet.show(supportFragmentManager, "ASK_OFFERING")
        }
    }
}