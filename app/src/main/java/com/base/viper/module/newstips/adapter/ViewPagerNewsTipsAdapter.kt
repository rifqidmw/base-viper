package com.base.viper.module.newstips.adapter

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.Lifecycle
import androidx.viewpager2.adapter.FragmentStateAdapter
import androidx.viewpager2.adapter.FragmentViewHolder
import com.base.viper.module.newstips.entity.newstips.NewsTipsCategory
import com.base.viper.module.newstips.view.category.NewsTipsCategoryFragment

class ViewPagerNewsTipsAdapter(
    private val listCategory: List<NewsTipsCategory>,
    private val fragmentManager: FragmentManager,
    lifecycle: Lifecycle,
) : FragmentStateAdapter(fragmentManager, lifecycle) {

    override fun getItemCount() = listCategory.size

    override fun createFragment(position: Int): Fragment {
        return NewsTipsCategoryFragment(listCategory[position])
    }

    override fun onBindViewHolder(
        holder: FragmentViewHolder,
        position: Int,
        payloads: MutableList<Any>
    ) {
        super.onBindViewHolder(holder, position, payloads)
    }
}