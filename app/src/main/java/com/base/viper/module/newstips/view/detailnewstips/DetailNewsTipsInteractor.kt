package com.base.viper.module.newstips.view.detailnewstips

import android.app.Activity
import com.base.viper.common.Constants
import com.base.viper.module.newstips.entity.detail.NewsTipsDetail
import com.base.viper.module.newstips.entity.newstips.NewsTips
import com.base.viper.module.promo.entity.promo.Promo
import com.base.viper.services.NetworkBuilder
import com.base.viper.services.Sessions
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.disposables.CompositeDisposable
import io.reactivex.rxjava3.schedulers.Schedulers

class DetailNewsTipsInteractor(activity: Activity) : DetailNewsTipsInterface.Interactor {

    private val disposable = CompositeDisposable()
    private val apiService = NetworkBuilder.apiService
    private val sessions = Sessions(activity)
    override fun getDetailNewsTips(
        slug: String?,
        onSuccess: (NewsTipsDetail) -> Unit,
        onError: (Throwable) -> Unit
    ) {
        apiService.getNewsTipsDetail(Constants.VersionType.V1.value, slug)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(onSuccess, onError)
            .let(disposable::add)
    }

    override fun getSubPromo(size: Int, onSuccess: (Promo) -> Unit, onError: (Throwable) -> Unit) {
        apiService.getPromo(Constants.VersionType.V1.value)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(onSuccess, onError)
            .let(disposable::add)
    }

    override fun getSubNewsTips(
        size: Int,
        onSuccess: (NewsTips) -> Unit,
        onError: (Throwable) -> Unit
    ) {
        apiService.getNewsTips(Constants.VersionType.V1.value, size = size.toString())
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(onSuccess, onError)
            .let(disposable::add)
    }
}