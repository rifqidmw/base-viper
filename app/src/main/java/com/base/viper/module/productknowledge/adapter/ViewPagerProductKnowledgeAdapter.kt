package com.base.viper.module.productknowledge.adapter

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.Lifecycle
import androidx.viewpager2.adapter.FragmentStateAdapter
import androidx.viewpager2.adapter.FragmentViewHolder
import com.base.viper.module.productknowledge.entity.ProductKnowledgeCategory
import com.base.viper.module.productknowledge.view.category.ProductCategoryFragment

class ViewPagerProductKnowledgeAdapter(
    private val listCategory: List<ProductKnowledgeCategory>,
    private val fragmentManager: FragmentManager,
    lifecycle: Lifecycle,
) : FragmentStateAdapter(fragmentManager, lifecycle) {

    override fun getItemCount() = listCategory.size

    override fun createFragment(position: Int): Fragment {
        return ProductCategoryFragment(listCategory[position])
    }

    override fun onBindViewHolder(
        holder: FragmentViewHolder,
        position: Int,
        payloads: MutableList<Any>
    ) {
        super.onBindViewHolder(holder, position, payloads)
    }
}