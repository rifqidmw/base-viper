package com.base.viper.module.login

import android.app.Activity
import com.base.viper.services.NetworkBuilder
import com.base.viper.services.Sessions
import io.reactivex.rxjava3.disposables.CompositeDisposable

class LoginInteractor(activity: Activity) : LoginInterface.Interactor {
    
    private val disposable = CompositeDisposable()
    private val apiService = NetworkBuilder.apiService
    private val sessions = Sessions(activity)
}