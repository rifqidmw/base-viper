package com.base.viper.module.home.view.chat

import androidx.fragment.app.Fragment

class ChatPresenter(
    private var view: ChatInterface.View? = null
) : ChatInterface.Presenter {

    private var interactor: ChatInterface.Interactor? = null
    private var router: ChatInterface.Router? = null

    override fun onCreate(fragment: Fragment) {
        view = fragment as ChatFragment
        interactor = ChatInteractor(fragment)
        router = ChatRouter(fragment)
    }
    
    override fun onViewCreated(fragment: Fragment) {
        //Not Yet Implemented
    }

    override fun onDestroy() {
        view = null
        router = null
    }
}