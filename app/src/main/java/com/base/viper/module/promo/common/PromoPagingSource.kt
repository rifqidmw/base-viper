package com.base.viper.module.promo.common

import androidx.paging.PagingSource
import androidx.paging.PagingState
import com.base.viper.common.Constants
import com.base.viper.module.promo.entity.promo.Content
import com.base.viper.services.ApiServices
import retrofit2.HttpException
import java.io.IOException

class PromoPagingSource(
    private val apiService: ApiServices,
    private val size: Int,
    private val contentCategory: String
) : PagingSource<Int, Content>() {

    override fun getRefreshKey(state: PagingState<Int, Content>): Int? {
        return null
    }

    override suspend fun load(params: LoadParams<Int>): LoadResult<Int, Content> {
        val currentLoadingPageKey = params.key ?: 0

        return try {
            val response = apiService.getPromo(
                version = Constants.VersionType.V1.value,
                page = currentLoadingPageKey.toString(),
                size = size.toString(),
                contentCategory = when (contentCategory) {
                    "SEMUA", "ALL" -> ""
                    else -> contentCategory
                }
            ).blockingGet()

            val data = response.content

//            val prevKey = if (currentLoadingPageKey == 1) null else currentLoadingPageKey - 1
            val nextKey = if (currentLoadingPageKey < (response.totalPages
                    ?: 0)
            ) currentLoadingPageKey.plus(1) else null

            LoadResult.Page(
                data = data,
                prevKey = null,
                nextKey = nextKey
            )
        } catch (e: IOException) {
            LoadResult.Error(e)
        } catch (e: HttpException) {
            LoadResult.Error(e)
        }
    }
}