package com.base.viper.module.home.view

import android.app.Activity
import com.base.viper.utils.Utils.toastShort

class MainRouter(private val activity: Activity) : MainInterface.Router {
    override fun goToSearch() {
        activity.toastShort("Todo: Search")
    }

    override fun goToNotification() {
        activity.toastShort("Todo: Notification")
    }

    override fun goToCart() {
        activity.toastShort("Todo: Cart")
    }
}