package com.base.viper.module.newcars.entity

data class NewCarCount(
    val type: NewCarConstant.ContentType,
    val count: Int
)