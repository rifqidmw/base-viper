package com.base.viper.module.home.view.services

import androidx.fragment.app.Fragment

interface ServicesInterface {
    interface View

    interface Presenter {
        fun onCreate(fragment: Fragment)
        fun onViewCreated(fragment: Fragment)
        fun onDestroy()
    }

    interface Interactor

    interface Router
}