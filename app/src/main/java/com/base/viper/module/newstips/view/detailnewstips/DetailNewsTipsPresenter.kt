package com.base.viper.module.newstips.view.detailnewstips

import android.app.Activity
import android.util.Log
import com.base.viper.common.Constants
import com.base.viper.module.newstips.common.NewsTipsConstant

class DetailNewsTipsPresenter(
    private var view: DetailNewsTipsInterface.View? = null
) : DetailNewsTipsInterface.Presenter {

    private var interactor: DetailNewsTipsInterface.Interactor? = null
    private var router: DetailNewsTipsInterface.Router? = null

    private var slug: String? = null
    private var origin: NewsTipsConstant.OriginEvent? = null

    override fun onCreate(activity: Activity) {
        view = activity as DetailNewsTipsActivity
        interactor = DetailNewsTipsInteractor(activity)
        router = DetailNewsTipsRouter(activity)

        activity.intent?.let {
            slug = it.getStringExtra(NewsTipsConstant.SLUG)
            origin = when(it.getStringExtra(NewsTipsConstant.ORIGIN)){
                NewsTipsConstant.OriginEvent.HOME.name -> NewsTipsConstant.OriginEvent.HOME
                else -> NewsTipsConstant.OriginEvent.NEWSTIPS
            }
        }

        view?.setupView()

        getDetailNewsTips(slug)
        getSubPromo()
        getSubNewsTips()
    }

    override fun onDestroy() {
        view = null
        router = null
    }

    override fun onSectionToolbar(event: Constants.ToolbarEvent) {
        when (event) {
            Constants.ToolbarEvent.SEARCH -> router?.goToSearch()
            Constants.ToolbarEvent.NOTIFICATION -> router?.goToNotification()
            Constants.ToolbarEvent.CART -> router?.goToCart()
        }
    }

    private fun getDetailNewsTips(slug: String?){
        interactor?.getDetailNewsTips(slug, {
            view?.setDataNewsAndTips(it)
        }, {
            Log.e(DetailNewsTipsPresenter::class.java.simpleName, "Error Detail News Tips: $it")
        })
    }

    private fun getSubPromo() {
        interactor?.getSubPromo(10,
            {
                val filteredPromo = it.content?.filter { content ->
                    content.status == Constants.StatusType.PUBLISH.value
                }
                view?.setupSubPromo(it.content)
                view?.fragmentPromoResultListener(it.content)
            }, {

            }
        )
    }

    private fun getSubNewsTips(){
        interactor?.getSubNewsTips(10, {
            val filteredNewsTips = it.content.filter { content ->
                content.status == Constants.StatusType.PUBLISH.value
            }
            view?.setupSubNewsTips(it.content)
            view?.fragmentNewsTipsResultListener(it.content)
        }, {

        })
    }

    override fun goToInquiry() {
        router?.goToInquiry()
    }

    override fun goToHome() {
        router?.goToHome()
    }

    override fun onSharePromo(event: Constants.ShareEvent) {
        when (event) {
            Constants.ShareEvent.WHATSAPP -> router?.shareViaWhatsapp()
            Constants.ShareEvent.FACEBOOK -> router?.shareViaFacebook()
            Constants.ShareEvent.TWITTER -> router?.shareViaTwitter()
            Constants.ShareEvent.TIKTOK -> router?.shareViaTiktok()
            Constants.ShareEvent.LINK -> router?.shareWithLink()
        }
    }

    override fun goToAllPromo() {
        router?.goToAllPromo()
    }

    override fun goToAllNews() {
        origin?.let { router?.goToAllNews(it) }
    }

    override fun goToDetailPromo(slug: String?) {
        router?.goToDetailPromo(slug)
    }

    override fun goToDetailNewsTips(slug: String?) {
        router?.goToDetailNewsTips(slug)
    }
}