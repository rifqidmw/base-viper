package com.base.viper.module.promo.view.detailpromo

import android.app.Activity
import com.base.viper.common.Constants
import com.base.viper.module.promo.entity.promo.Content
import com.base.viper.module.promo.entity.promo.Promo
import com.base.viper.module.promo.entity.promo.PromoDetail
import java.math.BigInteger

interface DetailPromoInterface {
    interface View {
        fun setupView()
        fun setupDataRegular(data: PromoDetail?)
        fun setupDataNonRegular(data: PromoDetail?)
        fun setCalculationResult(
            afterDiscount: BigInteger,
            beforeDiscountPrice: BigInteger,
            priceDifference: BigInteger
        )

        fun setupSubPromo(promoList: List<Content>?)
        fun fragmentResultListener(promoList: List<Content>?)
    }
    
    interface Presenter {
        fun onCreate(activity: Activity)
        fun onDestroy()
        fun onSectionToolbar(event: Constants.ToolbarEvent)
        fun goToFormRequestQuote()
        fun simulatePrice()
        fun onSharePromo(event: Constants.ShareEvent)
        fun goToAllPromo()
        fun goToDetailPromo(slug: String?)
        fun goToCart()
        fun goToHome()
    }
    
    interface Interactor {

        fun getDetailPromo(
            slug: String?,
            onSuccess: (PromoDetail) -> Unit,
            onError: (Throwable) -> Unit
        )

        fun getSubPromo(
            size: Int,
            onSuccess: (Promo) -> Unit,
            onError: (Throwable) -> Unit
        )
    }
    
    interface Router {
        fun goToSearch()
        fun goToNotification()
        fun goToCart()
        fun goToFormRequestQuote()
        fun shareViaWhatsapp()
        fun shareViaFacebook()
        fun shareViaTwitter()
        fun shareViaTiktok()
        fun shareWithLink()
        fun goToAllPromo()
        fun goToDetailPromo(slug: String?)
        fun goToHome()
    }
}