package com.base.viper.module.login.loginotp

import android.app.Activity

class LoginOtpPresenter(
    private var view: LoginOtpInterface.View? = null
) : LoginOtpInterface.Presenter {
    
    private var interactor: LoginOtpInterface.Interactor? = null
    private var router: LoginOtpInterface.Router? = null
    
    override fun onCreate(activity: Activity) {
        view = activity as LoginOtpActivity
        interactor = LoginOtpInteractor(activity)
        router = LoginOtpRouter(activity)

        view?.setupView()
    }
    
    override fun onDestroy() {
        view = null
        router = null
    }

    override fun goToHome() {
        interactor?.setToken("0e4714dfe7msh005fa84627b60bdp1d2971jsn3d2ecd0183bb")
        router?.goToHome()
    }
}