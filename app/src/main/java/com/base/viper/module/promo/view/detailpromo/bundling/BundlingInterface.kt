package com.base.viper.module.promo.view.detailpromo.bundling

import androidx.fragment.app.Fragment
import com.base.viper.module.promo.entity.bundle.BundleSample

interface BundlingInterface {
    interface View {
        fun setDataBundling(bundleSample: BundleSample?)
    }

    interface Presenter {
        fun onCreate(fragment: Fragment)
        fun onViewCreated(fragment: Fragment)
        fun onDestroy()
    }

    interface Interactor

    interface Router
}