package com.base.viper.module.promo.view.detailpromo

import android.app.Activity
import com.base.viper.common.Constants
import com.base.viper.module.promo.entity.promo.Promo
import com.base.viper.module.promo.entity.promo.PromoDetail
import com.base.viper.services.NetworkBuilder
import com.base.viper.services.Sessions
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.disposables.CompositeDisposable
import io.reactivex.rxjava3.schedulers.Schedulers

class DetailPromoInteractor(activity: Activity) : DetailPromoInterface.Interactor {

    private val disposable = CompositeDisposable()
    private val apiService = NetworkBuilder.apiService
    private val sessions = Sessions(activity)

    override fun getDetailPromo(
        slug: String?,
        onSuccess: (PromoDetail) -> Unit,
        onError: (Throwable) -> Unit
    ) {
        apiService.getPromoDetail(Constants.VersionType.V1.value, slug)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(onSuccess, onError)
            .let(disposable::add)
    }

    override fun getSubPromo(
        size: Int,
        onSuccess: (Promo) -> Unit,
        onError: (Throwable) -> Unit
    ) {
        apiService.getPromo(Constants.VersionType.V1.value, size = size.toString())
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(onSuccess, onError)
            .let(disposable::add)
    }
}