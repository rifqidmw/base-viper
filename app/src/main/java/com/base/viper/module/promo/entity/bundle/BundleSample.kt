package com.base.viper.module.promo.entity.bundle

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class BundleSample(
    val bundleImage: Int,
    val bundleTitle: String,
    val bundleColor: String,
    val bundlePrice: String,
) : Parcelable