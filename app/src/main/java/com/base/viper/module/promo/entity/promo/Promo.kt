package com.base.viper.module.promo.entity.promo


import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize


@Parcelize
data class Promo(
    @SerializedName("content")
    val content: List<Content> = emptyList(),
    @SerializedName("number")
    val number: Int? = null,
    @SerializedName("size")
    val size: Int? = null,
    @SerializedName("totalElements")
    val totalElements: Int? = null,
    @SerializedName("pageable")
    val pageable: Pageable? = null,
    @SerializedName("last")
    val last: Boolean? = false,
    @SerializedName("totalPages")
    val totalPages: Int? = null,
    @SerializedName("sort")
    val sort: Sort? = null,
    @SerializedName("first")
    val first: Boolean? = false,
    @SerializedName("numberOfElements")
    val numberOfElements: Int? = null,
    @SerializedName("empty")
    val empty: Boolean? = false
) : Parcelable