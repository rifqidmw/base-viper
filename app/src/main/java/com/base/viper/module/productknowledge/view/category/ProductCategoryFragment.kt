package com.base.viper.module.productknowledge.view.category

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.base.viper.databinding.FragmentProductCategoryBinding
import com.base.viper.module.productknowledge.adapter.RecyclerViewProductKnowledgeAdapter
import com.base.viper.module.productknowledge.entity.ProductKnowledge
import com.base.viper.module.productknowledge.entity.ProductKnowledgeCategory
import com.base.viper.utils.Utils.gone
import com.base.viper.utils.Utils.setImageViewUrl
import com.base.viper.utils.Utils.setOnSingleClickListener
import com.base.viper.utils.Utils.visible

class ProductCategoryFragment(
    category: ProductKnowledgeCategory
) : Fragment(),
    ProductCategoryInterface.View, RecyclerViewProductKnowledgeAdapter.OnItemClickListener {

    private val presenter: ProductCategoryPresenter = ProductCategoryPresenter(this, category.name?:"")
    private val binding: FragmentProductCategoryBinding by lazy {
        FragmentProductCategoryBinding.inflate(layoutInflater)
    }
    private val productAdapter: RecyclerViewProductKnowledgeAdapter by lazy {
        RecyclerViewProductKnowledgeAdapter(RecyclerViewProductKnowledgeAdapter.ViewType.PRODUCT)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        presenter.onCreate(this)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        presenter.onViewCreated(this)
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter.onDestroy()
    }

    override fun setupView() {
        setupProductKnowledgeAdapter()
    }

    private fun setupProductKnowledgeAdapter(){
        with(binding) {
            rvProduct.apply {
                setHasFixedSize(true)
                layoutManager = LinearLayoutManager(requireContext())
                adapter = productAdapter.apply {
                    setOnItemClickListener(this@ProductCategoryFragment)
                }

                addOnScrollListener(object : RecyclerView.OnScrollListener(){
                    override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                        super.onScrolled(recyclerView, dx, dy)
                        val layoutManager = recyclerView.layoutManager as LinearLayoutManager
                        val lastVisibleItemPosition = layoutManager.findLastVisibleItemPosition()
                        val totalItemCount = layoutManager.itemCount

                        if (lastVisibleItemPosition == totalItemCount - 1) {
                            if (binding.pbFooterLoading.isVisible.not())
                                presenter.getDataProductKnowledge()
                        }
                    }
                })
            }
        }
    }

    override fun setDataProductKnowledge(data: List<ProductKnowledge.Content>) {
        productAdapter.setDataListProduct(data)
    }

    override fun setDataHighlight(data: ProductKnowledge.Content) = with(binding) {
        lyHighlight.visible()

        ivHighlight.setImageViewUrl(data.heroImageLink)

        lyHighlight.setOnSingleClickListener {
            presenter.goToDetail(data)
        }
    }

    override fun setFooterLoading(isLoading: Boolean) = with(binding) {
        if (isLoading) pbFooterLoading.visible() else pbFooterLoading.gone()
    }

    override fun <T> onClickedListener(item: T) {
        if (item is ProductKnowledge.Content) presenter.goToDetail(item)
    }
}