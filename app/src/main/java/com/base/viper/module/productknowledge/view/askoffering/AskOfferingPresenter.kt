package com.base.viper.module.productknowledge.view.askoffering

import androidx.fragment.app.Fragment
import com.base.viper.utils.Utils.isValidEmail
import com.base.viper.utils.Utils.isValidPhoneLength
import com.base.viper.utils.Utils.isValidPhoneNumber

class AskOfferingPresenter(
    private var view: AskOfferingInterface.View? = null
) : AskOfferingInterface.Presenter {

    private var interactor: AskOfferingInterface.Interactor? = null
    private var router: AskOfferingInterface.Router? = null

    var fullName = ""
    var email = ""
    var phone = ""
    private val requiredField = mutableMapOf(
        "fullName" to false,
        "email" to false,
        "phone" to false,
    )

    override fun onCreate(fragment: Fragment) {
        view = fragment as AskOfferingFragment
        interactor = AskOfferingInteractor(fragment)
        router = AskOfferingRouter(fragment)
    }

    override fun onViewCreated(fragment: Fragment) {
        view?.setupView()
    }

    override fun onDestroy() {
        view = null
        router = null
    }

    override fun validateInput() {
        requiredField["fullName"] = when {
            fullName.isEmpty() -> {
                view?.setErrorFullName("Nama Lengkap Harus Diisi")
                false
            }

            else -> {
                view?.setErrorFullName(null)
                true
            }
        }

        requiredField["email"] = when {
            email.isEmpty() -> {
                view?.setErrorEmail("Email Harus Diisi")
                false
            }

            !email.isValidEmail() -> {
                view?.setErrorEmail("Email Tidak Valid")
                false
            }

            else -> {
                view?.setErrorEmail(null)
                true
            }
        }

        requiredField["phone"] = when {
            phone.isEmpty() -> {
                view?.setErrorPhone("No. Telp Harus Diisi")
                false
            }

            !phone.isValidPhoneNumber() -> {
                view?.setErrorPhone("No. Telp Harus Diawali 08/+62/62")
                false
            }

            !phone.isValidPhoneLength() -> {
                view?.setErrorPhone("No. Telp Harus >= 11 Digit")
                false
            }

            else -> {
                view?.setErrorPhone(null)
                true
            }
        }

        view?.enableButtonSubmit(!requiredField.containsValue(false))
    }

    override fun onSubmit() {
        router?.goToSuccessMessage()
    }
}