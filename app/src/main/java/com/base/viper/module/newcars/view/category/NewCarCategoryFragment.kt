package com.base.viper.module.newcars.view.category

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.GridLayoutManager
import com.base.viper.databinding.FragmentNewCarCategoryBinding
import com.base.viper.module.newcars.adapter.RecyclerViewAdapter
import com.base.viper.module.newcars.entity.NewCarConstant
import com.base.viper.module.newcars.entity.NewCarConstant.ContentType.ALL
import com.base.viper.module.newcars.entity.NewCarConstant.ContentType.HATCHBACK
import com.base.viper.module.newcars.entity.NewCarConstant.ContentType.HYBRID
import com.base.viper.module.newcars.entity.NewCarConstant.ContentType.MPV
import com.base.viper.module.newcars.entity.NewCarConstant.ContentType.SEDAN
import com.base.viper.module.newcars.entity.NewCarConstant.ContentType.SPORTS
import com.base.viper.module.newcars.entity.NewCarConstant.ContentType.SUV
import com.base.viper.module.newcars.entity.NewCarCount
import com.base.viper.module.newcars.entity.NewCarSample

class NewCarCategoryFragment(
    private val contentType: NewCarConstant.ContentType,
    private val countData: (NewCarCount) -> Unit,
    private val option: List<String>
) : Fragment(), NewCarCategoryInterface.View, RecyclerViewAdapter.OnItemClickListener {

    private val presenter: NewCarCategoryPresenter = NewCarCategoryPresenter(this)
    private val binding: FragmentNewCarCategoryBinding by lazy {
        FragmentNewCarCategoryBinding.inflate(layoutInflater)
    }
    private val allCarAdapter: RecyclerViewAdapter by lazy {
        RecyclerViewAdapter(RecyclerViewAdapter.ViewType.ALL, option)
    }
    private val hybridCarAdapter: RecyclerViewAdapter by lazy {
        RecyclerViewAdapter(RecyclerViewAdapter.ViewType.HYBRID, option)
    }
    private val mpvCarAdapter: RecyclerViewAdapter by lazy {
        RecyclerViewAdapter(RecyclerViewAdapter.ViewType.MPV, option)
    }
    private val suvCarAdapter: RecyclerViewAdapter by lazy {
        RecyclerViewAdapter(RecyclerViewAdapter.ViewType.SUV, option)
    }
    private val hatchbackCarAdapter: RecyclerViewAdapter by lazy {
        RecyclerViewAdapter(RecyclerViewAdapter.ViewType.HATCHBACK, option)
    }
    private val sedanCarAdapter: RecyclerViewAdapter by lazy {
        RecyclerViewAdapter(RecyclerViewAdapter.ViewType.SEDAN, option)
    }
    private val sportsCarAdapter: RecyclerViewAdapter by lazy {
        RecyclerViewAdapter(RecyclerViewAdapter.ViewType.SPORTS, option)
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        presenter.onCreate(this)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        presenter.onViewCreated(this)
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter.onDestroy()
    }

    override fun <T> onClickedListener(item: T) {
        if (item is NewCarSample) presenter.goToDetailNewCar(item = item)
    }

    override fun setupView() {
        setupNewCarAdapter()
    }

    private fun setupNewCarAdapter() {
        with(binding) {
            rvNewCarCategory.apply {
                adapter = when (contentType) {
                    ALL -> allCarAdapter.apply {
                        setOnItemClickListener(this@NewCarCategoryFragment)
                    }
                    HYBRID -> hybridCarAdapter.apply {
                        setOnItemClickListener(this@NewCarCategoryFragment)
                    }
                    MPV -> mpvCarAdapter.apply {
                        setOnItemClickListener(this@NewCarCategoryFragment)
                    }
                    SUV -> suvCarAdapter.apply {
                        setOnItemClickListener(this@NewCarCategoryFragment)
                    }
                    HATCHBACK -> hatchbackCarAdapter.apply {
                        setOnItemClickListener(this@NewCarCategoryFragment)
                    }
                    SEDAN -> sedanCarAdapter.apply {
                        setOnItemClickListener(this@NewCarCategoryFragment)
                    }
                    SPORTS -> sportsCarAdapter.apply {
                        setOnItemClickListener(this@NewCarCategoryFragment)
                    }
                }
                layoutManager = GridLayoutManager(requireContext(), 2)
            }
        }
    }

    override fun setDataAllCar(data: List<NewCarSample>) {
        if (data.isNotEmpty()) {
            allCarAdapter.setListAllCar(data)
        }
        countData.invoke(NewCarCount(ALL, data.size))
    }

    override fun setDataHybridCar(data: List<NewCarSample>) {
        if (data.isNotEmpty()) {
            hybridCarAdapter.setListHybridCar(data)
        }
        countData.invoke(NewCarCount(HYBRID, data.size))
    }

    override fun setDataMpvCar(data: List<NewCarSample>) {
        if (data.isNotEmpty()) {
            mpvCarAdapter.setListMpvCar(data)
        }
        countData.invoke(NewCarCount(MPV, data.size))
    }

    override fun setDataSuvCar(data: List<NewCarSample>) {
        if (data.isNotEmpty()) {
            suvCarAdapter.setListSuvCar(data)
        }
        countData.invoke(NewCarCount(SUV, data.size))
    }

    override fun setDataHatchBackCar(data: List<NewCarSample>) {
        if (data.isNotEmpty()) {
            hatchbackCarAdapter.setListHatchbackCar(data)
        }
        countData.invoke(NewCarCount(HATCHBACK, data.size))
    }

    override fun setDataSedanCar(data: List<NewCarSample>) {
        if (data.isNotEmpty()) {
            sedanCarAdapter.setListSedanCar(data)
        }
        countData.invoke(NewCarCount(SEDAN, data.size))
    }

    override fun setDataSportsCar(data: List<NewCarSample>) {
        if (data.isNotEmpty()) {
            sportsCarAdapter.setListSportsCar(data)
        }
        countData.invoke(NewCarCount(SPORTS, data.size))
    }
}