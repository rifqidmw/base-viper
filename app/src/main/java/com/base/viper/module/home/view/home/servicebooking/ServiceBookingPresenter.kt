package com.base.viper.module.home.view.home.servicebooking

import android.app.Activity
import com.base.viper.common.Constants

class ServiceBookingPresenter(
    private var view: ServiceBookingInterface.View? = null
) : ServiceBookingInterface.Presenter {
    
    private var interactor: ServiceBookingInterface.Interactor? = null
    private var router: ServiceBookingInterface.Router? = null
    
    override fun onCreate(activity: Activity) {
        view = activity as ServiceBookingActivity
        interactor = ServiceBookingInteractor(activity)
        router = ServiceBookingRouter(activity)

        view?.setupView()
    }
    
    override fun onDestroy() {
        view = null
        router = null
    }

    override fun onSectionToolbar(event: Constants.ToolbarEvent) {
        //Not Yet Implemented
    }
}