package com.base.viper.module.newstips.view

import android.app.Activity
import android.content.Intent
import com.base.viper.utils.Utils.toastShort

class NewsTipsRouter(private val activity: Activity) : NewsTipsInterface.Router {
    override fun goToSearch() {
        activity.toastShort("Todo: Search")
    }

    override fun goToNotification() {
        activity.toastShort("Todo: Notification")
    }

    override fun goToCart() {
        activity.toastShort("Todo: Cart")
    }

    override fun shareWithLink() {
        activity.startActivity(
            Intent.createChooser(
                Intent(Intent.ACTION_SEND).apply {
                    type = "text/plain"
                    putExtra(Intent.EXTRA_TEXT, "Todo: Copy Link")
                }, "Share Using"
            )
        )
    }
}