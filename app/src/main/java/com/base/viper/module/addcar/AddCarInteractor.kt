package com.base.viper.module.addcar

import android.app.Activity
import com.base.viper.services.NetworkBuilder
import com.base.viper.services.Sessions
import io.reactivex.rxjava3.disposables.CompositeDisposable

class AddCarInteractor(activity: Activity) : AddCarInterface.Interactor {
    
    private val disposable = CompositeDisposable()
    private val apiService = NetworkBuilder.apiService
    private val sessions = Sessions(activity)
}