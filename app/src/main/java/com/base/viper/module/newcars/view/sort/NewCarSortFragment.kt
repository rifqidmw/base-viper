package com.base.viper.module.newcars.view.sort

import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import com.base.viper.R
import com.base.viper.common.BaseBottomDialogFragment
import com.base.viper.databinding.FragmentNewCarSortBinding
import com.base.viper.module.newcars.adapter.RecyclerViewAdapter
import com.base.viper.module.newcars.adapter.RecyclerViewAdapter.ViewType.SORT
import com.base.viper.utils.Utils.setOnSingleClickListener
import com.base.viper.utils.Utils.toastShort

class NewCarSortFragment : BaseBottomDialogFragment<FragmentNewCarSortBinding>
    (FragmentNewCarSortBinding::inflate), NewCarSortInterface.View {

    private val presenter: NewCarSortPresenter = NewCarSortPresenter(this)

    private var selectedSortingOption: String? = null
    
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupView()
    }

    override fun setOnCreatePresenter() {
        presenter.onCreate(this)
    }

    override fun setOnViewCreatedPresenter(view: View, savedInstanceState: Bundle?) {
        presenter.onViewCreated(this)
    }

    override fun setOnDestroyPresenter() {
        presenter.onDestroy()
    }

    override fun setupView() = with(binding) {
        val sortedOptions = listOf(
            getString(R.string.title_sort_asc),
            getString(R.string.title_sort_desc),
            getString(R.string.title_sort_best_seller),
            getString(R.string.title_sort_disc),
        )
        val adapter = RecyclerViewAdapter(SORT, sortedOptions) { selectedItem ->
            if (selectedItem != selectedSortingOption) {
                selectedSortingOption = selectedItem
            }
            val toastMessage = when (selectedItem) {
            getString(R.string.title_sort_asc) -> "TODO: Ascending"
            getString(R.string.title_sort_desc) -> "TODO: Descending"
            getString(R.string.title_sort_best_seller) -> "TODO: Best Seller"
            getString(R.string.title_sort_disc) -> "TODO: Discount"
            else -> ""
        }
            requireActivity().toastShort(toastMessage)
        }
        rvSort.adapter = adapter
        rvSort.layoutManager = LinearLayoutManager(requireContext())

        ivClose.setOnSingleClickListener {
            this@NewCarSortFragment.dismiss()
        }
    }
}