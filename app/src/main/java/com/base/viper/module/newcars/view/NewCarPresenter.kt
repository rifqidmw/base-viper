package com.base.viper.module.newcars.view

import android.app.Activity
import com.base.viper.common.Constants
import com.base.viper.module.newcars.entity.NewCarSample
import com.base.viper.utils.Utils.parcelableArrayList

class NewCarPresenter(
    private var view: NewCarInterface.View? = null
) : NewCarInterface.Presenter {

    private var interactor: NewCarInterface.Interactor? = null
    private var router: NewCarInterface.Router? = null
    private var listNewCar: ArrayList<NewCarSample> = ArrayList()

    override fun onCreate(activity: Activity) {
        view = activity as NewCarActivity
        interactor = NewCarInteractor(activity)
        router = NewCarRouter(activity)

        initIntent(activity)
        view?.setupView()
    }

    override fun onDestroy() {
        view = null
        router = null
    }

    override fun onSectionToolbar(event: Constants.ToolbarEvent) {
        when (event) {
            Constants.ToolbarEvent.SEARCH -> router?.goToSearch()
            Constants.ToolbarEvent.NOTIFICATION -> router?.goToNotification()
            Constants.ToolbarEvent.CART -> router?.goToCart()
        }
    }

    override fun onSeeAllNewCar(data: ArrayList<NewCarSample>) {
        router?.goToAllNewCar(data)
    }

    override fun goToFilter() {
        router?.goToFilter()
    }

    override fun goToSort() {
        router?.goToSort()
    }

    private fun initIntent(activity: Activity) = with(activity.intent) {
        listNewCar = parcelableArrayList("LIST_DATA_NEW_CAR") ?: ArrayList()
    }
}