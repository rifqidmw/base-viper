package com.base.viper.module.newcars.view

import android.app.Activity
import com.base.viper.common.Constants
import com.base.viper.module.newcars.entity.NewCarSample

interface NewCarInterface {
    interface View {
        fun setupView()
        fun setDataNewCar(data: ArrayList<NewCarSample>)
    }

    interface Presenter {
        fun onCreate(activity: Activity)
        fun onDestroy()
        fun onSectionToolbar(event: Constants.ToolbarEvent)
        fun onSeeAllNewCar(data: ArrayList<NewCarSample>)
        fun goToFilter()
        fun goToSort()
    }

    interface Interactor

    interface Router {
        fun goToSearch()
        fun goToNotification()
        fun goToCart()
        fun goToAllNewCar(data: ArrayList<NewCarSample>)
        fun goToFilter()
        fun goToSort()
    }
}