package com.base.viper.module.promo.view.detailpromo.bundling

import androidx.fragment.app.Fragment
import com.base.viper.services.NetworkBuilder
import com.base.viper.services.Sessions
import io.reactivex.rxjava3.disposables.CompositeDisposable

class BundlingInteractor(fragment: Fragment) : BundlingInterface.Interactor {

    private val disposable = CompositeDisposable()
    private val apiService = NetworkBuilder.apiService
    private val sessions = fragment.context?.let { Sessions(it) }
}