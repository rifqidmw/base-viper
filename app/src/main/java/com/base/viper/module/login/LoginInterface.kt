package com.base.viper.module.login

import android.app.Activity

interface LoginInterface {
    interface View {
        fun setupView()
    }
    
    interface Presenter {
        fun onCreate(activity: Activity)
        fun onDestroy()

        fun goToAddCar()
        fun goToHome()
        fun goToForgotPassword()
        fun goToTermAndCondition()
        fun goToPrivacyPolicy()
    }

    interface Interactor

    interface Router {
        fun goToAddCar()
        fun goToHome()
        fun goToForgotPassword()
        fun goToTermAndCondition()
        fun goToPrivacyPolicy()
    }
}