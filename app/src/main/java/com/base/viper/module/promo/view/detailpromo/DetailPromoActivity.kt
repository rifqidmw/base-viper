package com.base.viper.module.promo.view.detailpromo

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.core.text.HtmlCompat
import androidx.recyclerview.widget.RecyclerView
import androidx.viewpager2.widget.ViewPager2
import com.base.viper.R
import com.base.viper.common.Constants
import com.base.viper.common.CustomPopUpDialog
import com.base.viper.common.CustomPopUpParam
import com.base.viper.components.CustomToolbar
import com.base.viper.components.promosection.CustomSubSection
import com.base.viper.components.promosection.MapperToDataSubSection.contentToListPromo
import com.base.viper.databinding.ActivityDetailPromoBinding
import com.base.viper.module.promo.adapter.BundlingPagerAdapter
import com.base.viper.module.promo.entity.bundle.BundleSample
import com.base.viper.module.promo.entity.promo.Content
import com.base.viper.module.promo.entity.promo.PromoDetail
import com.base.viper.utils.Utils.dateTimeFormat
import com.base.viper.utils.Utils.formatRupiah
import com.base.viper.utils.Utils.gone
import com.base.viper.utils.Utils.next
import com.base.viper.utils.Utils.previous
import com.base.viper.utils.Utils.setHtmlView
import com.base.viper.utils.Utils.setOnSingleClickListener
import com.base.viper.utils.Utils.visible
import com.bumptech.glide.Glide
import java.math.BigInteger

class DetailPromoActivity : AppCompatActivity(), DetailPromoInterface.View {

    private val presenter: DetailPromoPresenter = DetailPromoPresenter(this)
    private val binding: ActivityDetailPromoBinding by lazy {
        ActivityDetailPromoBinding.inflate(layoutInflater)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)
        presenter.onCreate(this)
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter.onDestroy()
    }

    override fun setupView() {
        setupToolbar()
        setupListener()
    }

    private fun setupListener() = with(binding) {
        ibWhatsapp.setOnSingleClickListener {
            presenter.onSharePromo(Constants.ShareEvent.WHATSAPP)
        }

        ibFacebook.setOnSingleClickListener {
            presenter.onSharePromo(Constants.ShareEvent.FACEBOOK)
        }

        ibTwitter.setOnSingleClickListener {
            presenter.onSharePromo(Constants.ShareEvent.TWITTER)
        }

        ibTiktok.setOnSingleClickListener {
            presenter.onSharePromo(Constants.ShareEvent.TIKTOK)
        }

        ibClipboard.setOnSingleClickListener {
            presenter.onSharePromo(Constants.ShareEvent.LINK)
        }
    }

    private fun setupToolbar() {
        binding.toolbar.setOnClickedListener(object : CustomToolbar.Listener {
            override fun onClickedBackButton() {
                onBackPressedDispatcher.onBackPressed()
            }

            override fun onClickedCart(event: Constants.ToolbarEvent) {
                presenter.onSectionToolbar(event)
            }

            override fun onClickedNotification(event: Constants.ToolbarEvent) {
                presenter.onSectionToolbar(event)
            }

            override fun onClickedSearch(event: Constants.ToolbarEvent) {
                presenter.onSectionToolbar(event)
            }
        })
    }

    override fun setupDataRegular(data: PromoDetail?) = with(binding) {
        clFixedPrice.visible()
        btnBuyBundling.visible()
        btnRequestQuote.gone()
        setupBundling()

        data?.let {
            Glide.with(this@DetailPromoActivity).load(it.heroImageLink).into(ivPromo)
            tvTitlePromo.text = it.titlePage
            tvPeriod.text = String.format(
                getString(R.string.format_period),
                it.startDate?.dateTimeFormat(),
                it.endDate?.dateTimeFormat()
            )
            tvDesc.text = it.detail?.detailContent?.let { it1 ->
                HtmlCompat.fromHtml(
                    it1,
                    HtmlCompat.FROM_HTML_MODE_LEGACY
                )
            }

            ibShare.setOnSingleClickListener {
                // TODO
            }

            btnBuyBundling.setOnSingleClickListener {
                presenter.goToCart()
            }
        }

        return@with
    }

    override fun setupDataNonRegular(data: PromoDetail?) = with(binding) {
        clFixedPrice.gone()
        btnBuyBundling.gone()
        btnRequestQuote.visible()
        clBundling.gone()

        data?.let {
            Glide.with(this@DetailPromoActivity).load(it.heroImageLink).into(ivPromo)
            tvTitlePromo.text = it.titlePage
            tvPeriod.text = String.format(
                getString(R.string.format_period),
                it.startDate?.dateTimeFormat(),
                it.endDate?.dateTimeFormat()
            )
            tvDesc.setHtmlView(it.detail?.detailContent)

            ibShare.setOnSingleClickListener {
                //TODO
            }

            btnRequestQuote.setOnSingleClickListener {
                presenter.goToFormRequestQuote()
            }
        }

        return@with
    }

    private fun setupBundling() {
        val bundleList1 = listOf(
            BundleSample(
                R.drawable.sample_my_car_1,
                "All New Vios 1.5 E M/T, 2021",
                "White",
                "310000000"
            ),
            BundleSample(
                R.drawable.sample_my_car_2,
                "All New Veloz 1.5 E M/T, 2022",
                "White",
                "320000000"
            ),
            BundleSample(
                R.drawable.sample_new_car_sedan_1,
                "All New Vios 1.5 E M/T, 2022",
                "White",
                "330000000"
            )
        )
        binding.vpBundle1.apply {
            offscreenPageLimit = bundleList1.size
            adapter = BundlingPagerAdapter(
                bundleList1, supportFragmentManager, lifecycle
            )
            (getChildAt(0) as RecyclerView).overScrollMode = RecyclerView.OVER_SCROLL_NEVER
            registerOnPageChangeCallback(object : ViewPager2.OnPageChangeCallback() {
                override fun onPageSelected(position: Int) {
                    presenter.bundlePrice1 = bundleList1[position].bundlePrice
                    presenter.simulatePrice()
                }
            })
        }

        binding.ibNextBundle1.setOnSingleClickListener {
            binding.vpBundle1.next()
        }
        binding.ibPreviousBundle1.setOnSingleClickListener {
            binding.vpBundle1.previous()
        }

        val bundleList2 = listOf(
            BundleSample(
                R.drawable.sample_accecories,
                "Trunk Spoiler",
                "White",
                "16000000"
            ),
            BundleSample(
                R.drawable.sample_accecories,
                "Trunk Spoiler",
                "White",
                "15999000"
            ),
            BundleSample(
                R.drawable.sample_accecories,
                "Trunk Spoiler",
                "White",
                "12000000"
            )
        )
        binding.vpBundle2.apply {
            offscreenPageLimit = bundleList2.size
            adapter = BundlingPagerAdapter(
                bundleList2, supportFragmentManager, lifecycle
            )
            (getChildAt(0) as RecyclerView).overScrollMode = RecyclerView.OVER_SCROLL_NEVER
            registerOnPageChangeCallback(object : ViewPager2.OnPageChangeCallback() {
                override fun onPageSelected(position: Int) {
                    presenter.bundlePrice2 = bundleList2[position].bundlePrice
                    presenter.simulatePrice()
                }
            })
        }

        binding.ibNextBundle2.setOnSingleClickListener {
            binding.vpBundle2.next()
        }
        binding.ibPreviousBundle2.setOnSingleClickListener {
            binding.vpBundle2.previous()
        }

        presenter.bundlePrice1 = bundleList1.first().bundlePrice
        presenter.bundlePrice2 = bundleList2.first().bundlePrice
        presenter.simulatePrice()
    }

    override fun setupSubPromo(promoList: List<Content>?) = with(binding) {
        val listDataPromo = promoList.contentToListPromo(this@DetailPromoActivity)
        if (listDataPromo.isNullOrEmpty()) cpsPromo.gone()
        else {
            cpsPromo.setDataPromo(listDataPromo)
            cpsPromo.setOnClickedListener(object : CustomSubSection.Listener {
                override fun onSubTitleClicked() {
                    presenter.goToAllPromo()
                }

                override fun onItemClicked(position: Int) {
                    presenter.goToDetailPromo(promoList?.get(position)?.slug)
                }
            })
        }
    }

    override fun fragmentResultListener(promoList: List<Content>?) {
        val listDataPromo = promoList.contentToListPromo(this@DetailPromoActivity)
        supportFragmentManager.setFragmentResultListener(
            "SUCCESS_INQUIRY",
            this@DetailPromoActivity
        ) { _, bundle ->
            val status = bundle.getString("status")
            if (status == "success" && !listDataPromo.isNullOrEmpty()) {
                val param = CustomPopUpParam.SuccessMessageParam(
                    title = getString(R.string.title_inquiry_success),
                    description = getString(R.string.desc_inquiry_success),
                    actionButton = getString(R.string.title_label_inquiry_go_to_home).uppercase(),
                    cancelable = false,
                    sectionTitle = getString(R.string.title_label_inquiry_other_promo).uppercase(),
                    sectionSubTitle = getString(R.string.title_label_inquiry_see_all).uppercase(),
                    sectionData = listDataPromo,
                    onClickedActionButton = {
                        presenter.goToHome()
                    },
                    onClickedItemSection = {
                        presenter.goToDetailPromo(promoList?.get(it)?.slug)
                    },
                    onClickedSectionSubTitle = {
                        presenter.goToAllPromo()
                    },
                    onDismissListener = null
                )
                CustomPopUpDialog(this@DetailPromoActivity).popUpSuccessMessage(param)
            }
        }
    }

    override fun setCalculationResult(
        afterDiscount: BigInteger,
        beforeDiscountPrice: BigInteger,
        priceDifference: BigInteger
    ) = with(binding) {
        tvBundlingPrice.text = String.format(
            getString(R.string.format_rupiah),
            afterDiscount.toString().formatRupiah()
        )
        tvPreviousPrice.text = String.format(
            getString(R.string.format_rupiah),
            beforeDiscountPrice.toString().formatRupiah()
        )
        tvSaved.text = "Hemat Rp. ${priceDifference.toString().formatRupiah()}"

        tvFixedBundlingPrice.text = String.format(
            getString(R.string.format_rupiah),
            afterDiscount.toString().formatRupiah()
        )
    }
}