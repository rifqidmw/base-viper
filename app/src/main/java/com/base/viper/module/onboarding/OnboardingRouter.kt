package com.base.viper.module.onboarding

import android.app.Activity
import android.content.Intent
import com.base.viper.module.login.LoginActivity
import com.base.viper.utils.Utils.toastShort

class OnboardingRouter(private val activity: Activity) : OnboardingInterface.Router {
    override fun goToLogin() {
        activity.startActivity(Intent(activity.applicationContext, LoginActivity::class.java))
        activity.finish()
    }

    override fun goToRegister() {
        activity.toastShort("TODO: Register")
    }
}