package com.base.viper.module.productknowledge.entity

data class ProductPromoSample(
    val image: Int,
    val title: String,
    val date: String
)
