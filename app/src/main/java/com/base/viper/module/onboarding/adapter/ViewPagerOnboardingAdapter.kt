package com.base.viper.module.onboarding.adapter

import android.content.Context
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.base.viper.R
import com.base.viper.module.onboarding.onboardingcontent.OnboardingContentFragment

class ViewPagerOnboardingAdapter(
    fragmentActivity: FragmentActivity,
    private val context: Context
): FragmentStateAdapter(fragmentActivity) {
    override fun getItemCount(): Int = 3

    override fun createFragment(position: Int): Fragment {
        return when(position){
            0 -> {
                OnboardingContentFragment(
                    context.getString(R.string.title_label_onboarding_1),
                    R.drawable.sample_onboarding_1
                )
            }
            1 -> {
                OnboardingContentFragment(
                    context.getString(R.string.title_label_onboarding_2),
                    R.drawable.sample_onboarding_2
                )
            }
            else -> {
                OnboardingContentFragment(
                    context.getString(R.string.title_label_onboarding_3),
                    R.drawable.sample_onboarding_3
                )
            }
        }
    }
}