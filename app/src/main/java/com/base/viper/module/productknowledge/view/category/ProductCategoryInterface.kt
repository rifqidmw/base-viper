package com.base.viper.module.productknowledge.view.category

import androidx.fragment.app.Fragment
import com.base.viper.module.productknowledge.entity.ProductKnowledge

interface ProductCategoryInterface {
    interface View {
        fun setupView()
        fun setDataProductKnowledge(data: List<ProductKnowledge.Content>)
        fun setDataHighlight(data: ProductKnowledge.Content)
        fun setFooterLoading(isLoading: Boolean)
    }

    interface Presenter {
        fun onCreate(fragment: Fragment)
        fun onViewCreated(fragment: Fragment)
        fun onDestroy()

        fun getDataProductKnowledge(firstLoad: Boolean = false)
        fun goToDetail(item: ProductKnowledge.Content)
    }

    interface Interactor {
        fun getDataProductKnowledge(page: Int, size: Int, contentCategory: String,
                            onSuccess: (ProductKnowledge) -> Unit,
                            onError: (Throwable) -> Unit
        )
    }

    interface Router {
        fun goToDetailProduct(item: ProductKnowledge.Content)
    }
}