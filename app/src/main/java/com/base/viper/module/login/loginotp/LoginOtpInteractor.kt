package com.base.viper.module.login.loginotp

import android.app.Activity
import com.base.viper.common.Constants
import com.base.viper.services.NetworkBuilder
import com.base.viper.services.Sessions
import io.reactivex.rxjava3.disposables.CompositeDisposable

class LoginOtpInteractor(activity: Activity) : LoginOtpInterface.Interactor {

    private val disposable = CompositeDisposable()
    private val apiService = NetworkBuilder.apiService
    private val sessions = Sessions(activity)

    override fun setToken(token: String) {
        sessions.putString(Constants.Sessions.TOKEN, token)
    }
}