package com.base.viper.module.newstips.view.detailnewstips

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import com.base.viper.common.Constants
import com.base.viper.module.home.view.MainActivity
import com.base.viper.module.newstips.common.NewsTipsConstant
import com.base.viper.module.newstips.view.NewsTipsActivity
import com.base.viper.module.newstips.view.inquiry.InquiryNewsTipsFragment
import com.base.viper.module.promo.view.PromoActivity
import com.base.viper.module.promo.view.detailpromo.DetailPromoActivity
import com.base.viper.utils.Utils.share
import com.base.viper.utils.Utils.toastShort

class DetailNewsTipsRouter(private val activity: AppCompatActivity) :
    DetailNewsTipsInterface.Router {
    override fun goToSearch() {
        activity.toastShort("Todo: Search")
    }

    override fun goToNotification() {
        activity.toastShort("Todo: Notification")
    }

    override fun goToCart() {
        activity.toastShort("Todo: Cart")
    }

    override fun goToInquiry() {
        val bottomSheet = InquiryNewsTipsFragment(Constants.InquiryType.NEWS_AND_TIPS)
        bottomSheet.show(activity.supportFragmentManager, "INQUIRY")
    }

    override fun goToHome() = with(activity) {
        startActivity(
            Intent(this, MainActivity::class.java).apply {
                addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TOP)
            }
        )
        finish()
    }

    override fun shareViaWhatsapp() {
        activity.share(
            "Todo: Copy Link",
            Constants.ShareType.WHATSAPP,
            Constants.ShareType.WHATSAPP_BUSINESS
        )
    }

    override fun shareViaFacebook() {
        activity.share("Todo: Copy Link", Constants.ShareType.FACEBOOK)
    }

    override fun shareViaTwitter() {
        activity.share("Todo: Copy Link", Constants.ShareType.TWITTER)
    }

    override fun shareViaTiktok() {
        activity.share("Todo: Copy Link", Constants.ShareType.TIKTOK)
    }

    override fun shareWithLink() {
        activity.startActivity(
            Intent.createChooser(
                Intent(Intent.ACTION_SEND).apply {
                    type = "text/plain"
                    putExtra(Intent.EXTRA_TEXT, "Todo: Copy Link")
                }, "Share Using"
            )
        )
    }

    override fun goToAllPromo() {
        activity.startActivity(
            Intent(
                activity.applicationContext,
                PromoActivity::class.java
            ).apply {
                addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TOP)
            })
        activity.finish()
    }

    override fun goToAllNews(origin: NewsTipsConstant.OriginEvent) {
        when(origin){
            NewsTipsConstant.OriginEvent.NEWSTIPS -> {
                activity.finish()
            }

            NewsTipsConstant.OriginEvent.HOME -> {
                activity.startActivity(Intent(activity.applicationContext, NewsTipsActivity::class.java))
                activity.finish()
            }
        }

    }

    override fun goToDetailPromo(slug: String?) = with(activity) {
        startActivity(
            Intent(this, DetailPromoActivity::class.java).apply {
                addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TOP)
                putExtra("PROMO_SLUG", slug)
            }
        )
        finish()
    }

    override fun goToDetailNewsTips(slug: String?) = with(activity) {
        startActivity(
            Intent(this, DetailNewsTipsActivity::class.java).apply {
                addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TOP)
                putExtra(NewsTipsConstant.SLUG, slug)
            }
        )
        finish()
    }
}