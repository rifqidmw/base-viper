package com.base.viper.module.newcars.view.sort

import androidx.fragment.app.Fragment

interface NewCarSortInterface {
    interface View {
        fun setupView()
    }

    interface Presenter {
        fun onCreate(fragment: Fragment)
        fun onViewCreated(fragment: Fragment)
        fun onDestroy()
    }

    interface Interactor

    interface Router
}