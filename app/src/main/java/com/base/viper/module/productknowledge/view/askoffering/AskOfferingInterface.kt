package com.base.viper.module.productknowledge.view.askoffering

import androidx.fragment.app.Fragment

interface AskOfferingInterface {
    interface View {

        fun setupView()
        fun setErrorFullName(message: String?)
        fun setErrorEmail(message: String?)
        fun setErrorPhone(message: String?)
        fun enableButtonSubmit(isEnable: Boolean)
    }

    interface Presenter {
        fun onCreate(fragment: Fragment)
        fun onViewCreated(fragment: Fragment)
        fun onDestroy()
        fun validateInput()
        fun onSubmit()
    }

    interface Interactor

    interface Router {
        fun goToSuccessMessage()
    }
}