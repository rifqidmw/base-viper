package com.base.viper.module.newstips.entity.inquiry


import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize

@Parcelize
data class Province(
    @SerializedName("id")
    var id: Int? = null,
    @SerializedName("isocode")
    var isocode: String? = null,
    @SerializedName("name")
    var name: String? = null
) : Parcelable