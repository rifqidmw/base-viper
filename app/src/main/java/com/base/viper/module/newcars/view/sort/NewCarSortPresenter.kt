package com.base.viper.module.newcars.view.sort

import androidx.fragment.app.Fragment

class NewCarSortPresenter(
    private var view: NewCarSortInterface.View? = null
) : NewCarSortInterface.Presenter {

    private var interactor: NewCarSortInterface.Interactor? = null
    private var router: NewCarSortInterface.Router? = null

    override fun onCreate(fragment: Fragment) {
        view = fragment as NewCarSortFragment
        interactor = NewCarSortInteractor(fragment)
        router = NewCarSortRouter(fragment)
    }
    
    override fun onViewCreated(fragment: Fragment) {
        view?.setupView()
    }

    override fun onDestroy() {
        view = null
        router = null
    }
}