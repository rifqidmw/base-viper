package com.base.viper.module.home.view.home.trendingcars

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import com.base.viper.databinding.FragmentTrendingCarBinding
import com.base.viper.module.home.entity.TrendingCarSample

class TrendingCarFragment(private var trendingCarSample: TrendingCarSample) : Fragment() {

    private val binding: FragmentTrendingCarBinding by lazy {
        FragmentTrendingCarBinding.inflate(layoutInflater)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.ivCars.setImageDrawable(
            ContextCompat.getDrawable(
                requireContext(), trendingCarSample.image
            )
        )
    }
}