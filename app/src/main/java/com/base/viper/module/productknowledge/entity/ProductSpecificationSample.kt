package com.base.viper.module.productknowledge.entity

data class ProductSpecificationSample(
    val title: String,
    val desc: String,
    val image: Int
)
