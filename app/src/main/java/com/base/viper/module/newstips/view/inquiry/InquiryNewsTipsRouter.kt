package com.base.viper.module.newstips.view.inquiry

import androidx.core.os.bundleOf
import androidx.fragment.app.setFragmentResult
import com.google.android.material.bottomsheet.BottomSheetDialogFragment

class InquiryNewsTipsRouter(private val fragment: BottomSheetDialogFragment) : InquiryNewsTipsInterface.Router {
    override fun goToSuccessMessage(title: String, description: String, seemore: String) {
        fragment.setFragmentResult("SUCCESS_INQUIRY", bundleOf("status" to "success"))
        fragment.dismiss()
    }
}