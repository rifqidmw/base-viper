package com.base.viper.module.newcars.view.filter

import androidx.fragment.app.Fragment
import com.base.viper.utils.Utils.toastShort

class NewCarFilterRouter(private val fragment: Fragment) : NewCarFilterInterface.Router {
    override fun goToApplyFilter() {
        fragment.requireActivity().toastShort("TODO: Apply Filter")
    }
}