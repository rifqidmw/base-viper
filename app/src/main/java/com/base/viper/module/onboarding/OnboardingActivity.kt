package com.base.viper.module.onboarding

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.base.viper.R
import com.base.viper.databinding.ActivityOnboardingBinding
import com.base.viper.module.onboarding.adapter.ViewPagerOnboardingAdapter
import com.base.viper.utils.Utils.invisible
import com.base.viper.utils.Utils.setOnSingleClickListener
import com.base.viper.utils.Utils.setStatusBarColor
import com.base.viper.utils.Utils.visible
import com.google.android.material.tabs.TabLayout
import com.google.android.material.tabs.TabLayoutMediator

class OnboardingActivity : AppCompatActivity(), OnboardingInterface.View {
    
    private val presenter: OnboardingPresenter = OnboardingPresenter(this)
    private val binding: ActivityOnboardingBinding by lazy {
        ActivityOnboardingBinding.inflate(layoutInflater)
    }
    
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)
        presenter.onCreate(this)
    }
    
    override fun onDestroy() {
        super.onDestroy()
        presenter.onDestroy()
    }

    override fun setupView() = with(binding) {
        setStatusBarColor(R.color.reliable_black, false)
        setupAdapter()

        btnLogin.setOnSingleClickListener {
            presenter.goToLogin()
        }

        btnRegister.setOnSingleClickListener {
            presenter.goToRegister()
        }
    }

    private fun setupAdapter() = with(binding){
        vpOnboarding.offscreenPageLimit = 3
        vpOnboarding.adapter = ViewPagerOnboardingAdapter(this@OnboardingActivity, this@OnboardingActivity)
        TabLayoutMediator(tlOnboarding, vpOnboarding) { _, _ -> }.attach()

        tlOnboarding.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabSelected(tab: TabLayout.Tab?) {
                if (tab?.position == 2){
                    lyButton.visible()
                } else {
                    lyButton.invisible()
                }
            }

            override fun onTabUnselected(tab: TabLayout.Tab?) {
                //Not Implemented
            }

            override fun onTabReselected(tab: TabLayout.Tab?) {
                //Not Implemented
            }

        })
    }
}