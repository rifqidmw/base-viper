package com.base.viper.module.newstips.view.inquiry

import androidx.fragment.app.Fragment
import com.base.viper.common.Constants
import com.base.viper.module.newstips.entity.inquiry.Branch
import com.base.viper.module.newstips.entity.inquiry.City
import com.base.viper.module.newstips.entity.inquiry.Product
import com.base.viper.module.newstips.entity.inquiry.Province

interface InquiryNewsTipsInterface {
    interface View {
        fun setupView()
        fun showBottomsheetProvince(data: List<Province>)
        fun showBottomsheetCity(data: List<City>)
        fun showBottomsheetCarModel(data: List<Product>)
        fun showBottomsheetBranch(data: List<Branch.Content>)
        fun setErrorFullName(message: String?)
        fun setErrorPhone(message: String?)
        fun setErrorProvince(message: String?)
        fun setErrorCity(message: String?)
        fun setErrorCarModel(message: String?)
        fun setErrorTnC(message: String?)
        fun enableButtonSubmit()
        fun disableButtonSubmit()
    }

    interface Presenter {
        fun onCreate(fragment: Fragment)
        fun onViewCreated(fragment: Fragment)
        fun onDestroy()
        fun showBottomsheetProvince()
        fun showBottomsheetCity(isocode: String?)
        fun showBottomsheetCarModel()
        fun showBottomsheetBranch()
        fun onSubmit(fragment: Fragment, type: Constants.InquiryType)
    }

    interface Interactor {
        fun getDataProvince(
            onSuccess: (List<Province>) -> Unit,
            onError: (Throwable) -> Unit)

        fun getDataCity(isocode: String,
                        onSuccess: (List<City>) -> Unit,
                        onError: (Throwable) -> Unit)

        fun getDataCarModel(
            onSuccess: (List<Product>) -> Unit,
            onError: (Throwable) -> Unit
        )

        fun getDataBranch(
            city: String,
            onSuccess: (Branch) -> Unit,
            onError: (Throwable) -> Unit
        )
    }

    interface Router {
        fun goToSuccessMessage(
            title: String,
            description: String,
            seemore: String
        )
    }
}