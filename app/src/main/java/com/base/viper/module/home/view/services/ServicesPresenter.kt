package com.base.viper.module.home.view.services

import androidx.fragment.app.Fragment

class ServicesPresenter(
    private var view: ServicesInterface.View? = null
) : ServicesInterface.Presenter {

    private var interactor: ServicesInterface.Interactor? = null
    private var router: ServicesInterface.Router? = null

    override fun onCreate(fragment: Fragment) {
        view = fragment as ServicesFragment
        interactor = ServicesInteractor(fragment)
        router = ServicesRouter(fragment)
    }
    
    override fun onViewCreated(fragment: Fragment) {
        //Not Yet Implemented
    }

    override fun onDestroy() {
        view = null
        router = null
    }
}