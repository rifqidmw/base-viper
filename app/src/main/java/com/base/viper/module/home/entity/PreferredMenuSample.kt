package com.base.viper.module.home.entity

enum class Menu {
    SERVICE_COUPON,
    ACCESSORIES,
    SERVICE_PACKAGE,
    TRADE_IN,
    TRACKING_ORDERS,
    ROAD_SIDE_ASSISTANCE
}

data class PreferredMenuSample(
    val menuCode: Menu,
    var icon: Int,
    var menuName: String
)