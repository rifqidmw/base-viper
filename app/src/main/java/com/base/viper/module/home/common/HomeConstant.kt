package com.base.viper.module.home.common

object HomeConstant {
    const val FRAGMENT_TAG = "FRAGMENT_TAG"

    enum class TrendingCarEvent {
        COMPARE, TEST_DRIVE, INQUIRE, EXPLORE
    }

    enum class FragmentTag {
        INITIALIZE,
        SERVICES,
        HOME,
        ACCOUNT,
        CHAT,
        MENU
    }
}