package com.base.viper.module.promo.view

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.res.ResourcesCompat
import com.base.viper.R
import com.base.viper.common.Constants
import com.base.viper.components.CustomToolbar
import com.base.viper.databinding.ActivityPromoBinding
import com.base.viper.databinding.CustomTabBinding
import com.base.viper.module.promo.adapter.ViewPagerAdapter
import com.base.viper.module.promo.entity.promo.Content
import com.base.viper.module.promo.entity.promo.PromoCategory
import com.google.android.material.tabs.TabLayout
import com.google.android.material.tabs.TabLayoutMediator

class PromoActivity : AppCompatActivity(), PromoInterface.View {

    private val presenter: PromoPresenter = PromoPresenter(this)
    private val binding: ActivityPromoBinding by lazy {
        ActivityPromoBinding.inflate(layoutInflater)
    }
    private lateinit var promoAdapter: ViewPagerAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)
        presenter.onCreate(this)
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter.onDestroy()
    }

    override fun setupView() {
        setupToolbar()
    }

    private fun setupToolbar() {
        binding.toolbar.setOnClickedListener(object : CustomToolbar.Listener {
            override fun onClickedBackButton() {
                onBackPressedDispatcher.onBackPressed()
            }

            override fun onClickedCart(event: Constants.ToolbarEvent) {
                presenter.onSectionToolbar(event)
            }

            override fun onClickedNotification(event: Constants.ToolbarEvent) {
                presenter.onSectionToolbar(event)
            }

            override fun onClickedSearch(event: Constants.ToolbarEvent) {
                presenter.onSectionToolbar(event)
            }
        })
    }

    override fun setupTabLayout(promoCategories: List<PromoCategory>) = with(binding) {
        promoAdapter = ViewPagerAdapter(promoCategories, supportFragmentManager, lifecycle)

        vpPromo.offscreenPageLimit = promoCategories.size
        vpPromo.adapter = promoAdapter

        TabLayoutMediator(tlPromo, vpPromo) { tab, position ->
            val tabViewBinding = CustomTabBinding.inflate(layoutInflater)
            if (position == 0) {
                tabViewBinding.tvTabText.typeface = ResourcesCompat.getFont(
                    this@PromoActivity,
                    R.font.montserrat_700_bold
                )
            }
            tabViewBinding.tvTabText.text = promoCategories[position].name
            tab.customView = tabViewBinding.root
        }.attach()

        tlPromo.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabSelected(tab: TabLayout.Tab?) {
                tab?.let {
                    val tabViewBinding = CustomTabBinding.bind(it.customView!!)
                    tabViewBinding.tvTabText.typeface = ResourcesCompat.getFont(
                        this@PromoActivity,
                        R.font.montserrat_700_bold
                    )
                }
            }

            override fun onTabUnselected(tab: TabLayout.Tab?) {
                tab?.let {
                    val tabViewBinding = CustomTabBinding.bind(tab.customView!!)
                    tabViewBinding.tvTabText.typeface = ResourcesCompat.getFont(
                        this@PromoActivity,
                        R.font.montserrat_400_regular
                    )
                }
            }

            override fun onTabReselected(tab: TabLayout.Tab?) {
                //Not Yet Implemented
            }
        })

        tlPromo.getTabAt(0)?.select()
    }

    override fun setDataPromo(data: List<Content>?) {
        if (data.isNullOrEmpty()) {
            //Todo Negative Case
        } else {
            promoAdapter.setDataListPromo(data)
        }
    }
}