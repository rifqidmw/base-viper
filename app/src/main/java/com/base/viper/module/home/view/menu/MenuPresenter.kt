package com.base.viper.module.home.view.menu

import androidx.fragment.app.Fragment

class MenuPresenter(
    private var view: MenuInterface.View? = null
) : MenuInterface.Presenter {

    private var interactor: MenuInterface.Interactor? = null
    private var router: MenuInterface.Router? = null

    override fun onCreate(fragment: Fragment) {
        view = fragment as MenuFragment
        interactor = MenuInteractor(fragment)
        router = MenuRouter(fragment)
    }
    
    override fun onViewCreated(fragment: Fragment) {
        //Not Yet Implemented
    }

    override fun onDestroy() {
        view = null
        router = null
    }
}