package com.base.viper.module.onboarding.onboardingcontent

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import com.base.viper.databinding.FragmentOnboardingContentBinding

class OnboardingContentFragment(
    private var title: String? = "",
    private var imageResource: Int = 0
) : Fragment(), OnboardingContentInterface.View {

    private val presenter: OnboardingContentPresenter = OnboardingContentPresenter(this)
    private val binding: FragmentOnboardingContentBinding by lazy {
        FragmentOnboardingContentBinding.inflate(layoutInflater)
    }
    
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        presenter.onCreate(this)
    }
    
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        presenter.onViewCreated(this)
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter.onDestroy()
    }

    override fun setupView() = with(binding) {
        ivOnboarding.setImageDrawable(
            ContextCompat.getDrawable(requireContext(), imageResource)
        )
        tvTitle.text = title
    }
}