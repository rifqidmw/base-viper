package com.base.viper.module.promo.view

import android.app.Activity
import com.base.viper.utils.Utils.toastShort

class PromoRouter(private val activity: Activity) : PromoInterface.Router {
    override fun goToSearch() {
        activity.toastShort("Todo: Search")
    }

    override fun goToNotification() {
        activity.toastShort("Todo: Notification")
    }

    override fun goToCart() {
        activity.toastShort("Todo: Cart")
    }
}