package com.base.viper.module.promo.view.category

import android.content.Intent
import androidx.fragment.app.Fragment
import com.base.viper.module.promo.entity.promo.Content
import com.base.viper.module.promo.view.detailpromo.DetailPromoActivity

class PromoCategoryRouter(private val fragment: Fragment) : PromoCategoryInterface.Router {
    override fun goToDetailPromo(item: Content) {
        fragment.requireActivity().startActivity(
            Intent(fragment.context, DetailPromoActivity::class.java).apply {
                putExtra("PROMO_SLUG", item.slug)
            }
        )
    }
}