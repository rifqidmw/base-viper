package com.base.viper.module.mycardetails.view

import android.os.Bundle
import androidx.fragment.app.Fragment
import com.base.viper.R
import com.base.viper.module.home.entity.MyCarsSample
import com.base.viper.module.mycardetails.common.MyCarDetailConstant
import com.base.viper.module.mycardetails.common.MyCarDetailConstant.Event.ACCESSORIES
import com.base.viper.module.mycardetails.common.MyCarDetailConstant.Event.BODY_PAINT
import com.base.viper.module.mycardetails.common.MyCarDetailConstant.Event.PROMOS
import com.base.viper.module.mycardetails.common.MyCarDetailConstant.Event.SERVICE_PACKAGE
import com.base.viper.module.mycardetails.common.MyCarDetailConstant.Event.THS
import com.base.viper.module.mycardetails.common.MyCarDetailConstant.Event.WORKSHOP_SERVICE
import com.base.viper.utils.Utils.parcelable
import java.util.Calendar

class MyCarDetailPresenter(
    private var view: MyCarDetailInterface.View? = null
) : MyCarDetailInterface.Presenter {

    private var interactor: MyCarDetailInterface.Interactor? = null
    private var router: MyCarDetailInterface.Router? = null
    private var dataMyCar: MyCarsSample? = null

    override fun onCreate(fragment: Fragment) {
        view = fragment as MyCarDetailFragment
        interactor = MyCarDetailInteractor(fragment)
        router = MyCarDetailRouter(fragment)

        initIntent(fragment.arguments)
    }

    override fun onViewCreated(fragment: Fragment) {
        setTheme()
        view?.setupView(dataMyCar)
    }

    override fun onDestroy() {
        view = null
        router = null
    }

    private fun initIntent(bundle: Bundle?) = with(bundle) {
        dataMyCar = this?.parcelable("DATA_MY_CAR")
    }

    private fun setTheme() {
        when (Calendar.getInstance()[Calendar.HOUR_OF_DAY]) {
            in 19..23 -> view?.myCarTheme(
                R.color.grey_12,
                R.drawable.bg_linear_grey,
                isLightColor = false,
            )

            else -> view?.myCarTheme(
                R.color.reliable_pink,
                R.drawable.bg_linear_pink,
                isLightColor = false
            )
        }
    }

    override fun onClickedSectionBookingServices(event: MyCarDetailConstant.Event) {
        when (event) {
            WORKSHOP_SERVICE -> router?.goToWorkShopService()
            THS -> router?.goToThs()
            BODY_PAINT -> router?.goToBodyPaint()

            ACCESSORIES -> router?.goToAccessories()
            SERVICE_PACKAGE -> router?.goToServicePackage()
            PROMOS -> router?.goToPromos()
        }
    }
}