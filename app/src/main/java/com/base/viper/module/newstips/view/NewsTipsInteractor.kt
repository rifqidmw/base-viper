package com.base.viper.module.newstips.view

import android.app.Activity
import com.base.viper.common.Constants
import com.base.viper.module.newstips.entity.newstips.NewsTipsCategory
import com.base.viper.services.NetworkBuilder
import com.base.viper.services.Sessions
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.disposables.CompositeDisposable
import io.reactivex.rxjava3.schedulers.Schedulers

class NewsTipsInteractor(val activity: Activity) : NewsTipsInterface.Interactor {
    
    private val disposable = CompositeDisposable()
    private val apiService = NetworkBuilder.apiService
    private val sessions = Sessions(activity)
    override fun getDataCategoryNewsTips(
        onSuccess: (List<NewsTipsCategory>) -> Unit,
        onError: (Throwable) -> Unit
    ) {
        apiService.getNewsTipsCategory(Constants.VersionType.V1.value, "newstips")
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(onSuccess, onError)
            .let(disposable::add)
    }
}