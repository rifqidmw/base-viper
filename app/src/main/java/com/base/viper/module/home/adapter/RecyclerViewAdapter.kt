package com.base.viper.module.home.adapter

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.core.text.HtmlCompat
import androidx.recyclerview.widget.RecyclerView
import com.base.viper.R
import com.base.viper.common.BaseViewHolder
import com.base.viper.databinding.ItemBranchLocationBinding
import com.base.viper.databinding.ItemListPromoBinding
import com.base.viper.databinding.ItemMyCarBinding
import com.base.viper.databinding.ItemNewsAndTipsBinding
import com.base.viper.databinding.ItemPreferredCategoryBinding
import com.base.viper.databinding.ItemServiceDueBinding
import com.base.viper.module.home.adapter.RecyclerViewAdapter.ViewType.BRANCH_LOCATION
import com.base.viper.module.home.adapter.RecyclerViewAdapter.ViewType.MENU
import com.base.viper.module.home.adapter.RecyclerViewAdapter.ViewType.MY_CARS
import com.base.viper.module.home.adapter.RecyclerViewAdapter.ViewType.NEWS_AND_TIPS
import com.base.viper.module.home.adapter.RecyclerViewAdapter.ViewType.PROMO
import com.base.viper.module.home.adapter.RecyclerViewAdapter.ViewType.SERVICE_DUE
import com.base.viper.module.home.entity.BranchLocationSample
import com.base.viper.module.home.entity.MyCarsSample
import com.base.viper.module.home.entity.PreferredMenuSample
import com.base.viper.module.home.entity.ServiceDueSample
import com.base.viper.module.newstips.entity.newstips.NewsTips
import com.base.viper.module.promo.entity.promo.Content
import com.base.viper.utils.Utils.setImageViewUrl
import com.base.viper.utils.Utils.setOnSingleClickListener
import com.bumptech.glide.Glide

class RecyclerViewAdapter(private val type: ViewType) : RecyclerView.Adapter<BaseViewHolder>() {

    enum class ViewType {
        SERVICE_DUE, MY_CARS, MENU, BRANCH_LOCATION, PROMO, NEWS_AND_TIPS
    }

    private val listServiceDue: MutableList<ServiceDueSample> = mutableListOf()
    private val listMyCars: MutableList<MyCarsSample> = mutableListOf()
    private val listMenu: MutableList<PreferredMenuSample> = mutableListOf()
    private val listBranch: MutableList<BranchLocationSample> = mutableListOf()
    private val listPromo: MutableList<Content?> = mutableListOf()
    private val listNewsAndTips: MutableList<NewsTips.Content> = mutableListOf()

    @SuppressLint("NotifyDataSetChanged")
    fun setListServiceDue(list: List<ServiceDueSample>) {
        listServiceDue.clear()
        listServiceDue.addAll(list)
        notifyDataSetChanged()
    }

    @SuppressLint("NotifyDataSetChanged")
    fun setListMyCars(list: List<MyCarsSample>) {
        listMyCars.clear()
        listMyCars.addAll(list)
        notifyDataSetChanged()
    }

    @SuppressLint("NotifyDataSetChanged")
    fun setListMenu(list: List<PreferredMenuSample>) {
        listMenu.clear()
        listMenu.addAll(list)
        notifyDataSetChanged()
    }

    @SuppressLint("NotifyDataSetChanged")
    fun setListBranch(list: List<BranchLocationSample>) {
        listBranch.clear()
        listBranch.addAll(list)
        notifyDataSetChanged()
    }

    @SuppressLint("NotifyDataSetChanged")
    fun setListPromo(list: List<Content?>) {
        listPromo.clear()
        listPromo.addAll(list)
        notifyDataSetChanged()
    }

    @SuppressLint("NotifyDataSetChanged")
    fun setListNewsAndTips(list: List<NewsTips.Content>) {
        listNewsAndTips.clear()
        listNewsAndTips.addAll(list)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder {
        return when (viewType) {
            SERVICE_DUE.ordinal -> ServiceDueHolder(
                ItemServiceDueBinding.inflate(
                    LayoutInflater.from(parent.context), parent, false
                )
            )

            MY_CARS.ordinal -> MyCarViewHolder(
                ItemMyCarBinding.inflate(
                    LayoutInflater.from(parent.context), parent, false
                )
            )

            MENU.ordinal -> PreferredMenuHolder(
                ItemPreferredCategoryBinding.inflate(
                    LayoutInflater.from(parent.context), parent, false
                )
            )

            BRANCH_LOCATION.ordinal -> BranchLocationHolder(
                ItemBranchLocationBinding.inflate(
                    LayoutInflater.from(parent.context), parent, false
                )
            )

            PROMO.ordinal -> PromoHolder(
                ItemListPromoBinding.inflate(
                    LayoutInflater.from(parent.context), parent, false
                )
            )

            NEWS_AND_TIPS.ordinal -> NewsAndTipsHolder(
                ItemNewsAndTipsBinding.inflate(
                    LayoutInflater.from(parent.context), parent, false
                )
            )

            else -> throw IllegalArgumentException("Invalid viewType $type")
        }
    }

    override fun onBindViewHolder(holder: BaseViewHolder, position: Int) {
        when (holder) {
            is ServiceDueHolder -> holder.bind(listServiceDue[position])
            is MyCarViewHolder -> holder.bind(listMyCars[position])
            is PreferredMenuHolder -> holder.bind(listMenu[position])
            is BranchLocationHolder -> holder.bind(listBranch[position])
            is NewsAndTipsHolder -> holder.bind(listNewsAndTips[position])
            is PromoHolder -> holder.bind(listPromo[position])
            else -> throw IllegalArgumentException("Invalid BaseViewHolder $holder")
        }
    }

    override fun getItemCount() = when (type) {
        SERVICE_DUE -> listServiceDue.size
        MY_CARS -> listMyCars.size
        MENU -> listMenu.size
        BRANCH_LOCATION -> listBranch.size
        PROMO -> listPromo.size
        NEWS_AND_TIPS -> listNewsAndTips.size
    }

    override fun getItemViewType(position: Int) = type.ordinal

    private var onItemClickListener: OnItemClickListener? = null

    interface OnItemClickListener {
        fun <T> onClickedListener(item: T)
    }

    fun setOnItemClickListener(onClickListener: OnItemClickListener) {
        this.onItemClickListener = onClickListener
    }

    inner class ServiceDueHolder(
        private val binding: ItemServiceDueBinding
    ) : BaseViewHolder(binding) {

        fun bind(serviceDue: ServiceDueSample) = with(binding) {
            tvServiceDue.text = serviceDue.due
            tvServiceName.text = serviceDue.serviceName
            tvServiceDesc.text = serviceDue.desc
            if (serviceDue.isNearExpire) {
                tvLabelServiceDue.text = root.context.getString(R.string.title_label_expiring_soon)
            } else {
                tvLabelServiceDue.text = root.context.getString(R.string.title_label_service_due)
            }
            root.setOnSingleClickListener {
                this@RecyclerViewAdapter.onItemClickListener?.onClickedListener(serviceDue)
            }
        }
    }

    inner class MyCarViewHolder(
        private val binding: ItemMyCarBinding
    ) : BaseViewHolder(binding) {

        fun bind(myCar: MyCarsSample) = with(binding) {
            tvCarName.text = myCar.carName
            Glide.with(ivCarImage)
                .load(
                    ContextCompat.getDrawable(
                        root.context,
                        myCar.image
                    )
                )
                .into(ivCarImage)
            root.setOnSingleClickListener {
                this@RecyclerViewAdapter.onItemClickListener?.onClickedListener(myCar)
            }
        }
    }

    inner class PreferredMenuHolder(
        private val binding: ItemPreferredCategoryBinding
    ) : BaseViewHolder(binding) {

        fun bind(preferredMenu: PreferredMenuSample) = with(binding) {
            ivMenuIcon.setImageDrawable(
                ContextCompat.getDrawable(
                    root.context, preferredMenu.icon
                )
            )
            tvMenuName.text = preferredMenu.menuName

            root.setOnSingleClickListener {
                this@RecyclerViewAdapter.onItemClickListener?.onClickedListener(preferredMenu)
            }
        }
    }

    inner class BranchLocationHolder(
        private val binding: ItemBranchLocationBinding
    ) : BaseViewHolder(binding) {

        fun bind(branchLocation: BranchLocationSample) = with(binding) {
            ivBranchImage.setImageDrawable(
                ContextCompat.getDrawable(
                    root.context, branchLocation.image
                )
            )
            tvBranchName.text = branchLocation.branchName
            tvBranchType.text = branchLocation.branchType
            tvBranchStatus.text = branchLocation.branchStatus
            tvOpenTime.text = branchLocation.branchOpeningHour
            tvBranchDistance.text = branchLocation.branchDistance

            btnDirection.setOnSingleClickListener {
                this@RecyclerViewAdapter.onItemClickListener?.onClickedListener(branchLocation)
            }
        }
    }

    inner class PromoHolder(
        private val binding: ItemListPromoBinding
    ) : BaseViewHolder(binding) {

        fun bind(article: Content?) = with(binding) {
            Glide.with(root.context).load(article?.heroImageLink).into(ivPromo)

            ivPromo.setOnSingleClickListener {
                this@RecyclerViewAdapter.onItemClickListener?.onClickedListener(article)
            }
        }
    }

    inner class NewsAndTipsHolder(
        private val binding: ItemNewsAndTipsBinding
    ) : BaseViewHolder(binding) {

        fun bind(data: NewsTips.Content) = with(binding) {
            ivNewsAndTips.setImageViewUrl(data.heroImageLink)

            tvNewsAndTips.text = data.titleHeader?.let { HtmlCompat.fromHtml(it, HtmlCompat.FROM_HTML_MODE_LEGACY) }
            root.setOnSingleClickListener {
                this@RecyclerViewAdapter.onItemClickListener?.onClickedListener(data)
            }
        }
    }

}