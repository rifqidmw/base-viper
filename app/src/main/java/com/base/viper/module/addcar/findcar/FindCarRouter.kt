package com.base.viper.module.addcar.findcar

import androidx.fragment.app.Fragment
import com.base.viper.module.addcar.registercar.RegisterCarFragment
import com.base.viper.utils.Utils.toastShort

class FindCarRouter(private val fragment: Fragment) : FindCarInterface.Router {
    override fun goToRegisterCar() {
        val bottomSheet = RegisterCarFragment()
        bottomSheet.show(fragment.childFragmentManager, "REGISTER CAR")
    }

    override fun goToNotExistingCustomer() {
        fragment.requireActivity().toastShort("TODO: Not Existing Customer")
    }
}