package com.base.viper.module.mycardetails.view

import androidx.fragment.app.Fragment
import com.base.viper.utils.Utils.toastShort

class MyCarDetailRouter(private val fragment: Fragment) : MyCarDetailInterface.Router {
    override fun goToWorkShopService() {
        fragment.context?.toastShort("Todo: Work Shop Service")
    }

    override fun goToThs() {
        fragment.context?.toastShort("Todo: THS")
    }

    override fun goToBodyPaint() {
        fragment.context?.toastShort("Todo: Body Paint")
    }

    override fun goToAccessories() {
        fragment.context?.toastShort("Todo: Accessories")
    }

    override fun goToServicePackage() {
        fragment.context?.toastShort("Todo: Service Package")
    }

    override fun goToPromos() {
        fragment.context?.toastShort("Todo: Promo")
    }
}
