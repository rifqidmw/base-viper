package com.base.viper.module.productknowledge.entity

data class ProductFaqSample(
    val title: String,
    val desc: String
)
