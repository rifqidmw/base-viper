package com.base.viper.module.addcar

import androidx.appcompat.app.AppCompatActivity
import com.base.viper.module.addcar.findcar.FindCarFragment
import com.base.viper.module.addcar.registercar.RegisterCarFragment

class AddCarRouter(private val activity: AppCompatActivity) : AddCarInterface.Router {
    override fun goToFindCar() {
        val bottomSheet = FindCarFragment()
        bottomSheet.show(activity.supportFragmentManager, "FIND CAR")
    }

    override fun goToRegisterCar() {
        val bottomSheet = RegisterCarFragment()
        bottomSheet.show(activity.supportFragmentManager, "REGISTER CAR")
    }
}