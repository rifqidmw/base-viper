package com.base.viper.module.home.entity

data class BranchLocationSample(
    val image: Int,
    val branchName: String,
    val branchType: String,
    val branchStatus: String,
    val branchOpeningHour: String,
    val branchDistance: String,
)