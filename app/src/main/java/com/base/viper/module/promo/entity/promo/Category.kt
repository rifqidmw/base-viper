package com.base.viper.module.promo.entity.promo


import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize

@Parcelize
data class Category(
    @SerializedName("createdDate")
    val createdDate: String? = null,
    @SerializedName("createdBy")
    val createdBy: String? = null,
    @SerializedName("modifiedDate")
    val modifiedDate: String? = null,
    @SerializedName("modifiedBy")
    val modifiedBy: String? = null,
    @SerializedName("modifiedReason")
    val modifiedReason: String? = null,
    @SerializedName("isDeleted")
    val isDeleted: Boolean? = null,
    @SerializedName("deletedDate")
    val deletedDate: String? = null,
    @SerializedName("id")
    val id: Int? = null,
    @SerializedName("type")
    val type: String? = null,
    @SerializedName("name")
    val name: String? = null,
    @SerializedName("description")
    val description: String? = null,
    @SerializedName("faqs")
    val faqs: List<Faqs>? = null
) : Parcelable