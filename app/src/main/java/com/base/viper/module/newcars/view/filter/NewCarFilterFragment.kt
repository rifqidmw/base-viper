package com.base.viper.module.newcars.view.filter

import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import com.base.viper.R
import com.base.viper.common.BaseBottomDialogFragment
import com.base.viper.databinding.FragmentNewCarFilterBinding
import com.base.viper.module.newcars.adapter.RecyclerViewAdapter
import com.base.viper.module.newcars.adapter.RecyclerViewAdapter.ViewType.FILTER
import com.base.viper.utils.Utils.setOnSingleClickListener
import com.base.viper.utils.Utils.toastShort

class NewCarFilterFragment : BaseBottomDialogFragment<FragmentNewCarFilterBinding>
    (FragmentNewCarFilterBinding::inflate), NewCarFilterInterface.View {

    private val presenter: NewCarFilterPresenter = NewCarFilterPresenter(this)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupView()
    }

    override fun setOnCreatePresenter() {
        presenter.onCreate(this)
    }

    override fun setOnViewCreatedPresenter(view: View, savedInstanceState: Bundle?) {
        presenter.onViewCreated(this)
    }

    override fun setOnDestroyPresenter() {
        presenter.onDestroy()
    }

    override fun setupView() = with(binding){
        val filteredOptions = listOf(
            getString(R.string.title_filter_price_range),
            getString(R.string.title_filter_color),
            getString(R.string.title_filter_fuel),
            getString(R.string.title_filter_transmission),
            getString(R.string.title_filter_seat),
            getString(R.string.title_filter_promo))
        val adapter = RecyclerViewAdapter(FILTER, filteredOptions) {
            selectedItem -> val toastMessage = when (selectedItem) {
            getString(R.string.title_filter_price_range) -> "TODO: Range Harga"
            getString(R.string.title_filter_color) -> "TODO: Warna"
            getString(R.string.title_filter_fuel) -> "TODO: Tipe Bensin"
            getString(R.string.title_filter_transmission) -> "TODO: Transmisi"
            getString(R.string.title_filter_seat) -> "TODO: Jumlah Kursi"
            getString(R.string.title_filter_promo) -> "TODO: Promo"
            else -> ""
        }
            requireActivity().toastShort(toastMessage)
        }
        rvFilter.adapter = adapter
        rvFilter.layoutManager = LinearLayoutManager(requireContext())

        btnFilter.setOnSingleClickListener {
            presenter.goToApplyFilter()
        }

        ivClose.setOnSingleClickListener {
            this@NewCarFilterFragment.dismiss()
        }
    }
}