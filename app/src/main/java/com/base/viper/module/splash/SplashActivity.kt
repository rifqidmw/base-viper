package com.base.viper.module.splash

import android.os.Bundle
import android.view.animation.Animation
import android.view.animation.RotateAnimation
import androidx.appcompat.app.AppCompatActivity
import androidx.constraintlayout.motion.widget.MotionLayout
import androidx.constraintlayout.motion.widget.TransitionAdapter
import com.base.viper.R
import com.base.viper.databinding.ActivitySplashBinding

class SplashActivity : AppCompatActivity(), SplashInterface.View {
    
    private val presenter: SplashPresenter = SplashPresenter(this)
    private val binding: ActivitySplashBinding by lazy {
        ActivitySplashBinding.inflate(layoutInflater)
    }
    
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)
        presenter.onCreate(this)
    }
    
    override fun onDestroy() {
        super.onDestroy()
        presenter.onDestroy()
    }

    override fun setupView() {
        startLoading()
    }

    private fun startLoading(){
        val rotationAnimation = RotateAnimation(
            0f, 1080f, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f
        )
        rotationAnimation.duration = 4000
        binding.ivLoading.startAnimation(rotationAnimation)

        rotationAnimation.setAnimationListener(object : Animation.AnimationListener {
            override fun onAnimationStart(animation: Animation?) {
                //Not Yet Implemented
            }

            override fun onAnimationEnd(animation: Animation?) {
                binding.mlRoot.transitionToEnd()
                binding.mlRoot.setTransitionListener(object : TransitionAdapter() {
                    override fun onTransitionCompleted(
                        motionLayout: MotionLayout?,
                        currentId: Int
                    ) {
                        if (currentId == R.id.end) {
                            presenter.goToLogin()
                        }
                    }
                })
            }

            override fun onAnimationRepeat(animation: Animation?) {
                //Not Implemented
            }
        })
    }
}