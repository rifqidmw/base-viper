package com.base.viper.module.promo.entity.promo

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize

@Parcelize
data class Province(
    @SerializedName("cities")
    var cities: List<Cities>? = null,
    @SerializedName("id")
    var id: Int? = null,
    @SerializedName("isocode")
    var isocode: String? = null,
    @SerializedName("name")
    var name: String? = null
) : Parcelable
