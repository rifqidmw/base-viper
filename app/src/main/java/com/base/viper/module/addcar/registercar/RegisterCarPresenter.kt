package com.base.viper.module.addcar.registercar

import androidx.fragment.app.Fragment

class RegisterCarPresenter(
    private var view: RegisterCarInterface.View? = null
) : RegisterCarInterface.Presenter {

    private var interactor: RegisterCarInterface.Interactor? = null
    private var router: RegisterCarInterface.Router? = null

    override fun onCreate(fragment: Fragment) {
        view = fragment as RegisterCarFragment
        interactor = RegisterCarInteractor(fragment)
        router = RegisterCarRouter(fragment)
    }
    
    override fun onViewCreated(fragment: Fragment) {
        view?.setupView()
    }

    override fun onDestroy() {
        view = null
        router = null
    }

    override fun goToNotYourCar() {
        router?.goToNotYourCar()
    }

    override fun goToRegisterCar() {
        router?.goToRegisterCar()
    }
}