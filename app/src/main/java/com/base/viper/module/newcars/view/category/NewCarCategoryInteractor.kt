package com.base.viper.module.newcars.view.category

import androidx.fragment.app.Fragment
import com.base.viper.R
import com.base.viper.module.newcars.entity.NewCarSample
import com.base.viper.services.NetworkBuilder
import com.base.viper.services.Sessions
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.disposables.CompositeDisposable
import io.reactivex.rxjava3.schedulers.Schedulers

class NewCarCategoryInteractor(fragment: Fragment) : NewCarCategoryInterface.Interactor {
    private val disposable = CompositeDisposable()
    private val apiServices = NetworkBuilder.apiService
    private val sessions = fragment.context?.let { Sessions(it) }
    override fun getDataAllCar(
        onSuccess: (List<NewCarSample>) -> Unit,
        onError: (Throwable) -> Unit
    ) {
        val hybridData = getHybridCarData()
        val mpvData = getMpvCarData()
        val suvData = getSuvCarData()
        val hatchbackData = getHatchbackCarData()
        val sedanData = getSedanCarData()
        val sportsData = getSportsCarData()

        val dataDummy = arrayListOf<NewCarSample>().apply {
            addAll(hybridData)
            addAll(mpvData)
            addAll(suvData)
            addAll(hatchbackData)
            addAll(sedanData)
            addAll(sportsData)
        }

        Observable.just(dataDummy)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(onSuccess, onError)
            .let(disposable::add)
    }

    override fun getDataHybridCar(
        onSuccess: (List<NewCarSample>) -> Unit,
        onError: (Throwable) -> Unit
    ) {
        val dataDummy = getHybridCarData()
        Observable.just(dataDummy)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(onSuccess, onError)
            .let(disposable::add)
    }

    private fun getHybridCarData(): List<NewCarSample> {
        return listOf(
            NewCarSample(
                "CAMRY HYBRID",
                R.drawable.sample_new_car_hybrid_1,
                "450000000",
                "425600000",
                "HYBRID",
                true
            ),
            NewCarSample(
                "COROLLA HYBRID",
                R.drawable.sample_new_car_hybrid_2,
                "450000000",
                "425600000",
                "HYBRID",
                false
            ),
            NewCarSample(
                "YARIS CROSS HYBRID",
                R.drawable.sample_new_car_hybrid_3,
                "450000000",
                "425600000",
                "HYBRID",
                false
            ),
        )
    }


    override fun getDataMpvCar(
        onSuccess: (List<NewCarSample>) -> Unit,
        onError: (Throwable) -> Unit
    ) {
        val dataDummy = getMpvCarData()
        Observable.just(dataDummy)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(onSuccess, onError)
            .let(disposable::add)
    }

    private fun getMpvCarData(): List<NewCarSample> {
        return listOf(
            NewCarSample(
                "VELOZ",
                R.drawable.sample_new_car_mpv_1,
                "450000000",
                "425600000",
                "MPV",
                true
            ),
            NewCarSample(
                "CALYA",
                R.drawable.sample_new_car_mpv_2,
                "450000000",
                "425600000",
                "MPV",
                false
            ),
        )
    }

    override fun getDataSuvCar(
        onSuccess: (List<NewCarSample>) -> Unit,
        onError: (Throwable) -> Unit
    ) {
        val dataDummy = getSuvCarData()
        Observable.just(dataDummy)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(onSuccess, onError)
            .let(disposable::add)
    }

    private fun getSuvCarData(): List<NewCarSample> {
        return listOf(
            NewCarSample(
                "BZ4X",
                R.drawable.sample_new_car_suv_1,
                "450000000",
                "425600000",
                "SUV",
                false
            ),
            NewCarSample(
                "RAIZE",
                R.drawable.sample_new_car_suv_2,
                "450000000",
                "425600000",
                "SUV",
                true
            ),
        )
    }

    override fun getDataHatchbackCar(
        onSuccess: (List<NewCarSample>) -> Unit,
        onError: (Throwable) -> Unit
    ) {
        val dataDummy = getHatchbackCarData()
        Observable.just(dataDummy)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(onSuccess, onError)
            .let(disposable::add)
    }

    private fun getHatchbackCarData(): List<NewCarSample> {
        return listOf(
            NewCarSample(
                "NEW AGYA",
                R.drawable.sample_new_car_hatchback_1,
                "450000000",
                "425600000",
                "HATCHBACK",
                false
            ),
        )
    }

    override fun getDataSedanCar(
        onSuccess: (List<NewCarSample>) -> Unit,
        onError: (Throwable) -> Unit
    ) {
        val dataDummy = getSedanCarData()
        Observable.just(dataDummy)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(onSuccess, onError)
            .let(disposable::add)
    }

    private fun getSedanCarData(): List<NewCarSample> {
        return listOf(
            NewCarSample(
                "NEW CAMRY",
                R.drawable.sample_new_car_sedan_1,
                "450000000",
                "425600000",
                "SEDAN",
                false
            ),
        )
    }

    override fun getDataSportsCar(
        onSuccess: (List<NewCarSample>) -> Unit,
        onError: (Throwable) -> Unit
    ) {
        val dataDummy = getSportsCarData()
        Observable.just(dataDummy)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(onSuccess, onError)
            .let(disposable::add)
    }

    private fun getSportsCarData(): List<NewCarSample> {
        return listOf(
            NewCarSample(
                "SUPRA",
                R.drawable.sample_new_car_sport_1,
                "450000000",
                "425600000",
                "SPORTS",
                true
            ),
        )
    }
}