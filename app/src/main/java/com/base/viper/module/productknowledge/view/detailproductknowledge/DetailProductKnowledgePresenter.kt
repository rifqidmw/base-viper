package com.base.viper.module.productknowledge.view.detailproductknowledge

import android.app.Activity
import com.base.viper.common.Constants
import com.base.viper.module.productknowledge.common.ProductKnowledgeConstant

class DetailProductKnowledgePresenter(
    private var view: DetailProductKnowledgeInterface.View? = null
) : DetailProductKnowledgeInterface.Presenter {
    
    private var interactor: DetailProductKnowledgeInterface.Interactor? = null
    private var router: DetailProductKnowledgeInterface.Router? = null

    private var slug: String? = null
    
    override fun onCreate(activity: Activity) {
        view = activity as DetailProductKnowledgeActivity
        interactor = DetailProductKnowledgeInteractor(activity)
        router = DetailProductKnowledgeRouter(activity)

        activity.intent?.let {
            slug = it.getStringExtra(ProductKnowledgeConstant.SLUG)
        }

        view?.setupView()

        getDetailProductKnowledge(slug = slug)
        getSubPromo()
        getDataProductAdvantage()
        getDataProductOffer()
        getDataProductSpecification()
    }
    
    override fun onDestroy() {
        view = null
        router = null
    }

    override fun onSectionToolbar(event: Constants.ToolbarEvent) {
        when (event) {
            Constants.ToolbarEvent.SEARCH -> router?.goToSearch()
            Constants.ToolbarEvent.NOTIFICATION -> router?.goToNotification()
            Constants.ToolbarEvent.CART -> router?.goToCart()
        }
    }

    override fun goToHome() {
        router?.goToHome()
    }

    override fun goToAllPromo() {
        router?.goToAllPromo()
    }

    override fun goToDetailPromo(slug: String?) {
        router?.goToDetailPromo(slug)
    }

    override fun shareWithLink() {
        router?.shareWithLink()
    }

    private fun getDetailProductKnowledge(slug: String?){
        interactor?.getDetailProductKnowledge(slug, {
            view?.setDataProductKnowledge(it)
            view?.setDataProductFaq(it.contentCategory?.category?.faqs)
        }, {

        })
    }

    private fun getSubPromo() {
        interactor?.getSubPromo(10,
            {
                val filteredPromo = it.content.filter { content ->
                    content.status == Constants.StatusType.PUBLISH.value
                }
                view?.setupSubPromo(it.content)
                view?.fragmentPromoResultListener(filteredPromo)
            }, {

            }
        )
    }

    private fun getDataProductAdvantage() {
        interactor?.getDataProductAdvantage({
            view?.setDataProductAdvantage(it)
        }, {

        })
    }
    private fun getDataProductOffer() {
        interactor?.getDataProductOffer({
            view?.setDataProductOffer(it)
        }, {

        })
    }
    private fun getDataProductSpecification() {
        interactor?.getDataProductSpecification({
            view?.setDataProductSpecification(it)
        }, {

        })
    }
}