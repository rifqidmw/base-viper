package com.base.viper.module.productknowledge.view.detailproductknowledge

import android.app.Activity
import com.base.viper.R
import com.base.viper.common.Constants
import com.base.viper.module.productknowledge.entity.ProductAdvantageSample
import com.base.viper.module.productknowledge.entity.ProductFaqSample
import com.base.viper.module.productknowledge.entity.ProductKnowledgeDetail
import com.base.viper.module.productknowledge.entity.ProductOfferSample
import com.base.viper.module.productknowledge.entity.ProductPromoSample
import com.base.viper.module.productknowledge.entity.ProductSpecificationSample
import com.base.viper.module.promo.entity.promo.Promo
import com.base.viper.services.NetworkBuilder
import com.base.viper.services.Sessions
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.disposables.CompositeDisposable
import io.reactivex.rxjava3.schedulers.Schedulers

class DetailProductKnowledgeInteractor(val activity: Activity) :
    DetailProductKnowledgeInterface.Interactor {
    
    private val disposable = CompositeDisposable()
    private val apiService = NetworkBuilder.apiService
    private val sessions = Sessions(activity)
    override fun getDetailProductKnowledge(
        slug: String?,
        onSuccess: (ProductKnowledgeDetail) -> Unit,
        onError: (Throwable) -> Unit
    ) {
        apiService.getProductKnowledgeDetail(Constants.VersionType.V1.value, slug)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(onSuccess, onError)
            .let(disposable::add)
    }

    override fun getSubPromo(size: Int, onSuccess: (Promo) -> Unit, onError: (Throwable) -> Unit) {
        apiService.getPromo(Constants.VersionType.V1.value)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(onSuccess, onError)
            .let(disposable::add)
    }

    override fun getDataProductAdvantage(
        onSuccess: (List<ProductAdvantageSample>) -> Unit,
        onError: (Throwable) -> Unit
    ) {
        val dataDummy = listOf(
            ProductAdvantageSample(
                "Performa Tiada Tara",
                "Satu hal yang akan langsung Anda rasakan ketika mengendarai mobil ini adalah performa yang stabil nan responsif, menjadikannya sebagai pilihan mobil yang cocok di berbagai medan."
            ),
            ProductAdvantageSample(
                "Akselerasi Mulus",
                "Seluruh varian transmisi Avanza matic sudah dilengkapi dengan CVT. Sehingga setiap perpindahan gigi, Anda akan bisa menikmati akselerasi yang sangat mulus."
            ),
            ProductAdvantageSample(
                "Minim Getaran",
                "Seluruh varian transmisi Avanza matic sudah dilengkapi dengan CVT. Sehingga setiap perpindahan gigi, Anda akan bisa menikmati akselerasi yang sangat mulus."
            ),
        )

        Observable.just(dataDummy)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(onSuccess, onError)
            .let(disposable::add)
    }

    override fun getDataProductOffer(
        onSuccess: (List<ProductOfferSample>) -> Unit,
        onError: (Throwable) -> Unit
    ) {
        val dataDummy = listOf(
            ProductOfferSample(
                R.drawable.sample_trending_car_1,
                "Toyota All New Avanza Tipe 1.5 G CVT",
                269800000,
                "Pilihan pertama ada Toyota All New Avanza 1.5 G CVT. Mobil ini dibekali mesin berkapasitas 1500 cc, transmisi CVT, dan beragam fitur mumpuni. Salah satunya adalah New Dynamic Head Unit ukuran 9 inci yang siap menemani perjalan AutoFamily."
            ),
            ProductOfferSample(
                R.drawable.sample_my_car_1,
                "Toyota All New Yaris Cross",
                310000000,
                "Pilihan pertama ada Toyota All New Avanza 1.6 G CVT. Mobil ini dibekali mesin berkapasitas 1500 cc, transmisi CVT, dan beragam fitur mumpuni. Salah satunya adalah New Dynamic Head Unit ukuran 9 inci yang siap menemani perjalan AutoFamily."
            ),
            ProductOfferSample(
                R.drawable.sample_my_car_2,
                "Toyota All New Avanza Tipe 1.5 G CVT",
                269800000,
                "Pilihan pertama ada Toyota All New Avanza 1.7 G CVT. Mobil ini dibekali mesin berkapasitas 1500 cc, transmisi CVT, dan beragam fitur mumpuni. Salah satunya adalah New Dynamic Head Unit ukuran 9 inci yang siap menemani perjalan AutoFamily."
            ),
        )

        Observable.just(dataDummy)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(onSuccess, onError)
            .let(disposable::add)
    }

    override fun getDataProductSpecification(
        onSuccess: (List<ProductSpecificationSample>) -> Unit,
        onError: (Throwable) -> Unit
    ) {
        val dataDummy = listOf(
            ProductSpecificationSample(
                "Dimensi",
                "Panjang : 4.395 mm, Lebar : 1.730 mm, Tinggi : 1.665 mm",
                R.drawable.ic_product_dimention
            ),
            ProductSpecificationSample(
                "Chasis",
                "Transmission / Transmission Type : AT",
                R.drawable.ic_product_chasis
            ),
            ProductSpecificationSample(
                "Mesin",
                "Tipe Mesin : 1.500 cc, 4 Cylinders in-line",
                R.drawable.ic_product_machine
            ),
            ProductSpecificationSample(
                "Fitur Keamanan",
                "Airbags untuk pengemudi & pengendara",
                R.drawable.ic_product_security
            ),
            ProductSpecificationSample(
                "Fitur Kenyamanan",
                "New Dynamic 7\" Head Unit Display, AC, 1 stereo",
                R.drawable.ic_product_comfort
            ),
        )

        Observable.just(dataDummy)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(onSuccess, onError)
            .let(disposable::add)
    }

    override fun getDataProductPromo(
        onSuccess: (List<ProductPromoSample>) -> Unit,
        onError: (Throwable) -> Unit
    ) {
        val dataDummy = listOf(
            ProductPromoSample(
                R.drawable.sample_news_and_tips_1,
                "Cicilan Ringan Mobil Toyota Avanza",
                "21 Nov 22 - 30 Jun 23",
            ),
            ProductPromoSample(
                R.drawable.sample_news_and_tips_2,
                "CalyaPromo EZ Deal Cicilan Ringan",
                "22 Nov 22 - 30 Jun 23",
            ),
            ProductPromoSample(
                R.drawable.sample_news_and_tips_3,
                "Cicilan Ringan Mobil Toyota Avanza",
                "23 Nov 22 - 30 Jun 23",
            ),
        )

        Observable.just(dataDummy)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(onSuccess, onError)
            .let(disposable::add)
    }

    override fun getDataProductFaq(
        onSuccess: (List<ProductFaqSample>) -> Unit,
        onError: (Throwable) -> Unit
    ) {
        val dataDummy = listOf(
            ProductFaqSample(
                "Bagaimana sistem keselamatan pada mobil matic Toyota Avanza?",
                activity.getString(R.string.lorem_ipsum)
            ),
            ProductFaqSample(
                "Seperti apa tampilan eksterior Toyota All New Avanza?",
                activity.getString(R.string.lorem_ipsum)
            ),
            ProductFaqSample(
                "Apakah masih ada promo menarik lainnya dari Auto2000?",
                activity.getString(R.string.lorem_ipsum)
            ),
        )

        Observable.just(dataDummy)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(onSuccess, onError)
            .let(disposable::add)
    }
}