package com.base.viper.module.addcar

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.base.viper.databinding.ActivityAddCarBinding
import com.base.viper.utils.Utils.setOnSingleClickListener

class AddCarActivity : AppCompatActivity(), AddCarInterface.View {
    
    private val presenter: AddCarPresenter = AddCarPresenter(this)
    private val binding: ActivityAddCarBinding by lazy {
        ActivityAddCarBinding.inflate(layoutInflater)
    }
    
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)
        presenter.onCreate(this)
    }
    
    override fun onDestroy() {
        super.onDestroy()
        presenter.onDestroy()
    }

    override fun setupView() {
        with(binding){
            btnAddCar.setOnSingleClickListener {
                presenter.goToFindCar()
            }
        }
    }
}