package com.base.viper.module.login.loginotp

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.core.text.HtmlCompat
import com.base.viper.R
import com.base.viper.databinding.ActivityLoginOtpBinding
import com.base.viper.utils.Utils.setOnSingleClickListener
import com.base.viper.utils.Utils.setStatusBarColor

class LoginOtpActivity : AppCompatActivity(), LoginOtpInterface.View {
    
    private val presenter: LoginOtpPresenter = LoginOtpPresenter(this)
    private val binding: ActivityLoginOtpBinding by lazy {
        ActivityLoginOtpBinding.inflate(layoutInflater)
    }
    
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)
        presenter.onCreate(this)
    }
    
    override fun onDestroy() {
        super.onDestroy()
        presenter.onDestroy()
    }

    override fun setupView() = with(binding) {
        setStatusBarColor(R.color.reliable_black, false)

        tvTitleOtp.text = HtmlCompat.fromHtml(getString(R.string.title_label_login_otp, "+62-89696016268"), HtmlCompat.FROM_HTML_MODE_LEGACY)

        btnLogin.setOnSingleClickListener {
            presenter.goToHome()
        }
    }
}