package com.base.viper.module.productknowledge.view.category

import androidx.fragment.app.Fragment
import com.base.viper.common.Constants
import com.base.viper.module.productknowledge.entity.ProductKnowledge
import com.base.viper.services.NetworkBuilder
import com.base.viper.services.Sessions
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.disposables.CompositeDisposable
import io.reactivex.rxjava3.schedulers.Schedulers

class ProductCategoryInteractor(fragment: Fragment) : ProductCategoryInterface.Interactor {
    
    private val disposable = CompositeDisposable()
    private val apiService = NetworkBuilder.apiService
    private val sessions = fragment.context?.let { Sessions(it) }
    override fun getDataProductKnowledge(
        page: Int,
        size: Int,
        contentCategory: String,
        onSuccess: (ProductKnowledge) -> Unit,
        onError: (Throwable) -> Unit
    ) {
        apiService.getProductKnowledge(Constants.VersionType.V1.value, page = page.toString(), size = size.toString(), contentCategory = contentCategory)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(onSuccess, onError)
            .let(disposable::add)
    }
}