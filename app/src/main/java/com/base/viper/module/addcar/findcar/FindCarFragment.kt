package com.base.viper.module.addcar.findcar

import android.os.Bundle
import android.view.View
import com.base.viper.common.BaseBottomDialogFragment
import com.base.viper.databinding.FragmentFindCarBinding
import com.base.viper.utils.Utils.setOnSingleClickListener

class FindCarFragment : BaseBottomDialogFragment<FragmentFindCarBinding>(FragmentFindCarBinding::inflate), FindCarInterface.View {

    private val presenter: FindCarPresenter = FindCarPresenter(this)

    override fun setOnCreatePresenter() {
        presenter.onCreate(this)
    }

    override fun setOnViewCreatedPresenter(view: View, savedInstanceState: Bundle?) {
        presenter.onViewCreated(this)
    }

    override fun setOnDestroyPresenter() {
        presenter.onDestroy()
    }

    override fun setupView() {
        with(binding){
            btnFind.setOnSingleClickListener {
                presenter.goToRegisterCar()
            }

            ivClose.setOnSingleClickListener {
                this@FindCarFragment.dismiss()
            }

            flNotAnExistingCustomer.setOnSingleClickListener {
                presenter.goToNotExistingCustomer()
            }
        }
    }
}