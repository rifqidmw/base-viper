package com.base.viper.module.promo.view

import android.app.Activity
import com.base.viper.common.Constants
import com.base.viper.module.promo.entity.promo.PromoCategory

class PromoPresenter(
    private var view: PromoInterface.View? = null
) : PromoInterface.Presenter {

    private var interactor: PromoInterface.Interactor? = null
    private var router: PromoInterface.Router? = null

    override fun onCreate(activity: Activity) {
        view = activity as PromoActivity
        interactor = PromoInteractor(activity)
        router = PromoRouter(activity)

        view?.setupView()

        getDataPromoCategory()
    }

    override fun onDestroy() {
        view = null
        router = null
    }

    override fun onSectionToolbar(event: Constants.ToolbarEvent) {
        when (event) {
            Constants.ToolbarEvent.SEARCH -> router?.goToSearch()
            Constants.ToolbarEvent.NOTIFICATION -> router?.goToNotification()
            Constants.ToolbarEvent.CART -> router?.goToCart()
        }
    }

    private fun getDataPromoCategory() {
        interactor?.getDataCategoryPromo(
            {
                getDataPromo(it)
            }, {

            }
        )
    }

    private fun getDataPromo(promoCategories: List<PromoCategory>) {
        interactor?.getDataPromo(
            {
                val mutableCategories = mutableListOf<PromoCategory>()
                mutableCategories.add(PromoCategory("ALL"))
                mutableCategories.addAll(promoCategories)
                view?.setupTabLayout(mutableCategories)
                val filteredPromo = it.content?.filter { content ->
                    content.status == "PUBLISH"
                }
                view?.setDataPromo(it.content)
            },
            {

            }
        )
    }
}