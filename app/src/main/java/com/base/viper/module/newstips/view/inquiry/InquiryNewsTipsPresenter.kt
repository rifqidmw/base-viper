package com.base.viper.module.newstips.view.inquiry

import androidx.fragment.app.Fragment
import com.base.viper.R
import com.base.viper.common.Constants
import com.base.viper.module.newstips.entity.inquiry.Branch
import com.base.viper.module.newstips.entity.inquiry.City
import com.base.viper.module.newstips.entity.inquiry.Product
import com.base.viper.module.newstips.entity.inquiry.Province
import com.base.viper.utils.Utils.isValidPhoneLength
import com.base.viper.utils.Utils.isValidPhoneNumber

class InquiryNewsTipsPresenter(
    private var view: InquiryNewsTipsInterface.View? = null
) : InquiryNewsTipsInterface.Presenter {

    private var interactor: InquiryNewsTipsInterface.Interactor? = null
    private var router: InquiryNewsTipsInterface.Router? = null

    var fullName = ""
    var phone = ""
    var province: Province? = null
    var city: City? = null
    var carModel: Product? = null
    var branch: Branch.Content? = null
    var tnc = false
    private val requiredField = mutableMapOf(
        "fullName" to false,
        "phone" to false,
        "province" to false,
        "city" to false,
        "carModel" to false,
        "tnc" to false
    )

    override fun onCreate(fragment: Fragment) {
        view = fragment as InquiryNewsTipsFragment
        interactor = InquiryNewsTipsInteractor(fragment)
        router = InquiryNewsTipsRouter(fragment)
    }

    override fun onViewCreated(fragment: Fragment) {
        view?.setupView()
        view?.disableButtonSubmit()
    }

    override fun onDestroy() {
        view = null
        router = null
    }

    override fun showBottomsheetProvince() {
        interactor?.getDataProvince({
            view?.showBottomsheetProvince(it)
        }, {

        })
    }

    override fun showBottomsheetCity(isocode: String?) {
        isocode?.let {
            interactor?.getDataCity(it, { data ->
                view?.showBottomsheetCity(data)
            }, {

            })
        }
    }

    override fun showBottomsheetCarModel() {
        interactor?.getDataCarModel({
            view?.showBottomsheetCarModel(it)
        }, {

        })
    }

    override fun showBottomsheetBranch() {
        city?.code?.let {
            interactor?.getDataBranch(city = it, { branch ->
                view?.showBottomsheetBranch(branch.content)
            }, {

            })
        }
    }

    override fun onSubmit(fragment: Fragment, type: Constants.InquiryType) {
        when (type){
            Constants.InquiryType.NEWS_AND_TIPS -> router?.goToSuccessMessage(
                fragment.getString(R.string.title_inquiry_success),
                fragment.getString(R.string.desc_inquiry_success),
                fragment.getString(R.string.title_label_inquiry_see_more_news_tips)
            )
            Constants.InquiryType.REQUEST_A_QUOTE -> router?.goToSuccessMessage(
                fragment.getString(R.string.title_inquiry_success),
                fragment.getString(R.string.desc_inquiry_success),
                fragment.getString(R.string.title_label_inquiry_see_more_promo)
            )
        }
    }

    fun validateInput() {
        requiredField["fullName"] = when {
            fullName.isEmpty() -> {
                view?.setErrorFullName("Nama Lengkap Harus Diisi")
                false
            }

            else -> {
                view?.setErrorFullName(null)
                true
            }
        }

        requiredField["phone"] = when {
            phone.isEmpty() -> {
                view?.setErrorPhone("No. Telp Harus Diisi")
                false
            }

            !phone.isValidPhoneNumber() -> {
                view?.setErrorPhone("No. Telp Harus Diawali 08/+62/62")
                false
            }

            !phone.isValidPhoneLength() -> {
                view?.setErrorPhone("No. Telp Harus >= 11 Digit")
                false
            }

            else -> {
                view?.setErrorPhone(null)
                true
            }
        }

        requiredField["province"] = when (province) {
            null -> {
                view?.setErrorProvince("Provinsi Harus Diisi")
                false
            }
            else -> {
                view?.setErrorProvince(null)
                true
            }
        }

        requiredField["city"] = when (city) {
            null -> {
                view?.setErrorCity("Kota Harus Diisi")
                false
            }
            else -> {
                view?.setErrorCity(null)
                true
            }
        }

        requiredField["carModel"] = when (carModel) {
            null -> {
                view?.setErrorCarModel("Model Mobil Harus Diisi")
                false
            }
            else -> {
                view?.setErrorCarModel(null)
                true
            }
        }

        requiredField["tnc"] = when {
            !tnc -> {
                view?.setErrorTnC("Syarat & Ketentuan Harus Di Centang")
                false
            }

            else -> {
                view?.setErrorTnC(null)
                true
            }
        }

        if (requiredField.containsValue(false)) {
            view?.disableButtonSubmit()
        } else {
            view?.enableButtonSubmit()
        }
    }
}