package com.base.viper.module.productknowledge.view.detailproductknowledge

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import com.base.viper.module.home.view.MainActivity
import com.base.viper.module.promo.view.PromoActivity
import com.base.viper.module.promo.view.detailpromo.DetailPromoActivity
import com.base.viper.utils.Utils.toastShort

class DetailProductKnowledgeRouter(private val activity: AppCompatActivity) :
    DetailProductKnowledgeInterface.Router {
    override fun goToSearch() {
        activity.toastShort("Todo: Search")
    }

    override fun goToNotification() {
        activity.toastShort("Todo: Notification")
    }

    override fun goToCart() {
        activity.toastShort("Todo: Cart")
    }

    override fun shareWithLink() {
        activity.startActivity(
            Intent.createChooser(
                Intent(Intent.ACTION_SEND).apply {
                    type = "text/plain"
                    putExtra(Intent.EXTRA_TEXT, "Todo: Copy Link")
                }, "Share Using"
            )
        )
    }

    override fun goToHome() = with(activity) {
        startActivity(
            Intent(this, MainActivity::class.java).apply {
                addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TOP)
            }
        )
        finish()
    }

    override fun goToAllPromo() = with(activity) {
        startActivity(
            Intent(
                this,
                PromoActivity::class.java
            ).apply {
                addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TOP)
            })
        finish()
    }

    override fun goToDetailPromo(slug: String?) = with(activity) {
        startActivity(
            Intent(this, DetailPromoActivity::class.java).apply {
                addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TOP)
                putExtra("PROMO_SLUG", slug)
            }
        )
        finish()
    }
}