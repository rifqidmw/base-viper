package com.base.viper.module.home.view.account

import androidx.fragment.app.Fragment

class AccountPresenter(
    private var view: AccountInterface.View? = null
) : AccountInterface.Presenter {

    private var interactor: AccountInterface.Interactor? = null
    private var router: AccountInterface.Router? = null

    override fun onCreate(fragment: Fragment) {
        view = fragment as AccountFragment
        interactor = AccountInteractor(fragment)
        router = AccountRouter(fragment)
    }
    
    override fun onViewCreated(fragment: Fragment) {
        // Not Yet Implemented
    }

    override fun onDestroy() {
        view = null
        router = null
    }
}