package com.base.viper.module.newcars.view.category

import androidx.fragment.app.Fragment
import com.base.viper.module.newcars.entity.NewCarSample

interface NewCarCategoryInterface {
    interface View {
        fun setupView()
        fun setDataAllCar(data: List<NewCarSample>)
        fun setDataHybridCar(data: List<NewCarSample>)
        fun setDataMpvCar(data: List<NewCarSample>)
        fun setDataSuvCar(data: List<NewCarSample>)
        fun setDataHatchBackCar(data: List<NewCarSample>)
        fun setDataSedanCar(data: List<NewCarSample>)
        fun setDataSportsCar(data: List<NewCarSample>)
    }

    interface Presenter {
        fun onCreate(fragment: Fragment)
        fun onViewCreated(fragment: Fragment)
        fun onDestroy()
        fun goToDetailNewCar(item: NewCarSample)
    }

    interface Interactor {
        fun getDataAllCar(
            onSuccess: (List<NewCarSample>) -> Unit,
            onError: (Throwable) -> Unit
        )
        fun getDataHybridCar(
            onSuccess: (List<NewCarSample>) -> Unit,
            onError: (Throwable) -> Unit
        )
        fun getDataMpvCar(
            onSuccess: (List<NewCarSample>) -> Unit,
            onError: (Throwable) -> Unit
        )
        fun getDataSuvCar(
            onSuccess: (List<NewCarSample>) -> Unit,
            onError: (Throwable) -> Unit
        )
        fun getDataHatchbackCar(
            onSuccess: (List<NewCarSample>) -> Unit,
            onError: (Throwable) -> Unit
        )
        fun getDataSedanCar(
            onSuccess: (List<NewCarSample>) -> Unit,
            onError: (Throwable) -> Unit
        )
        fun getDataSportsCar(
            onSuccess: (List<NewCarSample>) -> Unit,
            onError: (Throwable) -> Unit
        )

    }

    interface Router {
        fun goToDetailNewCar(item: NewCarSample)
    }
}