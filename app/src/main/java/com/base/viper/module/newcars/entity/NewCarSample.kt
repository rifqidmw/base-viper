package com.base.viper.module.newcars.entity

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class NewCarSample (
    val carName: String,
    val image: Int,
    val priceBefore: String,
    val price: String,
    val category: String,
    val isBestSeller: Boolean
): Parcelable