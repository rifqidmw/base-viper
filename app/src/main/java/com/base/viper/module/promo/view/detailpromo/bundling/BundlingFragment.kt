package com.base.viper.module.promo.view.detailpromo.bundling

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.base.viper.databinding.FragmentBundlingBinding
import com.base.viper.module.promo.entity.bundle.BundleSample
import com.base.viper.utils.Utils.formatRupiah
import com.bumptech.glide.Glide

class BundlingFragment : Fragment(), BundlingInterface.View {

    private val presenter: BundlingPresenter = BundlingPresenter(this)
    private val binding: FragmentBundlingBinding by lazy {
        FragmentBundlingBinding.inflate(layoutInflater)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        presenter.onCreate(this)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        presenter.onViewCreated(this)
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter.onDestroy()
    }

    override fun setDataBundling(bundleSample: BundleSample?) = with(binding) {
        bundleSample?.let {
            Glide.with(ivBundle)
                .load(it.bundleImage)
                .into(ivBundle)
            tvBundleTitle.text = it.bundleTitle
            tvBundleColor.text = "Color: ${it.bundleColor}"
            tvBundlePrice.text = "Rp. ${it.bundlePrice.formatRupiah()}"
        }
        return@with
    }
}