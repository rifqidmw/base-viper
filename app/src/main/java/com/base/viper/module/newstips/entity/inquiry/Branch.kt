package com.base.viper.module.newstips.entity.inquiry


import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize


@Parcelize
data class Branch(
    @SerializedName("content")
    var content: List<Content> = emptyList(),
    @SerializedName("empty")
    var empty: Boolean? = null,
    @SerializedName("first")
    var first: Boolean? = null,
    @SerializedName("last")
    var last: Boolean? = null,
    @SerializedName("number")
    var number: Int? = null,
    @SerializedName("numberOfElements")
    var numberOfElements: Int? = null,
    @SerializedName("pageable")
    var pageable: Pageable? = null,
    @SerializedName("size")
    var size: Int? = null,
    @SerializedName("sort")
    var sort: Sort? = null,
    @SerializedName("totalElements")
    var totalElements: Int? = null,
    @SerializedName("totalPages")
    var totalPages: Int? = null
) : Parcelable {

    @Parcelize
    data class Content(
        @SerializedName("addressId")
        var addressId: String? = null,
        @SerializedName("altText")
        var altText: String? = null,
        @SerializedName("branchName")
        var branchName: String? = null,
        @SerializedName("cellphone")
        var cellphone: String? = null,
        @SerializedName("city")
        var city: City? = null,
        @SerializedName("countryName")
        var countryName: String? = null,
        @SerializedName("defaultAddress")
        var defaultAddress: Boolean? = null,
        @SerializedName("displayName")
        var displayName: String? = null,
        @SerializedName("formattedAddress")
        var formattedAddress: String? = null,
        @SerializedName("id")
        var id: Int? = null,
        @SerializedName("isInterior")
        var isInterior: String? = null,
        @SerializedName("isShowGetAdvised")
        var isShowGetAdvised: Boolean? = null,
        @SerializedName("isocode")
        var isocode: String? = null,
        @SerializedName("latitude")
        var latitude: Double? = null,
        @SerializedName("line1")
        var line1: String? = null,
        @SerializedName("line2")
        var line2: String? = null,
        @SerializedName("longitude")
        var longitude: Double? = null,
        @SerializedName("openingHoursCode")
        var openingHoursCode: String? = null,
        @SerializedName("openingHoursName")
        var openingHoursName: String? = null,
        @SerializedName("phone")
        var phone: String? = null,
        @SerializedName("plpTestDrive")
        var plpTestDrive: Boolean? = null,
        @SerializedName("postalCode")
        var postalCode: String? = null,
        @SerializedName("shippingAddress")
        var shippingAddress: Boolean? = null,
        @SerializedName("town")
        var town: String? = null,
        @SerializedName("type")
        var type: String? = null,
        @SerializedName("url")
        var url: String? = null,
        @SerializedName("visibleDdAddressBook")
        var visibleDdAddressBook: String? = null,
        @SerializedName("workingSchedules")
        var workingSchedules: List<WorkingSchedule?>? = null
    ) : Parcelable {

        @Parcelize
        data class City(
            @SerializedName("code")
            var code: String? = null,
            @SerializedName("id")
            var id: Int? = null,
            @SerializedName("isocode")
            var isocode: String? = null,
            @SerializedName("name")
            var name: String? = null,
            @SerializedName("province")
            var province: String? = null
        ) : Parcelable


        @Parcelize
        data class WorkingSchedule(
            @SerializedName("closed")
            var closed: Boolean? = null,
            @SerializedName("closingFormattedHour")
            var closingFormattedHour: String? = null,
            @SerializedName("closingHour")
            var closingHour: Int? = null,
            @SerializedName("closingMinute")
            var closingMinute: Int? = null,
            @SerializedName("openingFormattedHour")
            var openingFormattedHour: String? = null,
            @SerializedName("openingHour")
            var openingHour: Int? = null,
            @SerializedName("openingMinute")
            var openingMinute: Int? = null,
            @SerializedName("weekDay")
            var weekDay: String? = null
        ) : Parcelable
    }


    @Parcelize
    data class Pageable(
        @SerializedName("offset")
        var offset: Int? = null,
        @SerializedName("pageNumber")
        var pageNumber: Int? = null,
        @SerializedName("pageSize")
        var pageSize: Int? = null,
        @SerializedName("paged")
        var paged: Boolean? = null,
        @SerializedName("sort")
        var sort: Sort? = null,
        @SerializedName("unpaged")
        var unpaged: Boolean? = null
    ) : Parcelable {

        @Parcelize
        data class Sort(
            @SerializedName("empty")
            var empty: Boolean? = null,
            @SerializedName("sorted")
            var sorted: Boolean? = null,
            @SerializedName("unsorted")
            var unsorted: Boolean? = null
        ) : Parcelable
    }


    @Parcelize
    data class Sort(
        @SerializedName("empty")
        var empty: Boolean? = null,
        @SerializedName("sorted")
        var sorted: Boolean? = null,
        @SerializedName("unsorted")
        var unsorted: Boolean? = null
    ) : Parcelable
}