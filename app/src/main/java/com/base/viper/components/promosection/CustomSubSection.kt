package com.base.viper.components.promosection

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.base.viper.R
import com.base.viper.common.HorizontalMarginItemDecoration
import com.base.viper.databinding.CustomPromoSectionBinding
import com.base.viper.utils.Utils.dp
import com.base.viper.utils.Utils.setOnSingleClickListener

class CustomSubSection(
    context: Context,
    attrs: AttributeSet? = null
) : ConstraintLayout(context, attrs), CustomSubSectionAdapter.OnItemClickListener {

    private val binding: CustomPromoSectionBinding = CustomPromoSectionBinding.inflate(
        LayoutInflater.from(context), this, true
    )

    private val customSubSectionAdapter: CustomSubSectionAdapter = CustomSubSectionAdapter()

    interface Listener {
        fun onSubTitleClicked()
        fun onItemClicked(position: Int)
    }

    private var onClickedListener: Listener? = null

    fun setOnClickedListener(listener: Listener) {
        onClickedListener = listener
    }

    var title: String? = null
        set(value) = with(binding)
        {
            tvTitle.text = value
            field = value
            invalidate()
            requestLayout()
        }

    var subTitle: String? = null
        set(value) = with(binding)
        {
            tvSubTitle.text = value
            field = value
            invalidate()
            requestLayout()
        }

    init {
        attrs?.let {
            context.theme.obtainStyledAttributes(
                it,
                R.styleable.CustomPromoSection,
                0,
                0
            ).apply {
                try {
                    title = getString(R.styleable.CustomPromoSection_title)
                    subTitle = getString(R.styleable.CustomPromoSection_sub_title)
                } finally {
                    setupPromoAdapter()
                    recycle()
                }
            }
        }
    }

    private fun setupPromoAdapter() {
        binding.rvPromo.apply {
            layoutManager = LinearLayoutManager(context, RecyclerView.HORIZONTAL, false)
            adapter = customSubSectionAdapter
            addItemDecoration(
                HorizontalMarginItemDecoration(
                    10.dp,
                    firstItemStartMargin = 16.dp,
                    lastItemEndMargin = 16.dp
                )
            )
        }
    }

    fun setDataPromo(listData: List<DataSubSection?>) {
        customSubSectionAdapter.setDataPromo(listData)
        customSubSectionAdapter.setOnItemClickListener(this)
        binding.tvSubTitle.setOnSingleClickListener {
            onClickedListener?.onSubTitleClicked()
        }
    }

    override fun onClickedListener(position: Int) {
        onClickedListener?.onItemClicked(position)
    }
}