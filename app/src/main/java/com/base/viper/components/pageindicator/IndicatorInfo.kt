package com.base.viper.components.pageindicator

import android.view.Gravity
import android.widget.LinearLayout
import androidx.annotation.AnimatorRes
import androidx.annotation.DrawableRes
import com.base.viper.R

data class IndicatorInfo(
    var width: Int = -1,
    var height: Int = -1,
    var margin: Int = -1,
    @AnimatorRes var animationResId: Int = R.animator.page_indicator_animation,
    @AnimatorRes var animatorReverseResId: Int = 0,
    @DrawableRes var backgroundId: Int = R.drawable.bg_custom_indicator_unselected,
    @DrawableRes var backgroundReverseId: Int = 0,
    var orientation: Int = LinearLayout.HORIZONTAL,
    var gravity: Int = Gravity.CENTER,
    var pageIndicator: Boolean = true,
    var maxIndicatorEnabled: Boolean = false
)