package com.base.viper.components

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import androidx.constraintlayout.widget.ConstraintLayout
import com.base.viper.R
import com.base.viper.common.Constants
import com.base.viper.common.Constants.ToolbarEvent.CART
import com.base.viper.common.Constants.ToolbarEvent.NOTIFICATION
import com.base.viper.common.Constants.ToolbarEvent.SEARCH
import com.base.viper.components.CustomToolbar.Type.TOOLBAR_NO_CTA_AND_NO_EXTRA_SECTION
import com.base.viper.components.CustomToolbar.Type.TOOLBAR_WITH_CTA_AND_NO_EXTRA_SECTION
import com.base.viper.components.CustomToolbar.Type.TOOLBAR_WITH_EXTRA_SECTION_AND_NO_CTA
import com.base.viper.components.CustomToolbar.Type.TOOLBAR_WITH_EXTRA_SECTION_AND_SECTION_CTA
import com.base.viper.databinding.CustomToolbarBinding
import com.base.viper.utils.Utils.gone
import com.base.viper.utils.Utils.setOnSingleClickListener
import com.base.viper.utils.Utils.visible

class CustomToolbar(
    context: Context,
    attrs: AttributeSet? = null
) : ConstraintLayout(context, attrs) {

    private val binding: CustomToolbarBinding = CustomToolbarBinding.inflate(
        LayoutInflater.from(context), this, true
    )

    enum class Type {
        TOOLBAR_WITH_EXTRA_SECTION_AND_SECTION_CTA,
        TOOLBAR_WITH_EXTRA_SECTION_AND_NO_CTA,
        TOOLBAR_WITH_CTA_AND_NO_EXTRA_SECTION,
        TOOLBAR_NO_CTA_AND_NO_EXTRA_SECTION
    }

    interface Listener {
        fun onClickedSearch(event: Constants.ToolbarEvent) {
            //Not Implemented Here
        }

        fun onClickedNotification(event: Constants.ToolbarEvent) {
            //Not Implemented Here
        }

        fun onClickedCart(event: Constants.ToolbarEvent) {
            //Not Implemented Here
        }

        fun onClickedBackButton()
    }

    private var onClickedListener: Listener? = null

    fun setOnClickedListener(listener: Listener) {
        onClickedListener = listener
    }

    var toolbarType: Type? = TOOLBAR_NO_CTA_AND_NO_EXTRA_SECTION
        set(value) = with(binding) {
            when (value) {
                TOOLBAR_WITH_EXTRA_SECTION_AND_SECTION_CTA -> {
                    clCta.visible()
                    clExtraSection.visible()
                }

                TOOLBAR_WITH_EXTRA_SECTION_AND_NO_CTA -> {
                    clCta.gone()
                    clExtraSection.visible()
                }

                TOOLBAR_WITH_CTA_AND_NO_EXTRA_SECTION -> {
                    clCta.visible()
                    clExtraSection.gone()
                }

                null, TOOLBAR_NO_CTA_AND_NO_EXTRA_SECTION -> {
                    clCta.gone()
                    clExtraSection.gone()
                }
            }
            field = value
            invalidate()
            requestLayout()
        }

    var titleAllCaps: Boolean = false
        set(value) = with(binding) {
            tvTitle.isAllCaps = value
            field = value
            invalidate()
            requestLayout()
        }

    var title: String? = null
        set(value) = with(binding) {
            tvTitle.text = value
            field = value
            invalidate()
            requestLayout()
        }

    init {
        attrs?.let {
            context.theme.obtainStyledAttributes(
                it,
                R.styleable.CustomToolbar,
                0,
                0
            ).apply {
                try {
                    titleAllCaps = getInt(R.styleable.CustomToolbar_android_textAllCaps, 0) != 0
                    title = getString(R.styleable.CustomToolbar_title)
                    toolbarType = Type.values()[getInt(
                        R.styleable.CustomToolbar_toolbarType,
                        TOOLBAR_NO_CTA_AND_NO_EXTRA_SECTION.ordinal
                    )]
                } finally {
                    setListener()
                    recycle()
                }
            }
        }
    }

    private fun setListener() = with(binding) {
        toolbar.setNavigationOnClickListener {
            onClickedListener?.onClickedBackButton()
        }

        ivSearch.setOnSingleClickListener {
            onClickedListener?.onClickedSearch(SEARCH)
        }

        ivNotification.setOnSingleClickListener {
            onClickedListener?.onClickedNotification(NOTIFICATION)
        }

        ivCart.setOnSingleClickListener {
            onClickedListener?.onClickedCart(CART)
        }
    }
}