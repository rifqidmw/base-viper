package com.base.viper.components.pageindicator

import android.view.View

interface IndicatorListener {
    fun indicatorCreated(indicator: View, i: Int)
}