package com.base.viper.components.promosection

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.base.viper.databinding.CustomPromoItemBinding
import com.base.viper.utils.Utils.setOnSingleClickListener
import com.bumptech.glide.Glide

class CustomSubSectionAdapter : RecyclerView.Adapter<CustomSubSectionAdapter.PromoViewHolder>() {

    private val listPromo: MutableList<DataSubSection?> = mutableListOf()

    private var onItemClickListener: OnItemClickListener? = null

    interface OnItemClickListener {
        fun onClickedListener(position: Int)
    }

    fun setOnItemClickListener(onClickListener: OnItemClickListener) {
        this.onItemClickListener = onClickListener
    }

    @SuppressLint("NotifyDataSetChanged")
    fun setDataPromo(listData: List<DataSubSection?>) {
        listPromo.clear()
        listPromo.addAll(listData)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PromoViewHolder {
        return PromoViewHolder(
            CustomPromoItemBinding.inflate(
                LayoutInflater.from(parent.context), parent, false
            )
        )
    }

    override fun onBindViewHolder(holder: PromoViewHolder, position: Int) {
        holder.bind(listPromo[position])
    }

    override fun getItemCount() = listPromo.size

    inner class PromoViewHolder(
        private val binding: CustomPromoItemBinding
    ) : RecyclerView.ViewHolder(binding.root) {
        fun bind(promo: DataSubSection?) = with(binding) {
            Glide.with(ivPromoImage)
                .load(promo?.image)
                .into(ivPromoImage)

            tvPromoTitle.text = promo?.title
            tvPromoPeriod.text = promo?.period

            root.setOnSingleClickListener {
                onItemClickListener?.onClickedListener(absoluteAdapterPosition)
            }
        }
    }
}