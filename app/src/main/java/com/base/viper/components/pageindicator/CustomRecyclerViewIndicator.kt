package com.base.viper.components.pageindicator

import android.content.Context
import android.util.AttributeSet
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.OnScrollListener
import androidx.recyclerview.widget.SnapHelper

class CustomRecyclerViewIndicator : BaseCircleIndicatorClass {
    private lateinit var recyclerView: RecyclerView
    private lateinit var snapHelper: SnapHelper

    constructor(context: Context) : super(context)
    constructor(context: Context, attributeSet: AttributeSet) : super(context, attributeSet)
    constructor(context: Context, attributeSet: AttributeSet, defStyleAttr: Int) : super(
        context,
        attributeSet,
        defStyleAttr
    )

    fun attachToRecyclerView(recyclerView: RecyclerView, pagerSnapHelper: SnapHelper) {
        this.recyclerView = recyclerView
        this.snapHelper = pagerSnapHelper
        lastPosition = 0
        createIndicators()
        recyclerView.removeOnScrollListener(scrollListener)
        recyclerView.addOnScrollListener(scrollListener)
    }

    private fun createIndicators() {
        val adapter = recyclerView.adapter
        val count = adapter?.itemCount ?: 0
        if (count == 0) return
        createIndicators(count, 0)
    }

    private fun getSnapPosition(layoutManager: RecyclerView.LayoutManager?): Int {
        if (layoutManager == null) {
            return RecyclerView.NO_POSITION
        }
        val snapView = snapHelper.findSnapView(layoutManager)
        return if (snapView == null) RecyclerView.NO_POSITION
        else layoutManager.getPosition(snapView)
    }

    private val scrollListener: OnScrollListener = object : OnScrollListener() {
        override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
            super.onScrolled(recyclerView, dx, dy)
            val adapter = recyclerView.adapter
            val actualCout: Int = adapter?.itemCount ?: 3
            val position = getSnapPosition(recyclerView.layoutManager)
            if (position == RecyclerView.NO_POSITION) return
            pageSelected(position % actualCout)
        }
    }
}