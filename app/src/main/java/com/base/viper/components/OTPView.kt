package com.base.viper.components

import android.content.Context
import android.text.Editable
import android.text.TextWatcher
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.LinearLayout
import com.base.viper.databinding.ItemOtpViewBinding
import com.google.android.material.textfield.TextInputEditText

class OTPView(context: Context, attrs: AttributeSet) : LinearLayout(context, attrs) {

    private val binding: ItemOtpViewBinding = ItemOtpViewBinding.inflate(
        LayoutInflater.from(context), this, true
    )

    init {
        orientation = HORIZONTAL

        val digitEditTexts = mutableListOf<TextInputEditText>()

        for (i in 1..6) {
            val editText = when(i){
                1 -> binding.etDigit1
                2 -> binding.etDigit2
                3 -> binding.etDigit3
                4 -> binding.etDigit4
                5 -> binding.etDigit5
                6 -> binding.etDigit6
                else -> binding.etDigit1
            }
            digitEditTexts.add(editText)

            editText.addTextChangedListener(object : TextWatcher {
                override fun beforeTextChanged(
                    s: CharSequence?,
                    start: Int,
                    count: Int,
                    after: Int
                ) {
                    //Not Yet Implemented
                }

                override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                    if (s?.length == 1) {
                        focusNextEditText(editText)
                    } /*else if (s?.isEmpty() == true) {
                        focusPreviousEditText(editText)
                    }*/
                }

                override fun afterTextChanged(s: Editable?) {
                    //Not Yet Implemented
                }
            })
        }
    }

    private fun focusNextEditText(editText: TextInputEditText) {
        val nextEditText = editText.focusSearch(FOCUS_RIGHT) as? TextInputEditText
        nextEditText?.requestFocus()
    }

    private fun focusPreviousEditText(editText: TextInputEditText) {
        val previousEditText = editText.focusSearch(FOCUS_LEFT) as? TextInputEditText
        previousEditText?.requestFocus()
    }

    // Method to retrieve the entered OTP
    fun getOTP(): String {
        val otp = StringBuilder()
        otp.append(binding.etDigit1.text.toString())
        otp.append(binding.etDigit2.text.toString())
        otp.append(binding.etDigit3.text.toString())
        otp.append(binding.etDigit4.text.toString())
        otp.append(binding.etDigit5.text.toString())
        otp.append(binding.etDigit6.text.toString())

        return otp.toString()
    }
}