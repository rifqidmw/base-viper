package com.base.viper.components.promosection

import android.content.Context
import com.base.viper.R
import com.base.viper.module.newstips.entity.newstips.NewsTips
import com.base.viper.module.promo.entity.promo.Content
import com.base.viper.utils.Utils.dateTimeFormat

object MapperToDataSubSection {
    fun List<Content>?.contentToListPromo(context: Context): List<DataSubSection>? {
        return this?.map {
            DataSubSection(
                it.heroImageLink,
                it.titlePage,
                String.format(
                    context.getString(R.string.format_period),
                    it.startDate?.dateTimeFormat(),
                    it.endDate?.dateTimeFormat()
                )
            )
        }
    }

    fun List<NewsTips.Content>?.contentToListNewsTips(context: Context): List<DataSubSection>? {
        return this?.map {
            DataSubSection(
                it.heroImageLink,
                it.titlePage,
                String.format(
                    context.getString(R.string.format_period),
                    it.startDate?.dateTimeFormat(),
                    it.endDate?.dateTimeFormat()
                )
            )
        }
    }
}