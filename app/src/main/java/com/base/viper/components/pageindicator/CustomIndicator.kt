package com.base.viper.components.pageindicator

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import androidx.constraintlayout.widget.ConstraintLayout
import com.base.viper.databinding.CustomIndicatorBinding
import com.base.viper.utils.Utils.gone
import com.base.viper.utils.Utils.visible

class CustomIndicator(
    context: Context,
    attrs: AttributeSet? = null
) : ConstraintLayout(context, attrs) {

    private val binding: CustomIndicatorBinding = CustomIndicatorBinding.inflate(
        LayoutInflater.from(context), this, true
    )

    var text: String? = null
        set(value) = with(binding) {
            tvSelected.text = value
            if (value == null) {
                tvSelected.gone()
                vSelected.gone()
                vUnselected.visible()
            } else if (value.isEmpty()) {
                tvSelected.gone()
                vSelected.visible()
                vUnselected.gone()
            } else {
                tvSelected.visible()
                vSelected.gone()
                vUnselected.gone()
            }
            field = value
            invalidate()
            requestLayout()
        }
}