package com.base.viper.components.pageindicator

import android.animation.Animator
import android.animation.AnimatorInflater
import android.content.Context
import android.content.res.TypedArray
import android.util.AttributeSet
import android.util.TypedValue
import android.view.Gravity
import android.view.animation.Interpolator
import android.widget.LinearLayout
import com.base.viper.R
import kotlin.math.abs

open class BaseCircleIndicatorClass : LinearLayout {

    private val DEFAULT_INDICATOR_WIDTH = 5
    private var mCIMargin = -1

    private var mAnimatorIn: Animator? = null
    private var mAnimatorOut: Animator? = null
    private var mImmediateAnimatorIn: Animator? = null
    private var mImmediateAnimatorOut: Animator? = null
    private var pageIndicator: Boolean = true
    private var maxIndicator: Int = 5
    private var maxIndicatorEnabled: Boolean = false

    var lastPosition = 0
    private var indicatorListener: IndicatorListener? = null
    private var mCount = 0

    constructor(context: Context) : super(context) {
        init(context, null)
    }

    constructor(context: Context, attributeSet: AttributeSet) : super(context, attributeSet) {
        init(context, attributeSet = attributeSet)
    }

    constructor(context: Context, attributeSet: AttributeSet, defStyleAttr: Int) : super(
        context,
        attributeSet,
        defStyleAttr
    ) {
        init(context, attributeSet)
    }

    private fun init(context: Context, attributeSet: AttributeSet?) {
        val indicatorInfo = getIndicatorInfo(context, attributeSet = attributeSet)
        initialise(indicatorInfo)
        if (isInEditMode) {
            createIndicators(3, 1)
        }
    }

    private fun getIndicatorInfo(context: Context, attributeSet: AttributeSet?): IndicatorInfo {
        val indicatorInfo = IndicatorInfo()
        val typeAttr: TypedArray = context.obtainStyledAttributes(
            attributeSet,
            R.styleable.BaseCircleIndicatorClass
        )
        indicatorInfo.margin = typeAttr.getDimensionPixelSize(
            R.styleable.BaseCircleIndicatorClass_ci_margin,
            -1
        )
        indicatorInfo.animationResId = typeAttr.getResourceId(
            R.styleable.BaseCircleIndicatorClass_ci_animator,
            R.animator.page_indicator_animation
        )
        indicatorInfo.animatorReverseResId = typeAttr.getResourceId(
            R.styleable.BaseCircleIndicatorClass_ci_animator_reverse,
            0
        )
        indicatorInfo.gravity = typeAttr.getResourceId(
            R.styleable.BaseCircleIndicatorClass_ci_gravity,
            -1
        )
        indicatorInfo.orientation = typeAttr.getResourceId(
            R.styleable.BaseCircleIndicatorClass_ci_orientation,
            -1
        )
        indicatorInfo.pageIndicator = typeAttr.getBoolean(
            R.styleable.BaseCircleIndicatorClass_ci_page_indicator,
            true
        )
        indicatorInfo.maxIndicatorEnabled = typeAttr.getBoolean(
            R.styleable.BaseCircleIndicatorClass_ci_max_indicator_enabled,
            false
        )
        typeAttr.recycle()
        return indicatorInfo
    }

    private fun initialise(indicatorInfo: IndicatorInfo) {
        val minimumSize: Int = (TypedValue.applyDimension(
            TypedValue.COMPLEX_UNIT_DIP,
            DEFAULT_INDICATOR_WIDTH.toFloat(), resources.displayMetrics
        ) + 0.5f).toInt()

        mCIMargin = if (indicatorInfo.margin < 0) minimumSize else indicatorInfo.margin

        mAnimatorIn = createAnimationIn(indicatorInfo)
        mImmediateAnimatorIn = createAnimationIn(indicatorInfo)
        mImmediateAnimatorIn?.duration = 0

        mAnimatorOut = createAnimatorOut(indicatorInfo)
        mImmediateAnimatorOut = createAnimatorOut(indicatorInfo)
        mImmediateAnimatorOut?.duration = 0

        orientation =
            if (indicatorInfo.orientation == VERTICAL) indicatorInfo.orientation else HORIZONTAL
        gravity = if (indicatorInfo.gravity >= 0) indicatorInfo.gravity else Gravity.CENTER
        pageIndicator = indicatorInfo.pageIndicator
        maxIndicatorEnabled = indicatorInfo.maxIndicatorEnabled
    }

    private fun createAnimationIn(indicatorInfo: IndicatorInfo): Animator {
        return if (indicatorInfo.animatorReverseResId == 0) {
            AnimatorInflater.loadAnimator(context, indicatorInfo.animationResId)
                .apply { interpolator = ReverseInterpolator() }
        } else {
            AnimatorInflater.loadAnimator(context, indicatorInfo.animatorReverseResId)
        }
    }

    private fun createAnimatorOut(indicatorInfo: IndicatorInfo): Animator {
        return AnimatorInflater.loadAnimator(context, indicatorInfo.animationResId)
    }

    fun pageSelected(position: Int) {
        if (lastPosition == position) return

        if (mAnimatorIn?.isRunning == true) {
            mAnimatorIn?.end()
            mAnimatorIn?.cancel()
        }

        if (mAnimatorOut?.isRunning == true) {
            mAnimatorOut?.end()
            mAnimatorOut?.cancel()
        }

        if (!maxIndicatorEnabled) {
            val view: CustomIndicator? = getChildAt(lastPosition) as? CustomIndicator
            if (lastPosition >= 0 && view != null) {
                view.text = null
                mAnimatorIn?.setTarget(view)
                mAnimatorIn?.start()
            }

            val v: CustomIndicator? = getChildAt(position) as? CustomIndicator
            if (v != null) {
                v.text = if (pageIndicator) "${position + 1}/$mCount" else ""
                mAnimatorOut?.setTarget(v)
                mAnimatorOut?.start()
            }
            lastPosition = position
        } else {
            val view: CustomIndicator? = when (lastPosition) {
                0, 1 -> getChildAt(lastPosition) as? CustomIndicator
                in 2 until mCount - 2 -> getChildAt(2) as? CustomIndicator
                mCount - 2 -> getChildAt(3) as? CustomIndicator
                else -> getChildAt(4) as? CustomIndicator
            }
            if (view != null) {
                view.text = null
                mAnimatorIn?.setTarget(view)
                mAnimatorIn?.start()
            }

            val v: CustomIndicator? = when (position) {
                0, 1 -> getChildAt(position) as? CustomIndicator
                in 2 until mCount - 2 -> getChildAt(2) as? CustomIndicator
                mCount - 2 -> getChildAt(3) as? CustomIndicator
                else -> getChildAt(4) as? CustomIndicator
            }

            if (v != null) {
                v.text = if (pageIndicator) "${position + 1}/$mCount" else ""
                mAnimatorOut?.setTarget(v)
                mAnimatorOut?.start()
            }
            lastPosition = position
        }
    }

    fun createIndicators(count: Int, currentPosition: Int) {
        mCount = count
        if (mImmediateAnimatorIn?.isRunning == true) {
            mImmediateAnimatorIn?.end()
            mImmediateAnimatorIn?.cancel()
        }
        if (mImmediateAnimatorOut?.isRunning == true) {
            mImmediateAnimatorOut?.end()
            mImmediateAnimatorOut?.cancel()
        }

        val childViewIndicatorCOunt = childCount

        when {
            !maxIndicatorEnabled || count <= maxIndicator -> {
                if (childViewIndicatorCOunt > count) {
                    removeViews(childViewIndicatorCOunt, childViewIndicatorCOunt - count)
                } else if (childViewIndicatorCOunt < count) {
                    val addCount = count - childViewIndicatorCOunt
                    val orientation = orientation
                    for (i in 0 until addCount) {
                        addIndicator(orientation)
                    }
                }
            }

            else -> {
                when (count) {
                    in (maxIndicator + 1) until childViewIndicatorCOunt -> {
                        removeViews(childViewIndicatorCOunt, childViewIndicatorCOunt - count)
                    }

                    else -> {
                        for (i in 0 until maxIndicator) {
                            addIndicator(orientation)
                        }
                    }
                }
            }
        }


        if (!maxIndicatorEnabled || count <= maxIndicator) {
            for (i in 0 until count) {
                initializeIndicator(i, currentPosition)
            }
        } else {
            for (i in 0 until maxIndicator) {
                initializeIndicator(i, currentPosition)
            }
        }

        lastPosition = currentPosition
    }

    private fun initializeIndicator(position: Int, currentPosition: Int) {
        val indicator: CustomIndicator = getChildAt(position) as CustomIndicator
        if (currentPosition == position) {
            indicator.text = if (pageIndicator) "1/$childCount" else ""
            mImmediateAnimatorOut?.setTarget(indicator)
            mImmediateAnimatorOut?.start()
            mImmediateAnimatorOut?.end()
        } else {
            mImmediateAnimatorIn?.setTarget(indicator)
            mImmediateAnimatorIn?.start()
            mImmediateAnimatorIn?.end()
        }
        indicatorListener?.indicatorCreated(indicator, position)
    }

    private fun addIndicator(orientation: Int) {
        val v = CustomIndicator(context)
        val layoutParams = generateDefaultLayoutParams()
        if (orientation == HORIZONTAL) {
            layoutParams.leftMargin = mCIMargin
            layoutParams.rightMargin = mCIMargin
        } else {
            layoutParams.topMargin = mCIMargin
            layoutParams.bottomMargin = mCIMargin
        }
        addView(v, layoutParams)
    }

    class ReverseInterpolator : Interpolator {
        override fun getInterpolation(input: Float): Float {
            return abs(1f - input)
        }
    }
}