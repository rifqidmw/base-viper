package com.base.viper.components

import android.annotation.SuppressLint
import android.content.Context
import android.content.res.ColorStateList
import android.graphics.drawable.Drawable
import android.text.Editable
import android.text.InputFilter
import android.text.InputType
import android.text.Spannable
import android.text.SpannableString
import android.text.SpannableStringBuilder
import android.text.Spanned
import android.text.TextUtils
import android.text.TextWatcher
import android.text.style.ForegroundColorSpan
import android.util.AttributeSet
import android.view.Gravity
import android.view.LayoutInflater
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import com.base.viper.R
import com.base.viper.databinding.ItemInputViewBinding
import com.base.viper.utils.Utils.gone
import com.base.viper.utils.Utils.setOnSingleClickListener
import com.base.viper.utils.Utils.visible
import com.google.android.material.textfield.TextInputLayout
import java.util.regex.Matcher
import java.util.regex.Pattern


class InputView : ConstraintLayout {

    /**
     * Variables
     */
    //State
    private lateinit var mContext: Context
    private lateinit var binding: ItemInputViewBinding

    enum class TYPE { TEXT, TEXTCAPWORD, TEXTCAPCHARACTER, TEXTCAPSENTENCE, TEXTPERSONNAME, TEXTFILTER, MULTILINE, PASSWORD, PASSWORD_NUMBER, NUMBER, NUMBER_DECIMAL, EMAIL, DISABLE, CURRENCY, CLICK }
    enum class ACTION { NEXT, DONE }

    private var title = ""
    private var hint = ""
    private var inputType: TYPE = TYPE.TEXT
    private var lines = 1
    private var enable = true
    private var labelColor: ColorStateList? = null
    private var textColor: ColorStateList? = null
    private var hintColor: ColorStateList? = null
    private var underlineColor: ColorStateList? = null
    private var boxBackgroundColor: ColorStateList? = null
    private var currentCurrency: String = ""
    private var limitCounter: String = ""
    private var imageEnd: Drawable? = null
    private var imageStart: Drawable? = null

    private var listener: InputListener? = null
    private var isDeleteMode: Boolean = false

    /**
     * Constructors
     */
    constructor(context: Context) : super(context) {
        init(context, null)
    }

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        init(context, attrs)
    }

    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int) : super(
        context,
        attrs,
        defStyleAttr
    ) {
        init(context, attrs)
    }

    /**
     * Initialization
     */
    @SuppressLint("ClickableViewAccessibility")
    private fun init(context: Context, attributeSet: AttributeSet?) {
        mContext = context

        //Inflate view
        binding = ItemInputViewBinding.bind(
            LayoutInflater.from(mContext).inflate(
                R.layout.item_input_view,
                this,
                true
            )
        )

        setupAttributes(attributeSet)

        binding.etInput.addTextChangedListener(textWatcher)

        if (inputType == TYPE.CLICK) {
            binding.etInput.setCompoundDrawablesWithIntrinsicBounds(
                0,
                0,
                R.drawable.ic_arrow_down_black_24,
                0
            )
            binding.etInput.isFocusable = false
            binding.etInput.isFocusableInTouchMode = false

            binding.etInput.setOnSingleClickListener {
                listener?.onInputClick()
            }

        } else {
            binding.etInput.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0)
            binding.etInput.isFocusable = true
            binding.etInput.isFocusableInTouchMode = true
        }
    }

    private fun setupAttributes(attrs: AttributeSet?) {
        val typedArray = context.theme.obtainStyledAttributes(attrs, R.styleable.InputView, 0, 0)
        title = typedArray.getString(R.styleable.InputView_title).toString()
        hint = typedArray.getString(R.styleable.InputView_hint).toString()
        lines = typedArray.getInt(R.styleable.InputView_lines, 1)
        inputType = TYPE.values()[typedArray.getInt(R.styleable.InputView_inputType, 0)]
        enable = typedArray.getBoolean(R.styleable.InputView_enable, true)
        imageStart = typedArray.getDrawable(R.styleable.InputView_imageStart)
        imageEnd = typedArray.getDrawable(R.styleable.InputView_imageEnd)
        labelColor = typedArray.getColorStateList(R.styleable.InputView_labelColor)
        textColor = typedArray.getColorStateList(R.styleable.InputView_textColor)
        hintColor = typedArray.getColorStateList(R.styleable.InputView_hintColor)
        underlineColor = typedArray.getColorStateList(R.styleable.InputView_underlineColor)
        boxBackgroundColor = typedArray.getColorStateList(R.styleable.InputView_boxBackgroundColor)

        typedArray.recycle()
        setupViews()
    }

    private fun setupViews() {
        with(binding) {
            setTitle(title)
            setHint(hint)
            setLines(lines)
            setInputType(inputType)
            setEnable(enable)
            setDrawable(drawableLeft = imageStart)
            setDrawable(drawableRight = imageEnd)
            setLabelColor(labelColor)
            setTextColor(textColor)
            setHintColor(hintColor)
            setUnderlineColor(underlineColor)
            setBoxBackgroundColor(boxBackgroundColor)
        }
    }

    private val textWatcher = object : TextWatcher {
        override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            //Not Implemented Here
        }

        override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            //Not Implemented Here
        }

        override fun afterTextChanged(s: Editable?) {
            binding.etInput.removeTextChangedListener(this)
            listener?.afterTextChanged(s.toString())
            binding.tvLabelInput.isSelected = binding.etInput.text.toString().isNotEmpty()
            binding.constInput.isSelected = binding.etInput.text.toString().isNotEmpty()
            binding.etInput.addTextChangedListener(this)
        }
    }

    fun setTitle(title: String) {
        binding.tvLabelInput.text = title
    }

    fun hideTitle() {
        binding.tvLabelInput.gone()
    }

    fun setHint(hint: String) {
        binding.etInput.hint = hint
    }

    fun setLines(lines: Int) {
        val layoutParams = binding.etLayoutInput.layoutParams
        layoutParams.width = ViewGroup.LayoutParams.MATCH_PARENT
        layoutParams.height = ViewGroup.LayoutParams.WRAP_CONTENT
        binding.etLayoutInput.layoutParams = layoutParams
        binding.etInput.setLines(lines)
        binding.etInput.gravity = Gravity.TOP or Gravity.START
    }

    fun setLabelColor(color: ColorStateList?){
        color?.let { binding.tvLabelInput.setTextColor(it) }
    }

    fun setTextColor(color: ColorStateList?) {
        color?.let { binding.etInput.setTextColor(it) }
    }

    fun setHintColor(color: ColorStateList?){
        color?.let { binding.etInput.setHintTextColor(it) }
    }

    fun setUnderlineColor(color: ColorStateList?){
        color?.let { binding.vUnderline.backgroundTintList = color }
    }
    fun setBoxBackgroundColor(color: ColorStateList?){
        color?.let { binding.etLayoutInput.setBoxBackgroundColorStateList(it) }
    }

    fun setInfo(info: String) {
        binding.etLayoutInput.error = info
    }

    fun resetInput() {
        binding.etInput.setText("")
    }

    /*private fun getHintTypeface(hint: String): SpannableString {
        val typeFontBook =
            ResourcesCompat.getFont(context, R.font.montserrat_400_regular)

        val typefaceSpan = typeFontBook?.let { CustomTypefaceSpan(it) }
        val spannableString = SpannableString(hint)
        spannableString.setSpan(
            typefaceSpan,
            0,
            spannableString.length,
            Spanned.SPAN_INCLUSIVE_EXCLUSIVE
        )
        return spannableString
    }*/

    fun setText(input: String?) {
        input?.length?.let {
            binding.etInput.setText(input)
            binding.etInput.setSelection(it)
        }
    }

    fun setInputType(type: TYPE) {
        inputType = type

        binding.etInput.inputType = when (type) {
            TYPE.TEXT -> InputType.TYPE_CLASS_TEXT
            TYPE.PASSWORD -> InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_VARIATION_PASSWORD
            TYPE.NUMBER -> InputType.TYPE_CLASS_NUMBER
            TYPE.EMAIL -> InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS
            TYPE.DISABLE -> InputType.TYPE_CLASS_TEXT
            TYPE.CURRENCY -> InputType.TYPE_CLASS_NUMBER
            TYPE.TEXTCAPWORD -> InputType.TYPE_TEXT_FLAG_CAP_WORDS
            TYPE.TEXTCAPCHARACTER -> InputType.TYPE_TEXT_FLAG_CAP_CHARACTERS
            TYPE.MULTILINE -> InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_FLAG_MULTI_LINE
            TYPE.TEXTCAPSENTENCE -> InputType.TYPE_TEXT_FLAG_CAP_SENTENCES
            TYPE.TEXTPERSONNAME -> InputType.TYPE_TEXT_VARIATION_PERSON_NAME
            TYPE.TEXTFILTER -> InputType.TYPE_TEXT_VARIATION_FILTER
            TYPE.PASSWORD_NUMBER -> InputType.TYPE_CLASS_NUMBER or InputType.TYPE_NUMBER_VARIATION_PASSWORD
            TYPE.NUMBER_DECIMAL -> InputType.TYPE_CLASS_NUMBER or InputType.TYPE_NUMBER_FLAG_DECIMAL
            TYPE.CLICK -> InputType.TYPE_CLASS_TEXT
        }

        binding.etLayoutInput.endIconMode =
            if (inputType == TYPE.PASSWORD || inputType == TYPE.PASSWORD_NUMBER) TextInputLayout.END_ICON_PASSWORD_TOGGLE else TextInputLayout.END_ICON_NONE
    }

    fun setImeOption(action: ACTION) {
        binding.etInput.imeOptions = when (action) {
            ACTION.NEXT -> EditorInfo.IME_ACTION_NEXT
            ACTION.DONE -> EditorInfo.IME_ACTION_DONE
        }
    }

    fun setVisibleIcon(visible: Boolean, drawable: Int? = null) {
        if (drawable != null) {
            val iconInput = ContextCompat.getDrawable(mContext, drawable) as Drawable
            binding.etLayoutInput.startIconDrawable = iconInput
        }
    }

    fun setVisibleIconClearText() {
        binding.etLayoutInput.endIconMode = TextInputLayout.END_ICON_CLEAR_TEXT
    }

    fun setEnable(value: Boolean) {
        binding.etInput.isEnabled = value
    }

    fun setDeleteMode(isDelete: Boolean) {
        isDeleteMode = isDelete
    }

    fun setMaxLenght(lenght: Int) {
        binding.etInput.filters = arrayOf<InputFilter>(InputFilter.LengthFilter(lenght))
    }

    fun disableEmoji() {
        binding.etInput.filters = arrayOf(InputFilter { source, _, _, _, _, _ ->
            source.filter {
                Character.getType(it) != Character.SURROGATE.toInt() && Character.getType(
                    it
                ) != Character.OTHER_SYMBOL.toInt()
            }
        })
    }

    fun checkEmptyInput(error: String? = "Must be filled!") {
        if (binding.etInput.text.toString().isEmpty()){
            showError(error)
        } else {
            hideError()
        }
    }

    fun showError(error: String? = "Must be filled!") {
        binding.tvLabelInput.setTextColor(ContextCompat.getColor(context, R.color.red_5))
        binding.tvLabelError.text = error
        binding.tvLabelError.visible()
        binding.vUnderline.backgroundTintList = ContextCompat.getColorStateList(context, R.color.red_5)
        binding.etInput.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_alert_circle_red, 0)
    }

    fun hideError() {
        binding.etLayoutInput.boxStrokeColor = ContextCompat.getColor(context, R.color.black)
        binding.tvLabelInput.setTextColor(ContextCompat.getColor(context, R.color.black))
        binding.tvLabelError.gone()
        binding.vUnderline.backgroundTintList = ContextCompat.getColorStateList(context, R.color.black)
        binding.etInput.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0)
    }

    fun setListener(inputListener: InputListener) {
        listener = inputListener
    }

    fun getText(): String {
        return binding.etInput.text.toString()
    }

    fun getError(): String {
        return if (binding.etLayoutInput.error == null) "" else binding.etLayoutInput.error.toString()
    }

    fun getSize(): Int {
        return binding.etInput.text!!.length
    }

    fun setClear() {
        binding.etInput.setText("")
    }

    fun setHelperText(helper: String) {
        binding.tvLabelHelper.visible()
        binding.tvLabelHelper.text = helper
    }

    fun setDisable() {
        binding.etInput.isEnabled = false
    }

    fun disableHelperText() {
        binding.tvLabelHelper.gone()
    }

    fun setFilterWordOnly() {
        binding.etInput.filters = arrayOf(acceptonlyAlphabetValuesnotNumbersMethod())
    }

    fun setCounterEnable(counter: Int) {
        binding.etLayoutInput.isCounterEnabled = true
        binding.etLayoutInput.counterMaxLength = counter
        binding.etInput.filters = arrayOf(InputFilter.LengthFilter(counter))
    }

    fun setIcon(drawableRight: Int = 0, drawableLeft: Int = 0) {
        binding.etInput.setCompoundDrawablesWithIntrinsicBounds(drawableLeft, 0, drawableRight, 0)
    }

    fun getInput() = binding.etInput

    fun setDrawable(
        drawableRight: Drawable? = null,
        drawableTop: Drawable? = null,
        drawableLeft: Drawable? = null,
        drawableBottom: Drawable? = null,
        drawablePadding: Int = 0
    ) {
        binding.etInput.compoundDrawablePadding = drawablePadding
        binding.etInput.setCompoundDrawablesWithIntrinsicBounds(
            drawableLeft,
            drawableTop,
            drawableRight,
            drawableBottom
        )
    }

    fun setStartCompoundText(
        textStart: String = "",
        textPadding: Int = 0,
        color: Int = R.color.reliable_black
    ) {
        val spannable = SpannableStringBuilder()

        fun appendText(text: String?) {
            if (!text.isNullOrEmpty()) {
                val start = spannable.length
                spannable.append(text)
                val end = spannable.length
                spannable.setSpan(ForegroundColorSpan(ContextCompat.getColor(context, color)), start, end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
            }
        }

        appendText(textStart)
        spannable.append(" ")
        spannable.append(binding.etInput.text.toString())

        binding.etInput.setPadding(textPadding, binding.etInput.paddingTop, textPadding, binding.etInput.paddingBottom)
        binding.etInput.text = spannable
        binding.etInput.addTextChangedListener(object: TextWatcher {
            override fun afterTextChanged(s: Editable?) {

            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                if (binding.etInput.length() < textStart.length || !binding.etInput.text.toString().startsWith("$textStart ")) {
                    binding.etInput.text = spannable
                    binding.etInput.setSelection(binding.etInput.length())
                }
            }
        })
    }

    fun setFilterRestrictSpace() {
        binding.etInput.inputType = InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD
        binding.etInput.filters = arrayOf(object : InputFilter {
            override fun filter(
                source: CharSequence?,
                start: Int,
                end: Int,
                dest: Spanned?,
                dstart: Int,
                dend: Int
            ): CharSequence? {
                val srcChangeLength = end - start
                if (srcChangeLength <= 0) {
                    return source
                }
                val result = if (source is SpannableStringBuilder) source else {
                    SpannableStringBuilder(source)
                }
                for (i in end - 1 downTo start) {
                    val currentChar = result[i]
                    if (Character.isSpaceChar(currentChar)) {
                        result.delete(i, i + 1)
                    }
                }
                return result
            }
        })
    }

    private fun acceptonlyAlphabetValuesnotNumbersMethod(): InputFilter {
        return object : InputFilter {
            override fun filter(
                source: CharSequence,
                start: Int,
                end: Int,
                dest: Spanned,
                dstart: Int,
                dend: Int
            ): CharSequence? {
                var isCheck = true
                val sb = StringBuilder(end - start)
                for (i in start until end) {
                    val c = source[i]
                    if (isCharAllowed(c)) {
                        sb.append(c)
                    } else {
                        isCheck = false
                    }
                }
                return if (isCheck) null else {
                    if (source is Spanned) {
                        val spannableString = SpannableString(sb)
                        TextUtils.copySpansFrom(source, start, sb.length, null, spannableString, 0)
                        spannableString
                    } else {
                        sb
                    }
                }
            }

            private fun isCharAllowed(c: Char): Boolean {
                val pattern: Pattern = Pattern.compile("^[a-zA-Z ]+$")
                val match: Matcher = pattern.matcher(c.toString())
                return match.matches()
            }
        }
    }

    interface InputListener {
        fun afterTextChanged(value: String) {}
        fun onInputClick() {}
    }
}