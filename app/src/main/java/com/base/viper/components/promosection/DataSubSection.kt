package com.base.viper.components.promosection

data class DataSubSection(
    val image: String? = null,
    val title: String? = null,
    val period: String? = null
)