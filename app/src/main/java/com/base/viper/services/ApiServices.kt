package com.base.viper.services

import com.base.viper.module.newstips.entity.detail.NewsTipsDetail
import com.base.viper.module.newstips.entity.inquiry.Branch
import com.base.viper.module.newstips.entity.inquiry.City
import com.base.viper.module.newstips.entity.inquiry.Product
import com.base.viper.module.newstips.entity.inquiry.Province
import com.base.viper.module.newstips.entity.newstips.NewsTips
import com.base.viper.module.newstips.entity.newstips.NewsTipsCategory
import com.base.viper.module.productknowledge.entity.ProductKnowledge
import com.base.viper.module.productknowledge.entity.ProductKnowledgeCategory
import com.base.viper.module.productknowledge.entity.ProductKnowledgeDetail
import com.base.viper.module.promo.entity.promo.Promo
import com.base.viper.module.promo.entity.promo.PromoCategory
import com.base.viper.module.promo.entity.promo.PromoDetail
import io.reactivex.rxjava3.core.Single
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface ApiServices {
    //PROMO
    @GET("parameter/{version}/category/{categoryType}")
    fun getCategoryByType(
        @Path("version") version: String,
        @Path("categoryType") categoryType: String,
    ): Single<List<PromoCategory>>

    @GET("promo/{version}/promo")
    fun getPromo(
        @Path("version") version: String,
        @Query("page") page: String = "",
        @Query("size") size: String = "",
        @Query("content-category") contentCategory: String = ""
    ): Single<Promo>

    @GET("promo/{version}/promo/slug/{slug}")
    fun getPromoDetail(
        @Path("version") version: String,
        @Path("slug") slug: String?,
    ): Single<PromoDetail>

    //NEWS & TIPS
    @GET("parameter/{version}/category/{categoryType}")
    fun getNewsTipsCategory(
        @Path("version") version: String,
        @Path("categoryType") categoryType: String,
    ): Single<List<NewsTipsCategory>>

    @GET("content/{version}/news")
    fun getNewsTips(
        @Path("version") version: String,
        @Query("page") page: String = "",
        @Query("size") size: String = "",
        @Query("content-category") contentCategory: String = ""
    ): Single<NewsTips>

    @GET("content/{version}/news/slug/{slug}")
    fun getNewsTipsDetail(
        @Path("version") version: String,
        @Path("slug") slug: String?,
    ): Single<NewsTipsDetail>

    //PRODUCT KNOWLEDGE
    @GET("parameter/{version}/category/{categoryType}")
    fun getProductKnowledgeCategory(
        @Path("version") version: String,
        @Path("categoryType") categoryType: String,
    ): Single<List<ProductKnowledgeCategory>>

    @GET("content/{version}/product-knowledge")
    fun getProductKnowledge(
        @Path("version") version: String,
        @Query("page") page: String = "",
        @Query("size") size: String = "",
        @Query("content-category") contentCategory: String = ""
    ): Single<ProductKnowledge>

    @GET("content/{version}/product-knowledge/slug/{slug}")
    fun getProductKnowledgeDetail(
        @Path("version") version: String,
        @Path("slug") slug: String?,
    ): Single<ProductKnowledgeDetail>

    //PROVINCE
    @GET("parameter/{version}/province")
    fun getProvince(
        @Path("version") version: String
    ): Single<List<Province>>

    //CITY
    @GET("parameter/{version}/province/{isocode}")
    fun getCity(
        @Path("version") version: String,
        @Path("isocode") isocode: String
    ): Single<List<City>>

    @GET("parameter/{version}/province/{city}")
    fun getBranch(
        @Path("version") version: String,
        @Path("city") isocode: String
    ): Single<Branch>

    //PRODUCT
    @GET("product/{version}/product-list")
    fun getProductList(
        @Path("version") version: String,
    ): Single<List<Product>>
}