package com.base.viper.services

import android.content.Context
import android.content.SharedPreferences
import android.security.keystore.KeyGenParameterSpec
import android.security.keystore.KeyProperties
import androidx.security.crypto.EncryptedSharedPreferences
import androidx.security.crypto.MasterKey
import com.base.viper.common.Constants

class Sessions(context: Context) {

    companion object {
        const val keyAlias: String = "_androidx_security_master_key_"
        const val secretSharedPref: String = "secret_shared_prefs"
    }

    private var editor: SharedPreferences.Editor
    private val spec = KeyGenParameterSpec.Builder(
        keyAlias,
        KeyProperties.PURPOSE_ENCRYPT or KeyProperties.PURPOSE_DECRYPT
    )
        .setBlockModes(KeyProperties.BLOCK_MODE_GCM)
        .setEncryptionPaddings(KeyProperties.ENCRYPTION_PADDING_NONE)
        .setKeySize(256)
        .build()

    private val masterKey: MasterKey = MasterKey.Builder(context)
        .setKeyGenParameterSpec(spec)
        .build()

    private val pref = EncryptedSharedPreferences.create(
        context,
        secretSharedPref,
        masterKey,
        EncryptedSharedPreferences.PrefKeyEncryptionScheme.AES256_SIV,
        EncryptedSharedPreferences.PrefValueEncryptionScheme.AES256_GCM
    )

    init {
        editor = pref.edit()
    }

    fun batch() = pref

    fun putString(key: Constants.Sessions, value: String?) {
        editor.putString(key.name, value)
        editor.commit()
    }

    fun putInt(key: Constants.Sessions, value: Int) {
        editor.putInt(key.name, value)
        editor.commit()
    }

    fun putBoolean(key: Constants.Sessions, value: Boolean) {
        editor.putBoolean(key.name, value)
        editor.commit()
    }

    fun getString(key: Constants.Sessions, defValue: String = "") =
        pref.getString(key.name, defValue).toString()

    fun getInt(key: Constants.Sessions, defValue: Int = 0) = pref.getInt(key.name, defValue)

    fun getBoolean(key: Constants.Sessions, defValue: Boolean = false) =
        pref.getBoolean(key.name, defValue)

    fun removeSingle(key: Constants.Sessions) {
        editor.remove(key.name).commit()
    }

    fun getToken() = pref.getString(Constants.Sessions.TOKEN.name, "") ?: ""

    fun logOut() {
        editor.clear()
        editor.putBoolean(Constants.Sessions.FIRST_RUN.name, true)
        editor.commit()
    }
}